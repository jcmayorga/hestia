<?php

namespace Administration\DefaultBundle\Controller;

use Administration\DefaultBundle\Controller\BaseController;

class DefaultController extends BaseController
{
    public function indexAction()
    {
        return $this->render('AdministrationDefaultBundle:Default:index.html.twig');
    }
}
