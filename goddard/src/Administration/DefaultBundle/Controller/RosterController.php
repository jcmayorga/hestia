<?php

namespace Administration\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RosterController extends Controller
{
    /**
     * @Route("/index",name="administration_default_roster_index")
     * @Method({"GET", "POST"})
     * @Template()
     * @return text/html AdministrationDefaultBundle:Roster:index.html.twig
     **/
    public function indexAction(Request $request)
    {
        $companyManager=$this->get('administration.default.manager.company');
        return array("companies"=>$companyManager->getAllCompanies());
    }

    /**
     * @Route("/upload",name="administration_default_roster_upload")
     * @Method({"GET", "POST"})
     **/
    public function uploadAction(Request $request)
    {

        $return=array("status"=>0,"message"=>"");
        $fileRequest=$request->files->get('fileUpload');
        $rosterService=$this->get("administration.default.service.roster");
        $response=$rosterService->validDataFileUpload($fileRequest);

               
        
        if($response["status"])
        {           
            $companyId=$request->get("companyId");
            $return=$rosterService->uploadData($fileRequest,$companyId);               

        }else{
            $return=$response;
        }

        return new JsonResponse($return);
    }

    
}
