<?php

namespace Administration\DefaultBundle\Controller;

use Aplication\DefaultBundle\Model\Constant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CompanyController extends Controller
{
    /**
     * @Route("/index",name="administration_default_company_index")
     * @Method({"GET", "POST"})
     * @Template()
     * @return text/html AdministrationDefaultBundle:Company:index.html.twig
     **/
    public function indexAction(Request $request)
    {
        $companyGrid = $this->get('administration.default.grid.company');
        $grid = $companyGrid->getResponse($request);

        return array("grid" => $grid);
    }

    /**
     * Retorna la vista de un formulario tipo ContractType
     * @Route("/add",name="administration_default_company_add")
     * @Method({"GET","POST"})
     * @Template()
     * @return text/html AplicationDefaultBundle:Contract:add.html.twig
     **/
    public function addAction()
    {
        $formView = "";

        try {
            $companyManager = $this->get("administration.default.manager.company");
            $form = $companyManager->createForm();
            $formView = $form->createView();
        } catch (\Exception $exc) {
            $this->get("session")->getFlashBag()->add("danger", $exc->getMessage());
        }

        return array("form" => $formView);
    }

    /**
     * Retorna la vista del formulario tipo Contract en modo de edición
     * @Route("/edit/{id}",name="administration_default_company_edit",requirements={"id" = "\d+"}, defaults={"id" = 0})
     * @Method({"GET","POST"})
     * @Template()
     * @param integer $id
     * @return text/html AplicationDefaultBundle:Contract:edit.html.twig
     **/
    public function editAction($id)
    {
        $formView = "";

        try {
            $companyManager = $this->get("administration.default.manager.company");
            $form = $companyManager->createForm($id);
            $formView = $form->createView();
        } catch (\Exception $exc) {
            $this->get("session")->getFlashBag()->add("danger", $exc->getMessage());
        }

        return array("form" => $formView);
    }

    /**
     * Guarda los datos de un formulario basado en un modelo Contract
     * @Route("/save/{id}",name="administration_default_company_save",requirements={"id" = "\d+"}, defaults={"id" = 0})
     * @Method({"GET","POST"})
     * @param Request $request
     * @param integer $id
     * @return JsonResponse  {"status","message"}
     **/
    public function saveAction(Request $request, $id)
    {
        $status = 0;
        $message = "";

        try {
            $companyManager = $this->get("administration.default.manager.company");
            $form = $companyManager->createForm($id);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $company = $form->getData();
                $companyManager->save($company);
            } else {
                $error = (string)$form->getErrors(true, false);
                throw new \Exception($error);
            }

            $status = 1;
            $message = (!$id) ? "Empresa agregada correctamente" : "Empresa actualizada correctamente";

        } catch (\Exception $exc) {
            $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible guardar el registro." : $exc->getMessage();
            $message = $exc->getMessage();
        }

        return new JsonResponse(array("status" => $status, "message" => $message));
    }

    /**
     * Elimina o inactiva un registro de la entidad company
     * @Route("/delete/{id}",name="administration_default_company_delete",requirements={"id" = "\d+"}, defaults={"id" = 0},options={"expose"=true})
     * @Method({"GET","POST"})
     * @param integer $id
     * @return JsonResponse {"status","message"}
     **/
    public function deleteAction($id)
    {
        $status = 0;
        $message = "";

        try {

            $companyManager = $this->get("administration.default.manager.company");
            $result = $companyManager->deleteById($id);
            if ($result) {
                $message = "Empresa eliminada correctamente";
                $status = 1;
            }

        } catch (\Exception $exc) {

            $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible eliminar el registro." : $exc->getMessage();

        }
        return new JsonResponse(array("status" => $status, "message" => $message));
    }

    /**
     * Retorna la vista del formulario tipo companydinner en modo de edición
     * @Route("/assigndinner/{company_id}",name="administration_default_company_assigndinner",requirements={"company_id" = "\d+"}, defaults={"company_id" = 0})
     * @Method({"GET","POST"})
     * @Template()
     * @param integer $company_id
     * @return text/html AplicationDefaultBundle:Company:assigndinner.html.twig
     **/
    public function assigndinnerAction($company_id)
    {
        $dinnerscompany = array();
        try {
            $dinnerManager = $this->get("administration.default.manager.dinner");
            $companydinnerManager = $this->get("administration.default.manager.companydinner");
            $dinners = $dinnerManager->getActiveDinners();
            foreach ($dinners as $dinner) {
                $dinner_id = $dinner->getId();
                $companydinner = array('dinner_id' => $dinner_id, 'dinner_title' => $dinner->getName(), 'add' => 0);

                $existMenuRole = $companydinnerManager->getCompanydinnerActive($company_id, $dinner_id);
                if ($existMenuRole) {
                    $companydinner['add'] = 1;
                }
                $dinnerscompany[] = $companydinner;
            }
        } catch (\Exception $exc) {
            $this->get("session")->getFlashBag()->add("danger", $exc->getMessage());
        }

        return array('dinnerscompany' => $dinnerscompany);
    }

    /**
     * Guarda los permisos de dinners del rol
     * @Route("/assigndinner/save/{company_id}",name="administration_default_company_assigndinner_save",requirements={"company_id" = "\d+"}, defaults={"company_id" = 0})
     * @Method({"GET","POST"})
     * @param integer $company_id
     * @return json {'status':integer,message:string}
     **/
    public function assigndinnerSaveAction($company_id, Request $request)
    {
        $assigndinners = $request->get('assigndinners');
        $companydinnerManager = $this->get("administration.default.manager.companydinner");
        $companyManager = $this->get("administration.default.manager.company");
        $dinnerManager = $this->get("administration.default.manager.dinner");
        $catalogManager = $this->get("aplication.default.manager.catalog");
        $status = 0;
        $message = 'Comedor/es asignado/s correctamente';
        try {
            $company = $companyManager->find($company_id);
            $client = $company->getClient();
            $dinners = $dinnerManager->getActiveDinners();

            foreach ($dinners as $dinner) {
                $dinner_id = $dinner->getId();
                $assigndinner = isset($assigndinners[$dinner_id]) ? $assigndinners[$dinner_id] : array();

                $companydinner = null;
                $add = (@$assigndinner['add']) ? 1 : 0;

                $exist = $companydinnerManager->findOneBy(array('companyId' => $company_id, 'dinnerId' => $dinner_id));
                if ($add) {
                    $companydinner = (!$exist) ? $companydinnerManager->create() : $exist;
                    $companydinner->setClient($client);
                    $companydinner->setCompany($company);
                    $companydinner->setDinner($dinnerManager->find($dinner_id));
                    $companydinner->setCtstatus($catalogManager->find(Constant::STATUS_ACTIVE_RECORD));
                    $companydinnerManager->save($companydinner, false);
                } elseif ($exist) {
                    if(!$companydinnerManager->deleteById($exist, false)){
                     $message = "No se pudo desasignar el commedor ".$exist->getDinner()->getName()." porque esta siendo usado en una transación";
                    }
                }
            }

            $companydinnerManager->flushAllChanges();
            $status = 1;
        } catch (\Exception $exc) {
            $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible guardar los registros." : $exc->getMessage();
        }

        return new JsonResponse(array("status" => $status, "message" => $message));
    }
}
