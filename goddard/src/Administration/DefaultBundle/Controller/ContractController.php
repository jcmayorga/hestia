<?php

namespace Administration\DefaultBundle\Controller;

use Administration\DefaultBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ContractController extends Controller
{
    /**
     * @Route("/index",name="administration_default_contract_index")
     * @Method({"GET", "POST"})
     * @Template()
     * @return text/html AdministrationDefaultBundle:Contract:index.html.twig
     **/
    public function indexAction(Request $request)
    {
        $contractGrid=$this->get('administration.default.grid.contract');
        $grid=$contractGrid->getResponse($request);

        return array("grid" => $grid);
    }

    /**
     * Retorna la vista de un formulario tipo ContractType
     * @Route("/add",name="administration_default_contract_add")
     * @Method({"GET","POST"})
     * @Template()
     * @return text/html AplicationDefaultBundle:Contract:add.html.twig
     **/
    public function addAction(){
        $formView = "";

        try {
            $contractManager=  $this->get("administration.default.manager.contract");
            $form=  $contractManager->createForm();
            $formView = $form->createView();
        } catch (\Exception $exc) {
            $this->get("session")->getFlashBag()->add("danger", $exc->getMessage());
        }

        return array("form" => $formView);
    }

    /**
     * Retorna la vista del formulario tipo Contract en modo de edición
     * @Route("/edit/{id}",name="administration_default_contract_edit",requirements={"id" = "\d+"}, defaults={"id" = 0})
     * @Method({"GET","POST"})
     * @Template()
     * @param integer $id
     * @return text/html AplicationDefaultBundle:Contract:edit.html.twig
     **/
    public function editAction($id){
        $formView = "";

        try {
            $contractManager=  $this->get("administration.default.manager.contract");
            $form=  $contractManager->createForm($id);
            $formView = $form->createView();
        } catch (\Exception $exc) {
            $this->get("session")->getFlashBag()->add("danger", $exc->getMessage());
        }

        return array("form" => $formView);
    }

    /**
     * Guarda los datos de un formulario basado en un modelo Contract
     * @Route("/save/{id}",name="administration_default_contract_save",requirements={"id" = "\d+"}, defaults={"id" = 0})
     * @Method({"GET","POST"})
     * @param Request $request
     * @param integer $id
     * @return JsonResponse  {"status","message"}
     **/
    public function saveAction(Request $request,$id){
        $status = 0;
        $message = "";

        try {
            $contractManager=  $this->get("administration.default.manager.contract");
            $form=$contractManager->createForm($id);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $contract=$form->getData();
                $contractManager->save($contract);
            } else {
                $error=(string) $form->getErrors(true, false);
                throw new \Exception($error);
            }

            $status = 1;
            $message=(!$id)?"Contrado agregado correctamente":"Contrato actualizado correctamente";

        } catch (\Exception $exc) {
            $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible guardar el registro." : $exc->getMessage();
            $message = $exc->getMessage();
        }

        return new JsonResponse(array("status" => $status, "message" => $message));
    }

    /**
     * Elimina o inactiva un registro de la entidad contract
     * @Route("/delete/{id}",name="administration_default_contract_delete",requirements={"id" = "\d+"}, defaults={"id" = 0},options={"expose"=true})
     * @Method({"GET","POST"})
     * @param integer $id
     * @return JsonResponse {"status","message"}
     **/
    public function deleteAction($id){
        $status = 0;
        $message = "";

        try {

            $contractManager=  $this->get("administration.default.manager.contract");
            $result=$contractManager->deleteById($id);
            if ($result) {
                $message="Contrato eliminado correctamente";
                $status = 1;
            }

        } catch (\Exception $exc) {

            $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible eliminar el registro." : $exc->getMessage();

        }
        return new JsonResponse(array("status" => $status, "message" => $message));
    }
}
