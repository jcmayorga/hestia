<?php

namespace Administration\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DinnerController extends Controller
{
    /**
     * @Route("/index",name="administration_default_dinner_index")
     * @Method({"GET", "POST"})
     * @Template()
     * @return text/html AdministrationDefaultBundle:Dinner:index.html.twig
     **/
    public function indexAction(Request $request)
    {
        $dinnerGrid=$this->get('administration.default.grid.dinner');
        $grid=$dinnerGrid->getResponse($request);

        return array("grid" => $grid);
    }

    /**
     * Retorna la vista de un formulario tipo ContractType
     * @Route("/add",name="administration_default_dinner_add")
     * @Method({"GET","POST"})
     * @Template()
     * @return text/html AplicationDefaultBundle:Contract:add.html.twig
     **/
    public function addAction(){
        $formView = "";

        try {
            $dinnerManager=  $this->get("administration.default.manager.dinner");
            $form=  $dinnerManager->createForm();
            $formView = $form->createView();
        } catch (\Exception $exc) {
            $this->get("session")->getFlashBag()->add("danger", $exc->getMessage());
        }

        return array("form" => $formView);
    }

    /**
     * Retorna la vista del formulario tipo Contract en modo de edición
     * @Route("/edit/{id}",name="administration_default_dinner_edit",requirements={"id" = "\d+"}, defaults={"id" = 0})
     * @Method({"GET","POST"})
     * @Template()
     * @param integer $id
     * @return text/html AplicationDefaultBundle:Contract:edit.html.twig
     **/
    public function editAction($id){
        $formView = "";

        try {
            $dinnerManager=  $this->get("administration.default.manager.dinner");
            $form=  $dinnerManager->createForm($id);
            $formView = $form->createView();
        } catch (\Exception $exc) {
            $this->get("session")->getFlashBag()->add("danger", $exc->getMessage());
        }

        return array("form" => $formView);
    }

    /**
     * Guarda los datos de un formulario basado en un modelo Contract
     * @Route("/save/{id}",name="administration_default_dinner_save",requirements={"id" = "\d+"}, defaults={"id" = 0})
     * @Method({"GET","POST"})
     * @param Request $request
     * @param integer $id
     * @return JsonResponse  {"status","message"}
     **/
    public function saveAction(Request $request,$id){
        $status = 0;
        $message = "";

        try {
            $dinnerManager=  $this->get("administration.default.manager.dinner");
            $form=$dinnerManager->createForm($id);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $dinner=$form->getData();
                $dinnerManager->save($dinner);
            } else {
                $error=(string) $form->getErrors(true, false);
                throw new \Exception($error);
            }

            $status = 1;
            $message=(!$id)?"Comedor agregado correctamente":"Comedor actualizado correctamente";

        } catch (\Exception $exc) {
            $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible guardar el registro." : $exc->getMessage();
            $message = $exc->getMessage();
        }

        return new JsonResponse(array("status" => $status, "message" => $message));
    }

    /**
     * Elimina o inactiva un registro de la entidad dinner
     * @Route("/delete/{id}",name="administration_default_dinner_delete",requirements={"id" = "\d+"}, defaults={"id" = 0},options={"expose"=true})
     * @Method({"GET","POST"})
     * @param integer $id
     * @return JsonResponse {"status","message"}
     **/
    public function deleteAction($id){
        $status = 0;
        $message = "";

        try {

            $dinnerManager=  $this->get("administration.default.manager.dinner");
            $result=$dinnerManager->deleteById($id);
            if ($result) {
                $message="Comedor eliminado correctamente";
                $status = 1;
            }

        } catch (\Exception $exc) {

            $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible eliminar el registro." : $exc->getMessage();

        }
        return new JsonResponse(array("status" => $status, "message" => $message));
    }
}
