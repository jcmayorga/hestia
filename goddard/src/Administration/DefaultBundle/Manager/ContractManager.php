<?php
/**
 * @author Richard Veliz
 */

namespace Administration\DefaultBundle\Manager;

use Administration\DefaultBundle\Form\Type\ContractType;
use Aplication\DefaultBundle\Model\MainManager;

class ContractManager extends MainManager
{

    /**
     * Crea el objeto formulario para el modelo Contract
     * @param integer $id Id del registro Contract
     * @return formType
     */
    public function createForm($id = 0, $options = array())
    {
        $contract = $this->create();
        if ($id) {
            $contract = $this->find($id);
            if (empty($contract)) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }
        }
        return $this->getContainer()->get('form.factory')->create(ContractType::class, $contract, $options);
    }

    /**
     * Inactiva o elimina un registro basado en el modelo Contract
     * @param integer $id Id del registro Contract
     * @param boolean $flush Indica si se realiza el flush en la persistencia
     * @return boolean true si se realizó la operación con éxito
     */
    public function deleteById($id, $flush = true)
    {
        $contract = $this->find($id);
        if (!$contract) {
            throw new NotFoundHttpException("No se encontró un registro con id $id");
        }

        $this->delete($contract, $flush);

        return true;
    }
}
