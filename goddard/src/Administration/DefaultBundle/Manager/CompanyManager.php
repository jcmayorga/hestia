<?php
/**
 * @author Richard Veliz
 */

namespace Administration\DefaultBundle\Manager;

use Administration\DefaultBundle\Form\Type\CompanyType;
use Aplication\DefaultBundle\Model\MainManager;

class CompanyManager extends MainManager
{

    /**
     * Crea el objeto formulario para el modelo Company
     * @param integer $id Id del registro Company
     * @return formType
     */
    public function createForm($id = 0, $options = array())
    {
        $company = $this->create();
        if ($id) {
            $company = $this->find($id);
            if (empty($company)) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }
        }
        return $this->getContainer()->get('form.factory')->create(CompanyType::class, $company, $options);
    }

    /**
     * Inactiva o elimina un registro basado en el modelo Company
     * @param integer $id Id del registro Company
     * @param boolean $flush Indica si se realiza el flush en la persistencia
     * @return boolean true si se realizó la operación con éxito
     */
    public function deleteById($id, $flush = true)
    {
        $company = $this->find($id);
        if (!$company) {
            throw new NotFoundHttpException("No se encontró un registro con id $id");
        }

        $this->delete($company, $flush);

        return true;
    }


    public function getAllCompanies(){

        return $this->getRepository()->findBy(array(),array("businessname"=>"ASC"));
    }
}
