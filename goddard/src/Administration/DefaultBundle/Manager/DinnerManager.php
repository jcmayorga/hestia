<?php
/**
 * @author Richard Veliz
 */

namespace Administration\DefaultBundle\Manager;

use Administration\DefaultBundle\Form\Type\DinnerType;
use Aplication\DefaultBundle\Model\Constant;
use Aplication\DefaultBundle\Model\MainManager;

class DinnerManager extends MainManager
{

    /**
     * Crea el objeto formulario para el modelo Dinner
     * @param integer $id Id del registro Dinner
     * @return formType
     */
    public function createForm($id = 0, $options = array())
    {
        $dinner = $this->create();
        if ($id) {
            $dinner = $this->find($id);
            if (empty($dinner)) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }
        }
        return $this->getContainer()->get('form.factory')->create(DinnerType::class, $dinner, $options);
    }

    /**
     * Inactiva o elimina un registro basado en el modelo Dinner
     * @param integer $id Id del registro Dinner
     * @param boolean $flush Indica si se realiza el flush en la persistencia
     * @return boolean true si se realizó la operación con éxito
     */
    public function deleteById($id, $flush = true)
    {
        $dinner = $this->find($id);
        if (!$dinner) {
            throw new NotFoundHttpException("No se encontró un registro con id $id");
        }

        $this->delete($dinner, $flush);

        return true;
    }

    public function getActiveDinners()
    {
        return $this->findBy(array('ctstatusId' => Constant::STATUS_ACTIVE_RECORD), array('name' => 'asc'));
    }
}
