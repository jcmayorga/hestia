<?php
/**
 * @author Luis Malquin
 */

namespace Administration\DefaultBundle\Manager;

use Administration\DefaultBundle\Form\Type\DinnerType;
use Aplication\DefaultBundle\Model\MainManager;
use Aplication\DefaultBundle\Model\Constant;

class RosterManager extends MainManager
{

      /**
     * Inactiva o elimina un registro basado en el modelo Dinneruser
     * @param integer $id Id del registro Dinner
     * @param boolean $flush Indica si se realiza el flush en la persistencia
     * @return boolean true si se realizó la operación con éxito
     */
    public function deleteById($id, $flush = true)
    {
        $roster = $this->find($id);
        if (!$roster) {
            throw new NotFoundHttpException("No se encontró un registro con id $id");
        }

        $this->delete($roster, $flush);

        return true;
    }

   


   
}