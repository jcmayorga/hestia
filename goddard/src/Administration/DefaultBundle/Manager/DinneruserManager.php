<?php
/**
 * @author Luis Malquin
 */

namespace Administration\DefaultBundle\Manager;

use Administration\DefaultBundle\Form\Type\DinnerType;
use Aplication\DefaultBundle\Model\MainManager;
use Aplication\DefaultBundle\Model\Constant;

class DinneruserManager extends MainManager
{

      /**
     * Inactiva o elimina un registro basado en el modelo Dinneruser
     * @param integer $id Id del registro Dinner
     * @param boolean $flush Indica si se realiza el flush en la persistencia
     * @return boolean true si se realizó la operación con éxito
     */
    public function deleteById($id, $flush = true)
    {
        $dinner = $this->find($id);
        if (!$dinner) {
            throw new NotFoundHttpException("No se encontró un registro con id $id");
        }

        $this->delete($dinner, $flush);

        return true;
    }

     public function getActiveDinner($user_id,$dinner_id){
            return $this->findOneBy(array('userId'=>$user_id,'dinnerId'=>$dinner_id,'ctstatusId'=>Constant::STATUS_ACTIVE_RECORD));
     } 

   
}
