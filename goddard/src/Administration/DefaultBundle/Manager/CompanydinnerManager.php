<?php
/*
* @autor Richard Veliz
*/

namespace Administration\DefaultBundle\Manager;

use Aplication\DefaultBundle\Model\MainManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Aplication\DefaultBundle\Model\Constant;


/**
 * Clase de manipulación del repositorio Menucompany
 */
class CompanydinnerManager extends MainManager
{

    /**
     * Inactiva o elimina un registro basado en el modelo Menucompany
     * @param integer $id Id del registro Menucompany
     * @param boolean $flush Indica si se realiza el flush en la persistencia
     * @return boolean true si se realizó la operación con éxito
     */
    public function deleteById($id, $flush = true)
    {
        $dinnercompany = $this->find($id);
        $response = true;
        if (!$dinnercompany) {
            throw new NotFoundHttpException("No se encontró un registro con id $id");
        }
//        $dinnercompany->setActivo(0);
//        $this->save($dinnercompany, $flush);
        $transactionManager = $this->getContainer()->get('bussiness.default.manager.transaction');
        ($transactionManager->existCompanydinnerId($id)) ? $response = false : $this->delete($dinnercompany);

        return $response;
    }

    public function getCompanydinnerActive($company_id, $dinner_id)
    {
        return $this->findOneBy(array('dinnerId' => $dinner_id, 'companyId' => $company_id, 'ctstatusId' => Constant::STATUS_ACTIVE_RECORD));
    }
}
