<?php

namespace Administration\DefaultBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Descripcion de ContractRepository
 * @author Richard Veliz
 */
class ContractRepository extends EntityRepository
{
    /**
     * Obtiene el QueryBuilder de la lista de menus a las cuales tiene permiso el usuario por aplicación
     * @return array
     */
    public function getList()
    {
        $qb = $this->createQueryBuilder("ct")
            ->select('ct.id, ct.name,ct.description,ct.ctstatusId,cts.name status')
            ->join('ct.ctstatus', 'cts');

        $query = $qb->getQuery();

        return $query->getResult();
    }
}