<?php

namespace Administration\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Companydinner
 *
 * @ORM\Table(name="administration.companydinner", indexes={@ORM\Index(name="IDX_6763F6B979B1AD6", columns={"company_id"}), @ORM\Index(name="IDX_6763F6BD0ED7024", columns={"ctstatus_id"}), @ORM\Index(name="IDX_6763F6BC8B1AA0C", columns={"dinner_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Companydinner
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="administration.companydinner_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

        /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="usucrud_id", type="integer", nullable=true)
     */
    private $usucrudId;

    /**
     * @var integer
     *
     * @ORM\Column(name="company_id", type="integer", nullable=false)
     */
    private $companyId;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var integer
     *
     * @ORM\Column(name="ctstatus_id", type="integer", nullable=true)
     */
    private $ctstatusId;

    /**
     * @var \Catalog
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\Catalog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ctstatus_id", referencedColumnName="id")
     * })
     */
    private $ctstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="dinner_id", type="integer", nullable=false)
     */
    private $dinnerId;
    /**
     * @var \Dinner
     *
     * @ORM\ManyToOne(targetEntity="Dinner")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dinner_id", referencedColumnName="id")
     * })
     */
    private $dinner;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set usucrudId
     *
     * @param integer $usucrudId
     */
    public function setUsucrudId($usucrudId)
    {
        $this->usucrudId = $usucrudId;
    }

    /**
     * Get usucrudId
     *
     * @return integer 
     */
    public function getUsucrudId()
    {
        return $this->usucrudId;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }

    /**
     * Get companyId
     *
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set company
     *
     * @param \Administration\DefaultBundle\Entity\Company $company
     */
    public function setCompany(\Administration\DefaultBundle\Entity\Company $company = null)
    {
        $this->company = $company;
    }

    /**
     * Get company
     *
     * @return \Administration\DefaultBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set client
     *
     * @param \Administration\DefaultBundle\Entity\Client $client
     */
    public function setClient(\Administration\DefaultBundle\Entity\Client $client = null)
    {
        $this->client = $client;
    }

    /**
     * Get client
     *
     * @return \Administration\DefaultBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }
    
    /**
     * Set ctstatus
     *
     * @param \Aplication\DefaultBundle\Entity\Catalog $ctstatus
     */
    public function setCtstatus(\Aplication\DefaultBundle\Entity\Catalog $ctstatus = null)
    {
        $this->ctstatus = $ctstatus;
    }

    /**
     * Get ctstatus
     *
     * @return \Aplication\DefaultBundle\Entity\Catalog
     */
    public function getCtstatus()
    {
        return $this->ctstatus;
    }

    /**
     * Set dinnerId
     *
     * @param integer $dinnerId
     */
    public function setDinnerId($dinnerId)
    {
        $this->dinnerId = $dinnerId;
    }

    /**
     * Get dinnerId
     *
     * @return integer
     */
    public function getDinnerId()
    {
        return $this->dinnerId;
    }

    /**
     * Set dinner
     *
     * @param \Administration\DefaultBundle\Entity\Dinner $dinner
     */
    public function setDinner(\Administration\DefaultBundle\Entity\Dinner $dinner = null)
    {
        $this->dinner = $dinner;
    }

    /**
     * Get dinner
     *
     * @return \Administration\DefaultBundle\Entity\Dinner 
     */
    public function getDinner()
    {
        return $this->dinner;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
