<?php 
/*
* @autor Luis Malquin
*/

namespace Administration\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="administration.roster")
* @ORM\Entity()
* @ORM\HasLifecycleCallbacks()
*/
class Roster{

    /**
    * @var integer $id
    * @ORM\Column(name="id", type="integer", nullable=false)
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    **/
    private $id;

    /**
    * @var string $identification
    * @ORM\Column(name="identification", type="string", nullable=false)
    * @Assert\NotNull(message="Debe ingresar el campo identification")
    **/
    private $identification;

    /**
    * @var integer $ctidentification_id
    * @ORM\Column(name="ctidentification_id", type="integer", nullable=true)
    **/
    private $ctidentification_id;

    /**
    * @ORM\ManyToOne(targetEntity="Aplication\DefaultBundle\Entity\Catalog")
    * @ORM\JoinColumn(name="ctidentification_id", referencedColumnName="id")
    **/
    private $ctidentification;

    /**
    * @var string $firstname
    * @ORM\Column(name="firstname", type="string", nullable=true)
    **/
    private $firstname;

    /**
    * @var string $lastname
    * @ORM\Column(name="lastname", type="string", nullable=true)
    **/
    private $lastname;

    /**
    * @var string $fullname
    * @ORM\Column(name="fullname", type="string", nullable=true)
    **/
    private $fullname;

    /**
    * @var string $email
    * @ORM\Column(name="email", type="string", nullable=true)
    **/
    private $email;

    /**
    * @var integer $company_id
    * @ORM\Column(name="company_id", type="integer", nullable=true)
    **/
    private $company_id;

    /**
    * @ORM\ManyToOne(targetEntity="Company")
    * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
    **/
    private $company;

    /**
    * @var string $employeecode
    * @ORM\Column(name="employeecode", type="string", nullable=true)
    **/
    private $employeecode;

    /**
    * @var string $barcode
    * @ORM\Column(name="barcode", type="string", nullable=true)
    **/
    private $barcode;

    /**
    * @var string $rfidcode
    * @ORM\Column(name="rfidcode", type="string", nullable=true)
    **/
    private $rfidcode;

    /**
    * @var string $biometriccode
    * @ORM\Column(name="biometriccode", type="string", nullable=true)
    **/
    private $biometriccode;

    /**
    * @var integer $ctstatus_id
    * @ORM\Column(name="ctstatus_id", type="integer", nullable=true)
    **/
    private $ctstatus_id;

    /**
    * @ORM\ManyToOne(targetEntity="Aplication\DefaultBundle\Entity\Catalog")
    * @ORM\JoinColumn(name="ctstatus_id", referencedColumnName="id")
    **/
    private $ctstatus;

    /**
    * @var integer $usucrud_id
    * @ORM\Column(name="usucrud_id", type="integer", nullable=true)
    **/
    private $usucrud_id;

   

    /**
    * @var string $unit
    * @ORM\Column(name="unit", type="string", nullable=true)
    **/
    private $unit;

    /**
    * @var string $costcenter
    * @ORM\Column(name="costcenter", type="string", nullable=true)
    **/
    private $costcenter;

    /**
    * @var string $namecenter
    * @ORM\Column(name="namecenter", type="string", nullable=true)
    **/
    private $namecenter;

    /**
    * @var datetime $created_at
    * @ORM\Column(name="created_at", type="datetime", nullable=true)
    **/
    private $created_at;

    /**
    * @var datetime $updated_at
    * @ORM\Column(name="updated_at", type="datetime", nullable=true)
    **/
    private $updated_at;


    /**
    * Get id
    * @return integer
    **/
    public function getId(){
         return $this->id;
    }

    /**
    * Set identification
    * @param string identification
    * @return roster
    **/
    public function setIdentification($identification){
         $this->identification = $identification;
         return $this;
    }

    /**
    * Get identification
    * @return string
    **/
    public function getIdentification(){
         return $this->identification;
    }

    /**
    * Set ctidentification_id
    * @param integer ctidentification_id
    * @return roster
    **/
    public function setCtidentificationId($ctidentification_id){
         $this->ctidentification_id = $ctidentification_id;
         return $this;
    }

    /**
    * Get ctidentification_id
    * @return integer
    **/
    public function getCtidentificationId(){
         return $this->ctidentification_id;
    }

    /**
    * Set ctidentification
    * @param \Core\AppBundle\Entity\Catalogo $ctidentification
    **/
    public function setCtidentification(\Aplication\DefaultBundle\Entity\Catalog $ctidentification=null){
         $this->ctidentification = $ctidentification;
         return $this;
    }

    /**
    * Get ctidentification
    * @return \Core\AppBundle\Entity\Catalogo
    **/
    public function getCtidentification(){
         return $this->ctidentification;
    }

    /**
    * Set firstname
    * @param string firstname
    * @return roster
    **/
    public function setFirstname($firstname){
         $this->firstname = $firstname;
         return $this;
    }

    /**
    * Get firstname
    * @return string
    **/
    public function getFirstname(){
         return $this->firstname;
    }

    /**
    * Set lastname
    * @param string lastname
    * @return roster
    **/
    public function setLastname($lastname){
         $this->lastname = $lastname;
         return $this;
    }

    /**
    * Get lastname
    * @return string
    **/
    public function getLastname(){
         return $this->lastname;
    }

    /**
    * Set fullname
    * @param string fullname
    * @return roster
    **/
    public function setFullname($fullname){
         $this->fullname = $fullname;
         return $this;
    }

    /**
    * Get fullname
    * @return string
    **/
    public function getFullname(){
         return $this->fullname;
    }

    /**
    * Set email
    * @param string email
    * @return roster
    **/
    public function setEmail($email){
         $this->email = $email;
         return $this;
    }

    /**
    * Get email
    * @return string
    **/
    public function getEmail(){
         return $this->email;
    }

    /**
    * Set company_id
    * @param integer company_id
    * @return roster
    **/
    public function setCompanyId($company_id){
         $this->company_id = $company_id;
         return $this;
    }

    /**
    * Get company_id
    * @return integer
    **/
    public function getCompanyId(){
         return $this->company_id;
    }

    /**
    * Set company
    * @param \Administration\DefaultBundle\Entity\Company $company
    **/
    public function setCompany(\Administration\DefaultBundle\Entity\Company $company){
         $this->company = $company;
         return $this;
    }

    /**
    * Get company
    * @return \Administration\DefaultBundle\Entity\Company
    **/
    public function getCompany(){
         return $this->company;
    }

    /**
    * Set employeecode
    * @param string employeecode
    * @return roster
    **/
    public function setEmployeecode($employeecode){
         $this->employeecode = $employeecode;
         return $this;
    }

    /**
    * Get employeecode
    * @return string
    **/
    public function getEmployeecode(){
         return $this->employeecode;
    }

    /**
    * Set barcode
    * @param string barcode
    * @return roster
    **/
    public function setBarcode($barcode){
         $this->barcode = $barcode;
         return $this;
    }

    /**
    * Get barcode
    * @return string
    **/
    public function getBarcode(){
         return $this->barcode;
    }

    /**
    * Set rfidcode
    * @param string rfidcode
    * @return roster
    **/
    public function setRfidcode($rfidcode){
         $this->rfidcode = $rfidcode;
         return $this;
    }

    /**
    * Get rfidcode
    * @return string
    **/
    public function getRfidcode(){
         return $this->rfidcode;
    }

    /**
    * Set biometriccode
    * @param string biometriccode
    * @return roster
    **/
    public function setBiometriccode($biometriccode){
         $this->biometriccode = $biometriccode;
         return $this;
    }

    /**
    * Get biometriccode
    * @return string
    **/
    public function getBiometriccode(){
         return $this->biometriccode;
    }

    /**
    * Set ctstatus_id
    * @param integer ctstatus_id
    * @return roster
    **/
    public function setCtstatusId($ctstatus_id){
         $this->ctstatus_id = $ctstatus_id;
         return $this;
    }

    /**
    * Get ctstatus_id
    * @return integer
    **/
    public function getCtstatusId(){
         return $this->ctstatus_id;
    }

    /**
    * Set ctstatus
    * @param \Core\AppBundle\Entity\Catalogo $ctstatus
    **/
    public function setCtstatus(\Aplication\DefaultBundle\Entity\Catalog $ctstatus){
         $this->ctstatus = $ctstatus;
         return $this;
    }

    /**
    * Get ctstatus
    * @return \Core\AppBundle\Entity\Catalogo
    **/
    public function getCtstatus(){
         return $this->ctstatus;
    }

    /**
    * Set usucrud_id
    * @param integer usucrud_id
    * @return roster
    **/
    public function setUsucrudId($usucrud_id){
         $this->usucrud_id = $usucrud_id;
         return $this;
    }

    /**
    * Get usucrud_id
    * @return integer
    **/
    public function getUsucrudId(){
         return $this->usucrud_id;
    }

   
    /**
    * Set unit
    * @param string unit
    * @return roster
    **/
    public function setUnit($unit){
         $this->unit = $unit;
         return $this;
    }

    /**
    * Get unit
    * @return string
    **/
    public function getUnit(){
         return $this->unit;
    }

    /**
    * Set costcenter
    * @param string costcenter
    * @return roster
    **/
    public function setCostcenter($costcenter){
         $this->costcenter = $costcenter;
         return $this;
    }

    /**
    * Get costcenter
    * @return string
    **/
    public function getCostcenter(){
         return $this->costcenter;
    }

    /**
    * Set namecenter
    * @param string namecenter
    * @return roster
    **/
    public function setNamecenter($namecenter){
         $this->namecenter = $namecenter;
         return $this;
    }

    /**
    * Get namecenter
    * @return string
    **/
    public function getNamecenter(){
         return $this->namecenter;
    }

   

    /**
    * @ORM\PrePersist
    **/
    public function prePersist(){
        $this->created_at=new \DateTime();
        $this->fullname=$this->lastname." ".$this->firstname;
    }

    /**
    * @ORM\PreUpdate
    **/
    public function preUpdate(){
        $this->updated_at=new \DateTime();
        $this->fullname=$this->lastname." ".$this->firstname;   
    }


    public function __clone() {
               $this->id=null;
             }

    public function copy() {
               return clone $this;
             }

}
 ?>