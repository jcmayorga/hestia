<?php

namespace Administration\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 * @ORM\Table(name="administration.company", indexes={@ORM\Index(name="IDX_68728D522576E0FD", columns={"contract_id"}), @ORM\Index(name="IDX_68728D5219EB6921", columns={"client_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Company
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="administration.company_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ruc", type="string", nullable=true)
     */
    private $ruc;

    /**
     * @var integer
     *
     * @ORM\Column(name="establishmentnumber", type="integer", nullable=true)
     */
    private $establishmentnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="businessname", type="string", nullable=true)
     */
    private $businessname;

    /**
     * @var string
     *
     * @ORM\Column(name="legalrepresentative", type="string", nullable=true)
     */
    private $legalrepresentative;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", nullable=true)
     */
    private $logo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="usucrud_id", type="integer", nullable=true)
     */
    private $usucrudId;

    /**
     * @var integer
     *
     * @ORM\Column(name="contract_id", type="integer", nullable=false)
     */
    private $contractId;

    /**
     * @var \Contract
     *
     * @ORM\ManyToOne(targetEntity="Contract")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="contract_id", referencedColumnName="id")
     * })
     */
    private $contract;

    /**
     * @var integer
     *
     * @ORM\Column(name="client_id", type="integer", nullable=false)
     */
    private $clientId;
    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ruc
     *
     * @param string $ruc
     */
    public function setRuc($ruc)
    {
        $this->ruc = $ruc;
    }

    /**
     * Get ruc
     *
     * @return string 
     */
    public function getRuc()
    {
        return $this->ruc;
    }

    /**
     * Set establishmentnumber
     *
     * @param integer $establishmentnumber
     */
    public function setEstablishmentnumber($establishmentnumber)
    {
        $this->establishmentnumber = $establishmentnumber;
    }

    /**
     * Get establishmentnumber
     *
     * @return integer 
     */
    public function getEstablishmentnumber()
    {
        return $this->establishmentnumber;
    }

    /**
     * Set businessname
     *
     * @param string $businessname
     */
    public function setBusinessname($businessname)
    {
        $this->businessname = $businessname;
    }

    /**
     * Get businessname
     *
     * @return string 
     */
    public function getBusinessname()
    {
        return $this->businessname;
    }

    /**
     * Set legalrepresentative
     *
     * @param string $legalrepresentative
     */
    public function setLegalrepresentative($legalrepresentative)
    {
        $this->legalrepresentative = $legalrepresentative;
    }

    /**
     * Get legalrepresentative
     *
     * @return string 
     */
    public function getLegalrepresentative()
    {
        return $this->legalrepresentative;
    }

    /**
     * Set address
     *
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set logo
     *
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set usucrudId
     *
     * @param integer $usucrudId
     */
    public function setUsucrudId($usucrudId)
    {
        $this->usucrudId = $usucrudId;
    }

    /**
     * Get usucrudId
     *
     * @return integer 
     */
    public function getUsucrudId()
    {
        return $this->usucrudId;
    }

    /**
     * Set contractId
     *
     * @param integer $contractId
     */
    public function setContractId($contractId)
    {
        $this->contractId = $contractId;
    }

    /**
     * Get contractId
     *
     * @return integer
     */
    public function getContractId()
    {
        return $this->contractId;
    }

    /**
     * Set contract
     *
     * @param \Administration\DefaultBundle\Entity\Contract $contract
     */
    public function setContract(\Administration\DefaultBundle\Entity\Contract $contract = null)
    {
        $this->contract = $contract;
    }

    /**
     * Get contract
     *
     * @return \Administration\DefaultBundle\Entity\Contract 
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * Set clientId
     *
     * @param integer $clientId
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * Get clientId
     *
     * @return integer
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set client
     *
     * @param \Administration\DefaultBundle\Entity\Client $client
     */
    public function setClient(\Administration\DefaultBundle\Entity\Client $client = null)
    {
        $this->client = $client;
    }

    /**
     * Get client
     *
     * @return \Administration\DefaultBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
