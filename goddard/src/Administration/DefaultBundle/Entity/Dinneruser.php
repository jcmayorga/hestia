<?php

namespace Administration\DefaultBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Aplication\DefaultBundle\Model\Constant;

/**
 * Dinneruser
  *
 * @ORM\Table(name="administration.dinneruser", indexes={@ORM\Index(name="IDX_C9689E7D0ED7024", columns={"ctstatus_id"}), @ORM\Index(name="IDX_C9689E7A76ED395", columns={"user_id"}), @ORM\Index(name="IDX_C9689E7C8B1AA0C", columns={"dinner_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Dinneruser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="administration.dinneruser_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="usucrud_id", type="integer", nullable=true)
     */
    private $usucrudId;
    
        /**
     * @var integer
     *
     * @ORM\Column(name="ctstatus_id", type="integer", nullable=true)
     */
    private $ctstatusId;


    /**
     * @var \Catalog
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\Catalog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ctstatus_id", referencedColumnName="id")
     * })
     */
    private $ctstatus;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;
    
    
         /**
     * @var integer
     *
     * @ORM\Column(name="dinner_id", type="integer", nullable=true)
     */
    private $dinnerId;

    /**
     * @var \Dinner
     *
     * @ORM\ManyToOne(targetEntity="\Administration\DefaultBundle\Entity\Dinner")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dinner_id", referencedColumnName="id")
     * })
     */
    private $dinner;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Dinneruser
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Dinneruser
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set usucrudId
     *
     * @param integer $usucrudId
     * @return Dinneruser
     */
    public function setUsucrudId($usucrudId)
    {
        $this->usucrudId = $usucrudId;

        return $this;
    }

    /**
     * Get usucrudId
     *
     * @return integer 
     */
    public function getUsucrudId()
    {
        return $this->usucrudId;
    }

    /**
     * Set ctstatusId
     *
     * @param integer $ctstatusId
     * @return Dinneruser
     */
    public function setCtstatusId($ctstatusId)
    {
        $this->ctstatusId = $ctstatusId;

        return $this;
    }

    /**
     * Get ctstatusId
     *
     * @return integer 
     */
    public function getCtstatusId()
    {
        return $this->ctstatusId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Dinneruser
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set dinnerId
     *
     * @param integer $dinnerId
     * @return Dinneruser
     */
    public function setDinnerId($dinnerId)
    {
        $this->dinnerId = $dinnerId;

        return $this;
    }

    /**
     * Get dinnerId
     *
     * @return integer 
     */
    public function getDinnerId()
    {
        return $this->dinnerId;
    }

    /**
     * Set ctstatus
     *
     * @param \Aplication\DefaultBundle\Entity\Catalog $ctstatus
     * @return Dinneruser
     */
    public function setCtstatus(\Aplication\DefaultBundle\Entity\Catalog $ctstatus = null)
    {
        $this->ctstatus = $ctstatus;

        return $this;
    }

    /**
     * Get ctstatus
     *
     * @return \Aplication\DefaultBundle\Entity\Catalog 
     */
    public function getCtstatus()
    {
        return $this->ctstatus;
    }

    /**
     * Set user
     *
     * @param \Aplication\DefaultBundle\Entity\User $user
     * @return Dinneruser
     */
    public function setUser(\Aplication\DefaultBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Aplication\DefaultBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set dinner
     *
     * @param \Administration\DefaultBundle\Entity\Dinner $dinner
     * @return Dinneruser
     */
    public function setDinner(\Administration\DefaultBundle\Entity\Dinner $dinner = null)
    {
        $this->dinner = $dinner;

        return $this;
    }

    /**
     * Get dinner
     *
     * @return \Administration\DefaultBundle\Entity\Dinner 
     */
    public function getDinner()
    {
        return $this->dinner;
    }

    /**
    * @ORM\PrePersist
    **/
    public function prePersist(){
            $this->ctstatusId=Constant::STATUS_ACTIVE_RECORD;
            $this->createdAt=new \DateTime();            
    }

    /**
    * @ORM\PreUpdate
    **/
    public function preUpdate(){
            $this->updatedAt=new \DateTime();
    }
    
    public function __toString() {
             }
    public function __clone() {
               $this->id=null;
             }
    public function copy() {
               return clone $this;
             }
}
