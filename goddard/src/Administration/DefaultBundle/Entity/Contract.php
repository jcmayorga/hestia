<?php

namespace Administration\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Aplication\DefaultBundle\Entity\Catalog;

/**
 * Contract
 *
 * @ORM\Table(name="administration.contract")
 * @ORM\Entity(repositoryClass="Administration\DefaultBundle\Entity\Repository\ContractRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Contract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="administration.contract_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var integer
     * @ORM\Column(name="usucreated_id", type="integer", nullable=true)
     */
    private $usucreatedId;

    /**
     * @var integer
     * @ORM\Column(name="usuupdated_id", type="integer", nullable=true)
     */
    private $usuupdatedId;

    /**
     * @var integer
     * @ORM\Column(name="ctstatus_id", type="integer", nullable=true)
     */
    private $ctstatusId;

    /**
     * @ORM\ManyToOne(targetEntity="Aplication\DefaultBundle\Entity\Catalog")
     * @ORM\JoinColumn(name="ctstatus_id", referencedColumnName="id", nullable=false)
     * */
    private $ctstatus;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get updatedAt
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set usucreatedId
     * @param integer $usucreatedId
     */
    public function setUsucreatedId($usucreatedId)
    {
        $this->usucreatedId = $usucreatedId;
    }

    /**
     * Get usucreatedId
     * @return integer
     */
    public function getUsucreatedId()
    {
        return $this->usucreatedId;
    }

    /**
     * Set usuupdatedId
     * @param integer $usuupdatedId
     */
    public function setUsuupdatedId($usuupdatedId)
    {
        $this->usuupdatedId = $usuupdatedId;
    }

    /**
     * Get usuupdatedId
     * @return integer
     */
    public function getUsuupdatedId()
    {
        return $this->usuupdatedId;
    }

    /**
     * Set ctstatusId
     * @param integer $ctstatusId
     */
    public function setCtstatusId($ctstatusId)
    {
        $this->ctstatusId = $ctstatusId;
    }

    /**
     * Get ctstatusId
     * @return integer
     */
    public function getCtstatusId()
    {
        return $this->ctstatusId;
    }

    /**
     * Set ctstatus
     * @param Catalog
     */
    public function setCtstatus(Catalog $ctstatus)
    {
        $this->ctstatus = $ctstatus;
    }

    /**
     * Get ctstatus
     * @return Catalog
     */
    public function getCtstatus()
    {
        return $this->ctstatus;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
