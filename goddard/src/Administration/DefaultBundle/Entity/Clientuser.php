<?php

namespace Administration\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clientuser
 *
 * @ORM\Table(name="administration.clientuser", indexes={@ORM\Index(name="IDX_E75C5C17D0ED7024", columns={"ctstatus_id"}), @ORM\Index(name="IDX_E75C5C17A76ED395", columns={"user_id"}), @ORM\Index(name="IDX_E75C5C1719EB6921", columns={"client_id"})})
 * @ORM\Entity
 */
class Clientuser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="clientuser_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="usucrud_id", type="integer", nullable=true)
     */
    private $usucrudId;

    /**
     * @var \Catalog
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\Catalog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ctstatus_id", referencedColumnName="id")
     * })
     */
    private $ctstatus;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="\Administration\DefaultBundle\Entity\Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

            /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

         /**
     * @var integer
     *
     * @ORM\Column(name="client_id", type="integer", nullable=true)
     */
    private $clientId;
    
            /**
     * @var integer
     *
     * @ORM\Column(name="ctstatus_id", type="integer", nullable=true)
     */
    private $ctstatusId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Clientuser
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Clientuser
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set usucrudId
     *
     * @param integer $usucrudId
     * @return Clientuser
     */
    public function setUsucrudId($usucrudId)
    {
        $this->usucrudId = $usucrudId;

        return $this;
    }

    /**
     * Get usucrudId
     *
     * @return integer 
     */
    public function getUsucrudId()
    {
        return $this->usucrudId;
    }

    /**
     * Set ctstatus
     *
     * @param \Aplication\DefaultBundle\Entity\Catalog $ctstatus
     * @return Clientuser
     */
    public function setCtstatus(\Aplication\DefaultBundle\Entity\Catalog $ctstatus = null)
    {
        $this->ctstatus = $ctstatus;

        return $this;
    }

    /**
     * Get ctstatus
     *
     * @return \Aplication\DefaultBundle\Entity\Catalog 
     */
    public function getCtstatus()
    {
        return $this->ctstatus;
    }

    /**
     * Set user
     *
     * @param \Aplication\DefaultBundle\Entity\User $user
     * @return Clientuser
     */
    public function setUser(\Aplication\DefaultBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Aplication\DefaultBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set client
     *
     * @param \Administration\DefaultBundle\Entity\Client $client
     * @return Clientuser
     */
    public function setClient(\Administration\DefaultBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Administration\DefaultBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

       /**
     * Set userId
     *
     * @param integer $userId
     * @return Clientuser
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }




    /**
     * Set clientId
     *
     * @param integer $clientId
     * @return Clientuser
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId
     *
     * @return integer 
     */
    public function getClientId()
    {
        return $this->clientId;
    }




    /**
     * Set ctstatusId
     *
     * @param integer $ctstatusId
     * @return Clientuser
     */
    public function setCtstatusId($ctstatusId)
    {
        $this->ctstatusId = $ctstatusId;

        return $this;
    }

    /**
     * Get ctstatusId
     *
     * @return integer 
     */
    public function getCtstatusId()
    {
        return $this->ctstatusId;
    }
}
