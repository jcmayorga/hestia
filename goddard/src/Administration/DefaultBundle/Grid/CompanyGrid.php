<?php
/*
* @autor Richard Veliz
*/

namespace Administration\DefaultBundle\Grid;

use Symfony\Component\DependencyInjection\Container;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Symfony\Component\HttpFoundation\Request;

/**
 * Clase CompanyGrid que contiene las propiedades y características de construcción del datatables para la la entidad Company
 */
class CompanyGrid
{

    private $container;
    private $gridConfig;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->gridConfig();
    }

    /**
     * Define las opciones y caracteristicas de construcción del datatables
     * @param array $options
     */
    private function gridConfig()
    {
        $repository = $this->container->get('doctrine.orm.entity_manager')->getRepository('AdministrationDefaultBundle:Company');
        $queryBuilder = $repository->createQueryBuilder('company')
            ->select('company,contract,client')
            ->join('company.contract', 'contract')
            ->join('company.client', 'client');

        $gridConfig = new GridConfig();
        $gridConfig
            ->setQueryBuilder($queryBuilder)
            ->setCountFieldName('company.id')
            ->addField('company.ruc', array('label' => 'RUC', 'filterable' => true, 'sortable' => true))
            ->addField('company.establishmentnumber', array('label' => 'Número', 'filterable' => true, 'sortable' => true))
            ->addField('company.businessname', array('label' => 'Nombre', 'filterable' => true, 'sortable' => true))
            ->addField('company.phone', array('label' => 'Telefono', 'filterable' => true, 'sortable' => true))
            ->addField('contract.name', array('label' => 'Contrato', 'filterable' => true, 'sortable' => true))
            ->addField('client.name', array('label' => 'Cliente', 'filterable' => true, 'sortable' => true))
        ;

        return $this->gridConfig = $gridConfig;
    }


    /**
     * Retorna la respuesta de datos en formato json de datatables
     * @return json
     */
    public function getResponse(Request $request)
    {
        $gridManager = $this->container->get('kitpages_data_grid.grid_manager');
        return $gridManager->getGrid($this->gridConfig, $request);
    }
}
