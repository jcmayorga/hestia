<?php
/*
* @autor Richard Veliz
*/

namespace Administration\DefaultBundle\Grid;

use Symfony\Component\DependencyInjection\Container;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Symfony\Component\HttpFoundation\Request;

/**
 * Clase ContractGrid que contiene las propiedades y características de construcción del datatables para la la entidad Contract
 */
class ContractGrid
{

    private $container;
    private $gridConfig;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->gridConfig();
    }

    /**
     * Define las opciones y caracteristicas de construcción del datatables
     * @param array $options
     */
    private function gridConfig()
    {
        $repository = $this->container->get('doctrine.orm.entity_manager')->getRepository('AdministrationDefaultBundle:Contract');
        $queryBuilder = $repository->createQueryBuilder('contract')
            ->select('contract,ctstatus')
            ->join('contract.ctstatus', 'ctstatus');

        $gridConfig = new GridConfig();
        $gridConfig
            ->setQueryBuilder($queryBuilder)
            ->setCountFieldName('contract.id')
            ->addField('contract.name', array('label' => 'Nombre', 'filterable' => true, 'sortable' => true))
            ->addField('contract.description', array('label' => 'Descripción', 'filterable' => true, 'sortable' => true))
            ->addField('ctstatus.name', array('label' => 'Estado', 'filterable' => true, 'sortable' => true))
        ;

        return $this->gridConfig = $gridConfig;
    }


    /**
     * Retorna la respuesta de datos en formato json de datatables
     * @return json
     */
    public function getResponse(Request $request)
    {
        $gridManager = $this->container->get('kitpages_data_grid.grid_manager');
        return $gridManager->getGrid($this->gridConfig, $request);
    }
}
