<?php

namespace Administration\DefaultBundle\Services;
use Aplication\DefaultBundle\Model\Constant;

class RosterService
{

	private $container;

	/**
     * Constructor.
     * @param Container $container
     */
    public function __construct($container)
    {
    	$this->container=$container;
    }

    public function validDataFileUpload($fileRequest){

    	$status=1;
    	$message="Información validada correctamente";
        
        $rows=$this->getRows($fileRequest);

        //number columns valid data
        $numberColumns=6;

        $errors=array();
        if(count($rows)>1){
            foreach ($rows as $key => $row) {
                if($key==0){
                    continue;
                }
                $result=$this->validRow($row,$key,$numberColumns);
                if(!$result[0]){
                    $errors=array_merge($errors,$result[1]);
                }
            }
            if(count($errors)>0){
                $status=0;
                $message="Se ha encontrado errores en los datos";
            }
        }else{
            $status=0;
            $message="No existen filas para importar";
        }


    	return array("status"=>$status,"message"=>$message,"errors"=>$errors);

    }

    private function validRow($row,$indexRow,$numberColumns){
        $status=1;
        $messages=array();

        for ($i=0; $i < $numberColumns; $i++) { 
            $data=@$row[$i];
            $result=$this->validAndFormatColumn($i,$data);
            if(!$result[0]){
                $status=0;
                $messages[]="Error en la fila ".($indexRow+1).": ".$result[1];
            }
        }
        
        return array($status,$messages);
    }

    private function validAndFormatColumn($index,$data){
        $status=false;
        $message="";
        switch ($index) {
            //identification
            case 0:
                if(!empty($data)){
                    if(!$this->existEmployeByIdentification($data)){
                       $status=true; 
                    }else{
                        $message="El empleado con identificación $data , ya se encuentra registrado";
                    }
                }else{
                    $message="El valor de la columna de identificación es requerido";
                }

                break;

            //firstname    
            case 1:
                if(!empty($data)){
                    $status=true; 
                }else{
                    $message="El valor de la columna nombre es requerido";
                }
            break; 
            //lastname    
            case 2:
                if(!empty($data)){
                    $status=true; 
                }else{
                    $message="El valor de la columna apellido es requerido";
                }
            break; 
            
            //unit    
            case 3:
                if(!empty($data)){
                    $status=true; 
                }else{
                    $message="El valor de la columna unidad es requerido";
                }
            break; 

            //costcenter    
            case 4:
                if(!empty($data)){
                    $status=true; 
                }else{
                    $message="El valor de la columna de código centro de costos es requerido";
                }
            break;
            
            //namecenter    
            case 5:
                if(!empty($data)){
                    $status=true; 
                }else{
                    $message="El valor de la columna de nombre de centro de costos es requerido";
                }
            break;

            default:
                # code...
                break;
        }

        return array($status,$message);
    }


    private function existEmployeByIdentification($identification){
          $rosterManager=$this->container->get('administration.default.manager.roster');
          $roster=$rosterManager->findOneBy(array("identification"=>$identification));
          return (!empty($roster));
    }


    public function uploadData($fileRequest,$companyId){
           $rosterManager=$this->container->get('administration.default.manager.roster');
           $companyManager=$this->container->get('administration.default.manager.company');
           $catalogManager=$this->container->get('aplication.default.manager.catalog');
           
           $company=$companyManager->find($companyId);
           $ctstatus=$catalogManager->find(Constant::STATUS_ACTIVE_ROSTER);


           $session = $this->container->get("session");    
           $sessionuser = $session->get('userdata');
           $userid = $sessionuser['id'];

           $status=0;
           $message="";

           $rows=$this->getRows($fileRequest);

           try {
               foreach ($rows as $key=>$row) { 
                    if($key===0)continue;  

                    $roster=$this->setDataRow($row,$rosterManager);
                    $roster->setCompany($company);
                    $roster->setCtstatus($ctstatus);
                    $roster->setUsucrudId($userid);
                    $rosterManager->save($roster,false);
                } 
                $rosterManager->flushAllChanges();
                $status=1;
                $message="Datos importados satisfactoriamente"; 

           } catch (\Exception $e) {
               $message=$e->getMessage();
           }

           return array("status"=>$status,"message"=>$message,"errors"=>array());

    }

    private function getRows($fileRequest){
        $filePath=$fileRequest->getRealPath();
        $phpExcelObject = $this->container->get('phpexcel')->createPHPExcelObject($filePath);

        $ws = $phpExcelObject->getSheet(0);
        return $ws->toArray();
    }

    private function setDataRow($row,$rosterManager){
         
         $roster=$rosterManager->create();
         $roster->setIdentification($row[0]);
         $roster->setFirstname($row[1]);
         $roster->setLastname($row[2]);
         $roster->setUnit($row[3]);
         $roster->setCostcenter($row[4]);
         $roster->setNamecenter($row[5]);
         return $roster;
    }
}

?>