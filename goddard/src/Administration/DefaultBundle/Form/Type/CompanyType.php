<?php

namespace Administration\DefaultBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class CompanyType extends AbstractType
{

    private $container;

    public function __construct(Container $container, $options = array())
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ruc', 'text', array(
                'label' => 'RUC',
                'attr' => array(
                    'class' => 'form-control numeric',
                    'placeholder' => 'RUC',
                    'maxlength' => '13',
                    'minlength' => '13'
                )))
            ->add('establishmentnumber', 'text', array(
                'label' => 'Número de Establecimiento',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control numeric',
                    'placeholder' => 'Número de Establecimiento',
                    'maxlength' => '10'
                )))
            ->add('businessname', 'text', array(
                'label' => 'Nombre del Negocio',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Nombre del Negocio',
                    'maxlength' => '200'
                )))
            ->add('legalrepresentative', 'text', array(
                'label' => 'Representante Legal',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Representante Legal',
                    'maxlength' => '200'
                )))
            ->add('contract', EntityType::class, array(
                'label' => 'Contrato',
                'class' => 'AdministrationDefaultBundle:Contract',
                'choice_label' => 'name',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control',
                )))
            ->add('client', EntityType::class, array(
                'label' => 'Cliente',
                'class' => 'AdministrationDefaultBundle:Client',
                'choice_label' => 'name',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control',
                )))
            ->add('address', 'textarea', array(
                'label' => 'Dirección',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Dirección',
                )))
            ->add('phone', 'text', array(
                'label' => 'Teléfono',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control numeric',
                    'placeholder' => 'Teléfono',
                    'maxlength' => '10'
                )))
        ;
    }

    public function getName()
    {
        return 'company';
    }
}
