<?php

namespace Administration\DefaultBundle\Form\Type;

use Aplication\DefaultBundle\Model\Constant;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class DinnerType extends AbstractType
{

    private $container;

    public function __construct(Container $container, $options = array())
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $catalogManager = $this->container->get('aplication.default.manager.catalog');

        $builder
            ->add('name', 'text', array(
                'label' => 'Nombre',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Nombre del Comedor',
                    'maxlength' => '100'
                )))
            ->add('location', 'textarea', array(
                'label' => 'Ubicación',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Ubicación',
                    'maxlength' => '250'
                )))
            ->add('address', 'textarea', array(
                'label' => 'Dirección',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Dirección',
                    'maxlength' => '250'
                )))
            ->add('phone', 'text', array(
                'label' => 'Teléfono',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control numeric',
                    'placeholder' => 'Teléfono',
                    'maxlength' => '10'
                )))
            ->add('ctstatus', EntityType::class, array(
                'label' => 'Estado',
                'class' => 'AplicationDefaultBundle:Catalog',
                'choices' => $catalogManager->getCatalog(Constant::CTYPE_REGISTER_STATUS),
                'choice_label' => 'name',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control',
                )))
        ;
    }

    public function getName()
    {
        return 'company';
    }
}
