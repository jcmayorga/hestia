<?php
/*
* @autor Richard Veliz
*/

namespace Bussiness\DefaultBundle\Grid;

use Symfony\Component\DependencyInjection\Container;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Symfony\Component\HttpFoundation\Request;

/**
 * Clase ServiceGrid que contiene las propiedades y características de construcción del datatables para la la entidad Service
 */
class ServiceGrid
{

    private $container;
    private $gridConfig;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->gridConfig();
    }

    /**
     * Define las opciones y caracteristicas de construcción del datatables
     * @param array $options
     * @return GridConfig
     */
    private function gridConfig()
    {
        $repository = $this->container->get('doctrine.orm.entity_manager')->getRepository('BussinessDefaultBundle:Service');
        $queryBuilder = $repository->createQueryBuilder('service')
            ->select('service,ctstatus,company')
            ->join('service.ctstatus', 'ctstatus')
            ->join('service.company', 'company')
        ;

        $gridConfig = new GridConfig();
        $gridConfig
            ->setQueryBuilder($queryBuilder)
            ->setCountFieldName('service.id')
            ->addField('company.businessname', array('label' => 'Empresa', 'filterable' => true, 'sortable' => true))
            ->addField('service.description', array('label' => 'Nombre del Servicio', 'filterable' => true, 'sortable' => true))
            ->addField('service.hoursince', array('label' => 'Hora Desde', 'filterable' => true, 'sortable' => true))
            ->addField('service.houruntil', array('label' => 'Hora Hasta', 'filterable' => true, 'sortable' => true))
            ->addField('service.acronym', array('label' => 'Siglas', 'filterable' => true, 'sortable' => true))
            ->addField('ctstatus.name', array('label' => 'Estado', 'filterable' => true, 'sortable' => true))
        ;

        return $this->gridConfig = $gridConfig;
    }


    /**
     * Retorna la respuesta de datos en formato json de datatables
     * @return json
     */
    public function getResponse(Request $request)
    {
        $gridManager = $this->container->get('kitpages_data_grid.grid_manager');
        return $gridManager->getGrid($this->gridConfig, $request);
    }
}
