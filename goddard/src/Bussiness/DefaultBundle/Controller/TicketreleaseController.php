<?php

namespace Bussiness\DefaultBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Bussiness\DefaultBundle\Entity\Transaction;
use Bussiness\DefaultBundle\Controller\BaseController;

//use Symfony\Component\HttpFoundation\Session;

class TicketreleaseController extends BaseController {
    private $cttransactiontype='510'; //ticket generado

    /**
     * @Route("/",name="_bussiness_default_ticketrelease_index")
     * @Template()    
     * */
    public function indexAction() {
        
        $session = $this->get("session");
        $sessionuser = $session->get('userdata');
        $userid = $sessionuser['id'];
        $em = $this->getDoctrine()->getManager();
        $ctactive = $this->CTACTIVE;
        $reposcompany = $em->getRepository("AdministrationDefaultBundle:Company");
        $reposcompanydinner = $em->getRepository("AdministrationDefaultBundle:Companydinner");
        //$objcompany = $reposcompany->find(6);
        $reposdinneruser = $em->getRepository("AdministrationDefaultBundle:Dinneruser");
        $objdinneruser=$reposdinneruser->findBy(array("ctstatusId" => $ctactive,"userId"=>$userid));
        //$reposdinner = $em->getRepository("AdministrationDefaultBundle:Dinner");
        //$objdinner = $reposdinner->findBy(array("ctstatus" => $ctactive));
        return $this->render('BussinessDefaultBundle:Ticketrelease:index.html.twig', array(
                    "objdinneruser" => $objdinneruser));
    }

    /**
     * @Route("/ticket",name="_bussiness_default_ticketrelease_ticket")
     * @Template()    
     * */
    public function ticketAction(Request $request) {

        $requestdinner = $request->get("dinner");

        if (!$requestdinner) {
            die("Seleccione un comedor");
        } else {
            return $this->render('BussinessDefaultBundle:Ticketrelease:ticket.html.twig'
                            , array("dinner" => $requestdinner));
        }
    }

    /**
     * @Route("/save",name="_bussiness_default_ticketrelease_save")                 
     * */
    public function saveAction(Request $request) {
        $status = 0;
        $message = "Registro no encontrado";
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $connection->beginTransaction();
        $reprintcode = trim($request->get('reprintcode'));
        try {
            $reposroster = $em->getRepository("AdministrationDefaultBundle:Roster");

            $identification = trim($request->get("identification"));


            $objroster = $reposroster->findOneBy(array("barcode" => $identification));
            if (!$objroster) {
                $objroster = $reposroster->findOneBy(array("identification" => $identification));
            }
            if (!$objroster) {
                $objroster = $reposroster->findOneBy(array("employeecode" => $identification));
            }

            if (!$objroster) {
                return new JsonResponse(array("status" => $status, "message" => $message));
            } else {
                $objcatalogo = $objroster->getCtstatus();
                $ctnominactstatus = $objcatalogo->getId();
                if ($ctnominactstatus != $this->CTNOMINAACTIVE) {
                    return new JsonResponse(array("status" => 0, "message" => "No se puede emitir ticket porque el empleado se encuentra: " . $objcatalogo->getName()));
                }


                $status = 1;
                $fullname = $objroster->getFirstname();
                // validate between time hour 
                $this->validateTimeHour($request, $objroster);
                // validate if exist companydinner
                $return = $this->validateCompanyDinner($request, $objroster);
                if ($return["status"] == 0) {
                    return new JsonResponse(array("status" => $return["status"], "message" => $return["message"]));
                } else {
                    $objcompanydinner = $return["objcompanydinner"];
                    $objservice = $this->validateTimeHour($request, $objroster);
                    if (!$objservice) {
                        return new JsonResponse(array("status" => 0, "message" => "No existe servicio parametrizado para esta hora"));
                    }
                }
            }
            
            $objtransaction=  $this->validateExistTransactionToday($request, $objroster,$objservice);
//            echo $objservice->getId()."\n";
//            echo $objroster->getId();
      
            if($objtransaction){
                if(!empty($reprintcode)){
                    $this->cttransactiontype=511;
                    $objuser=  $this->validateUserReprint($request);
                    if(!$objuser){
                        return new JsonResponse(array("status" => 0, "message" => "Código de autorización inválido"));
                    }
                }else{
                     return new JsonResponse(array("status" => 0, "message" => "Necesita una autorización para reimprimir el ticket","reprint"=>1));
                }
            }

            $objtransaction = $this->saveTransaction($request, $objroster, $objcompanydinner, $objservice);
            $connection->commit();
        } catch (Exception $exc) {
            $connection->rollback();
            return new JsonResponse(array("status" => 0, "message" => "No fue posible guardar el registro " . $exc->getTraceAsString()));
        }



        return new JsonResponse(array("status" => $status, "message" => $fullname, "transaction_id" => $objtransaction->getId()));
    }
    
    private function validateExistTransactionToday($request,$objroster,$objservice) {
       
        $em = $this->getDoctrine()->getManager();
        $repostransaction = $em->getRepository("BussinessDefaultBundle:Transaction");
        $timeregistry = new \DateTime("now");
        $objtransaction=$repostransaction->findOneBy(array("dateregistry"=>$timeregistry,"roster"=>$objroster,"service"=>$objservice));

        return $objtransaction;
      
    }

    private function validateUserReprint($request) {
        
        $reprintcode = $request->get("reprintcode");
        $em = $this->getDoctrine()->getManager();
        $reposuser = $em->getRepository("AplicationDefaultBundle:User");
        $objuser = $reposuser->findOneBy(array("identification" => $reprintcode, "ctstatususer" => $this->CTUSERACTIVE));
        return $objuser;

        
//        $repoclientuser = $em->getRepository("AdministrationDefaultBundle:Clientuser");
//        $arrayclientuser = $repoclientuser->findOneBy(array("userId" => $objuser->getId(), "ctstatusId" => $this->CTACTIVE));
    }

    private function saveTransaction($request, $objroster, $objcompanydinner, $objservice) {

        $requestuser = $request->get("user");
        $session = $this->get("session");
        $sessionuser = $session->get('userdata');
        $sessionclient = $session->get('clientdata');
        $clientid = $sessionclient['id'];
        $userid = $sessionuser['id'];

        $em = $this->getDoctrine()->getManager();
        $reposclient = $em->getRepository("AdministrationDefaultBundle:Client");
        $reposuser = $em->getRepository("AplicationDefaultBundle:User");
        $reposcatalogo = $em->getRepository("AplicationDefaultBundle:Catalog");
        $repostransaction = $em->getRepository("BussinessDefaultBundle:Transaction");

        $objclient = $reposclient->find($clientid);
        $objuser = $reposuser->find($userid);
        $cttransactiontype=  $this->cttransactiontype;
        $objcttransaction = $reposcatalogo->find($cttransactiontype);
        
        $objctstatus = $reposcatalogo->find($this->CTNOVALIDATED);
        $serviceid = $objservice->getId();
        $objtransaction = new Transaction();

        $objtransaction->setClient($objclient);
        $objtransaction->setUser($objuser);
        $objtransaction->setService($objservice);
        $objtransaction->setCompanydinner($objcompanydinner);
        $objtransaction->setCttransaction($objcttransaction);
        $objtransaction->setCtstatus($objctstatus);
        $objtransaction->setRoster($objroster);
        $dateregistry = new \DateTime("now");

        $objtransaction->setDateregistry($dateregistry);

        $timeregistry = new \DateTime("now");
        $objtransaction->setTimeregistry($timeregistry);
        $em->persist($objtransaction);
        $em->flush();

        //actualiza barcode
        $objtransaction = $repostransaction->find($objtransaction->getId());
        $barcode = $objroster->getId() . $objtransaction->getId() . $requestuser . $serviceid;
        $objtransaction->setBarcode($barcode);
        $em->persist($objtransaction);
        $em->flush();


        return $objtransaction;
    }

    private function validateCompanyDinner($request, $objroster) {
        $requestdinner = $request->get("dinner");
        $em = $this->getDoctrine()->getManager();
        $reposcatalogo = $em->getRepository("AplicationDefaultBundle:Catalog");
        $reposdinner = $em->getRepository("AdministrationDefaultBundle:Dinner");
        $reposcompanydinner = $em->getRepository("AdministrationDefaultBundle:Companydinner");
        $objdinner = $reposdinner->find($requestdinner);
        $objctstatusactivo = $reposcatalogo->find($this->CTACTIVE);
        //get the company of the roster table
        $objcompany = $objroster->getCompany();

        if (!$objcompany) {
            return array("status" => 0, "message" => "El empleado no tiene companía asignada)");
        }
        $objcompanydinner = $reposcompanydinner->findOneBy(array("company" => $objcompany
            , "dinner" => $objdinner, "ctstatus" => $objctstatusactivo));


        if (!$objcompanydinner) {
            return array("status" => 0, "message" => "No existe un comedor parametrizado en la companía (companydinner)");
        } else {
            return array("status" => 1, "message" => "ok", "objcompanydinner" => $objcompanydinner);
        }
    }

    function validateTimeHour($request, $objroster) {


        $em = $this->getDoctrine()->getManager();

        $requestservice = $request->get("service");
        $reposservice = $em->getRepository("BussinessDefaultBundle:Service");
        $company_id = 1;
        $time = new \DateTime();
        $hour = $time->format('H:i');
        $objcompany = $objroster->getCompany();
        $company_id = $objcompany->getId();
//        echo $company_id;
//        die();
        $objservice = $reposservice->getServiceHourSincetoUntil($company_id, $hour, $hour);
        return $objservice;
    }

}
