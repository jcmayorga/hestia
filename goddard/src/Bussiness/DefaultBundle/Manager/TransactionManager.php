<?php
/**
 * @author Richard Veliz
 */

namespace Bussiness\DefaultBundle\Manager;

use Aplication\DefaultBundle\Model\MainManager;

class TransactionManager extends MainManager
{

    /**
     * Elimina un registro basado en el modelo Transaction
     * @param integer $id Id del registro Transaction
     * @param boolean $flush Indica si se realiza el flush en la persistencia
     * @return boolean true si se realizó la operación con éxito
     */
    public function deleteById($id, $flush = true)
    {
        $transaction = $this->find($id);
        if (!$transaction) {
            throw new NotFoundHttpException("No se encontró un registro con id $id");
        }

        $this->delete($transaction, $flush);

        return true;
    }

    public function existCompanydinnerId($companydinner_id)
    {
        $count = $this->getRepository()->getCountCompanydinnerId($companydinner_id);

        return ($count > 0) ? true : false;
    }
}
