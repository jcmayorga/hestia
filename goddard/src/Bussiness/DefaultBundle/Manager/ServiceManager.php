<?php
/**
 * @author Richard Veliz
 */

namespace Bussiness\DefaultBundle\Manager;

use Aplication\DefaultBundle\Model\MainManager;
use Bussiness\DefaultBundle\Form\Type\ServiceType;

class ServiceManager extends MainManager
{
    /**
     * Crea el objeto formulario para el modelo Contract
     * @param integer $id Id del registro Contract
     * @return formType
     */
    public function createForm($id = 0, $options = array())
    {
        $service = $this->create();
        if ($id) {
            $service = $this->find($id);
            if (empty($service)) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }
        }
        return $this->getContainer()->get('form.factory')->create(ServiceType::class, $service, $options);
    }

    /**
     * Elimina un registro basado en el modelo Service
     * @param integer $id Id del registro Service
     * @param boolean $flush Indica si se realiza el flush en la persistencia
     * @return boolean true si se realizó la operación con éxito
     */
    public function deleteById($id, $flush = true)
    {
        $transaction = $this->find($id);
        if (!$transaction) {
            throw new NotFoundHttpException("No se encontró un registro con id $id");
        }

        $this->delete($transaction, $flush);

        return true;
    }
}
