<?php

namespace Bussiness\DefaultBundle\Form\Type;

use Aplication\DefaultBundle\Model\Constant;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class ServiceType extends AbstractType
{

    private $container;

    public function __construct(Container $container, $options = array())
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $catalogManager = $this->container->get('aplication.default.manager.catalog');

        $builder
            ->add('description', 'text', array(
                'label' => 'Nombre del Servicio',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Nombre del Servicio',
                    'maxlength' => '100'
                )))
            ->add('company', EntityType::class, array(
                'label' => 'Empresa',
                'class' => 'AdministrationDefaultBundle:Company',
//                'choices' => $catalogManager->getCatalog(Constant::CTYPE_REGISTER_STATUS),
                'choice_label' => 'businessname',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control',
                )))
            ->add('hoursince', 'text', array(
                'label' => 'Hora desde',
                'attr' => array(
                    'class' => 'form-control time-picker'
                )))
            ->add('houruntil', 'text', array(
                'label' => 'Hora hasta',
                'attr' => array(
                    'class' => 'form-control time-picker'
                )))
            ->add('acronym', 'text', array(
                'label' => 'Siglas',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Siglas del Servicio',
                    'maxlength' => '10'
                )))
            ->add('ctstatus', EntityType::class, array(
                'label' => 'Estado',
                'class' => 'AplicationDefaultBundle:Catalog',
                'choices' => $catalogManager->getCatalog(Constant::CTYPE_REGISTER_STATUS),
                'choice_label' => 'name',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control',
                )))
        ;
    }

    public function getName()
    {
        return 'service';
    }
}
