<?php

namespace Bussiness\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction
 *
 * @ORM\Table(name="business.transaction", indexes={@ORM\Index(name="IDX_8C1C112219EB6921", columns={"client_id"}), @ORM\Index(name="IDX_8C1C1122FEE6FA01", columns={"companydinner_id"}), @ORM\Index(name="IDX_8C1C1122A76ED395", columns={"user_id"}), @ORM\Index(name="IDX_8C1C1122ED5CA9E6", columns={"service_id"}), @ORM\Index(name="IDX_8C1C112275404483", columns={"roster_id"}), @ORM\Index(name="IDX_8C1C11228E697F27", columns={"cttransaction_id"}), @ORM\Index(name="IDX_8C1C1122D0ED7024", columns={"ctstatus_id"})})
 * @ORM\Entity(repositoryClass="Bussiness\DefaultBundle\Entity\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="business.transaction_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="barcode", type="string", nullable=true)
     */
    private $barcode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateregistry", type="date", nullable=true)
     */
    private $dateregistry;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeregistry", type="time", nullable=true)
     */
    private $timeregistry;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="usucrud_id", type="integer", nullable=true)
     */
    private $usucrudId;

    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="\Administration\DefaultBundle\Entity\Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var integer
     *
     * @ORM\Column(name="companydinner_id", type="integer", nullable=true)
     */
    private $companydinner_id;

    /**
     * @var \Companydinner
     *
     * @ORM\ManyToOne(targetEntity="\Administration\DefaultBundle\Entity\Companydinner")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="companydinner_id", referencedColumnName="id")
     * })
     */
    private $companydinner;

    /**
     * @var \User"
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \Service
     *
     * @ORM\ManyToOne(targetEntity="Service")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     * })
     */
    private $service;

    /**
     * @var \Roster
     *
     * @ORM\ManyToOne(targetEntity="\Administration\DefaultBundle\Entity\Roster")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roster_id", referencedColumnName="id")
     * })
     */
    private $roster;

    /**
     * @var \Catalog
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\Catalog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cttransaction_id", referencedColumnName="id")
     * })
     */
    private $cttransaction;

    /**
     * @var \Catalog
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\Catalog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ctstatus_id", referencedColumnName="id")
     * })
     */
    private $ctstatus;
    /**
     * @var integer
     *
     * @ORM\Column(name="ctstatus_id", type="integer", nullable=true)
     */
    private $ctstatusId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set barcode
     *
     * @param string $barcode
     * @return Transaction
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * Get barcode
     *
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * Set dateregistry
     *
     * @param \DateTime $dateregistry
     * @return Transaction
     */
    public function setDateregistry($dateregistry)
    {
        $this->dateregistry = $dateregistry;

        return $this;
    }

    /**
     * Get dateregistry
     *
     * @return \DateTime
     */
    public function getDateregistry()
    {
        return $this->dateregistry;
    }

    /**
     * Set timeregistry
     *
     * @param \DateTime $timeregistry
     * @return Transaction
     */
    public function setTimeregistry($timeregistry)
    {
        $this->timeregistry = $timeregistry;

        return $this;
    }

    /**
     * Get timeregistry
     *
     * @return \DateTime
     */
    public function getTimeregistry()
    {
        return $this->timeregistry;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Transaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Transaction
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set usucrudId
     *
     * @param integer $usucrudId
     * @return Transaction
     */
    public function setUsucrudId($usucrudId)
    {
        $this->usucrudId = $usucrudId;

        return $this;
    }

    /**
     * Get usucrudId
     *
     * @return integer
     */
    public function getUsucrudId()
    {
        return $this->usucrudId;
    }

    /**
     * Set client
     *
     * @param \Administration\DefaultBundle\Entity\Client $client
     * @return Transaction
     */
    public function setClient(\Administration\DefaultBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Administration\DefaultBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }


    /**
     * Set user
     *
     * @param \Aplication\DefaultBundle\Entity\User $user
     * @return Transaction
     */
    public function setUser(\Aplication\DefaultBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Aplication\DefaultBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set companydinner
     *
     * @param \Administration\DefaultBundle\Entity\Companydinner $companydinner
     * @return Transaction
     */
    public function setCompanydinner(\Administration\DefaultBundle\Entity\Companydinner $companydinner = null)
    {
        $this->companydinner = $companydinner;

        return $this;
    }

    /**
     * Get companydinner
     *
     * @return \Administration\DefaultBundle\Entity\Companydinner
     */
    public function getCompanydinner()
    {
        return $this->companydinner;
    }

    /**
     * Set service
     *
     * @param \Bussiness\DefaultBundle\Entity\Service $service
     * @return Transaction
     */
    public function setService(\Bussiness\DefaultBundle\Entity\Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \Bussiness\DefaultBundle\Entity\Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set roster
     *
     * @param \Administration\DefaultBundle\Entity\Roster $roster
     * @return Transaction
     */
    public function setRoster(\Administration\DefaultBundle\Entity\Roster $roster = null)
    {
        $this->roster = $roster;

        return $this;
    }

    /**
     * Get roster
     *
     * @return \Administration\DefaultBundle\Entity\Roster
     */
    public function getRoster()
    {
        return $this->roster;
    }

    /**
     * Set cttransaction
     *
     * @param \Aplication\DefaultBundle\Entity\Catalog $cttransaction
     * @return Transaction
     */
    public function setCttransaction(\Aplication\DefaultBundle\Entity\Catalog $cttransaction = null)
    {
        $this->cttransaction = $cttransaction;

        return $this;
    }

    /**
     * Get cttransaction
     *
     * @return \Aplication\DefaultBundle\Entity\Catalog
     */
    public function getCttransaction()
    {
        return $this->cttransaction;
    }

    /**
     * Set ctstatus
     *
     * @param \Aplication\DefaultBundle\Entity\Catalog $ctstatus
     * @return Transaction
     */
    public function setCtstatus(\Aplication\DefaultBundle\Entity\Catalog $ctstatus = null)
    {
        $this->ctstatus = $ctstatus;

        return $this;
    }

    /**
     * Get ctstatus
     *
     * @return \Aplication\DefaultBundle\Entity\Catalog
     */
    public function getCtstatus()
    {
        return $this->ctstatus;
    }

    /**
     * Set ctstatusId
     *
     * @param integer $ctstatusId
     * @return Transaction
     */
    public function setCtstatusId($ctstatusId)
    {
        $this->ctstatusId = $ctstatusId;

        return $this;
    }

    /**
     * Get ctstatusId
     *
     * @return integer
     */
    public function getCtstatusId()
    {
        return $this->ctstatusId;
    }
}
