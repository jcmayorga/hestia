<?php

namespace Bussiness\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 *
 * @ORM\Table(name="business.service", indexes={@ORM\Index(name="IDX_DC54B091979B1AD6", columns={"company_id"}), @ORM\Index(name="IDX_DC54B091D0ED7024", columns={"ctstatus_id"})})
 * @ORM\Entity(repositoryClass="Bussiness\DefaultBundle\Entity\Repository\ServiceRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Service
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="business.service_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="acronym", type="string", nullable=true)
     */
    private $acronym;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="usucrud_id", type="integer", nullable=true)
     */
    private $usucrudId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hoursince", type="string", nullable=true)
     */
    private $hoursince;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="houruntil", type="string", nullable=true)
     */
    private $houruntil;
    
    /**
     * @var integer 
     * @ORM\Column(name="company_id", type="integer", nullable=true)
     * */
    private $companyId;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="\Administration\DefaultBundle\Entity\Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;
    
        /**
     * @var integer 
     * @ORM\Column(name="ctstatus_id", type="integer", nullable=true)
     * */
    private $ctstatusId;

    /**
     * @var \Catalog
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\Catalog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ctstatus_id", referencedColumnName="id")
     * })
     */
    private $ctstatus;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set acronym
     *
     * @param string $acronym
     */
    public function setAcronym($acronym)
    {
        $this->acronym = $acronym;
    }

    /**
     * Get acronym
     *
     * @return string
     */
    public function getAcronym()
    {
        return $this->acronym;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set usucrudId
     *
     * @param integer $usucrudId
     */
    public function setUsucrudId($usucrudId)
    {
        $this->usucrudId = $usucrudId;
    }

    /**
     * Get usucrudId
     *
     * @return integer 
     */
    public function getUsucrudId()
    {
        return $this->usucrudId;
    }

    /**
     * Set hoursince
     *
     * @param string $hoursince
     */
    public function setHoursince($hoursince)
    {
        $this->hoursince = $hoursince;
    }

    /**
     * Get hoursince
     *
     * @return string
     */
    public function getHoursince()
    {
        return $this->hoursince;
    }

    /**
     * Set houruntil
     *
     * @param \DateTime $houruntil
     */
    public function setHouruntil($houruntil)
    {
        $this->houruntil = $houruntil;
    }

    /**
     * Get houruntil
     *
     * @return \DateTime 
     */
    public function getHouruntil()
    {
        return $this->houruntil;
    }

    /**
     * Set company
     *
     * @param \Administration\DefaultBundle\Entity\Company $company
     */
    public function setCompany(\Administration\DefaultBundle\Entity\Company $company = null)
    {
        $this->company = $company;
    }

    /**
     * Get company
     *
     * @return \Administration\DefaultBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set ctstatus
     *
     * @param \Aplication\DefaultBundle\Entity\Catalog $ctstatus
     */
    public function setCtstatus(\Aplication\DefaultBundle\Entity\Catalog $ctstatus = null)
    {
        $this->ctstatus = $ctstatus;
    }

    /**
     * Get ctstatus
     *
     * @return \Aplication\DefaultBundle\Entity\Catalog 
     */
    public function getCtstatus()
    {
        return $this->ctstatus;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
