<?php 
/*

* @autor Luis Malquin
*/

namespace Aplication\DefaultBundle\Manager;

use Aplication\DefaultBundle\Model\MainManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Aplication\DefaultBundle\Model\Constant;


/**
* Clase de manipulación del repositorio profile
*/
class ProfileroleManager extends MainManager{

	
	/**
        * Inactiva o elimina un registro basado en el modelo profile            
        * @param integer $id Id del registro profile 
        * @param boolean $flush Indica si se realiza el flush en la persistencia   
        * @return boolean true si se realizó la operación con éxito  
        */
	public function deleteById($id,$flush=true) {         
            $profile=  $this->find($id);
            if (!$profile) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }         
            
            $this->delete($profile); 
                
            return true;
         }


    public function getActiveRole($profile_id,$role_id){
            return $this->findOneBy(array('profileId'=>$profile_id,'roleId'=>$role_id,'ctstatusId'=>Constant::STATUS_ACTIVE_RECORD));
     } 

}
?>