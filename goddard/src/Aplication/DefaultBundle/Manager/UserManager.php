<?php 
/*

* @autor Luis Malquin
*/

namespace Aplication\DefaultBundle\Manager;

use Aplication\DefaultBundle\Model\MainManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Aplication\DefaultBundle\Form\Type\UserType;
use Aplication\DefaultBundle\Model\Constant;

/**
* Clase de manipulación del repositorio User
*/
class UserManager extends MainManager{

	/**
        * Crea el objeto formulario para el modelo user            
        * @param integer $id Id del registro user     
        * @return formType  
        */
	public function createForm($id=0,$options = array()){
                $user= $this->create();
                if($id){
                    $user=  $this->find($id);
                    if (empty($user)) {                                         
                            throw new NotFoundHttpException("No se encontró un registro con id $id");
                    }       
                }
                return $this->getContainer()->get('form.factory')->create(userType::class, $user,$options);
        }

	/**
        * Inactiva o elimina un registro basado en el modelo user            
        * @param integer $id Id del registro user 
        * @param boolean $flush Indica si se realiza el flush en la persistencia   
        * @return boolean true si se realizó la operación con éxito  
        */
	public function deleteById($id,$flush=true) {         
            $user=  $this->find($id);
            if (!$user) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }         
            
            $this->delete($user); 
                
            return true;
         }
    
    public function saveUserAndProfile($user,$profile_id){
         $profileuserManager=$this->getContainer()->get('aplication.default.profileuser');

         $this->beginTransaction();
         try {
             $this->save($user);
             $profileuserManager->saveUniqueProfile($user,$profile_id);
             $this->commit();
         } catch (\Exception $exc) {
            $this->rollback();
             throw new \Exception("Error Processing", 1);             
         }
         return $user;
    }     
}
?>