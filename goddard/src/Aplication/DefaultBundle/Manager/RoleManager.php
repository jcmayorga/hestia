<?php 
/*

* @autor Luis Malquin
*/

namespace Aplication\DefaultBundle\Manager;

use Aplication\DefaultBundle\Model\MainManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Aplication\DefaultBundle\Form\Type\RoleType;
use Aplication\DefaultBundle\Model\Constant;

/**
* Clase de manipulación del repositorio Role
*/
class RoleManager extends MainManager{

	/**
        * Crea el objeto formulario para el modelo Role            
        * @param integer $id Id del registro Role     
        * @return formType  
        */
	public function createForm($id=0,$options = array()){
                $role= $this->create();
                if($id){
                    $role=  $this->find($id);
                    if (empty($role)) {                                         
                            throw new NotFoundHttpException("No se encontró un registro con id $id");
                    }       
                }
                return $this->getContainer()->get('form.factory')->create(RoleType::class, $role,$options);
        }

	/**
        * Inactiva o elimina un registro basado en el modelo Role            
        * @param integer $id Id del registro Role 
        * @param boolean $flush Indica si se realiza el flush en la persistencia   
        * @return boolean true si se realizó la operación con éxito  
        */
	public function deleteById($id,$flush=true) {         
            $role=  $this->find($id);
            if (!$role) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }         
            
            $this->delete($role); 
                
            return true;
         }


      public function getActiveRoles(){
        $roles=$this->findBy(array('ctstatusId'=>Constant::STATUS_ACTIVE_RECORD),array('name'=>'asc'));
        return $roles;
      }       

}
?>