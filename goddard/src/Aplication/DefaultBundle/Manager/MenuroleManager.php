<?php 
/*
* @autor Luis Malquin
*/

namespace Aplication\DefaultBundle\Manager;

use Aplication\DefaultBundle\Model\MainManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Aplication\DefaultBundle\Model\Constant;


/**
* Clase de manipulación del repositorio Menurole
*/
class MenuroleManager extends MainManager{

	
    
	/**
        * Inactiva o elimina un registro basado en el modelo Menurole            
        * @param integer $id Id del registro Menurole 
        * @param boolean $flush Indica si se realiza el flush en la persistencia   
        * @return boolean true si se realizó la operación con éxito  
        */
	public function deleteById($id,$flush=true) {         
            $menurole=  $this->find($id);
            if (!$menurole) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }         
            $menurole->setActivo(0); 
            $this->save($menurole,$flush);                
            
            //$this->delete($menurole); 
                
            return true;
         }

     public function getMenuRoleActive($role_id,$menu_id){
            return $this->findOneBy(array('menuId'=>$menu_id,'roleId'=>$role_id,'ctstatusId'=>Constant::STATUS_ACTIVE_RECORD));
     }    

}
?>