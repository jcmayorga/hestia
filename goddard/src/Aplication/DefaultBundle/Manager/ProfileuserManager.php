<?php 
/*

* @autor Luis Malquin
*/

namespace Aplication\DefaultBundle\Manager;

use Aplication\DefaultBundle\Model\MainManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Aplication\DefaultBundle\Model\Constant;


/**
* Clase de manipulación del repositorio profileuser
*/
class ProfileuserManager extends MainManager{

	
	/**
        * Inactiva o elimina un registro basado en el modelo profile            
        * @param integer $id Id del registro profile 
        * @param boolean $flush Indica si se realiza el flush en la persistencia   
        * @return boolean true si se realizó la operación con éxito  
        */
	public function deleteById($id,$flush=true) {         
            $profile=  $this->find($id);
            if (!$profile) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }         
            
            $this->delete($profile); 
                
            return true;
         }


    /*public function getActiveProfile($user_id,$profile_id){
            return $this->findOneBy(array('profileId'=>$profile_id,'userId'=>$user_id,'ctstatusId'=>Constant::STATUS_ACTIVE_RECORD));
     }*/

     public function getProfileUser($user_id){
            return $this->findOneBy(array('userId'=>$user_id,'ctstatusId'=>Constant::STATUS_ACTIVE_RECORD));
     } 


    public function saveUniqueProfile($user,$profile_id){

          $profileManager=$this->getContainer()->get('aplication.default.manager.profile');          
          $catalogManager=$this->getContainer()->get('aplication.default.manager.catalog');
          $user_id=$user->getId();

          $profileuser=null;  
          $existProfile=$this->findOneBy(array('userId'=>$user_id),array('id'=>'desc'));                  
          if($existProfile){
             $profileuser=$existProfile;
          }else{
             $profileuser->create();
             $profileuser->setUser($user);    
          }
          $profileuser->setCtstatus($catalogManager->find(Constant::STATUS_ACTIVE_RECORD));
          $profileuser->setProfile($profileManager->find($profile_id));
          $this->save($profileuser);

    } 

}
?>