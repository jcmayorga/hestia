<?php
/**
 * @author Richard Veliz
 */

namespace Aplication\DefaultBundle\Manager;

use Aplication\DefaultBundle\Model\MainManager;

class CatalogManager extends MainManager
{

    /**
     * Obtiene la lista de catalogos identificados por un tipo de catálogo
     * @param integer $catalog_id identifica el tipo de catálogo
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "Aplication\DefaultBundle\Entity\Catalog"
     */
    public function getCatalog($catalog_id, $arrayResult = false)
    {
        return $this->getRepository()->getCatalog($catalog_id, $arrayResult);
    }
}
