<?php 

/*
* @autor Luis Malquin
*/

namespace Aplication\DefaultBundle\Manager;

use Aplication\DefaultBundle\Model\MainManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Aplication\DefaultBundle\Form\Type\MenuType;
use Aplication\DefaultBundle\Model\Constant;

/**
* Clase de manipulación del repositorio Menu
*/
class MenuManager extends MainManager{

	/**
        * Crea el objeto formulario para el modelo Menu            
        * @param integer $id Id del registro Menu     
        * @return formType  
        */
	public function createForm($id=0,$options = array()){
                $menu= $this->create();
                if($id){
                    $menu=  $this->find($id);
                    if (empty($menu)) {                                         
                            throw new NotFoundHttpException("No se encontró un registro con id $id");
                    }       
                }
                return $this->getContainer()->get('form.factory')->create(MenuType::class, $menu,$options); 
        }

	/**
        * Inactiva o elimina un registro basado en el modelo Menu            
        * @param integer $id Id del registro Menu 
        * @param boolean $flush Indica si se realiza el flush en la persistencia   
        * @return boolean true si se realizó la operación con éxito  
        */
	public function deleteById($id,$flush=true) {         
            $menu=  $this->find($id);
            if (!$menu) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }                     
            
            $this->delete($menu); 
                
            return true;
         }


      public function getActiveMenus(){
        $menus=$this->findBy(array('ctstatus_id'=>Constant::STATUS_ACTIVE_RECORD),array('organized'=>'asc'));
        return $menus;
      }   

}
?>