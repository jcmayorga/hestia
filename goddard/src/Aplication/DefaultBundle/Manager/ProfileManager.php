<?php 
/*

* @autor Luis Malquin
*/

namespace Aplication\DefaultBundle\Manager;

use Aplication\DefaultBundle\Model\MainManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Aplication\DefaultBundle\Form\Type\ProfileType;
use Aplication\DefaultBundle\Model\Constant;

/**
* Clase de manipulación del repositorio profile
*/
class ProfileManager extends MainManager{

	/**
        * Crea el objeto formulario para el modelo profile            
        * @param integer $id Id del registro profile     
        * @return formType  
        */
	public function createForm($id=0,$options = array()){
                $profile= $this->create();
                if($id){
                    $profile=  $this->find($id);
                    if (empty($profile)) {                                         
                            throw new NotFoundHttpException("No se encontró un registro con id $id");
                    }       
                }
                return $this->getContainer()->get('form.factory')->create(profileType::class, $profile,$options);
        }

	/**
        * Inactiva o elimina un registro basado en el modelo profile            
        * @param integer $id Id del registro profile 
        * @param boolean $flush Indica si se realiza el flush en la persistencia   
        * @return boolean true si se realizó la operación con éxito  
        */
	public function deleteById($id,$flush=true) {         
            $profile=  $this->find($id);
            if (!$profile) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }         
            
            $this->delete($profile); 
                
            return true;
         }


    public function getActiveProfiles(){
        return $this->findBy(array('ctstatusId'=>Constant::STATUS_ACTIVE_RECORD),array('name'=>'asc'));
    }     

}
?>