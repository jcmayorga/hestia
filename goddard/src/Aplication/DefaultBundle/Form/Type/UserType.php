<?php 

namespace Aplication\DefaultBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\Container;
use Aplication\DefaultBundle\Model\Constant;

class UserType extends AbstractType{
   private $container;

   public function __construct(Container $container){

        $this->container=$container;
   
    }

	public function buildForm(FormBuilderInterface $builder, array $options) {

		$catalogManager=$this->container->get('aplication.default.manager.catalog');
        $profileManager=$this->container->get('aplication.default.manager.profile');
        $profileuserManager=$this->container->get('aplication.default.manager.profileuser');

        $profile=null;
        $user=$options['data'];
        $userId=$user->getId();
        if($userId>0){
            $profileuser=$profileuserManager->getProfileUser($userId);
            $profile=$profileuser->getProfile();
        }

		$builder
		   ->add('name','text',array('label'=>'Nombre:'))
		   ->add('lastname','text',array('label'=>'Apellido:'))			   
		   ->add('cttypeidentification','entity', array(
                'label' => 'Tipo de Identificación:',
                'class' => 'AplicationDefaultBundle:Catalog',
                'choices' => $catalogManager->getCatalog(Constant::CTYPE_USER_IDENTIFICATION),
                'choice_label' => 'name',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'required input-sm',
                ),
                'placeholder' => '--Seleccione--'
            ))
		    ->add('identification','text',array('label'=>'Identificación:'))	
		    ->add('email','text',array('label'=>'Email:','attr' => array(
                    'class' => 'required email input-sm',
                ),'required'=>false))
            ->add('profile','entity', array(
                'label' => 'Perfil:',
                'class' => 'AplicationDefaultBundle:Profile',
                'choices' => $profileManager->getActiveProfiles(),
                'choice_label' => 'name',
                'empty_data' => null,
                'mapped'=>false,
                'attr' => array(
                    'class' => 'required input-sm',
                ),
                'placeholder' => '--Seleccione--',
                'data'=>$profile
            ))             
            ->add('password','password',array('label'=>'Clave:','attr' => array(
                    'class' => 'input-sm',                    
                    'value' => ''

                ),'required'=>false))	    	
            ->add('ctstatususer','entity', array(
                'label' => 'Estado:',
                'class' => 'AplicationDefaultBundle:Catalog',
                'choices' => $catalogManager->getCatalog(Constant::CTYPE_REGISTER_STATUS),
                'choice_label' => 'name',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'required input-sm',
                ),
                'placeholder' => '--Seleccione--',
            ))	   		   
		   ;
	}

        public function getName() {        
            return 'user';
        }
}
?>