<?php 

namespace Aplication\DefaultBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Aplication\DefaultBundle\Model\Constant;
use Symfony\Component\DependencyInjection\Container;


class RoleType extends AbstractType{

   private $container;

   public function __construct(Container $container){

        $this->container=$container;
   
   }

   public function buildForm(FormBuilderInterface $builder, array $options) {
		$catalogManager=$this->container->get('aplication.default.manager.catalog');
		$builder
		   ->add('name','text',array('label'=>'Nombre:',
          'attr' => array(
                    'class' => 'required input-sm ',
                )
         ))	
		    ->add('ctstatus','entity', array(
                'label' => 'Estado:',
                'class' => 'AplicationDefaultBundle:Catalog',
                'choices' => $catalogManager->getCatalog(Constant::CTYPE_REGISTER_STATUS),
                'choice_label' => 'name',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'required input-sm ',
                )
            )); 	   		   
	}

    public function getName() {        
        return 'role';
    }
}
?>