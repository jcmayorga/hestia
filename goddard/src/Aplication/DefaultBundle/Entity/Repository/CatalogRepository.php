<?php

namespace Aplication\DefaultBundle\Entity\Repository;

use Aplication\DefaultBundle\Model\Constant;
use Doctrine\ORM\EntityRepository;


/**
 * Descripcion de CatalogRepository
 * @author Richard Veliz
 */
class CatalogRepository extends EntityRepository
{

    /**
     * Obtiene el QueryBuilder de la lista de catalogos identificados por un tipo de catálogo
     * @param integer $catalog_id identifica el tipo de catálogo
     * @return QueryBuilder
     */
    public function getCatalogQueryBuilder($catalog_id)
    {
        return $this->createQueryBuilder('ct')
            ->Where('ct.status = :estatus_active_record')
            ->andWhere('ct.catalogId = :catalog_id')
            ->orderBy('ct.ordered, ct.name', 'ASC')
            ->setParameter('catalog_id', $catalog_id)
            ->setParameter('estatus_active_record', Constant::STATUS_ACTIVE_RECORD);
    }

    /**
     * Obtiene la lista de catalogos identificados por un tipo de catálogo
     * @param integer $catalog_id identifica el tipo de catálogo
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "Aplication\DefaultBundle\Entity\Catalog"
     */
    public function getCatalog($catalog_id, $arrayResult = false)
    {
        $queryBuilder = $this->getCatalogQueryBuilder($catalog_id);
        $query = $queryBuilder->getQuery();

        return ($arrayResult) ? $query->getArrayResult() : $query->getResult();

    }
}