<?php

namespace Aplication\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Menu
 *
 * @ORM\Table(name="application.menu", indexes={@ORM\Index(name="IDX_4E5B9B64CCD7E912", columns={"menu_id"}), @ORM\Index(name="IDX_4E5B9B64D0ED7024", columns={"ctstatus_id"}), @ORM\Index(name="IDX_4E5B9B64F3DD6548", columns={"cttype_id"})})
 * @ORM\Entity
 */

class Menu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="application.menu_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=200, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=200, nullable=true)
     */
    private $url;

    /**
     * @var integer
     *
     * @ORM\Column(name="organized", type="integer", nullable=true)
     */
    private $organized;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="usucrud_id", type="integer", nullable=true)
     */
    private $usucrudId;

    /**
     * @var \Menu
     *
     * @ORM\ManyToOne(targetEntity="Menu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     * })
     */
    private $menu;

     /**
     * @var integer
     *
     * @ORM\Column(name="ctstatus_id", type="integer", nullable=true)
     */
    private $ctstatus_id;


    /**
     * @var \Catalog
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\Catalog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ctstatus_id", referencedColumnName="id")
     * })
     */
    private $ctstatus;

    /**
     * @var \Catalog
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\Catalog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cttype_id", referencedColumnName="id")
     * })
     */
    private $cttype;
    
        /**
     * @var string
     *
     * @ORM\Column(name="target", type="string", length=15, nullable=true)
     */
    private $target;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Menu
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Menu
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Menu
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set organized
     *
     * @param integer $organized
     * @return Menu
     */
    public function setOrganized($organized)
    {
        $this->organized = $organized;

        return $this;
    }

    /**
     * Get organized
     *
     * @return integer 
     */
    public function getOrganized()
    {
        return $this->organized;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Menu
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Menu
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set usucrudId
     *
     * @param integer $usucrudId
     * @return Menu
     */
    public function setUsucrudId($usucrudId)
    {
        $this->usucrudId = $usucrudId;

        return $this;
    }

    /**
     * Get usucrudId
     *
     * @return integer 
     */
    public function getUsucrudId()
    {
        return $this->usucrudId;
    }

    /**
     * Set menu
     *
     * @param Menu $menu
     * @return Menu
     */
    public function setMenu(Menu $menu = null)
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu
     *
     * @return \AppBundle\Entity\Menu 
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Set ctstatus
     *
     * @param \Aplication\DefaultBundle\Entity\Catalog $ctstatus
     * @return Menu
     */
    public function setCtstatus(\Aplication\DefaultBundle\Entity\Catalog $ctstatus = null)
    {
        $this->ctstatus = $ctstatus;

        return $this;
    }

    /**
     * Get ctstatus
     *
     * @return \Aplication\DefaultBundle\Entity\Catalog 
     */
    public function getCtstatus()
    {
        return $this->ctstatus;
    }

    /**
     * Set cttype
     *
     * @param \Aplication\DefaultBundle\Entity\Catalog $cttype
     * @return Menu
     */
    public function setCttype(\Aplication\DefaultBundle\Entity\Catalog $cttype = null)
    {
        $this->cttype = $cttype;

        return $this;
    }

    /**
     * Get cttype
     *
     * @return \Aplication\DefaultBundle\Entity\Catalog
     */
    public function getCttype()
    {
        return $this->cttype;
    }
    
    
   /**
     * Set target
     *
     * @param string $target
     * @return Menu
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return string 
     */
    public function getTarget()
    {
        return $this->target;
    }
}
