<?php

namespace Aplication\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Aplication\DefaultBundle\Model\Constant;

/**
 * Profileuser
 *
 * @ORM\Table(name="application.profileuser", indexes={@ORM\Index(name="IDX_787E8675CCFA12B8", columns={"profile_id"}), @ORM\Index(name="IDX_787E8675A76ED395", columns={"user_id"}), @ORM\Index(name="IDX_787E8675D0ED7024", columns={"ctstatus_id"}), @ORM\Index(name="IDX_787E867519EB6921", columns={"client_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Profileuser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="application.profileuser_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="usucrud_id", type="integer", nullable=true)
     */
    private $usucrudId;

    /**
     * @var integer
     *
     * @ORM\Column(name="profile_id", type="integer", nullable=true)
     */
    private $profileId;

    /**
     * @var \Profile
     *
     * @ORM\ManyToOne(targetEntity="Profile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="profile_id", referencedColumnName="id")
     * })
     */
    private $profile;

     /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var integer
     *
     * @ORM\Column(name="ctstatus_id", type="integer", nullable=true)
     */
    private $ctstatusId;

    /**
     * @var \Catalog
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\Catalog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ctstatus_id", referencedColumnName="id")
     * })
     */
    private $ctstatus;

    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="\Administration\DefaultBundle\Entity\Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Profileuser
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Profileuser
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set usucrudId
     *
     * @param integer $usucrudId
     * @return Profileuser
     */
    public function setUsucrudId($usucrudId)
    {
        $this->usucrudId = $usucrudId;

        return $this;
    }

    /**
     * Get usucrudId
     *
     * @return integer 
     */
    public function getUsucrudId()
    {
        return $this->usucrudId;
    }

     /**
     * Set profileId
     *
     * @param integer $profileId
     * @return Profileuser
     */
    public function setProfileId($profileId)
    {
        $this->profileId = $profileId;

        return $this;
    }

    /**
     * Get profileId
     *
     * @return integer 
     */
    public function getProfileId()
    {
        return $this->profileId;
    }


    /**
     * Set profile
     *
     * @param Profile $profile
     * @return Profileuser
     */
    public function setProfile(Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return Profile 
     */
    public function getProfile()
    {
        return $this->profile;
    }

     /**
     * Set userId
     *
     * @param integer $userId
     * @return Profileuser
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Profileuser
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User 
     */
    public function getUser()
    {
        return $this->user;
    }

     /**
     * Set ctstatusId
     *
     * @param integer $ctstatusId
     * @return Profileuser
     */
    public function setCtstatusId($ctstatusId)
    {
        $this->ctstatusId = $ctstatusId;

        return $this;
    }

    /**
     * Get ctstatusId
     *
     * @return integer 
     */
    public function getCtstatusId()
    {
        return $this->ctstatusId;
    }

    /**
     * Set ctstatus
     *
     * @param \Aplication\DefaultBundle\Entity\Catalog $ctstatus
     * @return Profileuser
     */
    public function setCtstatus(\Aplication\DefaultBundle\Entity\Catalog $ctstatus = null)
    {
        $this->ctstatus = $ctstatus;

        return $this;
    }

    /**
     * Get ctstatus
     *
     * @return \Aplication\DefaultBundle\Entity\Catalog 
     */
    public function getCtstatus()
    {
        return $this->ctstatus;
    }

    /**
     * Set client
     *
     * @param \Administration\DefaultBundle\Entity\Client $client
     * @return Profileuser
     */
    public function setClient(\Administration\DefaultBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Administration\DefaultBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
    * @ORM\PrePersist
    **/
    public function prePersist(){
            $this->ctstatusId=Constant::STATUS_ACTIVE_RECORD;
            $this->createdAt=new \DateTime();            
    }

    /**
    * @ORM\PreUpdate
    **/
    public function preUpdate(){
            $this->updatedAt=new \DateTime();
    }
    
    public function __toString() {
             }
    public function __clone() {
               $this->id=null;
             }
    public function copy() {
               return clone $this;
             }
}
