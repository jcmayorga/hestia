<?php

namespace Aplication\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Aplication\DefaultBundle\Model\Constant;

/**
 * Menurole
 *
 * @ORM\Table(name="application.menurole", indexes={@ORM\Index(name="IDX_DA405527CCD7E912", columns={"menu_id"}), @ORM\Index(name="IDX_DA405527D60322AC", columns={"role_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Menurole
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="application.menurole_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="adding", type="integer", nullable=true)
     */
    private $adding;

    /**
     * @var integer
     *
     * @ORM\Column(name="visualize", type="integer", nullable=true)
     */
    private $visualize;

    /**
     * @var integer
     *
     * @ORM\Column(name="refresh", type="integer", nullable=true)
     */
    private $refresh;

    /**
     * @var integer
     *
     * @ORM\Column(name="remove", type="integer", nullable=true)
     */
    private $remove;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="usucrud_id", type="integer", nullable=true)
     */
    private $usucrudId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ctstatus_id", type="integer", nullable=true)
     */
    private $ctstatusId;

    /**
     * @var \Menu
     *
     * @ORM\ManyToOne(targetEntity="Menu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     * })
     */
    private $menu;

      /**
     * @var integer
     *
     * @ORM\Column(name="menu_id", type="integer", nullable=true)
     */
    private $menuId;

    /**
     * @var \Role
     *
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;

       /**
     * @var integer
     *
     * @ORM\Column(name="role_id", type="integer", nullable=false)
     */
    private $roleId;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set adding
     *
     * @param integer $adding
     * @return Menurole
     */
    public function setAdding($adding)
    {
        $this->adding = $adding;

        return $this;
    }

    /**
     * Get adding
     *
     * @return integer 
     */
    public function getAdding()
    {
        return $this->adding;
    }

    /**
     * Set visualize
     *
     * @param integer $visualize
     * @return Menurole
     */
    public function setVisualize($visualize)
    {
        $this->visualize = $visualize;

        return $this;
    }

    /**
     * Get visualize
     *
     * @return integer 
     */
    public function getVisualize()
    {
        return $this->visualize;
    }

    /**
     * Set refresh
     *
     * @param integer $refresh
     * @return Menurole
     */
    public function setRefresh($refresh)
    {
        $this->refresh = $refresh;

        return $this;
    }

    /**
     * Get refresh
     *
     * @return integer 
     */
    public function getRefresh()
    {
        return $this->refresh;
    }

    /**
     * Set remove
     *
     * @param integer $remove
     * @return Menurole
     */
    public function setRemove($remove)
    {
        $this->remove = $remove;

        return $this;
    }

    /**
     * Get remove
     *
     * @return integer 
     */
    public function getRemove()
    {
        return $this->remove;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Menurole
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Menurole
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set usucrudId
     *
     * @param integer $usucrudId
     * @return Menurole
     */
    public function setUsucrudId($usucrudId)
    {
        $this->usucrudId = $usucrudId;

        return $this;
    }

    /**
     * Get usucrudId
     *
     * @return integer 
     */
    public function getUsucrudId()
    {
        return $this->usucrudId;
    }

    /**
     * Set ctstatusId
     *
     * @param integer $ctstatusId
     * @return Menurole
     */
    public function setCtstatusId($ctstatusId)
    {
        $this->ctstatusId = $ctstatusId;

        return $this;
    }

    /**
     * Get ctstatusId
     *
     * @return integer 
     */
    public function getCtstatusId()
    {
        return $this->ctstatusId;
    }


    /**
     * Set menuId
     *
     * @param integer $menuId
     * @return Menurole
     */
    public function setMenuId($menuId)
    {
        $this->menuId = $menuId;

        return $this;
    }

    /**
     * Get menuId
     *
     * @return integer 
     */
    public function getMenuId()
    {
        return $this->menuId;
    }

    /**
     * Set menu
     *
     * @param Menu $menu
     * @return Menurole
     */
    public function setMenu(Menu $menu = null)
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu
     *
     * @return Menu 
     */
    public function getMenu()
    {
        return $this->menu;
    }

     /**
     * Set roleId
     *
     * @param integer $roleId
     * @return Menurole
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;

        return $this;
    }

    /**
     * Get roleId
     *
     * @return integer 
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /**
     * Set role
     *
     * @param Role $role
     * @return Menurole
     */
    public function setRole(Role $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return Role 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
    * @ORM\PrePersist
    **/
    public function prePersist(){
            $this->ctstatusId=Constant::STATUS_ACTIVE_RECORD;
            $this->createdAt=new \DateTime();            
    }

    /**
    * @ORM\PreUpdate
    **/
    public function preUpdate(){
            $this->updatedAt=new \DateTime();
    }
    
    public function __toString() {
             }
    public function __clone() {
               $this->id=null;
             }
    public function copy() {
               return clone $this;
             }
}
