<?php

namespace Aplication\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Aplication\DefaultBundle\Model\Constant;

/**
 * User
 *
 * @ORM\Table(name="application.user", indexes={@ORM\Index(name="IDX_FAAE817946496B9F", columns={"cttypeidentification_id"}), @ORM\Index(name="IDX_FAAE81799B30CAFA", columns={"ctstatususer_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class User {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="application.user_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identification", type="string", nullable=true)
     */
    private $identification;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", nullable=true)
     */
    private $fullname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="usucrud_id", type="integer", nullable=true)
     */
    private $usucrudId;

    /**
     * @var \Catalog
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\Catalog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cttypeidentification_id", referencedColumnName="id")
     * })
     */
    private $cttypeidentification;

    /**
     * @var \Catalog
     *
     * @ORM\ManyToOne(targetEntity="\Aplication\DefaultBundle\Entity\Catalog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ctstatususer_id", referencedColumnName="id")
     * })
     */
    private $ctstatususer;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", nullable=true)
     */
    private $password;
    
                /**
     * @var integer
     *
     * @ORM\Column(name="ctstatususer_id", type="integer", nullable=true)
     */
    private $ctstatususerId;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set identification
     *
     * @param string $identification
     * @return User
     */
    public function setIdentification($identification) {
        $this->identification = $identification;

        return $this;
    }

    /**
     * Get identification
     *
     * @return string 
     */
    public function getIdentification() {
        return $this->identification;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname) {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname() {
        return $this->lastname;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     * @return User
     */
    public function setFullname($fullname) {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string 
     */
    public function getFullname() {
        return $this->fullname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set usucrudId
     *
     * @param integer $usucrudId
     * @return User
     */
    public function setUsucrudId($usucrudId) {
        $this->usucrudId = $usucrudId;

        return $this;
    }

    /**
     * Get usucrudId
     *
     * @return integer 
     */
    public function getUsucrudId() {
        return $this->usucrudId;
    }

    /**
     * Set cttypeidentification
     *
     * @param \Aplication\DefaultBundle\Entity\Catalog $cttypeidentification
     * @return User
     */
    public function setCttypeidentification(\Aplication\DefaultBundle\Entity\Catalog $cttypeidentification = null) {
        $this->cttypeidentification = $cttypeidentification;

        return $this;
    }

    /**
     * Get cttypeidentification
     *
     * @return \Aplication\DefaultBundle\Entity\Catalog 
     */
    public function getCttypeidentification() {
        return $this->cttypeidentification;
    }

    /**
     * Set ctstatususer
     *
     * @param \Aplication\DefaultBundle\Entity\Catalog $ctstatususer
     * @return User
     */
    public function setCtstatususer(\Aplication\DefaultBundle\Entity\Catalog $ctstatususer = null) {
        $this->ctstatususer = $ctstatususer;

        return $this;
    }

    /**
     * Get ctstatususer
     *
     * @return \Aplication\DefaultBundle\Entity\Catalog
     */
    public function getCtstatususer() {
        return $this->ctstatususer;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password) {
        if($password){
            $this->password = $password;
        }
        return $this;
    }
    
    
    /**
     * Set ctstatususerId
     *
     * @param integer $ctstatususerId
     * @return User
     */
    public function setCtstatususerId($ctstatususerId)
    {
        $this->ctstatususerId = $ctstatususerId;

        return $this;
    }

    /**
     * Get ctstatususerId
     *
     * @return integer 
     */
    public function getCtstatusId()
    {
        return $this->ctstatususerId;
    }

      /**
    * @ORM\PrePersist
    **/
    public function prePersist(){
            $this->ctstatusId=Constant::STATUS_ACTIVE_USER;
            $this->createdAt=new \DateTime();   
            $this->fullname=$this->name." ".$this->lastname;         
    }

    /**
    * @ORM\PreUpdate
    **/
    public function preUpdate(){
            $this->updatedAt=new \DateTime();
            $this->fullname=$this->name." ".$this->lastname;
    }
    
    public function __toString() {
             }
    public function __clone() {
               $this->id=null;
             }
    public function copy() {
               return clone $this;
             }
    

}
