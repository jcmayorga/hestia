<?php 
/*
* @autor Luis Malquin
*/

namespace Aplication\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Aplication\DefaultBundle\Model\Constant;

class ProfileController extends Controller {
 
        /**
        * Retorna la vista de la lista de datos datatables tipo Profile
        * @Route("/",name="aplication_default_profile_index")
        * @Method({"GET"})    
        * @Template()           
        * @return text/html AplicationDefaultBundle:Profile:index.html.twig 
        **/
        public function indexAction(Request $request){
                
                $ProfileGrid=$this->get('aplication.default.grid.profile');
                $grid=$ProfileGrid->getResponse($request);
                return array(
                    "grid" => $grid,
                );               
        }
         
                        
        /**
        * Retorna la vista de un formulario tipo ProfileType
        * @Route("/add",name="aplication_default_profile_add")
        * @Method({"GET","POST"})        
        * @Template()   
        * @return text/html AplicationDefaultBundle:Profile:add.html.twig             
        **/    
        public function addAction(){
                $formView = "";
                try {
                    
                        $ProfileManager=  $this->get("aplication.default.manager.profile");                                         
                        $form=  $ProfileManager->createForm();
                        $formView = $form->createView();
                   
                } catch (\Exception $exc) {
                    
                    $this->get("session")->getFlashBag()->add(
                            "danger", $exc->getMessage()
                    );
                }
                return array(
                     "form" => $formView
                );        
        }
                 
        /**
        * Retorna la vista del formulario tipo Profile en modo de edición
        * @Route("/edit/{id}",name="aplication_default_profile_edit",requirements={"id" = "\d+"}, defaults={"id" = 0})                 
        * @Method({"GET","POST"}) 
        * @Template()
        * @param integer $id
        * @return text/html AplicationDefaultBundle:Profile:edit.html.twig   
        **/    
        public function editAction($id){
                $formView = "";
                try {
                       
                      
                          $ProfileManager=  $this->get("aplication.default.manager.profile");                                         
                          $form=  $ProfileManager->createForm($id);
                          $formView = $form->createView();
                      
                } catch (\Exception $exc) {

                    $this->get("session")->getFlashBag()->add(
                            "danger", $message
                    );
                }
                return array(
                     "form" => $formView
                ); 
               
        }
                 
       
        
        /**
        * Guarda los datos de un formulario basado en un modelo Profile
        * @Route("/save/{id}",name="aplication_default_profile_save",requirements={"id" = "\d+"}, defaults={"id" = 0})                 
        * @Method({"GET","POST"})    
        * @param Request $request    
        * @param integer $id        
        * @return JsonResponse  {"status","message"}     
        **/    
        public function saveAction(Request $request,$id){
                $status = 0;
                $message = "";
                
                //try {
                    
                        $ProfileManager=  $this->get("aplication.default.manager.profile");                   
                        $form=$ProfileManager->createForm($id);                        
                        $form->handleRequest($request);
                        $Profile=$form->getData();
                        if ($form->isValid()) {               
                            $ProfileManager->save($Profile);                                                
                        } else {
                            $error=(string) $form->getErrors(true, false);
                            throw new \Exception($error);
                        }
                        
                        $status = 1;
                        $message=(!$id)?"Registro perfile agregado correctamente":"Registro Perfil actualizado correctamente";
                    
               /* } catch (\Exception $exc) {
                    
                    $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible guardar el registro." : $exc->getMessage();
                    
                }*/
                
                return new JsonResponse(array("status" => $status, "message" => $message));
        }
        
        /**
        * Elimina o inactiva un registro de la entidad Profile
        * @Route("/delete/{id}",name="aplication_default_profile_delete",requirements={"id" = "\d+"}, defaults={"id" = 0},options={"expose"=true})                 
        * @Method({"GET","POST"})    
        * @param integer $id    
        * @return JsonResponse {"status","message"}      
        **/    
        public function deleteAction($id){
            $status = 0;
            $message = "";

            try {
                
                    $ProfileManager=  $this->get("aplication.default.manager.profile");   
                    $result=$ProfileManager->deleteById($id);
                    if ($result) {
                        $message="Registro Profile eliminado correctamente";
                        $status = 1;
                    }
                     
            } catch (\Exception $exc) {
                
                    $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible eliminar el registro." : $exc->getMessage();
                    
            }
            return new JsonResponse(array("status" => $status, "message" => $message));
        }

        /**
        * Retorna la vista del formulario tipo Profilerole en modo de edición
        * @Route("/roleassign/{profile_id}",name="aplication_default_profile_roles",requirements={"profile_id" = "\d+"}, defaults={"profile_id" = 0})                 
        * @Method({"GET","POST"}) 
        * @Template()
        * @param integer $Profile_id
        * @return text/html AplicationDefaultBundle:Profile:permission.html.twig   
        **/    
        public function rolesassignAction($profile_id){
                $profileroles = array();
                $roleManager=$this->get("aplication.default.manager.role");  
                $profileroleManager=$this->get("aplication.default.manager.profilerole");

                try {
                                
                    

                      $roles=$roleManager->getActiveRoles();

                      foreach ($roles as $rol) {
                          $rol_id=$rol->getId();
                          $profilerole=array('role_id'=>$rol_id,'role_name'=>$rol->getName(),'active'=>0);
                          $existProfilerole=$profileroleManager->getActiveRole($profile_id,$rol_id);
                          if($existProfilerole){
                               $profilerole['active']=1;
                          }
                          $profileroles[]=$profilerole;
                      }
                      
                      
                } catch (\Exception $exc) {

                    $this->get("session")->getFlashBag()->add(
                            "danger", $exc->getMessage()
                    );
                }
                return array(
                     'profileroles'=>$profileroles
                ); 
               
        }

         /**
        * Guarda los permisos de menus del rol
        * @Route("/roles/save/{profile_id}",name="aplication_default_profile_roles_save",requirements={"profile_id" = "\d+"}, defaults={"profile_id" = 0})                 
        * @Method({"GET","POST"})         
        * @param integer $profile_id
        * @return json {'status':integer,message:string}
        **/    
        public function rolesSaveAction($profile_id,Request $request){
                $rolesActive = $request->get('roles');
                $status=0;
                $message='';

                $roleManager=$this->get("aplication.default.manager.role");  
                $profileManager=$this->get("aplication.default.manager.profile");
                $profileroleManager=$this->get("aplication.default.manager.profilerole");
                $catalogManager=$this->get('aplication.default.manager.catalog');

                try {
                    $roles=$roleManager->getActiveRoles();
                    foreach ($roles as $role) {
                        $role_id=$role->getId();
                        $ctstatusId=isset($rolesActive[$role_id])?Constant::STATUS_ACTIVE_RECORD:Constant::STATUS_INACTIVE_RECORD;
                        $profilerole=null;
                        $exist=$profileroleManager->findOneBy(array('profileId'=>$profile_id,'roleId'=>$role_id));
                        if(!$exist){
                            $profilerole=$profileroleManager->create();
                            $profilerole->setProfile($profileManager->find($profile_id));
                            $profilerole->setRole($roleManager->find($role_id));
                        }else{
                            $profilerole=$exist;
                        }

                        $ctstatus=$catalogManager->find($ctstatusId);
                        $profilerole->setCtstatus($ctstatus);
                        $profileroleManager->save($profilerole,false);

                    }
                    $profileroleManager->flushAllChanges();    
                    $status=1;
                    $message='Rol o roles asignado correctamente';
                } catch (\Exception $exc) {
                      $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible guardar los registros." : $exc->getMessage();
            
                }
                
                return new JsonResponse(array("status" => $status, "message" => $message));

        }
        
}
 ?>