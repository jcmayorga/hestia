<?php

namespace Aplication\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

//use Symfony\Component\HttpFoundation\Session;

class BaseController extends Controller {

    protected $CTACTIVE = 500;
    protected $CTNOMINAACTIVE = 505;
    protected $CTUSERACTIVATE = 515;
    protected $CTUSERINACTIVATE = 516;
    protected $CTUSERPENDING = 517;
    protected $CTUSERLOCKED = 518;
    protected $MSNERRORUSER1 = "Error de configuración : El usuario existe pero no se encuentra parametrizado como cliente (clientuser)";
    protected $MSNERRORUSER2 = "Error de configuración : El usuario se encuentra parametrizado con más de 2 clientes (clientuser)";
    protected $MSNERRORUSER3 = "Error  : No existe cliente (client)";
    protected $MSNERRORUSER4 = "Error  : No no esta activo (client)";

}
