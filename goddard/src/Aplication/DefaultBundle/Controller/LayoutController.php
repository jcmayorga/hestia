<?php

namespace Aplication\DefaultBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Aplication\DefaultBundle\Controller\BaseController;
use Aplication\DefaultBundle\Model\Constant;

class LayoutController extends BaseController
{

    /**
     * @Route("/",name="_aplication_default_layout")
     * @Template()
     * */
    public function indexAction()
    {
        $session = $this->get("session");
        $sessionuser = $session->get('userdata');

        if (!$sessionuser) {
            return $this->redirect($this->generateUrl('_aplication_default_login'));
        }

        $sessionclient = $session->get('clientdata');

        return $this->render('AplicationDefaultBundle:Layout:index.html.twig', array("sessionclient" => $sessionclient));
    }

    /**
     * @Route("/sidebar",name="_aplication_default_layout_sidebar")
     * @Template()
     * */
    public function sidebarAction()
    {
        $session = $this->get("session");
        $sessionuser = $session->get('userdata');

        $arrayreposmenurole = $this->getMenurole();

        return $this->render('AplicationDefaultBundle:Layout:sidebar.html.twig',
            array(
                "arrayreposmenurole" => $arrayreposmenurole,
                "sessionuser" => $sessionuser
            )
        );
    }

    /**
     * @Route("/navbar",name="_aplication_default_layout_navbar")
     * @Template()
     * */
    public function navbarAction()
    {
        $session = $this->get("session");

        $sessionuser = $session->get('userdata');
        if (!$sessionuser) {
            return $this->redirect($this->generateUrl('_aplication_default_login'));
        }

        return $this->render('AplicationDefaultBundle:Layout:navbar.html.twig', array("sessionuser" => $sessionuser));
    }

    /**
     * @Route("/iframe",name="_aplication_default_iframe")
     * @Template()
     * */
    public function iframeAction()
    {
        return $this->render('AplicationDefaultBundle:Layout:iframe.html.twig');
    }

    /**
     * @Route("/closesession",name="_aplication_default_closesession")
     * @Template()
     * */
    public function closesessionAction()
    {
        $this->get("session")->set("userdata", null);
        return $this->redirect($this->generateUrl('_aplication_default_login'));

    }


    private function getMenurole()
    {
        $arrayobjmenurole = array();
        $session = $this->get("session");

        $sessionuser = $session->get('userdata');
        $userid = $sessionuser['id'];

        $em = $this->getDoctrine()->getManager();
        $reposuser = $em->getRepository("AplicationDefaultBundle:User");
        $objuser = $reposuser->find($userid);

        $reposprofileuser = $em->getRepository("AplicationDefaultBundle:Profileuser");

        $reposprofilerole = $em->getRepository("AplicationDefaultBundle:Profilerole");
        $reposmenurole = $em->getRepository("AplicationDefaultBundle:Menurole");

        $arrayobjprofileuser = $reposprofileuser->findBy(array("user" => $objuser,'ctstatusId'=>Constant::STATUS_ACTIVE_RECORD));
        //get profile

        $arrayprofile = array();
        if (!$arrayobjprofileuser) {

            die("No existe perfiles asignados a este usuario (profileuser)");
        } else {

            foreach ($arrayobjprofileuser as $objprofileuser) {
                $arrayprofile[] = $objprofileuser->getProfile();
            }
        }
        //get roles
        $arrayrole = array();
        foreach ($arrayprofile as $objprofile) {
            $arrayobjreposprofilerole = $reposprofilerole->findBy(array("profile" => $objprofile,'ctstatusId'=>Constant::STATUS_ACTIVE_RECORD));
            foreach ($arrayobjreposprofilerole as $objreposprofilerole) {
                $arrayrole[] = $objreposprofilerole->getRole();
            }
        }

        //get menus
        if (empty($arrayrole)) {
            die("Existe perfiles pero no existe roles  (profilerole)");
        } else {
            foreach ($arrayrole as $objrole) {
                $arrayreposmenurole = $reposmenurole->findBy(array("role" => $objrole,'ctstatusId'=>Constant::STATUS_ACTIVE_RECORD));
                foreach ($arrayreposmenurole as $objmenurole) {
                    $arrayobjmenurole[] = $objmenurole;
                }
            }
        }

        if (empty($arrayobjmenurole)) {
            die("No existe menus para el rol  (menurole)");
        }

        return $arrayobjmenurole;
    }
}
