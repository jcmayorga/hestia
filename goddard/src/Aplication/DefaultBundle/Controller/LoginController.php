<?php

namespace Aplication\DefaultBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Aplication\DefaultBundle\Controller\BaseController;

class LoginController extends BaseController {

    /**
     * @Route("/",name="_aplication_default_login")
     * @Template()    
     * */
    public function indexAction() {
        $session = $this->get("session");  

        $sessionuser = $session->get('userdata');
        if($sessionuser){
            return $this->redirect($this->generateUrl('_aplication_default_layout'));
        }

        return $this->render('AplicationDefaultBundle:Login:index.html.twig', array("noexistuser" => 0));
    }

    /**
     * @Route("/validateinput",name="_aplication_default_login_validateinput")
     * @Template()    
     * */
    public function validateinputAction(Request $request) {
       

        $requestidentification = $request->get("identification");
        $requestpassword = $request->get("password");
        $em = $this->getDoctrine()->getManager();
        $reposuser = $em->getRepository("AplicationDefaultBundle:User");
        $objuser = $reposuser->findOneBy(array("identification" => $requestidentification, "password" => $requestpassword));
        if (!$objuser) {
//            echo "entra";
            return $this->render('AplicationDefaultBundle:Login:index.html.twig', array("noexistuser" => 1));
//            return $this->redirect($this->generateUrl('_aplication_default_login'),1);
        } else {
            $this->setSessionUser($objuser);
//            return $this->render('AplicationDefaultBundle:Layout:index.html.twig');
             return $this->redirect($this->generateUrl('_aplication_default_layout'));
        }
    }

    private function setSessionUser($objuser) {
        $session = $this->get("session");
        

        $em = $this->getDoctrine()->getManager();
        $repoclientuser=$em->getRepository("AdministrationDefaultBundle:Clientuser");
        $arrayclientuser=$repoclientuser->findBy(array("userId"=>$objuser->getId(),"ctstatusId"=>$this->CTACTIVE));
        $numreg=count($arrayclientuser);

        if(empty($numreg)){
              die($this->MSNERRORUSER1); 
        }

        if($numreg>1){
            
              die($this->MSNERRORUSER2); 
        }

        $objclientuser=$arrayclientuser[0];
        $objclient=$objclientuser->getClient();
        if (!$objclient) {
                die($this->MSNERRORUSER3); // no esxiste cliente
        }else{
           
            $clientactive=$objclient->getCtstatus()->getId();
            if($clientactive!=500){
                die($objclient->getName()." ".$this->MSNERRORUSER4); // cliente no esta activo
            }
        }        

       
        if(!$repoclientuser){
            die($this->MSNERRORUSER1); 
        }
        $userdata = array(
             "id" => $objuser->getId()
            ,"identification" => $objuser->getIdentification()
            , "name" => $objuser->getName()
            , "lastname" => $objuser->getLastname()
            , "fullname" => $objuser->getFullname());
        $clientdata = array('id' => $objclient->getId()
             ,"name"=> $objclient->getName());
        $session->set('userdata', $userdata);
        $session->set('clientdata', $clientdata);
        return $userdata;
    }

}
