<?php 
/*
* @autor Luis Malquin
*/

namespace Aplication\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class RoleController extends Controller {
 
        /**
        * Retorna la vista de la lista de datos datatables tipo Role
        * @Route("/",name="aplication_default_role_index")
        * @Method({"GET"})    
        * @Template()           
        * @return text/html AplicationDefaultBundle:Role:index.html.twig 
        **/
        public function indexAction(Request $request){
                
                $roleGrid=$this->get('aplication.default.grid.role');
                $grid=$roleGrid->getResponse($request);
                return array(
                    "grid" => $grid,
                );               
        }
         
                        
        /**
        * Retorna la vista de un formulario tipo RoleType
        * @Route("/add",name="aplication_default_role_add")
        * @Method({"GET","POST"})        
        * @Template()   
        * @return text/html AplicationDefaultBundle:Role:add.html.twig             
        **/    
        public function addAction(){
                $formView = "";
                try {
                    
                        $roleManager=  $this->get("aplication.default.manager.role");                                         
                        $form=  $roleManager->createForm();
                        $formView = $form->createView();
                   
                } catch (\Exception $exc) {
                    
                    $this->get("session")->getFlashBag()->add(
                            "danger", $exc->getMessage()
                    );
                }
                return array(
                     "form" => $formView
                );        
        }
                 
        /**
        * Retorna la vista del formulario tipo Role en modo de edición
        * @Route("/edit/{id}",name="aplication_default_role_edit",requirements={"id" = "\d+"}, defaults={"id" = 0})                 
        * @Method({"GET","POST"}) 
        * @Template()
        * @param integer $id
        * @return text/html AplicationDefaultBundle:Role:edit.html.twig   
        **/    
        public function editAction($id){
                $formView = "";
                try {
                       
                      
                          $roleManager=  $this->get("aplication.default.manager.role");                                         
                          $form=  $roleManager->createForm($id);
                          $formView = $form->createView();
                      
                } catch (\Exception $exc) {

                    $this->get("session")->getFlashBag()->add(
                            "danger", $exc->getMessage()
                    );
                }
                return array(
                     "form" => $formView
                ); 
               
        }
                 
       
        
        /**
        * Guarda los datos de un formulario basado en un modelo Role
        * @Route("/save/{id}",name="aplication_default_role_save",requirements={"id" = "\d+"}, defaults={"id" = 0})                 
        * @Method({"GET","POST"})    
        * @param Request $request    
        * @param integer $id        
        * @return JsonResponse  {"status","message"}     
        **/    
        public function saveAction(Request $request,$id){
                $status = 0;
                $message = "";
                
                try {
                    
                        $roleManager=  $this->get("aplication.default.manager.role");                   
                        $form=$roleManager->createForm($id);                        
                        $form->handleRequest($request);
                        $role=$form->getData();
                        if ($form->isValid()) {               
                            $roleManager->save($role);                                                
                        } else {
                            $error=(string) $form->getErrors(true, false);
                            throw new \Exception($error);
                        }
                        
                        $status = 1;
                        $message=(!$id)?"Registro rol agregado correctamente":"Registro role actualizado correctamente";
                    
                } catch (\Exception $exc) {
                    
                    $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible guardar el registro." : $exc->getMessage();
                    
                }
                
                return new JsonResponse(array("status" => $status, "message" => $message));
        }
        
        /**
        * Elimina o inactiva un registro de la entidad role
        * @Route("/delete/{id}",name="aplication_default_role_delete",requirements={"id" = "\d+"}, defaults={"id" = 0},options={"expose"=true})                 
        * @Method({"GET","POST"})    
        * @param integer $id    
        * @return JsonResponse {"status","message"}      
        **/    
        public function deleteAction($id){
            $status = 0;
            $message = "";

            try {
                
                    $roleManager=  $this->get("aplication.default.manager.role");   
                    $result=$roleManager->deleteById($id);
                    if ($result) {
                        $message="Registro role eliminado correctamente";
                        $status = 1;
                    }
                     
            } catch (\Exception $exc) {
                
                    $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible eliminar el registro." : $exc->getMessage();
                    
            }
            return new JsonResponse(array("status" => $status, "message" => $message));
        }

        /**
        * Retorna la vista del formulario tipo rolemenu en modo de edición
        * @Route("/permission/{role_id}",name="aplication_default_role_permission",requirements={"role_id" = "\d+"}, defaults={"role_id" = 0})                 
        * @Method({"GET","POST"}) 
        * @Template()
        * @param integer $role_id
        * @return text/html AplicationDefaultBundle:Role:permission.html.twig   
        **/    
        public function permissionAction($role_id){
                $menusrole = array();
                try {
                                            
                      
                      $menuManager=$this->get("aplication.default.manager.menu");        
                      $menuroleManager=$this->get("aplication.default.manager.menurole");        
                      $menus=$menuManager->getActiveMenus();
                      
                      foreach ($menus as $menu) {
                      
                          $menu_id=$menu->getId();
                          $url=trim($menu->getUrl());
                          if($url!='#' && $url){
                              $menurole=array('menu_id'=>$menu_id,'menu_title'=>$menu->getTitle(),'view'=>0,'add'=>0,'edit'=>0,'delete'=>0);

                              $existMenuRole=$menuroleManager->getMenuRoleActive($role_id,$menu_id);
                              if($existMenuRole){
                                  $menurole['view']=$existMenuRole->getVisualize();
                                  $menurole['add']=$existMenuRole->getAdding();
                                  $menurole['edit']=$existMenuRole->getRefresh();
                                  $menurole['delete']=$existMenuRole->getRemove();
                              }
                              $menusrole[]=$menurole;
                          }

                      }

                      
                      
                      
                } catch (\Exception $exc) {

                    $this->get("session")->getFlashBag()->add(
                            "danger", $exc->getMessage()
                    );
                }
                
                return array(
                     'menusrole'=>$menusrole
                ); 
               
        }

         /**
        * Guarda los permisos de menus del rol
        * @Route("/permission/save/{role_id}",name="aplication_default_role_permission_save",requirements={"role_id" = "\d+"}, defaults={"role_id" = 0})                 
        * @Method({"GET","POST"})         
        * @param integer $role_id
        * @return json {'status':integer,message:string}
        **/    
        public function permissionSaveAction($role_id,Request $request){
                $permissions = $request->get('permissions');
                $menuroleManager=$this->get("aplication.default.manager.menurole");        
                $roleManager=$this->get("aplication.default.manager.role");        
                $menuManager=$this->get("aplication.default.manager.menu");
                $status=0;
                $message='';
                try {
                    $role=$roleManager->find($role_id);                    
                    $menus=$menuManager->getActiveMenus();

                    foreach ($menus as $menu) {
                        $menu_id=$menu->getId();
                        $permission=isset($permissions[$menu_id])?$permissions[$menu_id]:array();
                        
                        $menurole=null;

                                                
                        $view=(@$permission['view'])?1:0;
                        $add=(@$permission['add'])?1:0;
                        $edit=(@$permission['edit'])?1:0;
                        $delete=(@$permission['delete'])?1:0;

                        $exist=$menuroleManager->findOneBy(array('roleId'=>$role_id,'menuId'=>$menu_id));
                        if(!$exist){
                            $menurole=$menuroleManager->create();
                            $menurole->setRole($role);
                            $menurole->setMenu($menuManager->find($menu_id));
                        }else{
                            $menurole=$exist;
                        }
                        
                        $menurole->setVisualize($view);
                        $menurole->setAdding($add);
                        $menurole->setRefresh($edit);
                        $menurole->setRemove($delete);
                        $menuroleManager->save($menurole,false);
                    }

                    $menuroleManager->flushAllChanges();    
                    $status=1;
                    $message='Permisos asignado correctamente';
                } catch (\Exception $exc) {
                     $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible guardar los registros." : $exc->getMessage();
                }
                
                return new JsonResponse(array("status" => $status, "message" => $message));

        }
        
}
 ?>