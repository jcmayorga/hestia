<?php

namespace Aplication\DefaultBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Aplication\DefaultBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Aplication\DefaultBundle\Model\Constant;



class UserController extends BaseController {

    /**
     * @Route("/",name="aplication_default_user_index")
     * @Template()    
     * */
    public function indexAction(Request $request) {
          $userGrid=$this->get('aplication.default.grid.user');
          $grid=$userGrid->getResponse($request);
          return array(
                    "grid" => $grid,
          );               
    }

        /**
        * Retorna la vista de un formulario tipo userType
        * @Route("/add",name="aplication_default_user_add")
        * @Method({"GET","POST"})        
        * @Template()   
        * @return text/html AplicationDefaultBundle:user:add.html.twig             
        **/    
        public function addAction(){
                $formView = "";
                try {
                    
                        $userManager=  $this->get("aplication.default.manager.user");                                         
                        $form=  $userManager->createForm();
                        $formView = $form->createView();
                   
                } catch (\Exception $exc) {
                    
                    $this->get("session")->getFlashBag()->add(
                            "danger", $exc->getMessage()
                    );
                }
                return array(
                     "form" => $formView
                );        
        }
                 
        /**
        * Retorna la vista del formulario tipo user en modo de edición
        * @Route("/edit/{id}",name="aplication_default_user_edit",requirements={"id" = "\d+"}, defaults={"id" = 0})                 
        * @Method({"GET","POST"}) 
        * @Template()
        * @param integer $id
        * @return text/html AplicationDefaultBundle:user:edit.html.twig   
        **/    
        public function editAction($id){
                $formView = "";
                try {
                       
                      
                          $userManager=  $this->get("aplication.default.manager.user");                                         
                          $form=  $userManager->createForm($id);
                          $formView = $form->createView();
                      
                } catch (\Exception $exc) {

                    $this->get("session")->getFlashBag()->add(
                            "danger", $message
                    );
                }
                return array(
                     "form" => $formView
                ); 
               
        }
                 
       
        
        /**
        * Guarda los datos de un formulario basado en un modelo user
        * @Route("/save/{id}",name="aplication_default_user_save",requirements={"id" = "\d+"}, defaults={"id" = 0})                 
        * @Method({"GET","POST"})    
        * @param Request $request    
        * @param integer $id        
        * @return JsonResponse  {"status","message"}     
        **/    
        public function saveAction(Request $request,$id){
                $status = 0;
                $message = "";
                
                try {
                    
                        $userManager=  $this->get("aplication.default.manager.user");  
                        $profileuserManager=$this->get("aplication.default.manager.profileuser");                   
                        $form=$userManager->createForm($id);                        
                        $form->handleRequest($request);
                        $user=$form->getData();

                        if ($form->isValid()) {                                           
                            $userReq=$request->get('user');
                            $profile_id=$userReq['profile'];
                            $profileuserManager->saveUniqueProfile($user,$profile_id);                           
                        } else {
                            $error=(string) $form->getErrors(true, false);
                            throw new \Exception($error);
                        }
                        
                        $status = 1;
                        $message=(!$id)?"Registro usuario agregado correctamente":"Registro usuario actualizado correctamente";
                    
                } catch (\Exception $exc) {
                    
                    $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible guardar el registro." : $exc->getMessage();
                    
                }
                
                return new JsonResponse(array("status" => $status, "message" => $message));
        }
        
        /**
        * Elimina o inactiva un registro de la entidad user
        * @Route("/delete/{id}",name="aplication_default_user_delete",requirements={"id" = "\d+"}, defaults={"id" = 0},options={"expose"=true})                 
        * @Method({"GET","POST"})    
        * @param integer $id    
        * @return JsonResponse {"status","message"}      
        **/    
        public function deleteAction($id){
            $status = 0;
            $message = "";

            try {
                
                    $userManager=  $this->get("aplication.default.manager.user");   
                    $result=$userManager->deleteById($id);
                    if ($result) {
                        $message="Registro user eliminado correctamente";
                        $status = 1;
                    }
                     
            } catch (\Exception $exc) {
                
                    $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible eliminar el registro." : $exc->getMessage();
                    
            }
            return new JsonResponse(array("status" => $status, "message" => $message));
        }

        
        /**        
        * @Route("/dinnerassign/{user_id}",name="aplication_default_user_dinners",requirements={"user_id" = "\d+"}, defaults={"user_id" = 0})                 
        * @Method({"GET","POST"}) 
        * @Template()
        * @param integer $user_id
        * @return text/html AplicationDefaultBundle:Profile:dinnerassign.html.twig   
        **/    
        public function dinnersassignAction($user_id){
                $userdinners = array();
                $dinnerManager=$this->get("administration.default.manager.dinner");  
                $dinneruserManager=$this->get("administration.default.manager.dinneruser");

                try {
                                
                      $dinners=$dinnerManager->getActiveDinners();

                      foreach ($dinners as $dinner) {
                          $dinner_id=$dinner->getId();
                          $dinneruser=array('dinner_id'=>$dinner_id,'dinner_name'=>$dinner->getName(),'dinner_location'=>$dinner->getLocation(),'active'=>0);
                          $existDinneruser=$dinneruserManager->getActiveDinner($user_id,$dinner_id);
                          if($existDinneruser){
                               $dinneruser['active']=1;
                          }
                          $userdinners[]=$dinneruser;
                      }
                      
                      
                } catch (\Exception $exc) {

                    $this->get("session")->getFlashBag()->add(
                            "danger", $exc->getMessage()
                    );
                }
                return array(
                     'userdinners'=>$userdinners
                ); 
               
        }

         /**        
        * @Route("/dinners/save/{user_id}",name="aplication_default_user_dinner_save",requirements={"user_id" = "\d+"}, defaults={"user_id" = 0})                 
        * @Method({"GET","POST"})         
        * @param integer $profile_id
        * @return json {'status':integer,message:string}
        **/    
        public function dinnerSaveAction($user_id,Request $request){
                $dinnersActive = $request->get('dinners');
                $status=0;
                $message='';

                $dinnerManager=$this->get("administration.default.manager.dinner");  
                $userManager=$this->get("aplication.default.manager.user");
                $dinneruserManager=$this->get("administration.default.manager.dinneruser");
                $catalogManager=$this->get('aplication.default.manager.catalog');

                try {
                    $dinners=$dinnerManager->getActiveDinners();
                    foreach ($dinners as $dinner) {
                        $dinner_id=$dinner->getId();
                        $ctstatusId=isset($dinnersActive[$dinner_id])?Constant::STATUS_ACTIVE_RECORD:Constant::STATUS_INACTIVE_RECORD;
                        $dinneruser=null;
                        $exist=$dinneruserManager->findOneBy(array('userId'=>$user_id,'dinnerId'=>$dinner_id));
                        if(!$exist){
                            $dinneruser=$dinneruserManager->create();
                            $dinneruser->setUser($userManager->find($user_id));
                            $dinneruser->setDinner($dinnerManager->find($dinner_id));
                        }else{
                            $dinneruser=$exist;
                        }

                        $ctstatus=$catalogManager->find($ctstatusId);
                        $dinneruser->setCtstatus($ctstatus);
                        $dinneruserManager->save($dinneruser,false);

                    }
                    $dinneruserManager->flushAllChanges();    
                    $status=1;
                    $message='Comedor o comedores asignados correctamente';
                } catch (\Exception $exc) {
                      $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible guardar los registros." : $exc->getMessage();
            
                }
                
                return new JsonResponse(array("status" => $status, "message" => $message));

        }


}
