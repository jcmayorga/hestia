<?php 
/*
* @autor Luis Malquin
*/

namespace Aplication\DefaultBundle\Grid;

use Symfony\Component\DependencyInjection\Container;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Symfony\Component\HttpFoundation\Request;

/**
* Clase RoleGrid que contiene las propiedades y características de construcción del datatables para la la entidad Profile
*/ 

class ProfileGrid {

    private $container;
    private $gridConfig;

    public function __construct(Container $container){

        $this->container=$container;
        $this->gridConfig();
    }

    /**        
        * Define las opciones y caracteristicas de construcción del datatables
        * @param array $options         
        */
    private function gridConfig() {
        $repository = $this->container->get('doctrine.orm.entity_manager')->getRepository('AplicationDefaultBundle:Profile');
        $queryBuilder = $repository->createQueryBuilder('profile')
        ->select('profile,ctstatus')
        ->leftJoin('profile.ctstatus', 'ctstatus');
        
        $gridConfig= new GridConfig();
        $gridConfig
            ->setQueryBuilder($queryBuilder)
            ->setCountFieldName('profile.id')
            ->addField('profile.id',array('visible'=>false))
            ->addField('profile.name', array('label'=>'Nombre','filterable' => true,'sortable'=>true))
            ->addField('ctstatus.name',array('label'=>'Estado','filterable' => false,'sortable'=>false));
            
        return $this->gridConfig=$gridConfig;
    }

   
    /**        
        * Retorna la respuesta de datos en formato json de datatables
        * @return json 
        */
    public function getResponse(Request $request){
        
        $gridManager = $this->container->get('kitpages_data_grid.grid_manager');
        return  $gridManager->getGrid($this->gridConfig, $request);
    }

}
 ?>


