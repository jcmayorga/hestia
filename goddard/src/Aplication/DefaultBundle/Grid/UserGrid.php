<?php 
/*
* @autor Luis Malquin
*/

namespace Aplication\DefaultBundle\Grid;

use Symfony\Component\DependencyInjection\Container;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Symfony\Component\HttpFoundation\Request;

/**
* Clase UserGrid que contiene las propiedades y características de construcción del datatables para la la entidad User
*/ 

class UserGrid {

    private $container;
    private $gridConfig;

    public function __construct(Container $container){

        $this->container=$container;
        $this->gridConfig();
    }

    /**        
        * Define las opciones y caracteristicas de construcción del grid
        * @param array $options         
        */
    private function gridConfig() {
        $repository = $this->container->get('doctrine.orm.entity_manager')->getRepository('AplicationDefaultBundle:User');
        $queryBuilder = $repository->createQueryBuilder('user')
        ->select('user,ctstatus')
        ->leftJoin('user.ctstatususer', 'ctstatus');
        
        $gridConfig= new GridConfig();
        $gridConfig
            ->setQueryBuilder($queryBuilder)
            ->setCountFieldName('user.id')
            ->addField('user.id',array('visible'=>false))
            ->addField('user.identification',array('label'=>'Identificación','filterable' => true,'sortable'=>true))
            ->addField('user.fullname', array('label'=>'Nombre','filterable' => true,'sortable'=>true))
            ->addField('user.email',array('label'=>'Email','filterable' => true,'sortable'=>false))
            ->addField('ctstatus.name',array('label'=>'Estado','filterable' => false,'sortable'=>false));
            
        return $this->gridConfig=$gridConfig;
    }

   
    /**        
        * Retorna la respuesta de datos en formato object o json del grid
        * @return object|json 
        */
    public function getResponse(Request $request){
        
        $gridManager = $this->container->get('kitpages_data_grid.grid_manager');
        return  $gridManager->getGrid($this->gridConfig, $request);
    }

}
 ?>


