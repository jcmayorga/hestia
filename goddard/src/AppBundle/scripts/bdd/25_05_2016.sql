DELETE FROM administration."user"
ALTER TABLE business.transaction DROP CONSTRAINT fk_user_transaction;
DROP TABLE administration."user";
DROP TABLE administration.role;
DROP TABLE administration.profile;

CREATE TABLE application."user"
(
  id serial NOT NULL,
  cttypeidentification_id integer,
  identification character varying,
  name character varying,
  lastname character varying,
  fullname character varying,
  email character varying,
  created_at timestamp without time zone DEFAULT now(),
  updated_at timestamp without time zone,
  usucrud_id integer,
  ctstatususer_id integer,
  password character(100),
  CONSTRAINT pk_user PRIMARY KEY (id),
  CONSTRAINT fk_ctstatus_user FOREIGN KEY (ctstatususer_id)
      REFERENCES application.catalog (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_cttypeidentification_user FOREIGN KEY (cttypeidentification_id)
      REFERENCES application.catalog (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE application."user"
  OWNER TO postgres;




CREATE TABLE application.profile
(
  id serial NOT NULL,
  name character varying,
  profile_id integer,
  created_at timestamp without time zone DEFAULT now(),
  updated_at timestamp without time zone,
  usucrud_id integer,
  ctstatus_id integer,
  CONSTRAINT pk_profile PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE application.profile
  OWNER TO postgres;


ALTER TABLE application.profile
  ADD COLUMN description character(200);





CREATE TABLE application.role
(
  id serial NOT NULL,
  name character varying,
  created_at timestamp without time zone DEFAULT now(),
  updated_at timestamp without time zone,
  usucrud_id integer,
  ctstatus_id integer,
  CONSTRAINT pk_role PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE role
  OWNER TO postgres;

ALTER TABLE business.transaction
  ADD CONSTRAINT fk_user_transaction FOREIGN KEY (user_id) REFERENCES application."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION;





CREATE TABLE administration.clientuser
(
  id serial NOT NULL,
  client_id integer,
  user_id integer,
  created_at timestamp without time zone DEFAULT now(),
  updated_at timestamp without time zone,
  usucrud_id integer,
  ctstatus_id integer,
  CONSTRAINT pk_clientuser PRIMARY KEY (id),
  CONSTRAINT fk_ctstatus_clientuser FOREIGN KEY (ctstatus_id)
      REFERENCES application.catalog (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_user_clientuser FOREIGN KEY (user_id)
      REFERENCES application.user (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT fk_client_clientuser FOREIGN KEY (client_id)
      REFERENCES administration.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE administration.clientuser
  OWNER TO postgres;