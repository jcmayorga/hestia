<?php

namespace Service\DinnerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session;

class RegisterController extends Controller {

    /**
     * @Route("/",name="_service_dinner_register_index")
     * @Template()    
     * */
    public function indexAction() {
        return $this->render('ServiceDinnerBundle:Register:index.html.twig');
    }

}
