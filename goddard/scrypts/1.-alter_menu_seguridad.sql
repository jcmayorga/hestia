

insert into application.menu (title,description,url,organized,created_at,ctstatus_id,cttype_id) values('Contratos','Permite la gestión de los contratos','administration_default_contract_index',1,current_date,500,522);
insert into application.menu (title,description,url,organized,created_at,ctstatus_id,cttype_id) values('Comedores','Permite la gestión de los comedores','administration_default_dinner_index',10,current_date,500,522);
insert into application.menu (title,description,url,organized,created_at,ctstatus_id,cttype_id) values('Empresas','Permite la gestión de las empresas','administration_default_company_index',11,current_date,500,522);
insert into application.menu (title,description,url,organized,created_at,ctstatus_id,cttype_id) values('Servicios','Permite la gestión de los servicios','bussiness_default_service_index',12,current_date,500,522);
insert into application.menu (title,description,url,organized,created_at,ctstatus_id,cttype_id) values('Cargar comensales','Permite la importación de comensales','administration_default_roster_index',13,current_date,500,522);
insert into application.menu (title,description,url,organized,created_at,ctstatus_id,cttype_id) values('Roles','Permite la gestión de roles de usuario','aplication_default_role_index',6,current_date,500,522);
insert into application.menu (title,description,url,organized,created_at,ctstatus_id,cttype_id) values('Perfiles','Permite la gestión de perfiles de usuario','aplication_default_profile_index',7,current_date,500,522);
insert into application.menu (title,description,url,organized,created_at,ctstatus_id,cttype_id) values('Usuarios','Permite la gestión de usuario','aplication_default_user_index',8,current_date,500,522);
