 
(function ( $ ) {

    /*
     * Aplica los plugin date-picker, datetime-picker, timer-picker, date-range-picker a los inputs definida por las clases
     * y que se encuentran dentro de un formulario
     * Aplicacion form: $('form').inputDatePickerForm()  
     * Aplicacion input: 
     *          <input type='text' class='date-picker' />     
     *          <input type='text' class='datetime-picker' />     
     *          <input type='text' class='time-picker' />     
     *          <input type='text' class='date-range-picker' />     
     * @returns object form
     */
    $.fn.inputDatePickerForm=function(){
         var $form=this;
         
          //implementacion date-picker
         
                    $('.date-picker',$form).each(function(){
                         var startDate=$(this).attr('minDate');
                         var endDate=$(this).attr('maxDate');
                         var val=$(this).val();
                         
                         $(this).datepicker({
                         autoclose: true,
                         todayHighlight: true,
                         language: 'es',
                         startDate:(startDate)?startDate:null,
                         endDate:(endDate)?endDate:null
                         }).next().on('click', function(){
                            $(this).prev().focus();
                         }); 
                         
                         if(val){
                             $(this).datepicker("setDate", val);
                         }
                    });
         
         
         //Se incluye la libreria moment necesaria para datetimepicker y daterangepicker
        
             
                //implementacion datetime-picker
                
                    
                       $('.datetime-picker',$form).each(function(){
                           
                                var minDate=$(this).attr('minDate');
                                var maxDate=$(this).attr('maxDate');
                                
                                $(this).datetimepicker({
                                    //format: 'YYYY/MM/DD h:mm A',//use this option to display seconds                                    
                                    icons: {
                                           time: 'fa fa-clock-o',
                                           date: 'fa fa-calendar',
                                           up: 'fa fa-chevron-up',
                                           down: 'fa fa-chevron-down',
                                           previous: 'fa fa-chevron-left',
                                           next: 'fa fa-chevron-right',
                                           today: 'fa fa-arrows ',
                                           clear: 'fa fa-trash',
                                           close: 'fa fa-times'
                                    }
                                }).next().on('click', function(){
                                    $(this).prev().focus();
                                });
                                
                                if(minDate && minDate!='undefined'){
                                    $(this).datetimepicker( "option", "minDate",moment(minDate)); 
                                }
                                if(maxDate && maxDate!='undefined'){                                   
                                    $(this).datetimepicker( "option", "maxDate",moment(maxDate)); 
                                    
                                }
                                
                       });                
                       
                
                
                 //implementacion date-range-picker
                
                       /*$('.date-range-picker',$form).daterangepicker({
                               'applyClass' : 'btn-sm btn-success',
                               'cancelClass' : 'btn-sm btn-default',
                               locale: {
                                       applyLabel: 'Aplicar',
                                       cancelLabel: 'Cancelar',
                               }
                       })
                       .prev().on('click', function(){
                               $(this).next().focus();
                       });*/
                
                         
                      
        
        //implementacion time-picker
         
               $('.time-picker',$form).timepicker({
                      minuteStep: 1,
                      showSeconds: true,
                      showMeridian: false,
                      disableFocus: true,
                      icons: {
                              up: 'fa fa-chevron-up',
                              down: 'fa fa-chevron-down'
                      }
              }).on('focus', function() {
                      $(this).timepicker('showWidget');
              }).next().on('click', function(){
                      $(this).prev().focus();
              });
         
                              
    }

     /* Aplica los plugins necesarios para el ingreso da la informacion en los objetos que se encuentran dentro de un formulario,
     * la aplicación de los plugins se realiza por medio de una clase o un atributo
     * Aplicacion form: $('form').initInputsForm()          
     *        
     * @returns object form
     */
    $.fn.initInputsForm = function(callback) {
        
        var $form=this;
        
        $("input,textarea",$form).on("drop",function(){
            return false; 
        });
        
        //incializo tooltips y pophover de los input en el form
        /*$('[data-rel=tooltip]',$form).tooltip();
        $('[data-rel=popover]',$form).popover({html:true});*/
        
        //añade el * para los campos requeridos      
        $('.asterisk',$form).remove();
        
        $(".form-group .control-label.required",$form).each(function(){                       
            $(this).append('<i style="color:red" class="asterisk">*</i>');
        });                                
           
        $("label.required",$form).each(function(){    
            if(!$('.asterisk',this).length){                   
                $(this).append('<i style="color:red" class="asterisk">*</i>');
            }
        });                     
        
        //inicializa los inputs tipo date y tiempo
        $($form).inputDatePickerForm();                                
             
        //inicializa la mascara de moneda 
        //$($form).inputMaskMoneyForm();                                
             
               
        //inicializa los inputs para caracteres alfabeticos, numericos,alfanumericos,decimal, etc.
        //$($form).inputAlphanumForm();
        
        if(typeof callback!='undefined')callback();
        return this;
    };

 /*
     * Aplica el plugin de validacion de inputs de formularios 
     * Aplicacion form: $('form').validationForm({options})       
     * @returns object form
     */
    $.fn.validationForm=function(rules,messages,callback){
        var $form=this       
        
            //Inicializamos la validación del formulario
            $.each($form,function(){
                    $(this).validate({
                            errorElement: 'div',
                            errorClass: 'help-block',
                            focusInvalid: true,
                            ignore: "",
                            messages: (messages) ? messages:{},
                            rules: (rules) ? rules:{},
                            highlight: function (e) {
                                    $(e).closest('.form-group').removeClass('has-info').removeClass('has-success').addClass('has-error');                            

                            },

                            success: function (e,el) {
                                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');                            
                                    $(e).remove();
                            },

                            errorPlacement: function (error, element) {
                                    if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                                            var controls = element.closest('div[class*="col-"]');
                                            if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                                            else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                                    }
                                    else if(element.is('.select2')) {
                                            error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                                    }
                                    else if(element.is('.chosen-select')) {
                                            error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                                    }
                                    else {
                                        
                                        var parent=element.parent();
                                        if($(parent).hasClass('input-group')){
                                            error.insertAfter(parent);
                                        }else{
                                             parent.addClass('has-error');
                                             $(parent).append(error);
                                        }
                                    }


                            },
                            invalidHandler: function (form,validations) {
                                    var errors = validations.numberOfInvalids();
                                    if (errors>0) {
                                            var invalidElements = validations.invalidElements();
                                            var firstelem = invalidElements[0];
                                            // Encuentra la pestaña padre del elemento que está en el interior y obtiene el id
                                            var $closest = ($(firstelem).closest('.tab-pane').length) ? $(firstelem).closest('.tab-pane') : $(firstelem).closest('.ui-tabs-panel');

                                            if($closest.length){                                    
                                                var id = $closest.attr('id'); 
                                                // Buscar el enlace que corresponde al panel y lo muestra o activa
                                                $('.nav a[href="#'+id+'"]').tab('show');
                                                $('.ui-tabs-nav a[href="#'+id+'"]').trigger('click');
                                            }
                                    }
                            },
                            submitHandler:function(form){
                                //limpiar espacio en los inputs del formulario antes de enviar
                                $("input[type=text],textarea",form).each(function(){
                                    $(this).val($.trim(this.value))
                                });
                                if($(form).valid()){
                                    form.submit();
                                }
                                return false;
                            }
                    });              
                }); 
                if(typeof callback!='undefined')callback();
                                    
        return this;
    };


    //Crea la peticion ajax y carga los resultados en los combos dependientes o en cascada
    $.fn.cascadeCombobox = function(options) {

            var obj = this;
            var optionsdefault = {parents_id: '', id: $(this).attr('id'), using: 'id,descripcion', 
                urlLoad: '#',onLoad:null, method:'GET',data:{}
            };
            options = $.extend(optionsdefault, options);


            if (options.parents_id) {
                var strparents = '';
                var parentsparts = options.parents_id.split(',')


                if (parentsparts.length > 0) {
                    $.each(parentsparts, function() {
                        strparents += (strparents) ? ',' : '';
                        strparents += '#' + this;
                    });
                }

                var parentobjs = $(strparents);
                var defaultoption = '';
                $.each($('option', obj), function() {
                    if ($(this).attr('value') === '') {
                        defaultoption = this;
                    }
                });
                $(obj).after("<i id='load_" + options.id + "' class='fa fa-spinner fa-spin orange bigger-110' style='display:none'></i>");
                $(strparents).change(function() {
                    $(obj).empty();
                    if (defaultoption) {
                        $(obj).append(defaultoption);
                    }
                    if (parentobjs.length) {
                        var countvaluesparent = 0;
                        var strvalues = '';
                        $.each(parentobjs, function() {
                            var value = $(this).val();
                            if (value)
                                countvaluesparent++;
                            strvalues += (strvalues) ? '/' : '';
                            strvalues += value;
                        });
                        var using = options.using.split(',');

                        if (countvaluesparent == parentobjs.length) {
                            $.ajax({
                                url: options.urlLoad + '/' + strvalues,
                                type: options.method,
                                dataType: 'json',
                                aync: false,
                                data:options.data,
                                contentType: 'application/json; charset=utf-8',
                                success: function(json) {

                                    if (json.length > 0) {

                                        if ($.isArray(json)) {
                                            for (var i = 0; i < json.length; i++) {
                                                var item = json[i];
                                                var strattr = '';
                                                if (item['attr'] != undefined) {
                                                    var objattr = item['attr'];
                                                    $.each(objattr, function(key, value) {
                                                        strattr += " " + key + "=" + value;
                                                    });
                                                }
                                                if (item[using[0]] != undefined && item[using[1]] != undefined) {

                                                    $(obj).append('<option value=\"' + item[using[0]] + '\" ' + strattr + '>' + item[using[1]] + '</option>');
                                                }
                                            }
                                        } else {
                                            var item = json;
                                            var strattr = '';
                                            if (item['attr'] != undefined) {
                                                var objattr = item['attr'];
                                                $.each(objattr, function(key, value) {
                                                    strattr += " " + key + "=" + value;
                                                });
                                            }
                                            if (item[using[0]] != undefined && item[using[1]] != undefined) {
                                                $(obj).append('<option value=\"' + item[using[0]] + '\" ' + strattr + '>' + item[using[1]] + '</option>');
                                            }
                                        }
                                        if(options.onLoad){                                    
                                            options.onLoad();
                                        }
                                    }
                                    obj.change();
                                    $('#load_' + options.id).hide();
                                },
                                beforeSend: function() {
                                    $('#load_' + options.id).css('display', 'inline-block');

                                },
                                // error: function(xhr, ajaxOptions, thrownError) {
                                //
                                //     $.alert(thrownError ,'danger');
                                //     $('#load_' + options.id).hide();
                                // }
                            });
                        } else {
                            obj.change();
                        }
                    }
                });
            }
        }

//ventana modal de barra de progreso, de proceso en ejecución
var _processWait=null;
$.processWait=_processWait||(function(){
        var divWait=$("<div>",{'class':'modal fade','data-backdrop':'static','date-keyboard':'false','tabindex':'-1','role':'dialog','aria-hidden':"true"})
            .css({'overflow-y':'visible', 'z-index': 2000});
        var modaldialog=$('<div>',{'class':'modal-dialog'});
        var modalcontent=$('<div>',{'class':'modal-content'});
        var modalheader=$('<div>',{'class':'modal-header'});
        var modalbody=$('<div>',{'class':'modal-body'});
        var processbar=$('<div>',{'class':'progress progress-striped active'})
        //haader
        modalheader.append('<h4 class="blue bigger"><i class="fa fa-clock-o"></i> Procesando, por favor espere...</h4>');

        //body
        processbar.html('<div class="progress-bar progress-bar-yellow" style="width: 100%;"></div>');
        modalbody.html(processbar);

        //modal content

        modalcontent.html(modalheader).append(modalbody);
        //modal dialog
        modaldialog.html(modalcontent);
        divWait.html(modaldialog);
        divWait.keydown(function(e){
            //alert(e.keyCode)
            if (e.keyCode == 27) {
                return false;
            }
        });


        _processWait={
            show:function(){
                divWait.modal('show');
            },
            hide:function(){
                divWait.modal('hide');

            }
        };
        return _processWait;
    })();


}( jQuery ));   

 //funcion ajax para la peticion de eliminacion de un registro desde el datatables
$.ajaxDeleteRowGrid=function(config){
    var defaults={
       url:'',
       data:{},
       onSuccess: null,
                         
    };
    config=$.extend({},defaults,config);
    
    $.ajax({
       url: config.url,
       async:false,
       data: config.data,
       beforeSend:function(){
           //$.processWait.show();
       },
       success:function(response){
           if(response.status){
                if(response.message){
                            $.gritter.add({
                                    title: 'Información!',
                                    text: response.message,
                                    time:3000,
                                    class_name: 'gritter-success'
                            });
                }
                /*setTimeout(function(){
                            location.reload()
                        },500);*/
                if($.isFunction(config.onSuccess))config.onSuccess.call(response);        
            }else{
                $.gritter.add({
                    title: 'Error!',                                               
                    text: response.message,
                    time:3000,
                    class_name: 'gritter-danger'
                });
            }
       }
      
    });

    
}
