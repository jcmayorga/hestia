<?php

/* BussinessDefaultBundle:Ticketrelease:index.html.twig */
class __TwigTemplate_2cfff33d1d3d770f376d7ab5b277cc38aca5c0239ddc24659e5eaa56c209f913 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BussinessDefaultBundle:Ticketrelease:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        echo " 
    <div class=\"container\">    
        <div id=\"loginbox\" style=\"margin-top:50px;\" class=\"mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2\">                    
            <div class=\"panel panel-info\" >
                <div class=\"panel-heading\">
                    <div class=\"panel-title\">Apertura de comedor</div>

                </div>     

                <div style=\"padding-top:30px\" class=\"panel-body\" >

                    <div style=\"display:none\" id=\"login-alert\" class=\"alert alert-danger col-sm-12\"></div>

                    <form  class=\"form-horizontal\" role=\"form\" action=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_bussiness_default_ticketrelease_ticket");
        echo "\" method=\"post\" id=\"frmparameters\" data-toggle=\"validator\">

                        Comedor
                            <br>
                        <select name=\"dinner\" id=\"dinner\" required>
                            ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["objdinneruser"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["dinneruser"]) {
            // line 24
            echo "                                
                                <option value=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["dinneruser"], "dinner", array()), "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["dinneruser"], "dinner", array()), "name", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["dinneruser"], "dinner", array()), "location", array()), "html", null, true);
            echo "</option>
                          
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dinneruser'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "                        </select>
                        <br>
                        <br>


                        <div style=\"margin-left: 10px; text-align: center\" class=\"form-group\" >
                            <!-- Button -->
                            <button class=\"btn btn-success btn-lg\" type=\"submit\" id=\"btnaceptar\">Iniciar apertura comedor </button>

                        </div>







                </div>  
                </form>
            </div>  
        </div>

    </div>




";
    }

    // line 56
    public function block_javascripts($context, array $blocks = array())
    {
        // line 57
        echo "    <script type=\"text/javascript\">
        \$(function () {

        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "BussinessDefaultBundle:Ticketrelease:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 57,  105 => 56,  74 => 28,  61 => 25,  58 => 24,  54 => 23,  46 => 18,  29 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "BussinessDefaultBundle:Ticketrelease:index.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Bussiness/DefaultBundle/Resources/views/Ticketrelease/index.html.twig");
    }
}
