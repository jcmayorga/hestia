<?php

/* @AplicationDefault/Login/index.html.twig_ */
class __TwigTemplate_3fabce4086fdb93b093e2398fbe74ec3a32bbb15571c8c40313fcc1395f8dc4c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "@AplicationDefault/Login/index.html.twig_", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        echo " 
    ";
        // line 4
        if ((($context["noexistuser"] ?? null) == 1)) {
            // line 5
            echo "        <script type=\"text/javascript\">
            alert(\" El nombre de usuario o la contraseña introducidos no son correctos.Vuelva a intentarlo\");
        </script>
    ";
        }
        // line 9
        echo "    <div class=\"container\" style=\"margin-top:40px\">
        <div class=\"row\">
            <div class=\"col-sm-6 col-md-4 col-md-offset-4\">
                <div class=\"panel panel-primary\">
                    <div class=\"panel-heading\">
                        <strong> Inicia sesión para acceder</strong>
                    </div>
                    <div class=\"panel-body\">
                        <form name=\"frmlogin\"  id=\"frmlogin\" method=\"post\" action=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_aplication_default_login_validateinput");
        echo "\" data-toggle=\"validator\" >
                            <fieldset>
                                <div class=\"row\">
                                    <div class=\"center-block\" style=\"text-align: center;margin: 15px\">
                                        ";
        // line 22
        echo "                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-md-10  col-md-offset-1 \">
                                        <div class=\"form-group\">
                                            <div class=\"input-group\">
                                                <span class=\"input-group-addon\">
                                                    <i class=\"glyphicon glyphicon-user\"></i>
                                                </span> 
                                                <input class=\"form-control\" placeholder=\"Usuario\" name=\"identification\" id=\"identification\" type=\"text\" autofocus required>
                                            </div>
                                        </div>
                                        <div class=\"form-group\" >
                                            <div class=\"input-group\">
                                                <span class=\"input-group-addon\">
                                                    <i class=\"glyphicon glyphicon-lock\"></i>
                                                </span>
                                                <input class=\"form-control\" placeholder=\"Contraseña\" name=\"password\" id=\"password\" type=\"password\" value=\"\" required>
                                            </div>
                                        </div>


                                        <div class=\"form-group\" >
                                            <button type=\"submit\" class=\"btn btn-lg btn-primary btn-block\" onclick=\"validateinput()\">Iniciar sesión</button>


                                        </div>
                                        <div  style=\"padding: 1px; margin: 1px; font-size:95%; text-align: right\">
                                            ";
        // line 51
        echo "                                            Olvidó su contraseña?
                                            </a>
                                        </div>


                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"col-md-12 control\">
                            <div style=\"border-top: 1px solid#888; padding-top:15px; font-size:95%\" >
                                ";
        // line 68
        echo "                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "@AplicationDefault/Login/index.html.twig_";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 68,  88 => 51,  58 => 22,  51 => 17,  41 => 9,  35 => 5,  33 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@AplicationDefault/Login/index.html.twig_", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/Login/index.html.twig_");
    }
}
