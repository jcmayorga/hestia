<?php

/* AplicationDefaultBundle:Layout:base.html.twig */
class __TwigTemplate_7d7076805842e4a2655fa0104fe568d32668b45b00c1031117c95cd2331d7079 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'html_tag' => array($this, 'block_html_tag'),
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_tag' => array($this, 'block_body_tag'),
            'body_start' => array($this, 'block_body_start'),
            'body' => array($this, 'block_body'),
            'navbar' => array($this, 'block_navbar'),
            'container' => array($this, 'block_container'),
            'body_end_before_js' => array($this, 'block_body_end_before_js'),
            'foot_script' => array($this, 'block_foot_script'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
            'body_end' => array($this, 'block_body_end'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
";
        // line 3
        $this->displayBlock('html_tag', $context, $blocks);
        // line 6
        echo "
";
        // line 7
        $this->displayBlock('head', $context, $blocks);
        // line 88
        echo "
";
        // line 89
        $this->displayBlock('body_tag', $context, $blocks);
        // line 92
        echo "
";
        // line 93
        $this->displayBlock('body_start', $context, $blocks);
        // line 95
        echo "
";
        // line 96
        $this->displayBlock('body', $context, $blocks);
        // line 115
        echo "
";
        // line 116
        $this->displayBlock('body_end', $context, $blocks);
        // line 118
        echo "
</body>
</html>

";
    }

    // line 3
    public function block_html_tag($context, array $blocks = array())
    {
        // line 4
        echo "<html lang=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? null), "request", array()), "locale", array()), "html", null, true);
        echo "\">
";
    }

    // line 7
    public function block_head($context, array $blocks = array())
    {
        // line 8
        echo "    <head>
        <meta charset=\"UTF-8\"/>
        <title>";
        // line 10
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 15
        echo "        ";
        // line 16
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\"/>


        <!-- Bootstrap core JavaScript-->
        <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/jquery/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/bootstrap/js/bootstrap.bundle.min.js"), "html", null, true);
        echo "\"></script>



        <!-- Core plugin JavaScript-->
        <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/jquery-easing/jquery.easing.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Page level plugin JavaScript-->
        
        <!-- Custom scripts for all pages-->
        <script src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/themes/sbadmin/js/sb-admin.min.js"), "html", null, true);
        echo "\"></script>
        
        
        <!-- jvalidate -->                
        <script src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/jquery/js/jvalidate/jquery.validate.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/jquery/js/jvalidate/jquery-additional-methods.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/jquery/js/localization/validate.messages_es.js"), "html", null, true);
        echo "\"></script>
        
        
        <!--moment.js manejo de fecha y formatos de fecha-->

        <script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/jquery/js/moment.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/jquery/js/localization/moment.es.js"), "html", null, true);
        echo "\"></script>

            

         <!-- app js-->       

        <script src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/app/js/helpers.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/app/js/modal.form.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/app/js/modal.viewer.js"), "html", null, true);
        echo "\"></script>
         
        
         <!-- bootstrap-datepicker js-->       
        <script src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/bootstrap/js/bootstrap-datepicker.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/bootstrap/js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/bootstrap/js/bootstrap-timepicker.min.js"), "html", null, true);
        echo "\"></script>
                

         <!--Gritter-->       
         <script src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/jquery/js/jquery.gritter.min.js"), "html", null, true);
        echo "\"></script>
        
        
        <!-- Bootstrap core CSS-->
        <link href=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <!-- Custom fonts for this template-->
        <link href=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"
              type=\"text/css\">
        <!-- Page level plugin CSS-->
        
        <!-- Custom styles for this template-->
        <link href=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/themes/sbadmin/css/sb-admin.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        
        <!--Gritter-->       
         
         <link href=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/jquery/css/jquery.gritter.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">


         <!--main app css-->
         <link href=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/aplicationdefault/app/css/main.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">         
    </head>
    ";
        // line 83
        echo "    ";
        // line 84
        echo "    ";
        // line 85
        echo "    ";
        // line 86
        echo "    ";
    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
        echo "Hestia";
    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 13
        echo "
        ";
    }

    // line 89
    public function block_body_tag($context, array $blocks = array())
    {
        // line 90
        echo "<body>
";
    }

    // line 93
    public function block_body_start($context, array $blocks = array())
    {
    }

    // line 96
    public function block_body($context, array $blocks = array())
    {
        // line 97
        echo "    ";
        $this->displayBlock('navbar', $context, $blocks);
        // line 100
        echo "
    ";
        // line 101
        $this->displayBlock('container', $context, $blocks);
        // line 104
        echo "
    ";
        // line 105
        $this->displayBlock('body_end_before_js', $context, $blocks);
        // line 107
        echo "
    ";
        // line 108
        $this->displayBlock('foot_script', $context, $blocks);
        // line 113
        echo "
";
    }

    // line 97
    public function block_navbar($context, array $blocks = array())
    {
        // line 98
        echo "
    ";
    }

    // line 101
    public function block_container($context, array $blocks = array())
    {
        // line 102
        echo "
    ";
    }

    // line 105
    public function block_body_end_before_js($context, array $blocks = array())
    {
        // line 106
        echo "    ";
    }

    // line 108
    public function block_foot_script($context, array $blocks = array())
    {
        // line 109
        echo "        ";
        $this->displayBlock('foot_script_assetic', $context, $blocks);
        // line 112
        echo "    ";
    }

    // line 109
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 110
        echo "
        ";
    }

    // line 116
    public function block_body_end($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "AplicationDefaultBundle:Layout:base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  333 => 116,  328 => 110,  325 => 109,  321 => 112,  318 => 109,  315 => 108,  311 => 106,  308 => 105,  303 => 102,  300 => 101,  295 => 98,  292 => 97,  287 => 113,  285 => 108,  282 => 107,  280 => 105,  277 => 104,  275 => 101,  272 => 100,  269 => 97,  266 => 96,  261 => 93,  256 => 90,  253 => 89,  248 => 13,  245 => 12,  239 => 10,  235 => 86,  233 => 85,  231 => 84,  229 => 83,  224 => 80,  217 => 76,  209 => 71,  201 => 66,  196 => 64,  189 => 60,  182 => 56,  178 => 55,  174 => 54,  167 => 50,  163 => 49,  159 => 48,  150 => 42,  146 => 41,  138 => 36,  134 => 35,  130 => 34,  123 => 30,  116 => 26,  108 => 21,  104 => 20,  96 => 16,  94 => 15,  92 => 12,  87 => 10,  83 => 8,  80 => 7,  73 => 4,  70 => 3,  62 => 118,  60 => 116,  57 => 115,  55 => 96,  52 => 95,  50 => 93,  47 => 92,  45 => 89,  42 => 88,  40 => 7,  37 => 6,  35 => 3,  32 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AplicationDefaultBundle:Layout:base.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/Layout/base.html.twig");
    }
}
