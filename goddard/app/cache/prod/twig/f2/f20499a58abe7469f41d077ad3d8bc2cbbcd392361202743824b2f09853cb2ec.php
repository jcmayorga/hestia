<?php

/* AplicationDefaultBundle:Layout:flash.html.twig */
class __TwigTemplate_57c7e7401e436c79a4453be75b83798e148b4c62693ddbfceafc7ef4395de329 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 5
        echo "
";
        // line 20
        echo "
";
        // line 39
        echo "
";
    }

    // line 6
    public function getflash($__type__ = null, $__message__ = null, $__close__ = null, $__use_raw__ = null, $__class__ = null, $__domain__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "type" => $__type__,
            "message" => $__message__,
            "close" => $__close__,
            "use_raw" => $__use_raw__,
            "class" => $__class__,
            "domain" => $__domain__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 7
            echo "    <div class=\"alert";
            echo twig_escape_filter($this->env, ((($context["type"] ?? null)) ? ((" alert-" . ($context["type"] ?? null))) : ("")), "html", null, true);
            echo " fade in ";
            echo twig_escape_filter($this->env, ((array_key_exists("class", $context)) ? (_twig_default_filter(($context["class"] ?? null), "")) : ("")), "html", null, true);
            echo " ";
            if (((array_key_exists("close", $context)) ? (_twig_default_filter(($context["close"] ?? null), false)) : (false))) {
                echo "alert-dismissible";
            }
            echo "\">
    ";
            // line 8
            if (((array_key_exists("close", $context)) ? (_twig_default_filter(($context["close"] ?? null), false)) : (false))) {
                // line 9
                echo "        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">
            <i class=\"ace-icon fa fa-times\"></i>
        </button>
    ";
            }
            // line 13
            echo "    ";
            if (((array_key_exists("use_raw", $context)) ? (_twig_default_filter(($context["use_raw"] ?? null), false)) : (false))) {
                // line 14
                echo "        ";
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["message"] ?? null), array(), ((array_key_exists("domain", $context)) ? (_twig_default_filter(($context["domain"] ?? null), "messages")) : ("messages")));
                echo "
    ";
            } else {
                // line 16
                echo "        ";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["message"] ?? null), array(), ((array_key_exists("domain", $context)) ? (_twig_default_filter(($context["domain"] ?? null), "messages")) : ("messages"))), "html", null, true);
                echo "
    ";
            }
            // line 18
            echo "    </div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 21
    public function getadvanced_flash($__type__ = null, $__heading__ = null, $__message__ = null, $__close_tag__ = null, $__use_raw__ = null, $__class__ = null, $__domain__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "type" => $__type__,
            "heading" => $__heading__,
            "message" => $__message__,
            "close_tag" => $__close_tag__,
            "use_raw" => $__use_raw__,
            "class" => $__class__,
            "domain" => $__domain__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 22
            echo "    <div class=\"alert";
            echo twig_escape_filter($this->env, ((($context["type"] ?? null)) ? ((" alert-" . ($context["type"] ?? null))) : ("")), "html", null, true);
            echo " fade in ";
            echo twig_escape_filter($this->env, ((array_key_exists("class", $context)) ? (_twig_default_filter(($context["class"] ?? null), "")) : ("")), "html", null, true);
            echo " ";
            if (((array_key_exists("close_tag", $context)) ? (_twig_default_filter(($context["close_tag"] ?? null), false)) : (false))) {
                echo "alert-dismissible";
            }
            echo "\">
    ";
            // line 23
            if (((array_key_exists("close_tag", $context)) ? (_twig_default_filter(($context["close_tag"] ?? null), false)) : (false))) {
                // line 24
                echo "        ";
                if ((($context["close_tag"] ?? null) == "true")) {
                    // line 25
                    echo "            ";
                    $context["close_tag"] = "a";
                    // line 26
                    echo "        ";
                }
                // line 27
                echo "        <";
                echo twig_escape_filter($this->env, ($context["close_tag"] ?? null), "html", null, true);
                echo " class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\" ";
                if ((($context["close_tag"] ?? null) == "a")) {
                    echo "href=\"#\"";
                } elseif ((($context["close_tag"] ?? null) == "button")) {
                    echo "type=\"button\"";
                }
                echo ">&times;</";
                echo twig_escape_filter($this->env, ($context["close_tag"] ?? null), "html", null, true);
                echo ">
    ";
            }
            // line 29
            echo "    ";
            if (((array_key_exists("heading", $context)) ? (_twig_default_filter(($context["heading"] ?? null), false)) : (false))) {
                // line 30
                echo "    <h4 class=\"alert-heading\">";
                echo twig_escape_filter($this->env, ($context["heading"] ?? null), "html", null, true);
                echo "</h4>
    ";
            }
            // line 32
            echo "    ";
            if (((array_key_exists("use_raw", $context)) ? (_twig_default_filter(($context["use_raw"] ?? null), false)) : (false))) {
                // line 33
                echo "        ";
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["message"] ?? null), array(), ((array_key_exists("domain", $context)) ? (_twig_default_filter(($context["domain"] ?? null), "messages")) : ("messages")));
                echo "
    ";
            } else {
                // line 35
                echo "        ";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["message"] ?? null), array(), ((array_key_exists("domain", $context)) ? (_twig_default_filter(($context["domain"] ?? null), "messages")) : ("messages"))), "html", null, true);
                echo "
    ";
            }
            // line 37
            echo "    </div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 40
    public function getsession_flash($__close__ = null, $__use_raw__ = null, $__class__ = null, $__domain__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "close" => $__close__,
            "use_raw" => $__use_raw__,
            "class" => $__class__,
            "domain" => $__domain__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 41
            echo "    ";
            $context["flash_messages"] = $this;
            // line 42
            echo "
    ";
            // line 43
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? null), "session", array()), "flashbag", array()), "peekAll", array())) > 0)) {
                // line 44
                echo "        ";
                // line 45
                echo "        ";
                // line 46
                echo "        ";
                // line 47
                echo "
        ";
                // line 49
                echo "            ";
                // line 50
                echo "        ";
                // line 51
                echo "
        ";
                // line 52
                $context["flashes"] = $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? null), "session", array()), "flashbag", array()), "all", array(), "method");
                // line 53
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["flashes"] ?? null));
                foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                    // line 54
                    echo "            ";
                    if (($context["type"] == "fos_user_success")) {
                        // line 55
                        echo "                ";
                        $context["domain"] = "FOSUserBundle";
                        // line 56
                        echo "            ";
                    }
                    // line 57
                    echo "            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["messages"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                        // line 58
                        echo "                ";
                        // line 59
                        echo "                ";
                        echo $context["flash_messages"]->getflash($context["type"], $context["message"], ($context["close"] ?? null), ($context["use_raw"] ?? null), ($context["class"] ?? null), ($context["domain"] ?? null));
                        echo "
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 61
                    echo "        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 62
                echo "    ";
            }
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "AplicationDefaultBundle:Layout:flash.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 62,  262 => 61,  253 => 59,  251 => 58,  246 => 57,  243 => 56,  240 => 55,  237 => 54,  232 => 53,  230 => 52,  227 => 51,  225 => 50,  223 => 49,  220 => 47,  218 => 46,  216 => 45,  214 => 44,  212 => 43,  209 => 42,  206 => 41,  191 => 40,  175 => 37,  169 => 35,  163 => 33,  160 => 32,  154 => 30,  151 => 29,  137 => 27,  134 => 26,  131 => 25,  128 => 24,  126 => 23,  115 => 22,  97 => 21,  81 => 18,  75 => 16,  69 => 14,  66 => 13,  60 => 9,  58 => 8,  47 => 7,  30 => 6,  25 => 39,  22 => 20,  19 => 5,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AplicationDefaultBundle:Layout:flash.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/Layout/flash.html.twig");
    }
}
