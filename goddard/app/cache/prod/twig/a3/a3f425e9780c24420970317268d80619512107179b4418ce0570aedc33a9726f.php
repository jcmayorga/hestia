<?php

/* BussinessDefaultBundle:Service:edit.html.twig */
class __TwigTemplate_e81907077bbc7c088235eef2d701f9ee524396a787a60dedb5c824cff9f56f6b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AplicationDefaultBundle:Layout:view.html.twig", "BussinessDefaultBundle:Service:edit.html.twig", 2);
        $this->blocks = array(
            'view_content' => array($this, 'block_view_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AplicationDefaultBundle:Layout:view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_view_content($context, array $blocks = array())
    {
        // line 5
        echo "\t";
        if ((array_key_exists("form", $context) &&  !twig_test_empty(($context["form"] ?? null)))) {
            echo " 
      ";
            // line 6
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form');
            echo "
   ";
        }
    }

    public function getTemplateName()
    {
        return "BussinessDefaultBundle:Service:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 6,  31 => 5,  28 => 4,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "BussinessDefaultBundle:Service:edit.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Bussiness/DefaultBundle/Resources/views/Service/edit.html.twig");
    }
}
