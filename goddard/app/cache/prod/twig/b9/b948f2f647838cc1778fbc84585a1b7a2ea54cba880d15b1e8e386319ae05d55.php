<?php

/* KitpagesDataGridBundle:Paginator:bootstrap-paginator.html.twig */
class __TwigTemplate_0c5f1246edfa1a9d076a89c106004f9ccc13321d58a8c03eb55ceb2069828753 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'kit_grid_paginator' => array($this, 'block_kit_grid_paginator'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute(($context["paginator"] ?? null), "totalPageCount", array()) > 1)) {
            // line 2
            $this->displayBlock('kit_grid_paginator', $context, $blocks);
        }
    }

    public function block_kit_grid_paginator($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"pagination pagination-centered pagination-large kit-paginator-bootstrap\">
    <ul>
        <li ";
        // line 5
        if ( !$this->getAttribute(($context["paginator"] ?? null), "previousButtonPage", array())) {
            echo "class=\"disabled\"";
        }
        echo ">
            <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paginator"] ?? null), "getUrl", array(0 => "currentPage", 1 => $this->getAttribute(($context["paginator"] ?? null), "previousButtonPage", array())), "method"), "html", null, true);
        echo "\" title=\"previous\">&laquo;</a>
        </li>

        ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["paginator"] ?? null), "pageRange", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
            // line 10
            echo "            ";
            if (($context["page"] != $this->getAttribute(($context["paginator"] ?? null), "currentPage", array()))) {
                // line 11
                echo "                <li>
                    <a href=\"";
                // line 12
                echo twig_escape_filter($this->env, $this->getAttribute(($context["paginator"] ?? null), "getUrl", array(0 => "currentPage", 1 => $context["page"]), "method"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</a>
                </li>
            ";
            } else {
                // line 15
                echo "                <li class=\"active\">
                    <a href=\"#\">";
                // line 16
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</a>
                </li>
            ";
            }
            // line 19
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "
        <li ";
        // line 21
        if ( !$this->getAttribute(($context["paginator"] ?? null), "nextButtonPage", array())) {
            echo "class=\"disabled\"";
        }
        echo ">
            <a href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paginator"] ?? null), "getUrl", array(0 => "currentPage", 1 => $this->getAttribute(($context["paginator"] ?? null), "nextButtonPage", array())), "method"), "html", null, true);
        echo "\" title=\"next\">&raquo;</a>
        </li>
    </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "KitpagesDataGridBundle:Paginator:bootstrap-paginator.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 22,  81 => 21,  78 => 20,  72 => 19,  66 => 16,  63 => 15,  55 => 12,  52 => 11,  49 => 10,  45 => 9,  39 => 6,  33 => 5,  29 => 3,  22 => 2,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "KitpagesDataGridBundle:Paginator:bootstrap-paginator.html.twig", "/usr/share/nginx/html/hestia/goddard/vendor/kitpages/data-grid-bundle/Kitpages/DataGridBundle/Resources/views/Paginator/bootstrap-paginator.html.twig");
    }
}
