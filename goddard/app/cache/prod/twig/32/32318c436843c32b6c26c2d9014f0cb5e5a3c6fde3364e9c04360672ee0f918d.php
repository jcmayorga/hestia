<?php

/* AplicationDefaultBundle:Layout:layout.html.twig */
class __TwigTemplate_b63c969145fb8cfd1d3675af4e64c957834f7950d60d088c7a7bb76f3e7a18e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AplicationDefaultBundle:Layout:main_layout.html.twig", "AplicationDefaultBundle:Layout:layout.html.twig", 1);
        $this->blocks = array(
            'sidebar' => array($this, 'block_sidebar'),
            'navbar_content' => array($this, 'block_navbar_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AplicationDefaultBundle:Layout:main_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_sidebar($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_aplication_default_layout_sidebar"));
        echo "
";
    }

    // line 7
    public function block_navbar_content($context, array $blocks = array())
    {
        // line 8
        echo "     ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_aplication_default_layout_navbar"));
        echo "
 ";
    }

    public function getTemplateName()
    {
        return "AplicationDefaultBundle:Layout:layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  39 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AplicationDefaultBundle:Layout:layout.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/Layout/layout.html.twig");
    }
}
