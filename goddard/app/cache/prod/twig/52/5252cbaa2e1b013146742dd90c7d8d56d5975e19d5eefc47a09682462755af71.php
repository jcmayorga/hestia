<?php

/* AplicationDefaultBundle:Role:edit.html.twig */
class __TwigTemplate_41023d6ab34fe648c6527e208c5931b333791c40c44d0a4024fb835f9f4ee9a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AplicationDefaultBundle:Layout:view.html.twig", "AplicationDefaultBundle:Role:edit.html.twig", 2);
        $this->blocks = array(
            'view_content' => array($this, 'block_view_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AplicationDefaultBundle:Layout:view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_view_content($context, array $blocks = array())
    {
        // line 5
        echo "\t";
        if ((array_key_exists("form", $context) &&  !twig_test_empty(($context["form"] ?? null)))) {
            echo " 
      
      ";
            // line 7
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_start');
            echo "
       <div class=\"form-group row\">
                            <label class=\"col-sm-3 col-form-label\">
                                    ";
            // line 10
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "name", array()), 'label');
            echo "
                            </label>
                            <div class=\"col-sm-9\">                                
                                    ";
            // line 13
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "name", array()), 'widget');
            echo "                               
                            </div>
       </div>
        <div class=\"form-group row\">
                            <label class=\"col-sm-3 col-form-label\">
                                    ";
            // line 18
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "ctstatus", array()), 'label');
            echo "
                            </label>
                            <div class=\"col-sm-9\">                                
                                    ";
            // line 21
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "ctstatus", array()), 'widget');
            echo "                               
                            </div>
       </div>
      ";
            // line 24
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form');
            echo "
      ";
            // line 25
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_end');
            echo "
   ";
        }
    }

    public function getTemplateName()
    {
        return "AplicationDefaultBundle:Role:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 25,  69 => 24,  63 => 21,  57 => 18,  49 => 13,  43 => 10,  37 => 7,  31 => 5,  28 => 4,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AplicationDefaultBundle:Role:edit.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/Role/edit.html.twig");
    }
}
