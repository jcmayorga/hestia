<?php

/* KitpagesDataGridBundle:Grid:grid.html.twig */
class __TwigTemplate_f39b2fa9e897ee9e873773ae6ff551a97875f31024da7dd1e5f1709d3f6ae3fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'kit_grid_css' => array($this, 'block_kit_grid_css'),
            'kit_grid_main' => array($this, 'block_kit_grid_main'),
            'kit_grid_selector' => array($this, 'block_kit_grid_selector'),
            'kit_grid_filter' => array($this, 'block_kit_grid_filter'),
            'kit_grid_debug' => array($this, 'block_kit_grid_debug'),
            'kit_grid_thead' => array($this, 'block_kit_grid_thead'),
            'kit_grid_tbody' => array($this, 'block_kit_grid_tbody'),
            'kit_grid_row_class' => array($this, 'block_kit_grid_row_class'),
            'kit_grid_tbody_before_column' => array($this, 'block_kit_grid_tbody_before_column'),
            'kit_grid_no_data' => array($this, 'block_kit_grid_no_data'),
            'kit_grid_paginator' => array($this, 'block_kit_grid_paginator'),
            'kit_grid_javascript' => array($this, 'block_kit_grid_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('kit_grid_css', $context, $blocks);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('kit_grid_main', $context, $blocks);
    }

    // line 1
    public function block_kit_grid_css($context, array $blocks = array())
    {
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/kitpagesdatagrid/css/base.css"), "html", null, true);
        echo "\"/>";
    }

    // line 3
    public function block_kit_grid_main($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"kit-grid ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "getGridCssName", array()), "html", null, true);
        echo "\">
    ";
        // line 5
        $this->displayBlock('kit_grid_selector', $context, $blocks);
        // line 14
        echo "    ";
        $this->displayBlock('kit_grid_filter', $context, $blocks);
        // line 24
        echo "    ";
        $this->displayBlock('kit_grid_debug', $context, $blocks);
        // line 29
        echo "    ";
        $this->displayBlock("kit_grid_before_table", $context, $blocks);
        echo "
    <table>
        ";
        // line 31
        $this->displayBlock('kit_grid_thead', $context, $blocks);
        // line 50
        echo "        ";
        $this->displayBlock('kit_grid_tbody', $context, $blocks);
        // line 75
        echo "    </table>
    ";
        // line 76
        $this->displayBlock("kit_grid_after_table", $context, $blocks);
        echo "
    ";
        // line 77
        $this->displayBlock('kit_grid_paginator', $context, $blocks);
        // line 81
        echo "    ";
        $this->displayBlock(" kit_grid_after_paginator", $context, $blocks);
        echo "
</div>
";
        // line 83
        $this->displayBlock('kit_grid_javascript', $context, $blocks);
    }

    // line 5
    public function block_kit_grid_selector($context, array $blocks = array())
    {
        // line 6
        echo "        ";
        if (($this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "selectorList", array()) != null)) {
            // line 7
            echo "        <div class=\"kit-grid-selector\">
            ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "selectorList", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["selector"]) {
                // line 9
                echo "                <a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "getSelectorUrl", array(0 => $this->getAttribute($context["selector"], "field", array()), 1 => $this->getAttribute($context["selector"], "value", array())), "method"), "html", null, true);
                echo "\" class=\"kit-grid-selector ";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "getSelectorCssSelected", array(0 => $this->getAttribute($context["selector"], "field", array()), 1 => $this->getAttribute($context["selector"], "value", array())), "method"), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute($context["selector"], "label", array()), "html", null, true);
                echo "</a>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['selector'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 11
            echo "        </div>
        ";
        }
        // line 13
        echo "    ";
    }

    // line 14
    public function block_kit_grid_filter($context, array $blocks = array())
    {
        // line 15
        echo "    <div class=\"kit-grid-filter\">
        <form action=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "requestUri", array()), "html", null, true);
        echo "\" id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "_form\" method=\"GET\">
            <label for=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.Filter"), "html", null, true);
        echo "</label>
            <input type=\"text\" name=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "\" id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "\" size=\"10\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterValue", array()), "html", null, true);
        echo "\"/>
            <input type=\"submit\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.Apply"), "html", null, true);
        echo "\" name=\"submit\"/>
            <a href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath($this->getAttribute(($context["grid"] ?? null), "requestCurrentRoute", array()), $this->getAttribute(($context["grid"] ?? null), "requestCurrentRouteParams", array())), "html", null, true);
        echo "\" id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "_reset_button\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.Reset"), "html", null, true);
        echo "</a>
        </form>
    </div>
    ";
    }

    // line 24
    public function block_kit_grid_debug($context, array $blocks = array())
    {
        // line 25
        echo "        ";
        if ($this->getAttribute(($context["grid"] ?? null), "debugMode", array())) {
            // line 26
            echo "            ";
            echo $this->getAttribute(($context["grid"] ?? null), "dump", array(), "method");
            echo "
        ";
        }
        // line 28
        echo "    ";
    }

    // line 31
    public function block_kit_grid_thead($context, array $blocks = array())
    {
        // line 32
        echo "        <thead>
        <tr>
            ";
        // line 34
        $this->displayBlock("kit_grid_thead_before_column", $context, $blocks);
        echo "
            ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "fieldList", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 36
            echo "                ";
            if ($this->getAttribute($context["field"], "visible", array())) {
                // line 37
                echo "                    <th class=\"";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "getSortCssClass", array(0 => $this->getAttribute($context["field"], "fieldName", array())), "method"), "html", null, true);
                echo "\">
                        ";
                // line 38
                if ($this->getAttribute($context["field"], "sortable", array())) {
                    // line 39
                    echo "                            <a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "getSortUrl", array(0 => $this->getAttribute($context["field"], "fieldName", array())), "method"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($context["field"], "label", array())), "html", null, true);
                    echo "</a>
                        ";
                } else {
                    // line 41
                    echo "                            ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($context["field"], "label", array())), "html", null, true);
                    echo "
                        ";
                }
                // line 43
                echo "                    </th>
                ";
            }
            // line 45
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "            ";
        $this->displayBlock("kit_grid_thead_column", $context, $blocks);
        echo "
        </tr>
        </thead>
        ";
    }

    // line 50
    public function block_kit_grid_tbody($context, array $blocks = array())
    {
        // line 51
        echo "        <tbody>
        ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["grid"] ?? null), "itemList", array()));
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 53
            echo "            <tr class=\"";
            if ((($this->getAttribute($context["loop"], "index", array()) % 2) == 0)) {
                echo "kit-grid-even ";
            } else {
                echo "kit-grid-odd ";
            }
            echo " ";
            $this->displayBlock('kit_grid_row_class', $context, $blocks);
            echo "\">
                ";
            // line 54
            $this->displayBlock('kit_grid_tbody_before_column', $context, $blocks);
            // line 55
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "fieldList", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
                // line 56
                echo "                    ";
                if ($this->getAttribute($context["field"], "visible", array())) {
                    // line 57
                    echo "                        <td class=\"kit-grid-cell-";
                    echo twig_escape_filter($this->env, twig_replace_filter($this->getAttribute($context["field"], "fieldName", array()), array("." => "-")), "html", null, true);
                    echo "\">
                            ";
                    // line 58
                    if ($this->getAttribute($context["field"], "translatable", array())) {
                        // line 59
                        echo "                                ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["grid"] ?? null), "displayGridValue", array(0 => $context["item"], 1 => $context["field"]), "method")), "html", null, true);
                        echo "
                            ";
                    } else {
                        // line 61
                        echo "                                ";
                        echo $this->getAttribute(($context["grid"] ?? null), "displayGridValue", array(0 => $context["item"], 1 => $context["field"]), "method");
                        echo "
                            ";
                    }
                    // line 63
                    echo "                        </td>
                    ";
                }
                // line 65
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 66
            echo "                ";
            $this->displayBlock("kit_grid_tbody_column", $context, $blocks);
            echo "
            </tr>
        ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 69
            echo "            <tr>
                <td colspan=\"";
            // line 70
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "fieldList", array())), "html", null, true);
            echo "\" class=\"kit-grid-no-data\">";
            $this->displayBlock('kit_grid_no_data', $context, $blocks);
            echo "</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "        </tbody>
        ";
    }

    // line 53
    public function block_kit_grid_row_class($context, array $blocks = array())
    {
    }

    // line 54
    public function block_kit_grid_tbody_before_column($context, array $blocks = array())
    {
    }

    // line 70
    public function block_kit_grid_no_data($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.No_Data_Found"), "html", null, true);
    }

    // line 77
    public function block_kit_grid_paginator($context, array $blocks = array())
    {
        // line 78
        echo "        ";
        $this->loadTemplate("KitpagesDataGridBundle:Grid:grid.html.twig", "KitpagesDataGridBundle:Grid:grid.html.twig", 78, "158418741")->display(array_merge($context, array("paginator" => $this->getAttribute(($context["grid"] ?? null), "paginator", array()))));
        // line 80
        echo "    ";
    }

    // line 83
    public function block_kit_grid_javascript($context, array $blocks = array())
    {
        // line 84
        echo "    ";
        $this->loadTemplate("KitpagesDataGridBundle:Grid:javascript.html.twig", "KitpagesDataGridBundle:Grid:grid.html.twig", 84)->display($context);
    }

    public function getTemplateName()
    {
        return "KitpagesDataGridBundle:Grid:grid.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  383 => 84,  380 => 83,  376 => 80,  373 => 78,  370 => 77,  364 => 70,  359 => 54,  354 => 53,  349 => 73,  338 => 70,  335 => 69,  318 => 66,  312 => 65,  308 => 63,  302 => 61,  296 => 59,  294 => 58,  289 => 57,  286 => 56,  281 => 55,  279 => 54,  268 => 53,  250 => 52,  247 => 51,  244 => 50,  235 => 46,  229 => 45,  225 => 43,  219 => 41,  211 => 39,  209 => 38,  204 => 37,  201 => 36,  197 => 35,  193 => 34,  189 => 32,  186 => 31,  182 => 28,  176 => 26,  173 => 25,  170 => 24,  158 => 20,  154 => 19,  146 => 18,  140 => 17,  134 => 16,  131 => 15,  128 => 14,  124 => 13,  120 => 11,  107 => 9,  103 => 8,  100 => 7,  97 => 6,  94 => 5,  90 => 83,  84 => 81,  82 => 77,  78 => 76,  75 => 75,  72 => 50,  70 => 31,  64 => 29,  61 => 24,  58 => 14,  56 => 5,  51 => 4,  48 => 3,  40 => 1,  36 => 3,  33 => 2,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "KitpagesDataGridBundle:Grid:grid.html.twig", "/usr/share/nginx/html/hestia/goddard/vendor/kitpages/data-grid-bundle/Kitpages/DataGridBundle/Resources/views/Grid/grid.html.twig");
    }
}


/* KitpagesDataGridBundle:Grid:grid.html.twig */
class __TwigTemplate_f39b2fa9e897ee9e873773ae6ff551a97875f31024da7dd1e5f1709d3f6ae3fd_158418741 extends Twig_Template
{
    protected function doGetParent(array $context)
    {
        // line 78
        return $this->loadTemplate($this->getAttribute($this->getAttribute(($context["kitpages_data_grid"] ?? null), "paginator", array()), "default_twig", array()), "KitpagesDataGridBundle:Grid:grid.html.twig", 78);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "KitpagesDataGridBundle:Grid:grid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  418 => 78,  383 => 84,  380 => 83,  376 => 80,  373 => 78,  370 => 77,  364 => 70,  359 => 54,  354 => 53,  349 => 73,  338 => 70,  335 => 69,  318 => 66,  312 => 65,  308 => 63,  302 => 61,  296 => 59,  294 => 58,  289 => 57,  286 => 56,  281 => 55,  279 => 54,  268 => 53,  250 => 52,  247 => 51,  244 => 50,  235 => 46,  229 => 45,  225 => 43,  219 => 41,  211 => 39,  209 => 38,  204 => 37,  201 => 36,  197 => 35,  193 => 34,  189 => 32,  186 => 31,  182 => 28,  176 => 26,  173 => 25,  170 => 24,  158 => 20,  154 => 19,  146 => 18,  140 => 17,  134 => 16,  131 => 15,  128 => 14,  124 => 13,  120 => 11,  107 => 9,  103 => 8,  100 => 7,  97 => 6,  94 => 5,  90 => 83,  84 => 81,  82 => 77,  78 => 76,  75 => 75,  72 => 50,  70 => 31,  64 => 29,  61 => 24,  58 => 14,  56 => 5,  51 => 4,  48 => 3,  40 => 1,  36 => 3,  33 => 2,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "KitpagesDataGridBundle:Grid:grid.html.twig", "/usr/share/nginx/html/hestia/goddard/vendor/kitpages/data-grid-bundle/Kitpages/DataGridBundle/Resources/views/Grid/grid.html.twig");
    }
}
