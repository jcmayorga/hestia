<?php

/* AplicationDefaultBundle:Role:permission.html.twig */
class __TwigTemplate_43956b5616a1f9f7a591ab8577db3d251727cd2f5dcf55a6af8bc0015728dcd2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AplicationDefaultBundle:Layout:view.html.twig", "AplicationDefaultBundle:Role:permission.html.twig", 2);
        $this->blocks = array(
            'view_content' => array($this, 'block_view_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AplicationDefaultBundle:Layout:view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_view_content($context, array $blocks = array())
    {
        // line 5
        echo "\t<form>
\t\t<div class=\"table-responsive\">
\t\t<table class=\"table table-bordered table-hover\">\t\t\t
\t\t\t<thead class=\"thead-dark\">
\t\t\t\t<tr>
\t\t\t\t\t<th scope=\"col\">Menu</th>
\t\t\t\t\t<th scope=\"col\">Visualizar</th>
\t\t\t\t\t<th scope=\"col\">Agregar</th>
\t\t\t\t\t<th scope=\"col\">Editar</th>
\t\t\t\t\t<th scope=\"col\">Eliminar</th>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["menusrole"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 19
            echo "\t\t\t\t    ";
            $context["menu_id"] = $this->getAttribute($context["item"], "menu_id", array(), "array");
            // line 20
            echo "\t\t\t\t    <tr>
\t\t\t\t    \t<th scope=\"row\">";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "menu_title", array(), "array"), "html", null, true);
            echo "</th>
\t\t\t\t    \t<td class=\"text-center\"><input name=\"permissions[";
            // line 22
            echo twig_escape_filter($this->env, ($context["menu_id"] ?? null), "html", null, true);
            echo "][view]\" value=1 type='checkbox' ";
            echo (($this->getAttribute($context["item"], "view", array(), "array")) ? ("checked=true") : (null));
            echo "/></td>
\t\t\t\t    \t<td class=\"text-center\"><input name=\"permissions[";
            // line 23
            echo twig_escape_filter($this->env, ($context["menu_id"] ?? null), "html", null, true);
            echo "][add]\" value=1 type='checkbox' ";
            echo (($this->getAttribute($context["item"], "add", array(), "array")) ? ("checked=true") : (null));
            echo "/></td>
\t\t\t\t    \t<td class=\"text-center\"><input name=\"permissions[";
            // line 24
            echo twig_escape_filter($this->env, ($context["menu_id"] ?? null), "html", null, true);
            echo "][edit]\" value=1 type='checkbox' ";
            echo (($this->getAttribute($context["item"], "edit", array(), "array")) ? ("checked=true") : (null));
            echo "/></td>
\t\t\t\t    \t<td class=\"text-center\"><input name=\"permissions[";
            // line 25
            echo twig_escape_filter($this->env, ($context["menu_id"] ?? null), "html", null, true);
            echo "][delete]\" value=1 type='checkbox' ";
            echo (($this->getAttribute($context["item"], "delete", array(), "array")) ? ("checked=true") : (null));
            echo "/></td>
\t\t\t\t    </tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "\t\t\t</tbody>
\t\t</table>
\t</div>
\t</form>

";
    }

    public function getTemplateName()
    {
        return "AplicationDefaultBundle:Role:permission.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 28,  78 => 25,  72 => 24,  66 => 23,  60 => 22,  56 => 21,  53 => 20,  50 => 19,  46 => 18,  31 => 5,  28 => 4,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AplicationDefaultBundle:Role:permission.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/Role/permission.html.twig");
    }
}
