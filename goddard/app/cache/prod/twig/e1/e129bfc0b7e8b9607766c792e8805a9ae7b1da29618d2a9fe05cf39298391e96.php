<?php

/* AplicationDefaultBundle:Layout:sidebar.html.twig */
class __TwigTemplate_196ff80a6c3f973f1f6d4c0d9c90afa71a9a0e1abbb5d1bc99037efa39aeada2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"navbar-nav navbar-sidenav\" id=\"exampleAccordion\" style=\"overflow-y:auto;overflow-x:hidden\">
    <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Inicio\">
        <a class=\"nav-link\" href=\"";
        // line 3
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_aplication_default_layout");
        echo "\">
            <i class=\"fa fa-fw fa-dashboard\"></i>
            <span class=\"nav-link-text\">Inicio</span>
        </a>
    </li>
    ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["arrayreposmenurole"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["objmenurole"]) {
            // line 9
            echo "        ";
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["objmenurole"], "menu", array()), "cttype", array()), "id", array()) == 522)) {
                // line 10
                echo "            <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
                echo twig_escape_filter($this->env, twig_trim_filter($this->getAttribute($this->getAttribute($context["objmenurole"], "menu", array()), "title", array())), "html", null, true);
                echo "\">
                <a class=\"nav-link\" href=\"";
                // line 11
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath(twig_trim_filter($this->getAttribute($this->getAttribute($context["objmenurole"], "menu", array()), "url", array())));
                echo "\"
                   target=\"";
                // line 12
                echo twig_escape_filter($this->env, twig_trim_filter($this->getAttribute($this->getAttribute($context["objmenurole"], "menu", array()), "target", array())), "html", null, true);
                echo "\">
                    <i class=\"fa fa-fw fa-list-ul\"></i>
                    <span class=\"nav-link-text\">";
                // line 14
                echo twig_escape_filter($this->env, twig_trim_filter($this->getAttribute($this->getAttribute($context["objmenurole"], "menu", array()), "title", array())), "html", null, true);
                echo "</span>
                </a>
            </li>
            ";
                // line 18
                echo "            ";
                // line 19
                echo "            ";
                // line 20
                echo "        ";
            }
            // line 21
            echo "        ";
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["objmenurole"], "menu", array()), "cttype", array()), "id", array()) == 523)) {
                // line 22
                echo "            <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\"
                title=\"";
                // line 23
                echo twig_escape_filter($this->env, twig_trim_filter($this->getAttribute($this->getAttribute($context["objmenurole"], "menu", array()), "description", array())), "html", null, true);
                echo "\">
                <a class=\"nav-link\" href=\"";
                // line 24
                echo twig_escape_filter($this->env, twig_trim_filter($this->getAttribute($this->getAttribute($context["objmenurole"], "menu", array()), "url", array())), "html", null, true);
                echo "?user=";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["sessionuser"] ?? null), "id", array()), "html", null, true);
                echo "\"
                   target=\"";
                // line 25
                echo twig_escape_filter($this->env, twig_trim_filter($this->getAttribute($this->getAttribute($context["objmenurole"], "menu", array()), "target", array())), "html", null, true);
                echo "\">
                    <i class=\"fa fa-fw fa-list-ul\"></i>
                    <span class=\"nav-link-text\">";
                // line 27
                echo twig_escape_filter($this->env, twig_trim_filter($this->getAttribute($this->getAttribute($context["objmenurole"], "menu", array()), "title", array())), "html", null, true);
                echo "</span>
                </a>
            </li>
            ";
                // line 31
                echo "            ";
                // line 32
                echo "            ";
                // line 33
                echo "        ";
            }
            // line 34
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objmenurole'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "</ul>
";
    }

    public function getTemplateName()
    {
        return "AplicationDefaultBundle:Layout:sidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 35,  99 => 34,  96 => 33,  94 => 32,  92 => 31,  86 => 27,  81 => 25,  75 => 24,  71 => 23,  68 => 22,  65 => 21,  62 => 20,  60 => 19,  58 => 18,  52 => 14,  47 => 12,  43 => 11,  38 => 10,  35 => 9,  31 => 8,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AplicationDefaultBundle:Layout:sidebar.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/Layout/sidebar.html.twig");
    }
}
