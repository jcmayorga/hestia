<?php

/* AplicationDefaultBundle:Role:index.html.twig */
class __TwigTemplate_c6483f642798ce157d541ce502c4288d77bd695d731c8346999bacdc37b96936 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AplicationDefaultBundle:Layout:layout.html.twig", "AplicationDefaultBundle:Role:index.html.twig", 1);
        $this->blocks = array(
            'container_fluid' => array($this, 'block_container_fluid'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AplicationDefaultBundle:Layout:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_container_fluid($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"container-fluid\">
        ";
        // line 5
        $this->displayBlock('breadcrumb', $context, $blocks);
        // line 15
        echo "        <div class=\"row\">
            <div class=\"col-12\">
                ";
        // line 17
        $this->loadTemplate("AplicationDefaultBundle:Role:index.html.twig", "AplicationDefaultBundle:Role:index.html.twig", 17, "350160357")->display(array_merge($context, array("grid" => ($context["grid"] ?? null))));
        // line 39
        echo "            </div>
        </div>
    </div>
    <!-- /.container-fluid-->
";
    }

    // line 5
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 6
        echo "            ";
        // line 7
        echo "            <ol class=\"breadcrumb\">
                <li class=\"breadcrumb-item\">
                    <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_aplication_default_layout");
        echo "\">Seguridades</a>
                </li>
                <li class=\"breadcrumb-item active\">Roles</li>
            </ol>
            ";
        // line 14
        echo "        ";
    }

    // line 47
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 48
        echo "<script type=\"text/javascript\" charset=\"utf-8\">
\t\$(function(){
\t\t\$(\"#btnNew\").click(function(){      
      openAddForm();
    });
\t});
  
\tfunction openAddForm(){

\t\tvar div=\$(\"<div>\",{id:'divAdd'});
\t\t\$(div).modalForm({
\t\t\turl:\"";
        // line 59
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("aplication_default_role_add");
        echo "\",
\t\t\tautoOpen:true,
\t\t\talign:'center',
      width:'35%',
\t\t\turlSubmit: \"";
        // line 63
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("aplication_default_role_save");
        echo "\",
      onSuccess: function(){
            setTimeout(function(){
                            location.reload()
                        },500);
      }
\t\t});
\t}

\tfunction openEditForm(id){
       var div=\$(\"<div>\",{id:'divEdit'});
      \$(div).modalForm({
        url:\"";
        // line 75
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("aplication_default_role_edit");
        echo "/\"+id,
        autoOpen:true,
        align:'center',
        width:'35%',
        urlSubmit: \"";
        // line 79
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("aplication_default_role_save");
        echo "/\"+id,
        onSuccess: function(){
            setTimeout(function(){
                            location.reload()
                        },500);
        }
      });   
\t}
  function deleteRecord(id){
       if(confirm('Está seguro de eliminar el registro?')){
           \$.ajaxDeleteRowGrid({
               url:\"";
        // line 90
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("aplication_default_role_delete");
        echo "/\"+id,
               onSuccess: function(){
                  setTimeout(function(){
                                  location.reload()
                              },500);
              }
           });
       }
  }

  function assignPermission(id){
      var div=\$(\"<div>\",{id:'divPerm'});
      \$(div).modalForm({
        url:\"";
        // line 103
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("aplication_default_role_permission");
        echo "/\"+id,
        autoOpen:true,
        align:'center',
        urlSubmit: \"";
        // line 106
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("aplication_default_role_permission_save");
        echo "/\"+id,
        width:'50%',
        /*onSuccess: function(){
            setTimeout(function(){
                            location.reload()
                        },500);
        }*/
      });   
  }

</script>
";
    }

    public function getTemplateName()
    {
        return "AplicationDefaultBundle:Role:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 106,  147 => 103,  131 => 90,  117 => 79,  110 => 75,  95 => 63,  88 => 59,  75 => 48,  72 => 47,  68 => 14,  61 => 9,  57 => 7,  55 => 6,  52 => 5,  44 => 39,  42 => 17,  38 => 15,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AplicationDefaultBundle:Role:index.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/Role/index.html.twig");
    }
}


/* AplicationDefaultBundle:Role:index.html.twig */
class __TwigTemplate_c6483f642798ce157d541ce502c4288d77bd695d731c8346999bacdc37b96936_350160357 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'kit_grid_filter' => array($this, 'block_kit_grid_filter'),
            'kit_grid_thead_column' => array($this, 'block_kit_grid_thead_column'),
            'kit_grid_tbody_column' => array($this, 'block_kit_grid_tbody_column'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 17
        return $this->loadTemplate($this->getAttribute($this->getAttribute(($context["kitpages_data_grid"] ?? null), "grid", array()), "default_twig", array()), "AplicationDefaultBundle:Role:index.html.twig", 17);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 19
    public function block_kit_grid_filter($context, array $blocks = array())
    {
        // line 20
        echo "                    <div class=\"input-group\">
                      <button type=\"button\" id=\"btnNew\" class=\"btn btn-default\">Nuevo</button>
                    </div>
                    <hr/>
                    ";
        // line 24
        $this->displayParentBlock("kit_grid_filter", $context, $blocks);
        echo "
                  ";
    }

    // line 27
    public function block_kit_grid_thead_column($context, array $blocks = array())
    {
        // line 28
        echo "                      <th >Acción</th>
                  ";
    }

    // line 31
    public function block_kit_grid_tbody_column($context, array $blocks = array())
    {
        // line 32
        echo "                      <td>
                        <a href=\"javascript:void(0)\" onclick=\"openEditForm(";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute(($context["item"] ?? null), "role.id", array(), "array"), "html", null, true);
        echo ")\"
                        title=\"Editar\"><i class=\"fa fa-pencil bigger-125 fa-fw\" ></i></a>&nbsp;|&nbsp;<a href=\"javascript:void(0)\" onclick=\"assignPermission(";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute(($context["item"] ?? null), "role.id", array(), "array"), "html", null, true);
        echo ")\" title=\"Permisos\"><i class=\"fa fa-lock bigger-125 fa-fw\"></i></a>&nbsp;|&nbsp;<a href=\"javascript:void(0)\" onclick=\"deleteRecord(";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["item"] ?? null), "role.id", array(), "array"), "html", null, true);
        echo ")\" title=\"Eliminar\"><i class=\"fa fa-trash bigger-125 fa-fw\"></i></a></td>
                      
                  ";
    }

    public function getTemplateName()
    {
        return "AplicationDefaultBundle:Role:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 34,  253 => 33,  250 => 32,  247 => 31,  242 => 28,  239 => 27,  233 => 24,  227 => 20,  224 => 19,  215 => 17,  153 => 106,  147 => 103,  131 => 90,  117 => 79,  110 => 75,  95 => 63,  88 => 59,  75 => 48,  72 => 47,  68 => 14,  61 => 9,  57 => 7,  55 => 6,  52 => 5,  44 => 39,  42 => 17,  38 => 15,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AplicationDefaultBundle:Role:index.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/Role/index.html.twig");
    }
}
