<?php

/* KitpagesDataGridBundle:Grid:javascript_content.html.twig */
class __TwigTemplate_425e7d030eca61ff7b3be6907f33fd011fe025cc280187741dc3be78eca9ae0f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    (function(\$) {
        \"use strict\";

        var insertParamInQueryString = function (key, value) {
            key = encodeURIComponent(key);
            value = encodeURIComponent(value);

            var query = document.location.search.substr(1),
                kvp = query.length > 0 ? query.split('&') : [],
                i = kvp.length,
                x;

            while(i--) {
                x = kvp[i].split('=');
                if (x[0] == key) {
                    x[1] = value;
                    kvp[i] = x.join('=');
                    break;
                }
            }

            if(i < 0) {
                kvp[kvp.length] = [key, value].join('=');
            }

            //this will reload the page, it's likely better to store this until finished
            document.location.search = kvp.join('&');
        };

        \$('#";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "_form').submit(function(e) {
            e.preventDefault();
            var value = \$('#";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "').val();
            insertParamInQueryString('";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "', value);
        });
    })(jQuery);
";
    }

    public function getTemplateName()
    {
        return "KitpagesDataGridBundle:Grid:javascript_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 33,  55 => 32,  50 => 30,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "KitpagesDataGridBundle:Grid:javascript_content.html.twig", "/usr/share/nginx/html/hestia/goddard/vendor/kitpages/data-grid-bundle/Kitpages/DataGridBundle/Resources/views/Grid/javascript_content.html.twig");
    }
}
