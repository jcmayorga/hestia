<?php

/* AdministrationDefaultBundle:Contract:index.html.twig */
class __TwigTemplate_55e346402d0e2310be8463e530f16ed2e36c245fded68092fbdecb40eb17bc7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AplicationDefaultBundle:Layout:layout.html.twig", "AdministrationDefaultBundle:Contract:index.html.twig", 1);
        $this->blocks = array(
            'container_fluid' => array($this, 'block_container_fluid'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AplicationDefaultBundle:Layout:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_container_fluid($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"container-fluid\">
        ";
        // line 5
        $this->displayBlock('breadcrumb', $context, $blocks);
        // line 15
        echo "        <div class=\"row\">
            <div class=\"col-12\">
                ";
        // line 17
        $this->loadTemplate("AdministrationDefaultBundle:Contract:index.html.twig", "AdministrationDefaultBundle:Contract:index.html.twig", 17, "1932620904")->display(array_merge($context, array("grid" => ($context["grid"] ?? null))));
        // line 39
        echo "            </div>
        </div>
    </div>
    <!-- /.container-fluid-->
";
    }

    // line 5
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 6
        echo "            ";
        // line 7
        echo "            <ol class=\"breadcrumb\">
                <li class=\"breadcrumb-item\">
                    <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_contract_index");
        echo "\">Administración</a>
                </li>
                <li class=\"breadcrumb-item active\">Contratos</li>
            </ol>
            ";
        // line 14
        echo "        ";
    }

    // line 44
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 45
        echo "    <script type=\"text/javascript\" charset=\"utf-8\">
        \$(function () {
            \$(\"#btnNew\").click(function () {
                openAddForm();
            });
        });

        function openAddForm() {

            var div = \$(\"<div>\", {id: 'divAdd'});
            \$(div).modalForm({
                url: \"";
        // line 56
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_contract_add");
        echo "\",
                autoOpen: true,
                align: 'center',
                width: '50%',
                urlSubmit: \"";
        // line 60
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_contract_save");
        echo "\",
                onSuccess: function () {
                    setTimeout(function () {
                        location.reload()
                    }, 500);
                }
            });
        }

        function openEditForm(id) {
            var div = \$(\"<div>\", {id: 'divEdit'});
            \$(div).modalForm({
                url: \"";
        // line 72
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_contract_edit");
        echo "/\" + id,
                autoOpen: true,
                align: 'center',
                width: '50%',
                urlSubmit: \"";
        // line 76
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_contract_save");
        echo "/\" + id,
                onSuccess: function () {
                    setTimeout(function () {
                        location.reload()
                    }, 500);
                }
            });
        }

        function deleteRecord(id) {
            if (confirm('Está seguro de eliminar el registro?')) {
                \$.ajaxDeleteRowGrid({
                    url: \"";
        // line 88
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_contract_delete");
        echo "/\" + id,
                    onSuccess: function () {
                        setTimeout(function () {
                            location.reload()
                        }, 500);
                    }
                });
            }
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "AdministrationDefaultBundle:Contract:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 88,  117 => 76,  110 => 72,  95 => 60,  88 => 56,  75 => 45,  72 => 44,  68 => 14,  61 => 9,  57 => 7,  55 => 6,  52 => 5,  44 => 39,  42 => 17,  38 => 15,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AdministrationDefaultBundle:Contract:index.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Administration/DefaultBundle/Resources/views/Contract/index.html.twig");
    }
}


/* AdministrationDefaultBundle:Contract:index.html.twig */
class __TwigTemplate_55e346402d0e2310be8463e530f16ed2e36c245fded68092fbdecb40eb17bc7a_1932620904 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'kit_grid_filter' => array($this, 'block_kit_grid_filter'),
            'kit_grid_thead_column' => array($this, 'block_kit_grid_thead_column'),
            'kit_grid_tbody_column' => array($this, 'block_kit_grid_tbody_column'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 17
        return $this->loadTemplate($this->getAttribute($this->getAttribute(($context["kitpages_data_grid"] ?? null), "grid", array()), "default_twig", array()), "AdministrationDefaultBundle:Contract:index.html.twig", 17);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 19
    public function block_kit_grid_filter($context, array $blocks = array())
    {
        // line 20
        echo "                        <div class=\"input-group\">
                            <button type=\"button\" id=\"btnNew\" class=\"btn btn-default\">Nuevo</button>
                        </div>
                        <hr/>
                        ";
        // line 24
        $this->displayParentBlock("kit_grid_filter", $context, $blocks);
        echo "
                    ";
    }

    // line 27
    public function block_kit_grid_thead_column($context, array $blocks = array())
    {
        // line 28
        echo "                        <th>Acción</th>
                    ";
    }

    // line 31
    public function block_kit_grid_tbody_column($context, array $blocks = array())
    {
        // line 32
        echo "                        <td>
                            <a href=\"javascript:void(0)\" onclick=\"openEditForm(";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute(($context["item"] ?? null), "contract.id", array(), "array"), "html", null, true);
        echo ")\" title=\"Editar\"><i class=\"fa fa-pencil bigger-125 fa-fw\" ></i></a>&nbsp;|&nbsp;
                            <a href=\"javascript:void(0)\" onclick=\"deleteRecord(";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute(($context["item"] ?? null), "contract.id", array(), "array"), "html", null, true);
        echo ")\" title=\"Eliminar\"><i class=\"fa fa-trash bigger-125 fa-fw\" ></i></a>
                        </td>
                    ";
    }

    public function getTemplateName()
    {
        return "AdministrationDefaultBundle:Contract:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 34,  231 => 33,  228 => 32,  225 => 31,  220 => 28,  217 => 27,  211 => 24,  205 => 20,  202 => 19,  193 => 17,  132 => 88,  117 => 76,  110 => 72,  95 => 60,  88 => 56,  75 => 45,  72 => 44,  68 => 14,  61 => 9,  57 => 7,  55 => 6,  52 => 5,  44 => 39,  42 => 17,  38 => 15,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AdministrationDefaultBundle:Contract:index.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Administration/DefaultBundle/Resources/views/Contract/index.html.twig");
    }
}
