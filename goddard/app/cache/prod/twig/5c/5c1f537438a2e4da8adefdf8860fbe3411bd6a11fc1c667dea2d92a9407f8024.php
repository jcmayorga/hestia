<?php

/* KitpagesDataGridBundle:Paginator:paginator.html.twig */
class __TwigTemplate_d72dcee1c9a2a1535c8c60dae4c5bb153bf2500d125e6c3d2768aace1ce587ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'kit_grid_paginator' => array($this, 'block_kit_grid_paginator'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute(($context["paginator"] ?? null), "totalPageCount", array()) > 1)) {
            // line 2
            $this->displayBlock('kit_grid_paginator', $context, $blocks);
        }
    }

    public function block_kit_grid_paginator($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"kit-grid-paginator\">
    ";
        // line 4
        if ($this->getAttribute(($context["paginator"] ?? null), "previousButtonPage", array())) {
            // line 5
            echo "        <span class=\"kit-cms-paginator-previous\">
            <a href=\"";
            // line 6
            echo twig_escape_filter($this->env, $this->getAttribute(($context["paginator"] ?? null), "getUrl", array(0 => "currentPage", 1 => $this->getAttribute(($context["paginator"] ?? null), "previousButtonPage", array())), "method"), "html", null, true);
            echo "\" title=\"previous\">&lt;</a>
        </span>
    ";
        }
        // line 9
        echo "
    ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["paginator"] ?? null), "pageRange", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
            // line 11
            echo "    ";
            if (($context["page"] != $this->getAttribute(($context["paginator"] ?? null), "currentPage", array()))) {
                // line 12
                echo "            <span class=\"kit-grid-paginator-page\">
                <a href=\"";
                // line 13
                echo twig_escape_filter($this->env, $this->getAttribute(($context["paginator"] ?? null), "getUrl", array(0 => "currentPage", 1 => $context["page"]), "method"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</a>
            </span>
    ";
            } else {
                // line 16
                echo "    <span class=\"kit-grid-paginator-current\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</span>
    ";
            }
            // line 18
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
    ";
        // line 20
        if ($this->getAttribute(($context["paginator"] ?? null), "nextButtonPage", array())) {
            // line 21
            echo "        <span class=\"kkit-grid-paginator-next\">
            <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute(($context["paginator"] ?? null), "getUrl", array(0 => "currentPage", 1 => $this->getAttribute(($context["paginator"] ?? null), "nextButtonPage", array())), "method"), "html", null, true);
            echo "\" title=\"next\">&gt;</a>
        </span>
    ";
        }
        // line 25
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "KitpagesDataGridBundle:Paginator:paginator.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 25,  84 => 22,  81 => 21,  79 => 20,  76 => 19,  70 => 18,  64 => 16,  56 => 13,  53 => 12,  50 => 11,  46 => 10,  43 => 9,  37 => 6,  34 => 5,  32 => 4,  29 => 3,  22 => 2,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "KitpagesDataGridBundle:Paginator:paginator.html.twig", "/usr/share/nginx/html/hestia/goddard/vendor/kitpages/data-grid-bundle/Kitpages/DataGridBundle/Resources/views/Paginator/paginator.html.twig");
    }
}
