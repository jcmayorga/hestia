<?php

/* AdministrationDefaultBundle:Dinner:add.html.twig */
class __TwigTemplate_f866d35cac92b78179020851755a631619605e1e99aba932eaa4510e3e95a7a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AplicationDefaultBundle:Layout:view.html.twig", "AdministrationDefaultBundle:Dinner:add.html.twig", 1);
        $this->blocks = array(
            'view_content' => array($this, 'block_view_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AplicationDefaultBundle:Layout:view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_view_content($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        if ((array_key_exists("form", $context) &&  !twig_test_empty(($context["form"] ?? null)))) {
            // line 5
            echo "        ";
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_start');
            echo "
        <div class=\"form-group row\">
            <label class=\"col-sm-3 col-form-label\">
                ";
            // line 8
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "name", array()), 'label');
            echo "
            </label>
            <div class=\"col-sm-9\">
                ";
            // line 11
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "name", array()), 'widget');
            echo "
            </div>
        </div>
        <div class=\"form-group row\">
            <label class=\"col-sm-3 col-form-label\">
                ";
            // line 16
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "location", array()), 'label');
            echo "
            </label>
            <div class=\"col-sm-9\">
                ";
            // line 19
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "location", array()), 'widget');
            echo "
            </div>
        </div>
        <div class=\"form-group row\">
            <label class=\"col-sm-3 col-form-label\">
                ";
            // line 24
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "address", array()), 'label');
            echo "
            </label>
            <div class=\"col-sm-9\">
                ";
            // line 27
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "address", array()), 'widget');
            echo "
            </div>
        </div>
        <div class=\"form-group row\">
            <label class=\"col-sm-3 col-form-label\">
                ";
            // line 32
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "phone", array()), 'label');
            echo "
            </label>
            <div class=\"col-sm-9\">
                ";
            // line 35
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "phone", array()), 'widget');
            echo "
            </div>
        </div>
        <div class=\"form-group row\">
            <label class=\"col-sm-3 col-form-label\">
                ";
            // line 40
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "ctstatus", array()), 'label');
            echo "
            </label>
            <div class=\"col-sm-9\">
                ";
            // line 43
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "ctstatus", array()), 'widget');
            echo "
            </div>
        </div>
        ";
            // line 46
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_end');
            echo "
    ";
        }
    }

    public function getTemplateName()
    {
        return "AdministrationDefaultBundle:Dinner:add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 46,  103 => 43,  97 => 40,  89 => 35,  83 => 32,  75 => 27,  69 => 24,  61 => 19,  55 => 16,  47 => 11,  41 => 8,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AdministrationDefaultBundle:Dinner:add.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Administration/DefaultBundle/Resources/views/Dinner/add.html.twig");
    }
}
