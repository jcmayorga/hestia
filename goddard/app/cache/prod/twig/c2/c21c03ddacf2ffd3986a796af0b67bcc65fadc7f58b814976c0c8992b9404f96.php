<?php

/* KitpagesDataGridBundle:Grid:bootstrap-grid.html.twig */
class __TwigTemplate_bdf6a1c5551d41f37a92d8d8e033c584a3f34754dc3a200f18f902d313993695 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'kit_grid_css' => array($this, 'block_kit_grid_css'),
            'kit_grid_main' => array($this, 'block_kit_grid_main'),
            'kit_grid_selector' => array($this, 'block_kit_grid_selector'),
            'kit_grid_filter' => array($this, 'block_kit_grid_filter'),
            'kit_grid_debug' => array($this, 'block_kit_grid_debug'),
            'kit_grid_thead' => array($this, 'block_kit_grid_thead'),
            'kit_grid_tbody' => array($this, 'block_kit_grid_tbody'),
            'kit_grid_row_class' => array($this, 'block_kit_grid_row_class'),
            'kit_grid_tbody_before_column' => array($this, 'block_kit_grid_tbody_before_column'),
            'kit_grid_paginator' => array($this, 'block_kit_grid_paginator'),
            'kit_grid_javascript' => array($this, 'block_kit_grid_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('kit_grid_css', $context, $blocks);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('kit_grid_main', $context, $blocks);
    }

    // line 1
    public function block_kit_grid_css($context, array $blocks = array())
    {
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/kitpagesdatagrid/css/base.css"), "html", null, true);
        echo "\"/>";
    }

    // line 3
    public function block_kit_grid_main($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"kit-grid-bootstrap\">
    ";
        // line 5
        $this->displayBlock('kit_grid_selector', $context, $blocks);
        // line 14
        echo "    ";
        $this->displayBlock('kit_grid_filter', $context, $blocks);
        // line 23
        echo "    ";
        $this->displayBlock('kit_grid_debug', $context, $blocks);
        // line 28
        echo "    ";
        $this->displayBlock("kit_grid_before_table", $context, $blocks);
        echo "
    <table class=\"table table-striped table-bordered table-hover\">
        ";
        // line 30
        $this->displayBlock('kit_grid_thead', $context, $blocks);
        // line 49
        echo "        ";
        $this->displayBlock('kit_grid_tbody', $context, $blocks);
        // line 74
        echo "    </table>
    ";
        // line 75
        $this->displayBlock("kit_grid_after_table", $context, $blocks);
        echo "
    ";
        // line 76
        $this->displayBlock('kit_grid_paginator', $context, $blocks);
        // line 80
        echo "    ";
        $this->displayBlock(" kit_grid_after_paginator", $context, $blocks);
        echo "
</div>

";
        // line 83
        $this->displayBlock('kit_grid_javascript', $context, $blocks);
    }

    // line 5
    public function block_kit_grid_selector($context, array $blocks = array())
    {
        // line 6
        echo "        ";
        if (($this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "selectorList", array()) != null)) {
            // line 7
            echo "            <ul class=\"nav nav-tabs\">
                ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "selectorList", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["selector"]) {
                // line 9
                echo "                    <li><a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "getSelectorUrl", array(0 => $this->getAttribute($context["selector"], "field", array()), 1 => $this->getAttribute($context["selector"], "value", array())), "method"), "html", null, true);
                echo "\" class=\"";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "getSelectorCssSelected", array(0 => $this->getAttribute($context["selector"], "field", array()), 1 => $this->getAttribute($context["selector"], "value", array())), "method"), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute($context["selector"], "label", array()), "html", null, true);
                echo "</a></li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['selector'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 11
            echo "            </ul>
        ";
        }
        // line 13
        echo "    ";
    }

    // line 14
    public function block_kit_grid_filter($context, array $blocks = array())
    {
        // line 15
        echo "    <div class=\"clearfix\">
        <form class=\"form-search pull-right\" action=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "requestUri", array()), "html", null, true);
        echo "\" id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "_form\" method=\"GET\">
            <input class=\"input-medium search-query\" type=\"text\" name=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.Filter"), "html", null, true);
        echo "\" id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterValue", array()), "html", null, true);
        echo "\"/>
            <button type=\"submit\" class=\"btn\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.Apply"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.Apply"), "html", null, true);
        echo "</button>
            <a class=\"btn btn-mini\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath($this->getAttribute(($context["grid"] ?? null), "requestCurrentRoute", array()), $this->getAttribute(($context["grid"] ?? null), "requestCurrentRouteParams", array())), "html", null, true);
        echo "\" id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "_reset_button\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.Reset"), "html", null, true);
        echo "</a>
        </form>
    </div>
    ";
    }

    // line 23
    public function block_kit_grid_debug($context, array $blocks = array())
    {
        // line 24
        echo "        ";
        if ($this->getAttribute(($context["grid"] ?? null), "debugMode", array())) {
            // line 25
            echo "            ";
            echo $this->getAttribute(($context["grid"] ?? null), "dump", array(), "method");
            echo "
        ";
        }
        // line 27
        echo "    ";
    }

    // line 30
    public function block_kit_grid_thead($context, array $blocks = array())
    {
        // line 31
        echo "        <thead>
        <tr>
            ";
        // line 33
        $this->displayBlock("kit_grid_thead_before_column", $context, $blocks);
        echo "
            ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "fieldList", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 35
            echo "                ";
            if ($this->getAttribute($context["field"], "visible", array())) {
                // line 36
                echo "                    <th class=\"";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "getSortCssClass", array(0 => $this->getAttribute($context["field"], "fieldName", array())), "method"), "html", null, true);
                echo "\">
                        ";
                // line 37
                if ($this->getAttribute($context["field"], "sortable", array())) {
                    // line 38
                    echo "                            <a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "getSortUrl", array(0 => $this->getAttribute($context["field"], "fieldName", array())), "method"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($context["field"], "label", array())), "html", null, true);
                    echo "</a>
                        ";
                } else {
                    // line 40
                    echo "                            ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($context["field"], "label", array())), "html", null, true);
                    echo "
                        ";
                }
                // line 42
                echo "                    </th>
                ";
            }
            // line 44
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "            ";
        $this->displayBlock("kit_grid_thead_column", $context, $blocks);
        echo "
        </tr>
        </thead>
        ";
    }

    // line 49
    public function block_kit_grid_tbody($context, array $blocks = array())
    {
        // line 50
        echo "        <tbody>
        ";
        // line 51
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["grid"] ?? null), "itemList", array()));
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 52
            echo "            <tr class=\"";
            if ((($this->getAttribute($context["loop"], "index", array()) % 2) == 0)) {
                echo "kit-grid-even ";
            } else {
                echo "kit-grid-odd ";
            }
            echo " ";
            $this->displayBlock('kit_grid_row_class', $context, $blocks);
            echo "\">
                ";
            // line 53
            $this->displayBlock('kit_grid_tbody_before_column', $context, $blocks);
            // line 54
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "fieldList", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
                // line 55
                echo "                    ";
                if ($this->getAttribute($context["field"], "visible", array())) {
                    // line 56
                    echo "                        <td class=\"kit-grid-cell-";
                    echo twig_escape_filter($this->env, twig_replace_filter($this->getAttribute($context["field"], "fieldName", array()), array("." => "-")), "html", null, true);
                    echo "\">
                            ";
                    // line 57
                    if ($this->getAttribute($context["field"], "translatable", array())) {
                        // line 58
                        echo "                                ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["grid"] ?? null), "displayGridValue", array(0 => $context["item"], 1 => $context["field"]), "method")), "html", null, true);
                        echo "
                            ";
                    } else {
                        // line 60
                        echo "                                ";
                        echo $this->getAttribute(($context["grid"] ?? null), "displayGridValue", array(0 => $context["item"], 1 => $context["field"]), "method");
                        echo "
                            ";
                    }
                    // line 62
                    echo "                        </td>
                    ";
                }
                // line 64
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "                ";
            $this->displayBlock("kit_grid_tbody_column", $context, $blocks);
            echo "
            </tr>
        ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 68
            echo "            <tr>
                <td colspan=\"";
            // line 69
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "fieldList", array())), "html", null, true);
            echo "\" class=\"kit-grid-no-data\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.No_Data_Found"), "html", null, true);
            echo "</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "        </tbody>
        ";
    }

    // line 52
    public function block_kit_grid_row_class($context, array $blocks = array())
    {
    }

    // line 53
    public function block_kit_grid_tbody_before_column($context, array $blocks = array())
    {
    }

    // line 76
    public function block_kit_grid_paginator($context, array $blocks = array())
    {
        // line 77
        echo "        ";
        $this->loadTemplate("KitpagesDataGridBundle:Grid:bootstrap-grid.html.twig", "KitpagesDataGridBundle:Grid:bootstrap-grid.html.twig", 77, "1725039401")->display(array_merge($context, array("paginator" => $this->getAttribute(($context["grid"] ?? null), "paginator", array()))));
        // line 79
        echo "    ";
    }

    // line 83
    public function block_kit_grid_javascript($context, array $blocks = array())
    {
        // line 84
        echo "    ";
        $this->loadTemplate("KitpagesDataGridBundle:Grid:javascript.html.twig", "KitpagesDataGridBundle:Grid:bootstrap-grid.html.twig", 84)->display($context);
    }

    public function getTemplateName()
    {
        return "KitpagesDataGridBundle:Grid:bootstrap-grid.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  373 => 84,  370 => 83,  366 => 79,  363 => 77,  360 => 76,  355 => 53,  350 => 52,  345 => 72,  334 => 69,  331 => 68,  314 => 65,  308 => 64,  304 => 62,  298 => 60,  292 => 58,  290 => 57,  285 => 56,  282 => 55,  277 => 54,  275 => 53,  264 => 52,  246 => 51,  243 => 50,  240 => 49,  231 => 45,  225 => 44,  221 => 42,  215 => 40,  207 => 38,  205 => 37,  200 => 36,  197 => 35,  193 => 34,  189 => 33,  185 => 31,  182 => 30,  178 => 27,  172 => 25,  169 => 24,  166 => 23,  154 => 19,  148 => 18,  138 => 17,  132 => 16,  129 => 15,  126 => 14,  122 => 13,  118 => 11,  105 => 9,  101 => 8,  98 => 7,  95 => 6,  92 => 5,  88 => 83,  81 => 80,  79 => 76,  75 => 75,  72 => 74,  69 => 49,  67 => 30,  61 => 28,  58 => 23,  55 => 14,  53 => 5,  50 => 4,  47 => 3,  39 => 1,  35 => 3,  32 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "KitpagesDataGridBundle:Grid:bootstrap-grid.html.twig", "/usr/share/nginx/html/hestia/goddard/vendor/kitpages/data-grid-bundle/Kitpages/DataGridBundle/Resources/views/Grid/bootstrap-grid.html.twig");
    }
}


/* KitpagesDataGridBundle:Grid:bootstrap-grid.html.twig */
class __TwigTemplate_bdf6a1c5551d41f37a92d8d8e033c584a3f34754dc3a200f18f902d313993695_1725039401 extends Twig_Template
{
    protected function doGetParent(array $context)
    {
        // line 77
        return $this->loadTemplate($this->getAttribute($this->getAttribute(($context["kitpages_data_grid"] ?? null), "paginator", array()), "default_twig", array()), "KitpagesDataGridBundle:Grid:bootstrap-grid.html.twig", 77);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "KitpagesDataGridBundle:Grid:bootstrap-grid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  408 => 77,  373 => 84,  370 => 83,  366 => 79,  363 => 77,  360 => 76,  355 => 53,  350 => 52,  345 => 72,  334 => 69,  331 => 68,  314 => 65,  308 => 64,  304 => 62,  298 => 60,  292 => 58,  290 => 57,  285 => 56,  282 => 55,  277 => 54,  275 => 53,  264 => 52,  246 => 51,  243 => 50,  240 => 49,  231 => 45,  225 => 44,  221 => 42,  215 => 40,  207 => 38,  205 => 37,  200 => 36,  197 => 35,  193 => 34,  189 => 33,  185 => 31,  182 => 30,  178 => 27,  172 => 25,  169 => 24,  166 => 23,  154 => 19,  148 => 18,  138 => 17,  132 => 16,  129 => 15,  126 => 14,  122 => 13,  118 => 11,  105 => 9,  101 => 8,  98 => 7,  95 => 6,  92 => 5,  88 => 83,  81 => 80,  79 => 76,  75 => 75,  72 => 74,  69 => 49,  67 => 30,  61 => 28,  58 => 23,  55 => 14,  53 => 5,  50 => 4,  47 => 3,  39 => 1,  35 => 3,  32 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "KitpagesDataGridBundle:Grid:bootstrap-grid.html.twig", "/usr/share/nginx/html/hestia/goddard/vendor/kitpages/data-grid-bundle/Kitpages/DataGridBundle/Resources/views/Grid/bootstrap-grid.html.twig");
    }
}
