<?php

/* AplicationDefaultBundle:User/Partials:headTab.html.twig */
class __TwigTemplate_0670258b8e7fcfeb46dc54641a131e1e589cf1a6030e82395230088406cb2d4f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"nav nav-tabs nav-justified\" role=\"tablist\">
    <li class=\"nav-item\">
        <a href=\"#tabPersonal\" id=\"personal-tab\" class=\"nav-link active\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"tabPersonal\" aria-selected=\"true\">
            <i class=\"green ace-icon fa fa-male bigger-120\"></i>
            Datos Personales
        </a>
    </li>
    <li  class=\"nav-item\">
        <a href=\"#tabUser\" id=\"user-tab\" class=\"nav-link\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"tabuser\">
            <i class=\"purple ace-icon fa fa-user bigger-120\"></i>
            Datos Usuario
        </a>
    </li>    
</ul>";
    }

    public function getTemplateName()
    {
        return "AplicationDefaultBundle:User/Partials:headTab.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AplicationDefaultBundle:User/Partials:headTab.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/User/Partials/headTab.html.twig");
    }
}
