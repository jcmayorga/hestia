<?php

/* AplicationDefaultBundle:Layout:view.html.twig */
class __TwigTemplate_3eefc192bb5344fa92f06b9cd383f06ec04a7b25c13d7b2458955b03ef730e44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'main_view' => array($this, 'block_main_view'),
            'alerts_view' => array($this, 'block_alerts_view'),
            'view_content' => array($this, 'block_view_content'),
            'js_view' => array($this, 'block_js_view'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["__internal_ba891d5cd5e779690601fd9cff238ff15da4c06c8d5704080368ba598d46643e"] = $this->loadTemplate("AplicationDefaultBundle:Layout:flash.html.twig", "AplicationDefaultBundle:Layout:view.html.twig", 1);
        // line 2
        $this->displayBlock('main_view', $context, $blocks);
    }

    public function block_main_view($context, array $blocks = array())
    {
        // line 3
        echo "    <!--<div class=\"row\">-->
        <div class=\"container\">
            <div class=\"col-xs-12\">
                ";
        // line 6
        $this->displayBlock('alerts_view', $context, $blocks);
        // line 10
        echo "    
                ";
        // line 11
        $this->displayBlock('view_content', $context, $blocks);
        // line 12
        echo "    
            </div>
    </div>  
    ";
        // line 15
        $this->displayBlock('js_view', $context, $blocks);
        // line 25
        echo "    
";
    }

    // line 6
    public function block_alerts_view($context, array $blocks = array())
    {
        // line 7
        echo "                    ";
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? null), "session", array()), "flashbag", array()), "peekAll", array())) > 0)) {
            echo "                                                                   
                            ";
            // line 8
            echo $context["__internal_ba891d5cd5e779690601fd9cff238ff15da4c06c8d5704080368ba598d46643e"]->getsession_flash(false);
            echo "                                                                   
                    ";
        }
        // line 10
        echo "                ";
    }

    // line 11
    public function block_view_content($context, array $blocks = array())
    {
        // line 12
        echo "                ";
    }

    // line 15
    public function block_js_view($context, array $blocks = array())
    {
        echo "        
        ";
        // line 16
        if ((array_key_exists("mode", $context) && (($context["mode"] ?? null) == "view"))) {
            // line 17
            echo "            <script type=\"text/javascript\">
                \$(function(){                   
                    var \$form=\$(\"form\",\".modal-dialog .modal-body\");                        
                    \$(\"input,textarea\",\$form).attr(\"readonly\",true);
                    \$(\"select\",\$form).attr(\"disabled\",true);
                });               
            </script>
        ";
        }
        // line 25
        echo "    ";
    }

    public function getTemplateName()
    {
        return "AplicationDefaultBundle:Layout:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 25,  86 => 17,  84 => 16,  79 => 15,  75 => 12,  72 => 11,  68 => 10,  63 => 8,  58 => 7,  55 => 6,  50 => 25,  48 => 15,  43 => 12,  41 => 11,  38 => 10,  36 => 6,  31 => 3,  25 => 2,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AplicationDefaultBundle:Layout:view.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/Layout/view.html.twig");
    }
}
