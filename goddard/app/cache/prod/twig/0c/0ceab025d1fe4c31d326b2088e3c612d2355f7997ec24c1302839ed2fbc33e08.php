<?php

/* AdministrationDefaultBundle:Company:assigndinner.html.twig */
class __TwigTemplate_ad3b457843a5e5a3cfcf8866cb73f054f4e9abfc855bbeb1de8a6501ca3c381e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AplicationDefaultBundle:Layout:view.html.twig", "AdministrationDefaultBundle:Company:assigndinner.html.twig", 2);
        $this->blocks = array(
            'view_content' => array($this, 'block_view_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AplicationDefaultBundle:Layout:view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_view_content($context, array $blocks = array())
    {
        // line 5
        echo "\t<form>
\t\t<div class=\"table-responsive\">
\t\t<table class=\"table table-bordered table-hover\">\t\t\t
\t\t\t<thead class=\"thead-dark\">
\t\t\t\t<tr>
\t\t\t\t\t<th scope=\"col\">Comedor</th>
\t\t\t\t\t<th style=\"text-align: center\" scope=\"col\">Agregar</th>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["dinnerscompany"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 16
            echo "\t\t\t\t    ";
            $context["dinner_id"] = $this->getAttribute($context["item"], "dinner_id", array(), "array");
            // line 17
            echo "\t\t\t\t    <tr>
\t\t\t\t    \t<th scope=\"row\">";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "dinner_title", array(), "array"), "html", null, true);
            echo "</th>
\t\t\t\t    \t<td class=\"text-center\"><input name=\"assigndinners[";
            // line 19
            echo twig_escape_filter($this->env, ($context["dinner_id"] ?? null), "html", null, true);
            echo "][add]\" value=1 type='checkbox' ";
            echo (($this->getAttribute($context["item"], "add", array(), "array")) ? ("checked=true") : (null));
            echo "/></td>
\t\t\t\t    </tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "\t\t\t</tbody>
\t\t</table>
\t</div>
\t</form>

";
    }

    public function getTemplateName()
    {
        return "AdministrationDefaultBundle:Company:assigndinner.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 22,  57 => 19,  53 => 18,  50 => 17,  47 => 16,  43 => 15,  31 => 5,  28 => 4,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AdministrationDefaultBundle:Company:assigndinner.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Administration/DefaultBundle/Resources/views/Company/assigndinner.html.twig");
    }
}
