<?php

/* KitpagesDataGridBundle:Paginator:bootstrap3-paginator.html.twig */
class __TwigTemplate_32524184ecf7d7f08f6bf969758d21ea15aaae75deb634f990e1997d9902bb98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'kit_grid_paginator' => array($this, 'block_kit_grid_paginator'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute(($context["paginator"] ?? null), "totalPageCount", array()) > 1)) {
            // line 2
            $this->displayBlock('kit_grid_paginator', $context, $blocks);
        }
    }

    public function block_kit_grid_paginator($context, array $blocks = array())
    {
        // line 3
        echo "<nav class=\"kit-grid-paginator\">
    <ul class=\"pagination\">
        ";
        // line 5
        if ($this->getAttribute(($context["paginator"] ?? null), "previousButtonPage", array())) {
            // line 6
            echo "            <li>
                <a href=\"";
            // line 7
            echo twig_escape_filter($this->env, $this->getAttribute(($context["paginator"] ?? null), "getUrl", array(0 => "currentPage", 1 => $this->getAttribute(($context["paginator"] ?? null), "previousButtonPage", array())), "method"), "html", null, true);
            echo "\" title=\"previous\">
                    <span aria-hidden=\"true\">&laquo;</span><span class=\"sr-only\">";
            // line 8
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.Previous"), "html", null, true);
            echo "</span></a>
                </a>
            </li>
        ";
        }
        // line 12
        echo "
        ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["paginator"] ?? null), "pageRange", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
            // line 14
            echo "            <li class=\"";
            if (($context["page"] == $this->getAttribute(($context["paginator"] ?? null), "currentPage", array()))) {
                echo "active";
            }
            echo "\">
                <a href=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute(($context["paginator"] ?? null), "getUrl", array(0 => "currentPage", 1 => $context["page"]), "method"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["page"], "html", null, true);
            echo "</a>
            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
        ";
        // line 19
        if ($this->getAttribute(($context["paginator"] ?? null), "nextButtonPage", array())) {
            // line 20
            echo "            <li>
                <a href=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute(($context["paginator"] ?? null), "getUrl", array(0 => "currentPage", 1 => $this->getAttribute(($context["paginator"] ?? null), "nextButtonPage", array())), "method"), "html", null, true);
            echo "\" title=\"next\">
                    <span aria-hidden=\"true\">&raquo;</span><span class=\"sr-only\">";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.Next"), "html", null, true);
            echo "</span></a>
                </a>
            </li>
        ";
        }
        // line 26
        echo "    </ul>
</nav>
";
    }

    public function getTemplateName()
    {
        return "KitpagesDataGridBundle:Paginator:bootstrap3-paginator.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 26,  86 => 22,  82 => 21,  79 => 20,  77 => 19,  74 => 18,  63 => 15,  56 => 14,  52 => 13,  49 => 12,  42 => 8,  38 => 7,  35 => 6,  33 => 5,  29 => 3,  22 => 2,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "KitpagesDataGridBundle:Paginator:bootstrap3-paginator.html.twig", "/usr/share/nginx/html/hestia/goddard/vendor/kitpages/data-grid-bundle/Kitpages/DataGridBundle/Resources/views/Paginator/bootstrap3-paginator.html.twig");
    }
}
