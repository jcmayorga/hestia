<?php

/* AdministrationDefaultBundle:Contract:edit.html.twig */
class __TwigTemplate_66a23d55d5acf80ca1d77831f29ac8fe043ee72f9c517cab61b9df342a426283 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AplicationDefaultBundle:Layout:view.html.twig", "AdministrationDefaultBundle:Contract:edit.html.twig", 1);
        $this->blocks = array(
            'view_content' => array($this, 'block_view_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AplicationDefaultBundle:Layout:view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_view_content($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        if ((array_key_exists("form", $context) &&  !twig_test_empty(($context["form"] ?? null)))) {
            // line 5
            echo "        ";
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_start');
            echo "
        <div class=\"form-group row\">
            <label class=\"col-sm-3 col-form-label\">
                ";
            // line 8
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "name", array()), 'label');
            echo "
            </label>
            <div class=\"col-sm-9\">
                ";
            // line 11
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "name", array()), 'widget');
            echo "
            </div>
        </div>
        <div class=\"form-group row\">
            <label class=\"col-sm-3 col-form-label\">
                ";
            // line 16
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "description", array()), 'label');
            echo "
            </label>
            <div class=\"col-sm-9\">
                ";
            // line 19
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "description", array()), 'widget');
            echo "
            </div>
        </div>
        <div class=\"form-group row\">
            <label class=\"col-sm-3 col-form-label\">
                ";
            // line 24
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "ctstatus", array()), 'label');
            echo "
            </label>
            <div class=\"col-sm-9\">
                ";
            // line 27
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "ctstatus", array()), 'widget');
            echo "
            </div>
        </div>
        ";
            // line 30
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_end');
            echo "
    ";
        }
    }

    public function getTemplateName()
    {
        return "AdministrationDefaultBundle:Contract:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 30,  75 => 27,  69 => 24,  61 => 19,  55 => 16,  47 => 11,  41 => 8,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AdministrationDefaultBundle:Contract:edit.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Administration/DefaultBundle/Resources/views/Contract/edit.html.twig");
    }
}
