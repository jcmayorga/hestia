<?php

/* :default:index.html.twig */
class __TwigTemplate_7b7c494b4de2e9d5abce1af3cdb0f19a311930316f206045b03b590b02d6932a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":default:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"btn-group\">
        <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
            Action <span class=\"caret\"></span>
        </button>

    </div>
    <div id=\"wrapper\">
        <div id=\"container\">
            <button type=\"button\" class=\"btn btn-default btn-lg\">
                <span class=\"glyphicon glyphicon-star\" aria-hidden=\"true\"></span> Star
            </button>
            <div class=\"input-group input-group-lg\">
                <span class=\"glyphicon glyphicon-user\" id=\"sizing-addon1\">@</span>
                <input type=\"text\" class=\"form-control\" placeholder=\"Username\" aria-describedby=\"sizing-addon1\">
            </div>

            <ul class=\"list-group\">
                <li class=\"list-group-item list-group-item-success\">Dapibus ac facilisis in</li>
                <li class=\"list-group-item list-group-item-info\">Cras sit amet nibh libero</li>
                <li class=\"list-group-item list-group-item-warning\">Porta ac consectetur ac</li>
                <li class=\"list-group-item list-group-item-danger\">Vestibulum at eros</li>
            </ul>
            <div class=\"list-group\">
                <a href=\"#\" class=\"list-group-item list-group-item-success\">Dapibus ac facilisis in</a>
                <a href=\"#\" class=\"list-group-item list-group-item-info\">Cras sit amet nibh libero</a>
                <a href=\"#\" class=\"list-group-item list-group-item-warning\">Porta ac consectetur ac</a>
                <a href=\"#\" class=\"list-group-item list-group-item-danger\">Vestibulum at eros</a>
            </div>

        </div>
    </div>
";
    }

    // line 37
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 38
        echo "    <style>
        body { background: #F5F5F5; font: 18px/1.5 sans-serif; }
        h1, h2 { line-height: 1.2; margin: 0 0 .5em; }
        h1 { font-size: 36px; }
        h2 { font-size: 21px; margin-bottom: 1em; }
        p { margin: 0 0 1em 0; }
        a { color: #0000F0; }
        a:hover { text-decoration: none; }
        code { background: #F5F5F5; max-width: 100px; padding: 2px 6px; word-wrap: break-word; }
        #wrapper { background: #FFF; margin: 1em auto; max-width: 800px; width: 95%; }
        #container { padding: 2em; }
        #welcome, #status { margin-bottom: 2em; }
        #welcome h1 span { display: block; font-size: 75%; }
        #icon-status, #icon-book { float: left; height: 64px; margin-right: 1em; margin-top: -4px; width: 64px; }
        #icon-book { display: none; }

        @media (min-width: 768px) {
            #wrapper { width: 80%; margin: 2em auto; }
            #icon-book { display: inline-block; }
            #status a, #next a { display: block; }

            @-webkit-keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
            @keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
            .sf-toolbar { opacity: 0; -webkit-animation: fade-in 1s .2s forwards; animation: fade-in 1s .2s forwards;}
        }
    </style>
";
    }

    public function getTemplateName()
    {
        return ":default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 38,  67 => 37,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":default:index.html.twig", "/usr/share/nginx/html/hestia/goddard/app/Resources/views/default/index.html.twig");
    }
}
