<?php

/* AdministrationDefaultBundle:Roster:index.html.twig */
class __TwigTemplate_b7abbba00b9d3c179d460efc4f09d831b81e7b42f5301a415ae6f86ab73279f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AplicationDefaultBundle:Layout:layout.html.twig", "AdministrationDefaultBundle:Roster:index.html.twig", 1);
        $this->blocks = array(
            'container_fluid' => array($this, 'block_container_fluid'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AplicationDefaultBundle:Layout:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_container_fluid($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"container-fluid\">
        ";
        // line 5
        $this->displayBlock('breadcrumb', $context, $blocks);
        // line 15
        echo "        <div class=\"row\">
            <div class=\"col-12\">
                <div class=\"alert alert-info\">Seleccione el archivo a importar</div>
                <div class=\"row col-sm-12\">
                      <div class=\"col-sm-5\">
                            <form id=\"formUpload\" autocomplete=off action=\"javascript:void(0)\" enctype=\"multipart/form-data\" method=\"post\">  
                              <div class=\"form-group row\">
                                   <label class=\"col-sm-2\">
                                      Empresa:
                                  </label>
                                    <div class=\"col-sm-10\"> 
                                        <select class=\"required\" name=\"companyId\" style=\"width:270px\">
                                          <option value=\"\">--Seleccione--</option>
                                          ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["companies"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 29
            echo "                                            <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "businessname", array()), "html", null, true);
            echo "</option>
                                          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "                                        </select>
                                    </div>  
                               </div> 
                               <div class=\"form-group row\">
                                  <label class=\"col-sm-2\">
                                      
                                  </label>
                                    <div class=\"col-sm-10\"> 
                                        <input type=\"file\" class=\"required\" name=\"fileUpload\" accept=\".xls,.xlsx,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\"> 
                                    </div>  
                               </div> 
                               <div class=\"form-group row content-progress\" style=\"display:none\">
                                  <label class=\"col-sm-2\">
                                      
                                  </label>
                                  <div class=\"col-sm-5\">
                                     <div class=\"progress\">
                                      <div class=\"progress-bar progress-bar-striped progress-bar-animated\" role=\"progressbar\" aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%\"></div>

                                    </div>
                                   
                                    Procesando por favor espere...
                                  </div>
                               </div>
                               <div class=\"form-group row content-info-message\" style=\"display:none\">
                                   <label class=\"col-sm-2\">
                                      
                                   </label>
                                   <div id=\"info-message\" class=\"alert\">

                                   </div>
                               </div> 
                               <div class=\"form-group row\">
                                  <label class=\"col-sm-2\">
                                      
                                  </label>
                                   <div class=\"col-sm-10\"> 
                                      <input type=\"submit\" id=\"btnImport\" name=\"submit\" value=\"Validar e importar\" class=\"btn btn-primary\"/>
                                   </div>
                               </div> 
                            </form>
                         </div>  
                         <div class=\"col-sm-7\">
                            <div>
                                <table class=\"table table-bordered\" style=\"font-size:10px\">
                                    <thead >
                                      <tr><th colspan=\"6\" scope=\"col\" class=\"text-center\"><span>FORMATO</span></th></tr>
                                      <tr>
                                        <th scope=\"col\"><b>Identificación</b></th>
                                        <th scope=\"col\"><b>Nombres</b></th>
                                        <th scope=\"col\"><b>Apellidos</b></th>
                                        <th scope=\"col\"><b>Unidad</b></th>
                                        <th scope=\"col\"><b>Código centro de costos</b></th>
                                        <th scope=\"col\"><b>Nombre centro de costos</b></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                          <th>&nbsp;</th>
                                          <td>&nbsp;</td>
                                          <th>&nbsp;</th>
                                          <td>&nbsp;</td>
                                          <th>&nbsp;</th>
                                          <th>&nbsp;</th>                                         
                                        </tr>
                                    </tbody>    
                                </table>
                            </div>
                            <div id=\"info-errors\" class=\"alert alert-danger\" style=\"display:none\">

                            </div>
                         </div> 
                     </div>    
            </div>
        </div>
    </div>
    <!-- /.container-fluid-->
";
    }

    // line 5
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 6
        echo "            ";
        // line 7
        echo "            <ol class=\"breadcrumb\">
                <li class=\"breadcrumb-item\">
                    <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_dinner_index");
        echo "\">Administración</a>
                </li>
                <li class=\"breadcrumb-item active\">Cargar comensales</li>
            </ol>
            ";
        // line 14
        echo "        ";
    }

    // line 109
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 110
        echo "    <script type=\"text/javascript\" charset=\"utf-8\">
        \$(function () {

             \$(\"#formUpload\").validate({
                            errorElement: 'div',
                            errorClass: 'help-block',
                            focusInvalid: true,
                            highlight: function (e) {
                                    \$(e).closest('.form-group').removeClass('has-info').removeClass('has-success').addClass('has-error');                            

                            },

                            success: function (e,el) {
                                    \$(e).closest('.form-group').removeClass('has-error');
                                    \$(e).remove();
                            }
                       }); 

             \$(\"#formUpload\").submit(function(event, data){

                   event.stopPropagation(); // Stop stuff happening
                   event.preventDefault(); // Totally stop stuff happening 
                    
                   if(\$(this).valid()){
                        hideMessages();
                        // Create a formdata object 
                        var formData = new FormData(\$(this)[0]);
                        \$.ajax({
                            url: '";
        // line 138
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_roster_upload");
        echo "',
                            type: 'POST',
                            data: formData,
                            cache: false,
                            async: false,
                            dataType: 'json',
                            processData: false, // Don't process the files
                            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                            beforeSend: function(){
                                \$(\".content-progress\").show();
                            },
                            success: function(data, textStatus, jqXHR)
                            {
                                 \$(\".content-progress\").hide();
                                 showMessages(data.status,data.message,data.errors); 
                                 
                                 if(data.status){
                                    clearForm(); 
                                 }
                            },
                            error: function(jqXHR, textStatus, errorThrown)
                            {  
                                 \$(\".content-progress\").hide();
                                 showMessages(0,\"No es posible cargar el archivo\",null);
                            }
                        });
                  }
                return false;

             });
        });

    function showMessages(status,message,errors){
         var strclass=\"alert-danger\";
         if(status){
             strclass=\"alert-success\";
         }
         \$(\".content-info-message\").show();
         \$(\"#info-message\").addClass(strclass).html(message);

         if(errors.length){
             showErrors(errors);
         }else{
             \$(\"#info-errors\").empty();
         }
    }

    function hideMessages(){
         \$(\"·content-info-message,#info-errors\").hide();
         \$(\"#info-message\").removeClass(\"alert-danger\").removeClass(\"alert-success\").empty();
         \$(\"#info-errors\").empty();
    }

    function clearForm(){
        \$(\"#formUpload\").reset();
            
    }



    function showErrors(errors){
          var objError=\$(\"#info-errors\");
          for (var i = 0; i <errors.length; i++) {
              objError.append(\"- \"+errors[i]+\"<br/>\");
          }

          \$(objError).show();
    }

    </script>
";
    }

    public function getTemplateName()
    {
        return "AdministrationDefaultBundle:Roster:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 138,  172 => 110,  169 => 109,  165 => 14,  158 => 9,  154 => 7,  152 => 6,  149 => 5,  68 => 31,  57 => 29,  53 => 28,  38 => 15,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AdministrationDefaultBundle:Roster:index.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Administration/DefaultBundle/Resources/views/Roster/index.html.twig");
    }
}
