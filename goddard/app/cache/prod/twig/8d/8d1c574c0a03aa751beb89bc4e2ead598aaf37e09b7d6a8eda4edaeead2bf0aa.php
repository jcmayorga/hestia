<?php

/* KitpagesDataGridBundle:Grid:javascript.html.twig */
class __TwigTemplate_bfdeab33019d0c692213e05f40bf5f28ba42b703946e618dc85587e5e01cec8e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\">
    ";
        // line 2
        $this->loadTemplate("KitpagesDataGridBundle:Grid:javascript_content.html.twig", "KitpagesDataGridBundle:Grid:javascript.html.twig", 2)->display($context);
        // line 3
        echo "</script>
";
    }

    public function getTemplateName()
    {
        return "KitpagesDataGridBundle:Grid:javascript.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 3,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "KitpagesDataGridBundle:Grid:javascript.html.twig", "/usr/share/nginx/html/hestia/goddard/vendor/kitpages/data-grid-bundle/Kitpages/DataGridBundle/Resources/views/Grid/javascript.html.twig");
    }
}
