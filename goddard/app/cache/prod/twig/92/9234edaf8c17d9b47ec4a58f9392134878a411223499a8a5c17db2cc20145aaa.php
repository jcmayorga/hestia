<?php

/* AdministrationDefaultBundle:Company:index.html.twig */
class __TwigTemplate_c48b6c39a02e658b8c6cecfb7160ecd335d60d6aacf155c5d6951d77f7f3f830 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AplicationDefaultBundle:Layout:layout.html.twig", "AdministrationDefaultBundle:Company:index.html.twig", 1);
        $this->blocks = array(
            'container_fluid' => array($this, 'block_container_fluid'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AplicationDefaultBundle:Layout:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_container_fluid($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"container-fluid\">
        ";
        // line 5
        $this->displayBlock('breadcrumb', $context, $blocks);
        // line 15
        echo "        <div class=\"row\">
            <div class=\"col-12\">
                ";
        // line 17
        $this->loadTemplate("AdministrationDefaultBundle:Company:index.html.twig", "AdministrationDefaultBundle:Company:index.html.twig", 17, "290429770")->display(array_merge($context, array("grid" => ($context["grid"] ?? null))));
        // line 40
        echo "            </div>
        </div>
    </div>
    <!-- /.container-fluid-->
";
    }

    // line 5
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 6
        echo "            ";
        // line 7
        echo "            <ol class=\"breadcrumb\">
                <li class=\"breadcrumb-item\">
                    <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_company_index");
        echo "\">Administración</a>
                </li>
                <li class=\"breadcrumb-item active\">Empresa</li>
            </ol>
            ";
        // line 14
        echo "        ";
    }

    // line 45
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 46
        echo "    <script type=\"text/javascript\" charset=\"utf-8\">
        \$(function () {
            \$(\"#btnNew\").click(function () {
                openAddForm();
            });
        });

        function openAddForm() {

            var div = \$(\"<div>\", {id: 'divAdd'});
            \$(div).modalForm({
                url: \"";
        // line 57
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_company_add");
        echo "\",
                autoOpen: true,
                align: 'center',
                width: '70%',
                urlSubmit: \"";
        // line 61
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_company_save");
        echo "\",
                onSuccess: function () {
                    setTimeout(function () {
                        location.reload()
                    }, 500);
                }
            });
        }

        function openEditForm(id) {
            var div = \$(\"<div>\", {id: 'divEdit'});
            \$(div).modalForm({
                url: \"";
        // line 73
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_company_edit");
        echo "/\" + id,
                autoOpen: true,
                align: 'center',
                width: '70%',
                urlSubmit: \"";
        // line 77
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_company_save");
        echo "/\" + id,
                onSuccess: function () {
                    setTimeout(function () {
                        location.reload()
                    }, 500);
                }
            });
        }

        function deleteRecord(id) {
            if (confirm('Está seguro de eliminar el registro?')) {
                \$.ajaxDeleteRowGrid({
                    url: \"";
        // line 89
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_company_delete");
        echo "/\" + id,
                    onSuccess: function () {
                        setTimeout(function () {
                            location.reload()
                        }, 500);
                    }
                });
            }
        }

        function assignDinner(id) {
            var div = \$(\"<div>\", {id: 'divAssignDinner'});
            \$(div).modalForm({
                url: \"";
        // line 102
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_company_assigndinner");
        echo "/\" + id,
                autoOpen: true,
                // title:'Asignar Comedores',
                align: 'center',
                urlSubmit: \"";
        // line 106
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration_default_company_assigndinner_save");
        echo "/\" + id,
                width: '50%',
            });
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "AdministrationDefaultBundle:Company:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 106,  148 => 102,  132 => 89,  117 => 77,  110 => 73,  95 => 61,  88 => 57,  75 => 46,  72 => 45,  68 => 14,  61 => 9,  57 => 7,  55 => 6,  52 => 5,  44 => 40,  42 => 17,  38 => 15,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AdministrationDefaultBundle:Company:index.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Administration/DefaultBundle/Resources/views/Company/index.html.twig");
    }
}


/* AdministrationDefaultBundle:Company:index.html.twig */
class __TwigTemplate_c48b6c39a02e658b8c6cecfb7160ecd335d60d6aacf155c5d6951d77f7f3f830_290429770 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'kit_grid_filter' => array($this, 'block_kit_grid_filter'),
            'kit_grid_thead_column' => array($this, 'block_kit_grid_thead_column'),
            'kit_grid_tbody_column' => array($this, 'block_kit_grid_tbody_column'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 17
        return $this->loadTemplate($this->getAttribute($this->getAttribute(($context["kitpages_data_grid"] ?? null), "grid", array()), "default_twig", array()), "AdministrationDefaultBundle:Company:index.html.twig", 17);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 19
    public function block_kit_grid_filter($context, array $blocks = array())
    {
        // line 20
        echo "                        <div class=\"input-group\">
                            <button type=\"button\" id=\"btnNew\" class=\"btn btn-default\">Nuevo</button>
                        </div>
                        <hr/>
                        ";
        // line 24
        $this->displayParentBlock("kit_grid_filter", $context, $blocks);
        echo "
                    ";
    }

    // line 27
    public function block_kit_grid_thead_column($context, array $blocks = array())
    {
        // line 28
        echo "                        <th>Acción</th>
                    ";
    }

    // line 31
    public function block_kit_grid_tbody_column($context, array $blocks = array())
    {
        // line 32
        echo "                        <td>
                            <a href=\"javascript:void(0)\" onclick=\"openEditForm(";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute(($context["item"] ?? null), "company.id", array(), "array"), "html", null, true);
        echo ")\" title=\"Editar\"><i class=\"fa fa-pencil bigger-125 fa-fw\" ></i></a>&nbsp;|&nbsp;
                            <a href=\"javascript:void(0)\" onclick=\"assignDinner(";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute(($context["item"] ?? null), "company.id", array(), "array"), "html", null, true);
        echo ")\" title=\"Asiganr comedores\"><i class=\"fa fa-check-square-o bigger-125 fa-fw\" ></i></a>&nbsp;|&nbsp;
                            <a href=\"javascript:void(0)\" onclick=\"deleteRecord(";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute(($context["item"] ?? null), "company.id", array(), "array"), "html", null, true);
        echo ")\" title=\"Eliminar\"><i class=\"fa fa-trash bigger-125 fa-fw\" ></i></a>
                        </td>
                    ";
    }

    public function getTemplateName()
    {
        return "AdministrationDefaultBundle:Company:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 35,  253 => 34,  249 => 33,  246 => 32,  243 => 31,  238 => 28,  235 => 27,  229 => 24,  223 => 20,  220 => 19,  211 => 17,  155 => 106,  148 => 102,  132 => 89,  117 => 77,  110 => 73,  95 => 61,  88 => 57,  75 => 46,  72 => 45,  68 => 14,  61 => 9,  57 => 7,  55 => 6,  52 => 5,  44 => 40,  42 => 17,  38 => 15,  36 => 5,  33 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AdministrationDefaultBundle:Company:index.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Administration/DefaultBundle/Resources/views/Company/index.html.twig");
    }
}
