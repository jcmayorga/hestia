<?php

/* AplicationDefaultBundle:Layout:main_layout.html.twig */
class __TwigTemplate_e331d47e31cef4a9042444f471f5e2150dcfec805b2b3c80b7df4ad5639fc1f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("AplicationDefaultBundle:Layout:base.html.twig", "AplicationDefaultBundle:Layout:main_layout.html.twig", 2);
        $this->blocks = array(
            'body_tag' => array($this, 'block_body_tag'),
            'navbar' => array($this, 'block_navbar'),
            'sidebar' => array($this, 'block_sidebar'),
            'navbar_content' => array($this, 'block_navbar_content'),
            'container' => array($this, 'block_container'),
            'container_fluid' => array($this, 'block_container_fluid'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AplicationDefaultBundle:Layout:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_body_tag($context, array $blocks = array())
    {
        // line 5
        echo "<body  class=\"fixed-nav sticky-footer bg-dark\" id=\"page-top\">
";
    }

    // line 7
    public function block_navbar($context, array $blocks = array())
    {
        // line 8
        echo "    <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark fixed-top\" id=\"mainNav\">
        <a class=\"navbar-brand\" href=\"#\">Hestia 2.0</a>
        <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>
        <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
        ";
        // line 14
        $this->displayBlock('sidebar', $context, $blocks);
        // line 107
        echo "        ";
        $this->displayBlock('navbar_content', $context, $blocks);
        // line 212
        echo "        </div>
    </nav>
";
    }

    // line 14
    public function block_sidebar($context, array $blocks = array())
    {
        // line 15
        echo "            <ul class=\"navbar-nav navbar-sidenav\" id=\"exampleAccordion\">
                <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Dashboard\">
                    <a class=\"nav-link\" href=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_aplication_default_layout");
        echo "\">
                        <i class=\"fa fa-fw fa-dashboard\"></i>
                        <span class=\"nav-link-text\">Inicio</span>
                    </a>
                </li>
                <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Charts\">
                    <a class=\"nav-link\" href=\"charts.html\">
                        <i class=\"fa fa-fw fa-area-chart\"></i>
                        <span class=\"nav-link-text\">Charts</span>
                    </a>
                </li>
                <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Tables\">
                    <a class=\"nav-link\" href=\"tables.html\">
                        <i class=\"fa fa-fw fa-table\"></i>
                        <span class=\"nav-link-text\">Tables</span>
                    </a>
                </li>
                <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Components\">
                    <a class=\"nav-link nav-link-collapse collapsed\" data-toggle=\"collapse\" href=\"#collapseComponents\" data-parent=\"#exampleAccordion\">
                        <i class=\"fa fa-fw fa-wrench\"></i>
                        <span class=\"nav-link-text\">Components</span>
                    </a>
                    <ul class=\"sidenav-second-level collapse\" id=\"collapseComponents\">
                        <li>
                            <a href=\"navbar.html\">Navbar</a>
                        </li>
                        <li>
                            <a href=\"cards.html\">Cards</a>
                        </li>
                    </ul>
                </li>
                <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Example Pages\">
                    <a class=\"nav-link nav-link-collapse collapsed\" data-toggle=\"collapse\" href=\"#collapseExamplePages\" data-parent=\"#exampleAccordion\">
                        <i class=\"fa fa-fw fa-file\"></i>
                        <span class=\"nav-link-text\">Example Pages</span>
                    </a>
                    <ul class=\"sidenav-second-level collapse\" id=\"collapseExamplePages\">
                        <li>
                            <a href=\"login.html\">Login Page</a>
                        </li>
                        <li>
                            <a href=\"register.html\">Registration Page</a>
                        </li>
                        <li>
                            <a href=\"forgot-password.html\">Forgot Password Page</a>
                        </li>
                        <li>
                            <a href=\"blank.html\">Blank Page</a>
                        </li>
                    </ul>
                </li>
                <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Menu Levels\">
                    <a class=\"nav-link nav-link-collapse collapsed\" data-toggle=\"collapse\" href=\"#collapseMulti\" data-parent=\"#exampleAccordion\">
                        <i class=\"fa fa-fw fa-sitemap\"></i>
                        <span class=\"nav-link-text\">Menu Levels</span>
                    </a>
                    <ul class=\"sidenav-second-level collapse\" id=\"collapseMulti\">
                        <li>
                            <a href=\"#\">Second Level Item</a>
                        </li>
                        <li>
                            <a href=\"#\">Second Level Item</a>
                        </li>
                        <li>
                            <a href=\"#\">Second Level Item</a>
                        </li>
                        <li>
                            <a class=\"nav-link-collapse collapsed\" data-toggle=\"collapse\" href=\"#collapseMulti2\">Third Level</a>
                            <ul class=\"sidenav-third-level collapse\" id=\"collapseMulti2\">
                                <li>
                                    <a href=\"#\">Third Level Item</a>
                                </li>
                                <li>
                                    <a href=\"#\">Third Level Item</a>
                                </li>
                                <li>
                                    <a href=\"#\">Third Level Item</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Link\">
                    <a class=\"nav-link\" href=\"#\">
                        <i class=\"fa fa-fw fa-link\"></i>
                        <span class=\"nav-link-text\">Link</span>
                    </a>
                </li>
            </ul>
        ";
    }

    // line 107
    public function block_navbar_content($context, array $blocks = array())
    {
        // line 108
        echo "            <ul class=\"navbar-nav sidenav-toggler\">
                <li class=\"nav-item\">
                    <a class=\"nav-link text-center\" id=\"sidenavToggler\">
                        <i class=\"fa fa-fw fa-angle-left\"></i>
                    </a>
                </li>
            </ul>
            <ul class=\"navbar-nav ml-auto\">
                <li class=\"nav-item dropdown\">
                    <a class=\"nav-link dropdown-toggle mr-lg-2\" id=\"messagesDropdown\" href=\"#\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        <i class=\"fa fa-fw fa-envelope\"></i>
                        <span class=\"d-lg-none\">Messages
              <span class=\"badge badge-pill badge-primary\">12 New</span>
            </span>
                        <span class=\"indicator text-primary d-none d-lg-block\">
              <i class=\"fa fa-fw fa-circle\"></i>
            </span>
                    </a>
                    <div class=\"dropdown-menu\" aria-labelledby=\"messagesDropdown\">
                        <h6 class=\"dropdown-header\">New Messages:</h6>
                        <div class=\"dropdown-divider\"></div>
                        <a class=\"dropdown-item\" href=\"#\">
                            <strong>David Miller</strong>
                            <span class=\"small float-right text-muted\">11:21 AM</span>
                            <div class=\"dropdown-message small\">Hey there! This new version of SB Admin is pretty awesome! These messages clip off when they reach the end of the box so they don't overflow over to the sides!</div>
                        </a>
                        <div class=\"dropdown-divider\"></div>
                        <a class=\"dropdown-item\" href=\"#\">
                            <strong>Jane Smith</strong>
                            <span class=\"small float-right text-muted\">11:21 AM</span>
                            <div class=\"dropdown-message small\">I was wondering if you could meet for an appointment at 3:00 instead of 4:00. Thanks!</div>
                        </a>
                        <div class=\"dropdown-divider\"></div>
                        <a class=\"dropdown-item\" href=\"#\">
                            <strong>John Doe</strong>
                            <span class=\"small float-right text-muted\">11:21 AM</span>
                            <div class=\"dropdown-message small\">I've sent the final files over to you for review. When you're able to sign off of them let me know and we can discuss distribution.</div>
                        </a>
                        <div class=\"dropdown-divider\"></div>
                        <a class=\"dropdown-item small\" href=\"#\">View all messages</a>
                    </div>
                </li>
                <li class=\"nav-item dropdown\">
                    <a class=\"nav-link dropdown-toggle mr-lg-2\" id=\"alertsDropdown\" href=\"#\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        <i class=\"fa fa-fw fa-bell\"></i>
                        <span class=\"d-lg-none\">Alerts
              <span class=\"badge badge-pill badge-warning\">6 New</span>
            </span>
                        <span class=\"indicator text-warning d-none d-lg-block\">
              <i class=\"fa fa-fw fa-circle\"></i>
            </span>
                    </a>
                    <div class=\"dropdown-menu\" aria-labelledby=\"alertsDropdown\">
                        <h6 class=\"dropdown-header\">New Alerts:</h6>
                        <div class=\"dropdown-divider\"></div>
                        <a class=\"dropdown-item\" href=\"#\">
              <span class=\"text-success\">
                <strong>
                  <i class=\"fa fa-long-arrow-up fa-fw\"></i>Status Update</strong>
              </span>
                            <span class=\"small float-right text-muted\">11:21 AM</span>
                            <div class=\"dropdown-message small\">This is an automated server response message. All systems are online.</div>
                        </a>
                        <div class=\"dropdown-divider\"></div>
                        <a class=\"dropdown-item\" href=\"#\">
              <span class=\"text-danger\">
                <strong>
                  <i class=\"fa fa-long-arrow-down fa-fw\"></i>Status Update</strong>
              </span>
                            <span class=\"small float-right text-muted\">11:21 AM</span>
                            <div class=\"dropdown-message small\">This is an automated server response message. All systems are online.</div>
                        </a>
                        <div class=\"dropdown-divider\"></div>
                        <a class=\"dropdown-item\" href=\"#\">
              <span class=\"text-success\">
                <strong>
                  <i class=\"fa fa-long-arrow-up fa-fw\"></i>Status Update</strong>
              </span>
                            <span class=\"small float-right text-muted\">11:21 AM</span>
                            <div class=\"dropdown-message small\">This is an automated server response message. All systems are online.</div>
                        </a>
                        <div class=\"dropdown-divider\"></div>
                        <a class=\"dropdown-item small\" href=\"#\">View all alerts</a>
                    </div>
                </li>
                <li class=\"nav-item\">
                    <form class=\"form-inline my-2 my-lg-0 mr-lg-2\">
                        <div class=\"input-group\">
                            <input class=\"form-control\" type=\"text\" placeholder=\"Search for...\">
                            <span class=\"input-group-append\">
                <button class=\"btn btn-primary\" type=\"button\">
                  <i class=\"fa fa-search\"></i>
                </button>
              </span>
                        </div>
                    </form>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#exampleModal\"  href=\"";
        // line 206
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_aplication_default_closesession");
        echo "\">
                        <i class=\"fa fa-fw fa-sign-out\"></i>Cerrar Sesión</a>
                </li>
            </ul>

        ";
    }

    // line 215
    public function block_container($context, array $blocks = array())
    {
        // line 216
        echo "    <div class=\"content-wrapper\">
        ";
        // line 217
        $this->displayBlock('container_fluid', $context, $blocks);
        // line 232
        echo "
        ";
        // line 233
        $this->displayBlock('footer', $context, $blocks);
        // line 243
        echo "        <!-- Scroll to Top Button-->
        <a class=\"scroll-to-top rounded\" href=\"#page-top\">
            <i class=\"fa fa-angle-up\"></i>
        </a>
        <!-- Logout Modal-->
        ";
        // line 249
        echo "            ";
        // line 250
        echo "                ";
        // line 251
        echo "                    ";
        // line 252
        echo "                        ";
        // line 253
        echo "                        ";
        // line 254
        echo "                            ";
        // line 255
        echo "                        ";
        // line 256
        echo "                    ";
        // line 257
        echo "                    ";
        // line 258
        echo "                    ";
        // line 259
        echo "                        ";
        // line 260
        echo "                        ";
        // line 261
        echo "                    ";
        // line 262
        echo "                ";
        // line 263
        echo "            ";
        // line 264
        echo "        ";
        // line 265
        echo "
    </div>
";
    }

    // line 217
    public function block_container_fluid($context, array $blocks = array())
    {
        // line 218
        echo "        <div class=\"container-fluid\">
            ";
        // line 219
        $this->displayBlock('breadcrumb', $context, $blocks);
        // line 229
        echo "        </div>
        <!-- /.container-fluid-->
        ";
    }

    // line 219
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 220
        echo "            ";
        // line 221
        echo "            <ol class=\"breadcrumb\">
                <li class=\"breadcrumb-item\">
                    <a href=\"";
        // line 223
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_aplication_default_layout");
        echo "\">Inicio</a>
                </li>
                <li class=\"breadcrumb-item active\">Mi Inicio</li>
            </ol>
            ";
        // line 228
        echo "            ";
    }

    // line 233
    public function block_footer($context, array $blocks = array())
    {
        // line 234
        echo "        <!-- /.content-wrapper-->
        <footer class=\"sticky-footer\">
            <div class=\"container\">
                <div class=\"text-center\">
                    <small>Copyright &copy; Hestia 2018</small>
                </div>
            </div>
        </footer>
        ";
    }

    public function getTemplateName()
    {
        return "AplicationDefaultBundle:Layout:main_layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  374 => 234,  371 => 233,  367 => 228,  360 => 223,  356 => 221,  354 => 220,  351 => 219,  345 => 229,  343 => 219,  340 => 218,  337 => 217,  331 => 265,  329 => 264,  327 => 263,  325 => 262,  323 => 261,  321 => 260,  319 => 259,  317 => 258,  315 => 257,  313 => 256,  311 => 255,  309 => 254,  307 => 253,  305 => 252,  303 => 251,  301 => 250,  299 => 249,  292 => 243,  290 => 233,  287 => 232,  285 => 217,  282 => 216,  279 => 215,  269 => 206,  169 => 108,  166 => 107,  72 => 17,  68 => 15,  65 => 14,  59 => 212,  56 => 107,  54 => 14,  46 => 8,  43 => 7,  38 => 5,  35 => 4,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AplicationDefaultBundle:Layout:main_layout.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/Layout/main_layout.html.twig");
    }
}
