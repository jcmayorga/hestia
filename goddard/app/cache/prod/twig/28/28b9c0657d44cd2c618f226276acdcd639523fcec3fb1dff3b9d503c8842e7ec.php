<?php

/* KitpagesDataGridBundle:Grid:bootstrap3-grid.html.twig */
class __TwigTemplate_5b8b2449598dea4984efe314f6377b026a1164c88d616f3c0c82b4b5aa2ffd7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'kit_grid_main' => array($this, 'block_kit_grid_main'),
            'kit_grid_selector' => array($this, 'block_kit_grid_selector'),
            'kit_grid_filter' => array($this, 'block_kit_grid_filter'),
            'kit_grid_debug' => array($this, 'block_kit_grid_debug'),
            'kit_grid_thead' => array($this, 'block_kit_grid_thead'),
            'kit_grid_tbody' => array($this, 'block_kit_grid_tbody'),
            'kit_grid_row_class' => array($this, 'block_kit_grid_row_class'),
            'kit_grid_tbody_before_column' => array($this, 'block_kit_grid_tbody_before_column'),
            'kit_grid_paginator' => array($this, 'block_kit_grid_paginator'),
            'kit_grid_javascript' => array($this, 'block_kit_grid_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('kit_grid_main', $context, $blocks);
    }

    public function block_kit_grid_main($context, array $blocks = array())
    {
        // line 2
        echo "    <div class=\"kit-grid ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "getGridCssName", array()), "html", null, true);
        echo "\">
        ";
        // line 3
        $this->displayBlock('kit_grid_selector', $context, $blocks);
        // line 16
        echo "        ";
        $this->displayBlock('kit_grid_filter', $context, $blocks);
        // line 30
        echo "        ";
        $this->displayBlock('kit_grid_debug', $context, $blocks);
        // line 35
        echo "        ";
        $this->displayBlock("kit_grid_before_table", $context, $blocks);
        echo "
        <table class=\"table table-striped table-bordered table-hover\">
            ";
        // line 37
        $this->displayBlock('kit_grid_thead', $context, $blocks);
        // line 56
        echo "            ";
        $this->displayBlock('kit_grid_tbody', $context, $blocks);
        // line 81
        echo "        </table>
        ";
        // line 82
        $this->displayBlock("kit_grid_after_table", $context, $blocks);
        echo "
        ";
        // line 83
        $this->displayBlock('kit_grid_paginator', $context, $blocks);
        // line 87
        echo "        ";
        $this->displayBlock(" kit_grid_after_paginator", $context, $blocks);
        echo "
    </div>
    ";
        // line 89
        $this->displayBlock('kit_grid_javascript', $context, $blocks);
    }

    // line 3
    public function block_kit_grid_selector($context, array $blocks = array())
    {
        // line 4
        echo "            ";
        if (($this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "selectorList", array()) != null)) {
            // line 5
            echo "                <div class=\"kit-grid-selector\">
                    <ul class=\"nav nav-pills\">
                    ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "selectorList", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["selector"]) {
                // line 8
                echo "                        <li class=\"";
                if ($this->getAttribute(($context["grid"] ?? null), "isSelectorSelected", array(0 => $this->getAttribute($context["selector"], "field", array()), 1 => $this->getAttribute($context["selector"], "value", array())), "method")) {
                    echo "active";
                }
                echo "\">
                            <a href=\"";
                // line 9
                echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "getSelectorUrl", array(0 => $this->getAttribute($context["selector"], "field", array()), 1 => $this->getAttribute($context["selector"], "value", array())), "method"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["selector"], "label", array()), "html", null, true);
                echo "</a>
                        </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['selector'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "                    </ul>
                </div>
            ";
        }
        // line 15
        echo "        ";
    }

    // line 16
    public function block_kit_grid_filter($context, array $blocks = array())
    {
        // line 17
        echo "            <div class=\"kit-grid-filter\">
                <form class=\"form-inline\" action=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "requestUri", array()), "html", null, true);
        echo "\" id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "_form\" method=\"GET\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <label class=\"sr-only\" for=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.Filter"), "html", null, true);
        echo "</label>
                            <input type=\"text\" class=\"form-control\" id=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterValue", array()), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.Filter"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                    <button type=\"submit\" class=\"btn btn-default\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.Apply"), "html", null, true);
        echo "</button>
                    <a href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath($this->getAttribute(($context["grid"] ?? null), "requestCurrentRoute", array()), $this->getAttribute(($context["grid"] ?? null), "requestCurrentRouteParams", array())), "html", null, true);
        echo "\" id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "filterFormName", array()), "html", null, true);
        echo "_reset_button\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.Reset"), "html", null, true);
        echo "</a>
                </form>
            </div>
        ";
    }

    // line 30
    public function block_kit_grid_debug($context, array $blocks = array())
    {
        // line 31
        echo "            ";
        if ($this->getAttribute(($context["grid"] ?? null), "debugMode", array())) {
            // line 32
            echo "                ";
            echo $this->getAttribute(($context["grid"] ?? null), "dump", array(), "method");
            echo "
            ";
        }
        // line 34
        echo "        ";
    }

    // line 37
    public function block_kit_grid_thead($context, array $blocks = array())
    {
        // line 38
        echo "                <thead>
                <tr>
                    ";
        // line 40
        $this->displayBlock("kit_grid_thead_before_column", $context, $blocks);
        echo "
                    ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "fieldList", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 42
            echo "                        ";
            if ($this->getAttribute($context["field"], "visible", array())) {
                // line 43
                echo "                            <th class=\"";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "getSortCssClass", array(0 => $this->getAttribute($context["field"], "fieldName", array())), "method"), "html", null, true);
                echo "\">
                                ";
                // line 44
                if ($this->getAttribute($context["field"], "sortable", array())) {
                    // line 45
                    echo "                                    <a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute(($context["grid"] ?? null), "getSortUrl", array(0 => $this->getAttribute($context["field"], "fieldName", array())), "method"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($context["field"], "label", array())), "html", null, true);
                    echo "</a>
                                ";
                } else {
                    // line 47
                    echo "                                    ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($context["field"], "label", array())), "html", null, true);
                    echo "
                                ";
                }
                // line 49
                echo "                            </th>
                        ";
            }
            // line 51
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "                    ";
        $this->displayBlock("kit_grid_thead_column", $context, $blocks);
        echo "
                </tr>
                </thead>
            ";
    }

    // line 56
    public function block_kit_grid_tbody($context, array $blocks = array())
    {
        // line 57
        echo "                <tbody>
                ";
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["grid"] ?? null), "itemList", array()));
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 59
            echo "                    <tr class=\"";
            if ((($this->getAttribute($context["loop"], "index", array()) % 2) == 0)) {
                echo "kit-grid-even ";
            } else {
                echo "kit-grid-odd ";
            }
            echo " ";
            $this->displayBlock('kit_grid_row_class', $context, $blocks);
            echo "\">
                        ";
            // line 60
            $this->displayBlock('kit_grid_tbody_before_column', $context, $blocks);
            // line 61
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "fieldList", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
                // line 62
                echo "                            ";
                if ($this->getAttribute($context["field"], "visible", array())) {
                    // line 63
                    echo "                                <td class=\"kit-grid-cell-";
                    echo twig_escape_filter($this->env, twig_replace_filter($this->getAttribute($context["field"], "fieldName", array()), array("." => "-")), "html", null, true);
                    echo "\">
                                    ";
                    // line 64
                    if ($this->getAttribute($context["field"], "translatable", array())) {
                        // line 65
                        echo "                                        ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["grid"] ?? null), "displayGridValue", array(0 => $context["item"], 1 => $context["field"]), "method")), "html", null, true);
                        echo "
                                    ";
                    } else {
                        // line 67
                        echo "                                        ";
                        echo $this->getAttribute(($context["grid"] ?? null), "displayGridValue", array(0 => $context["item"], 1 => $context["field"]), "method");
                        echo "
                                    ";
                    }
                    // line 69
                    echo "                                </td>
                            ";
                }
                // line 71
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 72
            echo "                        ";
            $this->displayBlock("kit_grid_tbody_column", $context, $blocks);
            echo "
                    </tr>
                ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 75
            echo "                    <tr>
                        <td colspan=\"";
            // line 76
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute($this->getAttribute(($context["grid"] ?? null), "gridConfig", array()), "fieldList", array())), "html", null, true);
            echo "\" class=\"kit-grid-no-data\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("kitpages_data_grid.No-data-found"), "html", null, true);
            echo "</td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "                </tbody>
            ";
    }

    // line 59
    public function block_kit_grid_row_class($context, array $blocks = array())
    {
    }

    // line 60
    public function block_kit_grid_tbody_before_column($context, array $blocks = array())
    {
    }

    // line 83
    public function block_kit_grid_paginator($context, array $blocks = array())
    {
        // line 84
        echo "            ";
        $this->loadTemplate("KitpagesDataGridBundle:Grid:bootstrap3-grid.html.twig", "KitpagesDataGridBundle:Grid:bootstrap3-grid.html.twig", 84, "459089929")->display(array_merge($context, array("paginator" => $this->getAttribute(($context["grid"] ?? null), "paginator", array()))));
        // line 86
        echo "        ";
    }

    // line 89
    public function block_kit_grid_javascript($context, array $blocks = array())
    {
        // line 90
        echo "        ";
        $this->loadTemplate("KitpagesDataGridBundle:Grid:javascript.html.twig", "KitpagesDataGridBundle:Grid:bootstrap3-grid.html.twig", 90)->display($context);
        // line 91
        echo "    ";
    }

    public function getTemplateName()
    {
        return "KitpagesDataGridBundle:Grid:bootstrap3-grid.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  377 => 91,  374 => 90,  371 => 89,  367 => 86,  364 => 84,  361 => 83,  356 => 60,  351 => 59,  346 => 79,  335 => 76,  332 => 75,  315 => 72,  309 => 71,  305 => 69,  299 => 67,  293 => 65,  291 => 64,  286 => 63,  283 => 62,  278 => 61,  276 => 60,  265 => 59,  247 => 58,  244 => 57,  241 => 56,  232 => 52,  226 => 51,  222 => 49,  216 => 47,  208 => 45,  206 => 44,  201 => 43,  198 => 42,  194 => 41,  190 => 40,  186 => 38,  183 => 37,  179 => 34,  173 => 32,  170 => 31,  167 => 30,  155 => 26,  151 => 25,  139 => 22,  133 => 21,  125 => 18,  122 => 17,  119 => 16,  115 => 15,  110 => 12,  99 => 9,  92 => 8,  88 => 7,  84 => 5,  81 => 4,  78 => 3,  74 => 89,  68 => 87,  66 => 83,  62 => 82,  59 => 81,  56 => 56,  54 => 37,  48 => 35,  45 => 30,  42 => 16,  40 => 3,  35 => 2,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "KitpagesDataGridBundle:Grid:bootstrap3-grid.html.twig", "/usr/share/nginx/html/hestia/goddard/vendor/kitpages/data-grid-bundle/Kitpages/DataGridBundle/Resources/views/Grid/bootstrap3-grid.html.twig");
    }
}


/* KitpagesDataGridBundle:Grid:bootstrap3-grid.html.twig */
class __TwigTemplate_5b8b2449598dea4984efe314f6377b026a1164c88d616f3c0c82b4b5aa2ffd7a_459089929 extends Twig_Template
{
    protected function doGetParent(array $context)
    {
        // line 84
        return $this->loadTemplate($this->getAttribute($this->getAttribute(($context["kitpages_data_grid"] ?? null), "paginator", array()), "default_twig", array()), "KitpagesDataGridBundle:Grid:bootstrap3-grid.html.twig", 84);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "KitpagesDataGridBundle:Grid:bootstrap3-grid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  411 => 84,  377 => 91,  374 => 90,  371 => 89,  367 => 86,  364 => 84,  361 => 83,  356 => 60,  351 => 59,  346 => 79,  335 => 76,  332 => 75,  315 => 72,  309 => 71,  305 => 69,  299 => 67,  293 => 65,  291 => 64,  286 => 63,  283 => 62,  278 => 61,  276 => 60,  265 => 59,  247 => 58,  244 => 57,  241 => 56,  232 => 52,  226 => 51,  222 => 49,  216 => 47,  208 => 45,  206 => 44,  201 => 43,  198 => 42,  194 => 41,  190 => 40,  186 => 38,  183 => 37,  179 => 34,  173 => 32,  170 => 31,  167 => 30,  155 => 26,  151 => 25,  139 => 22,  133 => 21,  125 => 18,  122 => 17,  119 => 16,  115 => 15,  110 => 12,  99 => 9,  92 => 8,  88 => 7,  84 => 5,  81 => 4,  78 => 3,  74 => 89,  68 => 87,  66 => 83,  62 => 82,  59 => 81,  56 => 56,  54 => 37,  48 => 35,  45 => 30,  42 => 16,  40 => 3,  35 => 2,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "KitpagesDataGridBundle:Grid:bootstrap3-grid.html.twig", "/usr/share/nginx/html/hestia/goddard/vendor/kitpages/data-grid-bundle/Kitpages/DataGridBundle/Resources/views/Grid/bootstrap3-grid.html.twig");
    }
}
