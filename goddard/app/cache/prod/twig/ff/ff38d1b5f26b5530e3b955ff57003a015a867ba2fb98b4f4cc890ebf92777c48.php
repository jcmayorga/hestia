<?php

/* AplicationDefaultBundle:Layout:navbar.html.twig */
class __TwigTemplate_52703546e8a786b9f6b43d05aad1dee900a13fd9bdf0cab364e1adb7bce4d827 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"navbar-nav ml-auto\">
    <li class=\"nav-item\">
        <a class=\"nav-link mr-lg-2\" href=\"#\">";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute(($context["sessionuser"] ?? null), "fullname", array()), "html", null, true);
        echo "
            <i class=\"fa fa-fw fa-user\"></i>
        </a>
    </li>
    <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_aplication_default_closesession");
        echo "\">
            <i class=\"fa fa-fw fa-sign-out\"></i>Cerrar Sesión</a>
    </li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "AplicationDefaultBundle:Layout:navbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 8,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AplicationDefaultBundle:Layout:navbar.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/Layout/navbar.html.twig");
    }
}
