<?php

/* AplicationDefaultBundle:Layout:index.html.twig */
class __TwigTemplate_82c877c5ccd132ce1f2eaf160fb316e26d66101f495c77f4882ed52d8d9b8dc2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AplicationDefaultBundle:Layout:layout.html.twig", "AplicationDefaultBundle:Layout:index.html.twig", 1);
        $this->blocks = array(
            'container_fluid' => array($this, 'block_container_fluid'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AplicationDefaultBundle:Layout:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_container_fluid($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"container-fluid\">
        ";
        // line 5
        $this->displayBlock('breadcrumb', $context, $blocks);
        // line 15
        echo "        <div class=\"row\">
            <div class=\"col-12\">
                <div style=\"text-align: center\">
                    <br>
                    <img src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("resources/goddard/goddard.gif"), "html", null, true);
        echo "\" class=\"img-thumbnail\"
                         alt=\"Cinque Terre\" width=600\" height=\"450\">
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid-->
";
    }

    // line 5
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 6
        echo "            ";
        // line 7
        echo "            <ol class=\"breadcrumb\">
                <li class=\"breadcrumb-item\">
                    <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_aplication_default_layout");
        echo "\">Inicio</a>
                </li>
                <li class=\"breadcrumb-item active\">Mi Inicio</li>
            </ol>
            ";
        // line 14
        echo "        ";
    }

    public function getTemplateName()
    {
        return "AplicationDefaultBundle:Layout:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 14,  64 => 9,  60 => 7,  58 => 6,  55 => 5,  43 => 19,  37 => 15,  35 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AplicationDefaultBundle:Layout:index.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Aplication/DefaultBundle/Resources/views/Layout/index.html.twig");
    }
}
