<?php

/* BussinessDefaultBundle:Ticketrelease:ticket.html.twig */
class __TwigTemplate_996e56d691f48b5b7b70a383024e65bdb3dcfe9e08d45d51524dbb9597fd8a0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "BussinessDefaultBundle:Ticketrelease:ticket.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        echo " 
    <div class=\"container\">    
        <div id=\"loginbox\" style=\"margin-top:50px;\" class=\"mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2\">                    
            <div class=\"panel panel-info\" >
                <div class=\"panel-heading\">
                    <div class=\"panel-title\">Goddard Catering</div>
                    <div style=\"float:right; font-size: 100%; position: relative; top:-10px\">Impresión Ticket</div>
                </div>     

                <div style=\"padding-top:30px\" class=\"panel-body\" >

                    <div style=\"display:none\" id=\"login-alert\" class=\"alert alert-danger col-sm-12\"></div>

                    <form  class=\"form-horizontal\" role=\"form\" action=\"#\" method=\"post\" id=\"frmticket\">
                        <div class=\"input-group input-group-lg\" style=\"text-align: center\">
                            <div style=\"margin-bottom: 25px\" class=\"input-group\">
                                <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-user\"></i></span>
                                <input type=\"text\" class=\"form-control\" placeholder=\"Ingrese la identificación\" id=\"identification\" name=\"identification\">
                            </div>

                            <div style=\"margin-bottom: 25px;display: none\" class=\"input-group\" id=\"divreprintcode\">
                                <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-lock\"></i></span>
                                <input type=\"password\" class=\"form-control\" placeholder=\"Código de autorización\" id=\"reprintcode\" name=\"reprintcode\">
                                <br>
                            </div>
                            <div style=\"display: none\">

                                Dinner
                                <input type=\"text\" class=\"form-control\" placeholder=\"dinner\"
                                       aria-describedby=\"sizing-addon1\" id=\"dinner\" name=\"dinner\" value=\"";
        // line 34
        echo twig_escape_filter($this->env, ($context["dinner"] ?? null), "html", null, true);
        echo "\">
                                <br>






                                cttransaction ticketgenerado
                                <input type=\"text\" class=\"form-control\" placeholder=\"cttransaction\"
                                       aria-describedby=\"sizing-addon1\" id=\"cttransaction\" name=\"cttransaction\" value=\"510\">

                                <br>
                                ctstatus 
                                <input type=\"text\" class=\"form-control\" placeholder=\"ctstatus\"
                                       aria-describedby=\"sizing-addon1\" id=\"ctstatus\" name=\"ctstatus\" value=\"513\">

                            </div>

                            <div style=\"margin-left: 10px; text-align: center\" class=\"form-group\" >
                                <!-- Button -->
                                <button class=\"btn btn-success btn-lg\" type=\"button\" id=\"print\">Generar Ticket</button>
                                <button class=\"btn btn-primary btn-lg\" type=\"button\" id=\"cancelreprint\">Cancelar reimpresión</button>
                                <div id=\"procesando\" style=\"display: none\"> Procesando por favor espere .....</div>
                            </div>



                            <div class=\"form-group\">
                                <div class=\"col-md-12 control\">
                                    <div style=\"border-top: 1px solid#888; padding-top:15px; font-size:85%\" >


                                    </div>
                                </div>
                            </div>    




                        </div>  
                    </form>
                </div>  
            </div>

        </div>




    ";
    }

    // line 85
    public function block_javascripts($context, array $blocks = array())
    {
        // line 86
        echo "        <script type=\"text/javascript\">
            \$(function () {
                \$(\"#cancelreprint\").hide();
                \$(\"#identification,#reprintcode\").keypress(function (e) {
                    if (e.which == 13) {
                        // Acciones a realizar, por ej: enviar formulario.
                        sendFormTicket();
                    }
                });

                \$(\"#print\").click(function () {
                    sendFormTicket();
                })
            });

            function sendFormTicket() {
                var identification = \$(\"#identification\").val();
                var data = \$(\"#frmticket\").serialize();
                \$(\"#identification\").removeAttr(\"readonly\");
                \$(\"#cancelreprint\").hide();
                \$.ajax({
                    url: \"";
        // line 107
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_bussiness_default_ticketrelease_save");
        echo "\",
                    type: \"post\",
                    async: false,
                    dataType: \"json\",
                    data: data,
                    beforeSend: function (xhr) {
                        \$(\"#print\").hide();
                        \$(\"#procesando\").show();
                        
                    },
                    success: function (data) {
                         \$(\"#identification\").val(\"\");
                         
                        if (data.status) {
                            var transaction_id = data.transaction_id;
                             \$(\"#divreprintcode\").hide();
                            window.open(\"http://37.139.9.128/ticket.php?id=\" + transaction_id, \"_blank\", \"toolbar=no,scrollbars=no,resizable=no,top=500,left=500,width=400,height=400\");
                        } else {                            
                            alert(data.message);
                            \$(\"#divreprintcode\").hide();
                                 \$(\"#reprintcode\").val(\"\");
                                 \$(\"#cancelreprint\").hide();
                            if(data.reprint){
                                 
                                 \$(\"#cancelreprint\").show();
                                 \$(\"#divreprintcode\").show();
                                 \$(\"#reprintcode\").show();
                                 \$(\"#identification\").val(identification);
                                 \$(\"#identification\").attr(\"readonly\",\"readonly\");
                                 \$(\"#reprintcode\").val(\"\");
                                 
                            }
                        }


                    }
                }).done(function () {
                    \$(\"#print\").show();
                    \$(\"#procesando\").hide();
                   
                    \$(\"#reprintcode\").val(\"\");
                    //\$(\"#divreprintcode\").hide();
                });
            }
            
            \$(\"#cancelreprint\").click(function (){
               \$(\"#reprintcode\").val(\"\");
               \$(\"#divreprintcode\").hide();
                \$(\"#identification\").removeAttr(\"readonly\");
                 \$(\"#identification\").val(\"\");
                 \$(\"#cancelreprint\").hide();
            })


        </script>
    ";
    }

    public function getTemplateName()
    {
        return "BussinessDefaultBundle:Ticketrelease:ticket.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 107,  120 => 86,  117 => 85,  62 => 34,  29 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "BussinessDefaultBundle:Ticketrelease:ticket.html.twig", "/usr/share/nginx/html/hestia/goddard/src/Bussiness/DefaultBundle/Resources/views/Ticketrelease/ticket.html.twig");
    }
}
