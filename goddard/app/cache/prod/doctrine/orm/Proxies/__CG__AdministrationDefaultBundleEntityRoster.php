<?php

namespace Proxies\__CG__\Administration\DefaultBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Roster extends \Administration\DefaultBundle\Entity\Roster implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'id', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'identification', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'ctidentification_id', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'ctidentification', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'firstname', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'lastname', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'fullname', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'email', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'company_id', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'company', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'employeecode', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'barcode', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'rfidcode', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'biometriccode', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'ctstatus_id', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'ctstatus', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'usucrud_id', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'unit', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'costcenter', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'namecenter', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'created_at', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'updated_at'];
        }

        return ['__isInitialized__', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'id', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'identification', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'ctidentification_id', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'ctidentification', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'firstname', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'lastname', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'fullname', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'email', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'company_id', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'company', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'employeecode', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'barcode', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'rfidcode', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'biometriccode', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'ctstatus_id', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'ctstatus', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'usucrud_id', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'unit', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'costcenter', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'namecenter', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'created_at', '' . "\0" . 'Administration\\DefaultBundle\\Entity\\Roster' . "\0" . 'updated_at'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Roster $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * {@inheritDoc}
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);

        parent::__clone();
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setIdentification($identification)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIdentification', [$identification]);

        return parent::setIdentification($identification);
    }

    /**
     * {@inheritDoc}
     */
    public function getIdentification()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIdentification', []);

        return parent::getIdentification();
    }

    /**
     * {@inheritDoc}
     */
    public function setCtidentificationId($ctidentification_id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCtidentificationId', [$ctidentification_id]);

        return parent::setCtidentificationId($ctidentification_id);
    }

    /**
     * {@inheritDoc}
     */
    public function getCtidentificationId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCtidentificationId', []);

        return parent::getCtidentificationId();
    }

    /**
     * {@inheritDoc}
     */
    public function setCtidentification(\Aplication\DefaultBundle\Entity\Catalog $ctidentification = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCtidentification', [$ctidentification]);

        return parent::setCtidentification($ctidentification);
    }

    /**
     * {@inheritDoc}
     */
    public function getCtidentification()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCtidentification', []);

        return parent::getCtidentification();
    }

    /**
     * {@inheritDoc}
     */
    public function setFirstname($firstname)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFirstname', [$firstname]);

        return parent::setFirstname($firstname);
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstname()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFirstname', []);

        return parent::getFirstname();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastname($lastname)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastname', [$lastname]);

        return parent::setLastname($lastname);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastname()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastname', []);

        return parent::getLastname();
    }

    /**
     * {@inheritDoc}
     */
    public function setFullname($fullname)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFullname', [$fullname]);

        return parent::setFullname($fullname);
    }

    /**
     * {@inheritDoc}
     */
    public function getFullname()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFullname', []);

        return parent::getFullname();
    }

    /**
     * {@inheritDoc}
     */
    public function setEmail($email)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEmail', [$email]);

        return parent::setEmail($email);
    }

    /**
     * {@inheritDoc}
     */
    public function getEmail()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEmail', []);

        return parent::getEmail();
    }

    /**
     * {@inheritDoc}
     */
    public function setCompanyId($company_id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCompanyId', [$company_id]);

        return parent::setCompanyId($company_id);
    }

    /**
     * {@inheritDoc}
     */
    public function getCompanyId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCompanyId', []);

        return parent::getCompanyId();
    }

    /**
     * {@inheritDoc}
     */
    public function setCompany(\Administration\DefaultBundle\Entity\Company $company)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCompany', [$company]);

        return parent::setCompany($company);
    }

    /**
     * {@inheritDoc}
     */
    public function getCompany()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCompany', []);

        return parent::getCompany();
    }

    /**
     * {@inheritDoc}
     */
    public function setEmployeecode($employeecode)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEmployeecode', [$employeecode]);

        return parent::setEmployeecode($employeecode);
    }

    /**
     * {@inheritDoc}
     */
    public function getEmployeecode()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEmployeecode', []);

        return parent::getEmployeecode();
    }

    /**
     * {@inheritDoc}
     */
    public function setBarcode($barcode)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setBarcode', [$barcode]);

        return parent::setBarcode($barcode);
    }

    /**
     * {@inheritDoc}
     */
    public function getBarcode()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getBarcode', []);

        return parent::getBarcode();
    }

    /**
     * {@inheritDoc}
     */
    public function setRfidcode($rfidcode)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRfidcode', [$rfidcode]);

        return parent::setRfidcode($rfidcode);
    }

    /**
     * {@inheritDoc}
     */
    public function getRfidcode()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRfidcode', []);

        return parent::getRfidcode();
    }

    /**
     * {@inheritDoc}
     */
    public function setBiometriccode($biometriccode)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setBiometriccode', [$biometriccode]);

        return parent::setBiometriccode($biometriccode);
    }

    /**
     * {@inheritDoc}
     */
    public function getBiometriccode()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getBiometriccode', []);

        return parent::getBiometriccode();
    }

    /**
     * {@inheritDoc}
     */
    public function setCtstatusId($ctstatus_id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCtstatusId', [$ctstatus_id]);

        return parent::setCtstatusId($ctstatus_id);
    }

    /**
     * {@inheritDoc}
     */
    public function getCtstatusId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCtstatusId', []);

        return parent::getCtstatusId();
    }

    /**
     * {@inheritDoc}
     */
    public function setCtstatus(\Aplication\DefaultBundle\Entity\Catalog $ctstatus)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCtstatus', [$ctstatus]);

        return parent::setCtstatus($ctstatus);
    }

    /**
     * {@inheritDoc}
     */
    public function getCtstatus()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCtstatus', []);

        return parent::getCtstatus();
    }

    /**
     * {@inheritDoc}
     */
    public function setUsucrudId($usucrud_id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUsucrudId', [$usucrud_id]);

        return parent::setUsucrudId($usucrud_id);
    }

    /**
     * {@inheritDoc}
     */
    public function getUsucrudId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUsucrudId', []);

        return parent::getUsucrudId();
    }

    /**
     * {@inheritDoc}
     */
    public function setUnit($unit)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUnit', [$unit]);

        return parent::setUnit($unit);
    }

    /**
     * {@inheritDoc}
     */
    public function getUnit()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUnit', []);

        return parent::getUnit();
    }

    /**
     * {@inheritDoc}
     */
    public function setCostcenter($costcenter)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCostcenter', [$costcenter]);

        return parent::setCostcenter($costcenter);
    }

    /**
     * {@inheritDoc}
     */
    public function getCostcenter()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCostcenter', []);

        return parent::getCostcenter();
    }

    /**
     * {@inheritDoc}
     */
    public function setNamecenter($namecenter)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNamecenter', [$namecenter]);

        return parent::setNamecenter($namecenter);
    }

    /**
     * {@inheritDoc}
     */
    public function getNamecenter()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNamecenter', []);

        return parent::getNamecenter();
    }

    /**
     * {@inheritDoc}
     */
    public function prePersist()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'prePersist', []);

        return parent::prePersist();
    }

    /**
     * {@inheritDoc}
     */
    public function preUpdate()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'preUpdate', []);

        return parent::preUpdate();
    }

    /**
     * {@inheritDoc}
     */
    public function copy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'copy', []);

        return parent::copy();
    }

}
