<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);

        // _aplication_default_login
        if ('' === rtrim($pathinfo, '/')) {
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                goto not__aplication_default_login;
            } else {
                return $this->redirect($rawPathinfo.'/', '_aplication_default_login');
            }

            return array (  '_controller' => 'Aplication\\DefaultBundle\\Controller\\LoginController::indexAction',  '_route' => '_aplication_default_login',);
        }
        not__aplication_default_login:

        // _aplication_default_login_validateinput
        if ('/validateinput' === $pathinfo) {
            return array (  '_controller' => 'Aplication\\DefaultBundle\\Controller\\LoginController::validateinputAction',  '_route' => '_aplication_default_login_validateinput',);
        }

        if (0 === strpos($pathinfo, '/layout')) {
            // _aplication_default_layout
            if ('/layout' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not__aplication_default_layout;
                } else {
                    return $this->redirect($rawPathinfo.'/', '_aplication_default_layout');
                }

                return array (  '_controller' => 'Aplication\\DefaultBundle\\Controller\\LayoutController::indexAction',  '_route' => '_aplication_default_layout',);
            }
            not__aplication_default_layout:

            // _aplication_default_layout_sidebar
            if ('/layout/sidebar' === $pathinfo) {
                return array (  '_controller' => 'Aplication\\DefaultBundle\\Controller\\LayoutController::sidebarAction',  '_route' => '_aplication_default_layout_sidebar',);
            }

            // _aplication_default_layout_navbar
            if ('/layout/navbar' === $pathinfo) {
                return array (  '_controller' => 'Aplication\\DefaultBundle\\Controller\\LayoutController::navbarAction',  '_route' => '_aplication_default_layout_navbar',);
            }

            // _aplication_default_iframe
            if ('/layout/iframe' === $pathinfo) {
                return array (  '_controller' => 'Aplication\\DefaultBundle\\Controller\\LayoutController::iframeAction',  '_route' => '_aplication_default_iframe',);
            }

            // _aplication_default_closesession
            if ('/layout/closesession' === $pathinfo) {
                return array (  '_controller' => 'Aplication\\DefaultBundle\\Controller\\LayoutController::closesessionAction',  '_route' => '_aplication_default_closesession',);
            }

        }

        if (0 === strpos($pathinfo, '/role')) {
            // aplication_default_role_index
            if ('/role' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not_aplication_default_role_index;
                } else {
                    return $this->redirect($rawPathinfo.'/', 'aplication_default_role_index');
                }

                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_aplication_default_role_index;
                }

                return array (  '_controller' => 'Aplication\\DefaultBundle\\Controller\\RoleController::indexAction',  '_route' => 'aplication_default_role_index',);
            }
            not_aplication_default_role_index:

            // aplication_default_role_add
            if ('/role/add' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_aplication_default_role_add;
                }

                return array (  '_controller' => 'Aplication\\DefaultBundle\\Controller\\RoleController::addAction',  '_route' => 'aplication_default_role_add',);
            }
            not_aplication_default_role_add:

            // aplication_default_role_edit
            if (0 === strpos($pathinfo, '/role/edit') && preg_match('#^/role/edit(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_aplication_default_role_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_role_edit')), array (  'id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\RoleController::editAction',));
            }
            not_aplication_default_role_edit:

            // aplication_default_role_save
            if (0 === strpos($pathinfo, '/role/save') && preg_match('#^/role/save(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_aplication_default_role_save;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_role_save')), array (  'id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\RoleController::saveAction',));
            }
            not_aplication_default_role_save:

            // aplication_default_role_delete
            if (0 === strpos($pathinfo, '/role/delete') && preg_match('#^/role/delete(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_aplication_default_role_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_role_delete')), array (  'id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\RoleController::deleteAction',));
            }
            not_aplication_default_role_delete:

            if (0 === strpos($pathinfo, '/role/permission')) {
                // aplication_default_role_permission
                if (preg_match('#^/role/permission(?:/(?P<role_id>\\d+))?$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_aplication_default_role_permission;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_role_permission')), array (  'role_id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\RoleController::permissionAction',));
                }
                not_aplication_default_role_permission:

                // aplication_default_role_permission_save
                if (0 === strpos($pathinfo, '/role/permission/save') && preg_match('#^/role/permission/save(?:/(?P<role_id>\\d+))?$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_aplication_default_role_permission_save;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_role_permission_save')), array (  'role_id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\RoleController::permissionSaveAction',));
                }
                not_aplication_default_role_permission_save:

            }

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // aplication_default_profile_index
            if ('/profile' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not_aplication_default_profile_index;
                } else {
                    return $this->redirect($rawPathinfo.'/', 'aplication_default_profile_index');
                }

                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_aplication_default_profile_index;
                }

                return array (  '_controller' => 'Aplication\\DefaultBundle\\Controller\\ProfileController::indexAction',  '_route' => 'aplication_default_profile_index',);
            }
            not_aplication_default_profile_index:

            // aplication_default_profile_add
            if ('/profile/add' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_aplication_default_profile_add;
                }

                return array (  '_controller' => 'Aplication\\DefaultBundle\\Controller\\ProfileController::addAction',  '_route' => 'aplication_default_profile_add',);
            }
            not_aplication_default_profile_add:

            // aplication_default_profile_edit
            if (0 === strpos($pathinfo, '/profile/edit') && preg_match('#^/profile/edit(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_aplication_default_profile_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_profile_edit')), array (  'id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\ProfileController::editAction',));
            }
            not_aplication_default_profile_edit:

            // aplication_default_profile_save
            if (0 === strpos($pathinfo, '/profile/save') && preg_match('#^/profile/save(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_aplication_default_profile_save;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_profile_save')), array (  'id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\ProfileController::saveAction',));
            }
            not_aplication_default_profile_save:

            // aplication_default_profile_delete
            if (0 === strpos($pathinfo, '/profile/delete') && preg_match('#^/profile/delete(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_aplication_default_profile_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_profile_delete')), array (  'id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\ProfileController::deleteAction',));
            }
            not_aplication_default_profile_delete:

            if (0 === strpos($pathinfo, '/profile/role')) {
                // aplication_default_profile_roles
                if (0 === strpos($pathinfo, '/profile/roleassign') && preg_match('#^/profile/roleassign(?:/(?P<profile_id>\\d+))?$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_aplication_default_profile_roles;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_profile_roles')), array (  'profile_id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\ProfileController::rolesassignAction',));
                }
                not_aplication_default_profile_roles:

                // aplication_default_profile_roles_save
                if (0 === strpos($pathinfo, '/profile/roles/save') && preg_match('#^/profile/roles/save(?:/(?P<profile_id>\\d+))?$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_aplication_default_profile_roles_save;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_profile_roles_save')), array (  'profile_id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\ProfileController::rolesSaveAction',));
                }
                not_aplication_default_profile_roles_save:

            }

        }

        if (0 === strpos($pathinfo, '/user')) {
            // aplication_default_user_index
            if ('/user' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not_aplication_default_user_index;
                } else {
                    return $this->redirect($rawPathinfo.'/', 'aplication_default_user_index');
                }

                return array (  '_controller' => 'Aplication\\DefaultBundle\\Controller\\UserController::indexAction',  '_route' => 'aplication_default_user_index',);
            }
            not_aplication_default_user_index:

            // aplication_default_user_add
            if ('/user/add' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_aplication_default_user_add;
                }

                return array (  '_controller' => 'Aplication\\DefaultBundle\\Controller\\UserController::addAction',  '_route' => 'aplication_default_user_add',);
            }
            not_aplication_default_user_add:

            // aplication_default_user_edit
            if (0 === strpos($pathinfo, '/user/edit') && preg_match('#^/user/edit(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_aplication_default_user_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_user_edit')), array (  'id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\UserController::editAction',));
            }
            not_aplication_default_user_edit:

            // aplication_default_user_save
            if (0 === strpos($pathinfo, '/user/save') && preg_match('#^/user/save(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_aplication_default_user_save;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_user_save')), array (  'id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\UserController::saveAction',));
            }
            not_aplication_default_user_save:

            if (0 === strpos($pathinfo, '/user/d')) {
                // aplication_default_user_delete
                if (0 === strpos($pathinfo, '/user/delete') && preg_match('#^/user/delete(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_aplication_default_user_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_user_delete')), array (  'id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\UserController::deleteAction',));
                }
                not_aplication_default_user_delete:

                if (0 === strpos($pathinfo, '/user/dinner')) {
                    // aplication_default_user_dinners
                    if (0 === strpos($pathinfo, '/user/dinnerassign') && preg_match('#^/user/dinnerassign(?:/(?P<user_id>\\d+))?$#sD', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_aplication_default_user_dinners;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_user_dinners')), array (  'user_id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\UserController::dinnersassignAction',));
                    }
                    not_aplication_default_user_dinners:

                    // aplication_default_user_dinner_save
                    if (0 === strpos($pathinfo, '/user/dinners/save') && preg_match('#^/user/dinners/save(?:/(?P<user_id>\\d+))?$#sD', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_aplication_default_user_dinner_save;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'aplication_default_user_dinner_save')), array (  'user_id' => 0,  '_controller' => 'Aplication\\DefaultBundle\\Controller\\UserController::dinnerSaveAction',));
                    }
                    not_aplication_default_user_dinner_save:

                }

            }

        }

        if (0 === strpos($pathinfo, '/bussiness')) {
            if (0 === strpos($pathinfo, '/bussiness/ticketrelease')) {
                // _bussiness_default_ticketrelease_index
                if ('/bussiness/ticketrelease' === rtrim($pathinfo, '/')) {
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                        goto not__bussiness_default_ticketrelease_index;
                    } else {
                        return $this->redirect($rawPathinfo.'/', '_bussiness_default_ticketrelease_index');
                    }

                    return array (  '_controller' => 'Bussiness\\DefaultBundle\\Controller\\TicketreleaseController::indexAction',  '_route' => '_bussiness_default_ticketrelease_index',);
                }
                not__bussiness_default_ticketrelease_index:

                // _bussiness_default_ticketrelease_ticket
                if ('/bussiness/ticketrelease/ticket' === $pathinfo) {
                    return array (  '_controller' => 'Bussiness\\DefaultBundle\\Controller\\TicketreleaseController::ticketAction',  '_route' => '_bussiness_default_ticketrelease_ticket',);
                }

                // _bussiness_default_ticketrelease_save
                if ('/bussiness/ticketrelease/save' === $pathinfo) {
                    return array (  '_controller' => 'Bussiness\\DefaultBundle\\Controller\\TicketreleaseController::saveAction',  '_route' => '_bussiness_default_ticketrelease_save',);
                }

            }

            if (0 === strpos($pathinfo, '/bussiness/service')) {
                // bussiness_default_service_index
                if ('/bussiness/service/index' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_bussiness_default_service_index;
                    }

                    return array (  '_controller' => 'Bussiness\\DefaultBundle\\Controller\\ServiceController::indexAction',  '_route' => 'bussiness_default_service_index',);
                }
                not_bussiness_default_service_index:

                // bussiness_default_service_add
                if ('/bussiness/service/add' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_bussiness_default_service_add;
                    }

                    return array (  '_controller' => 'Bussiness\\DefaultBundle\\Controller\\ServiceController::addAction',  '_route' => 'bussiness_default_service_add',);
                }
                not_bussiness_default_service_add:

                // bussiness_default_service_edit
                if (0 === strpos($pathinfo, '/bussiness/service/edit') && preg_match('#^/bussiness/service/edit(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_bussiness_default_service_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'bussiness_default_service_edit')), array (  'id' => 0,  '_controller' => 'Bussiness\\DefaultBundle\\Controller\\ServiceController::editAction',));
                }
                not_bussiness_default_service_edit:

                // bussiness_default_service_save
                if (0 === strpos($pathinfo, '/bussiness/service/save') && preg_match('#^/bussiness/service/save(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_bussiness_default_service_save;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'bussiness_default_service_save')), array (  'id' => 0,  '_controller' => 'Bussiness\\DefaultBundle\\Controller\\ServiceController::saveAction',));
                }
                not_bussiness_default_service_save:

                // bussiness_default_service_delete
                if (0 === strpos($pathinfo, '/bussiness/service/delete') && preg_match('#^/bussiness/service/delete(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_bussiness_default_service_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'bussiness_default_service_delete')), array (  'id' => 0,  '_controller' => 'Bussiness\\DefaultBundle\\Controller\\ServiceController::deleteAction',));
                }
                not_bussiness_default_service_delete:

            }

        }

        if (0 === strpos($pathinfo, '/admin')) {
            // administration_default_homepage
            if ('/admin' === $pathinfo) {
                return array (  '_controller' => 'Administration\\DefaultBundle\\Controller\\DefaultController::indexAction',  '_route' => 'administration_default_homepage',);
            }

            if (0 === strpos($pathinfo, '/admin/co')) {
                if (0 === strpos($pathinfo, '/admin/contract')) {
                    // administration_default_contract_index
                    if ('/admin/contract/index' === $pathinfo) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_administration_default_contract_index;
                        }

                        return array (  '_controller' => 'Administration\\DefaultBundle\\Controller\\ContractController::indexAction',  '_route' => 'administration_default_contract_index',);
                    }
                    not_administration_default_contract_index:

                    // administration_default_contract_add
                    if ('/admin/contract/add' === $pathinfo) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_administration_default_contract_add;
                        }

                        return array (  '_controller' => 'Administration\\DefaultBundle\\Controller\\ContractController::addAction',  '_route' => 'administration_default_contract_add',);
                    }
                    not_administration_default_contract_add:

                    // administration_default_contract_edit
                    if (0 === strpos($pathinfo, '/admin/contract/edit') && preg_match('#^/admin/contract/edit(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_administration_default_contract_edit;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'administration_default_contract_edit')), array (  'id' => 0,  '_controller' => 'Administration\\DefaultBundle\\Controller\\ContractController::editAction',));
                    }
                    not_administration_default_contract_edit:

                    // administration_default_contract_save
                    if (0 === strpos($pathinfo, '/admin/contract/save') && preg_match('#^/admin/contract/save(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_administration_default_contract_save;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'administration_default_contract_save')), array (  'id' => 0,  '_controller' => 'Administration\\DefaultBundle\\Controller\\ContractController::saveAction',));
                    }
                    not_administration_default_contract_save:

                    // administration_default_contract_delete
                    if (0 === strpos($pathinfo, '/admin/contract/delete') && preg_match('#^/admin/contract/delete(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_administration_default_contract_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'administration_default_contract_delete')), array (  'id' => 0,  '_controller' => 'Administration\\DefaultBundle\\Controller\\ContractController::deleteAction',));
                    }
                    not_administration_default_contract_delete:

                }

                if (0 === strpos($pathinfo, '/admin/company')) {
                    // administration_default_company_index
                    if ('/admin/company/index' === $pathinfo) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_administration_default_company_index;
                        }

                        return array (  '_controller' => 'Administration\\DefaultBundle\\Controller\\CompanyController::indexAction',  '_route' => 'administration_default_company_index',);
                    }
                    not_administration_default_company_index:

                    // administration_default_company_add
                    if ('/admin/company/add' === $pathinfo) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_administration_default_company_add;
                        }

                        return array (  '_controller' => 'Administration\\DefaultBundle\\Controller\\CompanyController::addAction',  '_route' => 'administration_default_company_add',);
                    }
                    not_administration_default_company_add:

                    // administration_default_company_edit
                    if (0 === strpos($pathinfo, '/admin/company/edit') && preg_match('#^/admin/company/edit(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_administration_default_company_edit;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'administration_default_company_edit')), array (  'id' => 0,  '_controller' => 'Administration\\DefaultBundle\\Controller\\CompanyController::editAction',));
                    }
                    not_administration_default_company_edit:

                    // administration_default_company_save
                    if (0 === strpos($pathinfo, '/admin/company/save') && preg_match('#^/admin/company/save(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_administration_default_company_save;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'administration_default_company_save')), array (  'id' => 0,  '_controller' => 'Administration\\DefaultBundle\\Controller\\CompanyController::saveAction',));
                    }
                    not_administration_default_company_save:

                    // administration_default_company_delete
                    if (0 === strpos($pathinfo, '/admin/company/delete') && preg_match('#^/admin/company/delete(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_administration_default_company_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'administration_default_company_delete')), array (  'id' => 0,  '_controller' => 'Administration\\DefaultBundle\\Controller\\CompanyController::deleteAction',));
                    }
                    not_administration_default_company_delete:

                    if (0 === strpos($pathinfo, '/admin/company/assigndinner')) {
                        // administration_default_company_assigndinner
                        if (preg_match('#^/admin/company/assigndinner(?:/(?P<company_id>\\d+))?$#sD', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                                goto not_administration_default_company_assigndinner;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'administration_default_company_assigndinner')), array (  'company_id' => 0,  '_controller' => 'Administration\\DefaultBundle\\Controller\\CompanyController::assigndinnerAction',));
                        }
                        not_administration_default_company_assigndinner:

                        // administration_default_company_assigndinner_save
                        if (0 === strpos($pathinfo, '/admin/company/assigndinner/save') && preg_match('#^/admin/company/assigndinner/save(?:/(?P<company_id>\\d+))?$#sD', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                                goto not_administration_default_company_assigndinner_save;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'administration_default_company_assigndinner_save')), array (  'company_id' => 0,  '_controller' => 'Administration\\DefaultBundle\\Controller\\CompanyController::assigndinnerSaveAction',));
                        }
                        not_administration_default_company_assigndinner_save:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/dinner')) {
                // administration_default_dinner_index
                if ('/admin/dinner/index' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_administration_default_dinner_index;
                    }

                    return array (  '_controller' => 'Administration\\DefaultBundle\\Controller\\DinnerController::indexAction',  '_route' => 'administration_default_dinner_index',);
                }
                not_administration_default_dinner_index:

                // administration_default_dinner_add
                if ('/admin/dinner/add' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_administration_default_dinner_add;
                    }

                    return array (  '_controller' => 'Administration\\DefaultBundle\\Controller\\DinnerController::addAction',  '_route' => 'administration_default_dinner_add',);
                }
                not_administration_default_dinner_add:

                // administration_default_dinner_edit
                if (0 === strpos($pathinfo, '/admin/dinner/edit') && preg_match('#^/admin/dinner/edit(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_administration_default_dinner_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'administration_default_dinner_edit')), array (  'id' => 0,  '_controller' => 'Administration\\DefaultBundle\\Controller\\DinnerController::editAction',));
                }
                not_administration_default_dinner_edit:

                // administration_default_dinner_save
                if (0 === strpos($pathinfo, '/admin/dinner/save') && preg_match('#^/admin/dinner/save(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_administration_default_dinner_save;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'administration_default_dinner_save')), array (  'id' => 0,  '_controller' => 'Administration\\DefaultBundle\\Controller\\DinnerController::saveAction',));
                }
                not_administration_default_dinner_save:

                // administration_default_dinner_delete
                if (0 === strpos($pathinfo, '/admin/dinner/delete') && preg_match('#^/admin/dinner/delete(?:/(?P<id>\\d+))?$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_administration_default_dinner_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'administration_default_dinner_delete')), array (  'id' => 0,  '_controller' => 'Administration\\DefaultBundle\\Controller\\DinnerController::deleteAction',));
                }
                not_administration_default_dinner_delete:

            }

            if (0 === strpos($pathinfo, '/admin/roster')) {
                // administration_default_roster_index
                if ('/admin/roster/index' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_administration_default_roster_index;
                    }

                    return array (  '_controller' => 'Administration\\DefaultBundle\\Controller\\RosterController::indexAction',  '_route' => 'administration_default_roster_index',);
                }
                not_administration_default_roster_index:

                // administration_default_roster_upload
                if ('/admin/roster/upload' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_administration_default_roster_upload;
                    }

                    return array (  '_controller' => 'Administration\\DefaultBundle\\Controller\\RosterController::uploadAction',  '_route' => 'administration_default_roster_upload',);
                }
                not_administration_default_roster_upload:

            }

        }

        // _service_dinner_register_index
        if ('/service/dinner/register' === rtrim($pathinfo, '/')) {
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                goto not__service_dinner_register_index;
            } else {
                return $this->redirect($rawPathinfo.'/', '_service_dinner_register_index');
            }

            return array (  '_controller' => 'Service\\DinnerBundle\\Controller\\RegisterController::indexAction',  '_route' => '_service_dinner_register_index',);
        }
        not__service_dinner_register_index:

        // homepage
        if ('' === rtrim($pathinfo, '/')) {
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                goto not_homepage;
            } else {
                return $this->redirect($rawPathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }
        not_homepage:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
