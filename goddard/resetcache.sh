#! /bin/bash
echo "Limpiando Cache"
php app/console cache:clear --env=prod
echo "Eliminando cache, logs "
rm -rf app/cache
rm -rf app/logs
echo "Permisos"
chmod -R 775 src


