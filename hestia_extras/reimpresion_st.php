<?php
include_once dirname(__FILE__) . '/etc/conf.php';
include_once dirname(__FILE__) . '/etc/da_reimpresion_st.php';

if (!empty($_REQUEST['accion'])) {
    $op = $_REQUEST['accion'];
} else {
    $op = '';
}
switch ($op) {
    case 'ident':
        $da_reim = new da_reimpresion_st();
        $identi = explode("\n", $_REQUEST['ident']);
        echo "<table>";
        $flag = 0;
        $arrFin[] = "";
        foreach ($identi as &$value) {
            $value = trim($value);
            $rs = $da_reim->revisa($value);
            echo "<tr>";
            if ($rs) {
                if ($rs[0]['ctstatus_id'] == 505) {
                    $flag++;
                    $arrFin[] = $rs[0]['id'];
                    echo "<td><img src='./etc/img/icon-true.png'/></td><td><b>" . $value . "</b></td><td>Registro Encontrado<td>";
                } else
                    echo "<td><img src='./etc/img/icon-x.png'/></td><td><b>" . $value . "</b></td><td>" . $rs[0]['name'] . "</br>";
            }
            else {
                echo "<td><img src='./etc/img/icon-x.png'/></td><td><b>" . $value . "</b></td><td>No existe registro</br>";
            }
            echo "</tr>";
        }
        echo "</table>";
        if ($flag > 0) {
            echo "<script>$('#div_print').show();</script>";
        } else {
            echo "<script>$('#div_print').hide();</script>";
        }
        unset($arrFin[0]);

        $datos['detail'] = "'" . $_REQUEST['obs'] . "'";
        //$datos['usucrud_id'] = $_REQUEST['user'];
        $da_reim->crearPadreSticker($datos);
        $idPadre = $da_reim->ultimoPadreSticker();
        foreach ($arrFin as &$value) {
            $dat['roster_id'] = $value;
            $dat['sticker_id'] = $idPadre[0]['id'];
            $da_reim->crearGrupoStickerRoster($dat);
        }
        $grupo = $idPadre[0]['id'];

        function round_up($number, $precision = 2) {
            $fig = (int) str_pad('1', $precision, '0');
            return (ceil($number * $fig) / $fig);
        }

        $contGD = $da_reim->countGrupoSticker($grupo);
        $limit = 100;
        $num = $contGD[0]['conteo'] / $limit;
        $num = round_up($num, 0);
        $botPrint = '';
        for ($i = 0; $i < $num; $i++) {
            $g = 0;
            $g = $i + 1;
            $to = 0;
            $to = $limit * $i;
            $botPrint = $botPrint . '<a id="btn" href="print_ident_bar.php?gd=' . $grupo . '&to=' . $to . '" target="_blank" class="easyui-linkbutton"><img src="./etc/img/printer.png"/> -> Imprimir Grupo ' . ($g) . '</a></br>';
        }

        echo"<script>$('#div_print').html('$botPrint');</script>";
        echo"<script>    
            $('#win_imp_indent').window({
                width:600,
                height:350,
                modal:true,
                collapsible:false, 
                minimizable:false,
                maximizable:false,
                resizable:false, 
                title:'Reimpresión de Adhesivos'
            });
            $('#win_imp_indent').show();
            </script>";
        exit();
        break;


    case 'catalogoEmpresa':
        $cat = new Bl_Reporte();
        $rs = $cat->catalogoEmpresa();
        echo json_encode($rs);
        exit();
        break;
}
?>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>HESTIA - Reimpresión Adhesivos </title>
        <link rel="stylesheet" href="etc/lib/css/estilo.css"/>

        <!-- jquery.easy -->
        <link rel="stylesheet" type="text/css" href="etc/lib/jquery-easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="etc/lib/jquery-easyui/themes/icon.css">
        <script src="etc/lib/jquery-easyui/jquery-1.12.4.min.js" type="text/javascript"></script>
        <script src="etc/lib/jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>
        <script src="etc/lib/jquery-easyui/locale/easyui-lang-es.js" type="text/javascript"></script>


        <!-- extensiones para validar-->
        <script src="etc/lib/js/valida.js" type="text/javascript" ></script>
        <script src="etc/lib/js/hestia.js" type="text/javascript" ></script>

        <script type="text/javascript">
            $(function () {
                function frm(rec) {
                    $("#div_idn").hide();
                    $("#div_emp").hide();
                    $("#div_cc").hide();
                    $("#div_obs").hide();
                    $('#txt_identificacion').val("");
                    $('#txt_observacion').val("");
                    $('#cmb_empresa').combobox({
                        required: false
                    });
                    $('#cmb_cc').combobox({
                        required: false
                    });
                    $('#txt_identificacion').validatebox({
                        required: false
                    });
                    $('#txt_observacion').validatebox({
                        required: false
                    });
                    if (rec.value == 1) { //Empresa
                        $("#div_emp").show();
                        $("#div_cc").show();
                        $("#div_obs").show();
                        $('#txt_observacion').validatebox({
                            required: true
                        });
                        $('#cmb_empresa').combobox({
                            required: true
                        });
                        $('#cmb_cc').combobox({
                            required: true
                        });
                    } else if (rec.value == 2) { //Identificacion
                        $("#div_idn").show();
                        $('#txt_identificacion').validatebox({
                            required: true
                        });
                        $("#div_obs").show();
                        $('#txt_observacion').validatebox({
                            required: true
                        });
                    }

                }


                $('#tipo').combobox({
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 100,
                    width: 250,
                    editable: false,
                    required: true,
                    data: [
//                        {
//                            label: 'Por Empresa',
//                            value: '1'
//                        }
//                        , 
                        {
                            label: 'Por Identificación/Código Empleado',
                            value: '2'
                        }
                    ],
                    onSelect: function (rec) {
                        frm(rec);
                    }
                });
                $('#cmb_cc').combobox({
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 100,
                    width: 250,
                    editable: false,
                    required: true,
                    data: [{
                            label: 'E401100001 | Reporte de Negocio',
                            value: '0'
                        }


                        , {
                            label: 'E409100001 | Procesos',
                            value: '1'
                        }
                        , {
                            label: 'E404300001 | Seguridad',
                            value: '2'
                        }
                        , {
                            label: 'E404200001 | Seguridad Industrial',
                            value: '3'
                        }
                        //, {
//                            label: 'Por Identificación/Código Empleado',
//                            value: '4'
//                        }
//                    

                    ],
                    onSelect: function (rec) {
                        //frm(rec);
                    }
                });
                $('#cmb_empresa').combobox({
                    url: 'index.php?accion=catalogoEmpresa',
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 150,
                    width: 350,
                    editable: false,
                });
                $('#btnCerrar').click(function () {
                    $('#win_imp_indent').window('close');
                });

                $('#btnImprimir').click(function () {
                    if ($("#frmDatos").form('validate')) {
                        if ($('#tipo').combo('getValue') == 2) {
                            $.post('reimpresion_st.php', {accion: 'ident', ident: $('#txt_identificacion').val(), obs: $('#txt_observacion').val()},
                                    function (resultado) {
                                        $('#div_html').html(resultado)
                                    }, 'html');
                        } else {

                        }


                    } else {
                        $.messager.show({
                            title: 'Atención',
                            msg: 'Por favor, llene todos los campos requeridos para poder generar el reporte'
                        });
                    }
                });
            });
        </script>
    </head>

    <body>
        <div id="header">
            <div class="fila" >
                <div id="divNombreAplicacion" style="float:right;"> 
                    <img src="./etc/lib/img/goddard.gif" style="width: 110px; height: 50px"/>
                    &nbsp;
                    Reimpresión Adhesivos
                </div>
            </div>
        </div>
        <div id="cuerpo" style="background-color: #FFFFFF; padding: 20px;">
            <form id="frmDatos" method="post" >
                <fieldset class="clsFieldset" style="width:90%; text-align:left; padding-left: 10px; padding-top: 10px;">
                    <div class="fila">
                        <div class="celda">
                            <div class="etiqueta">
                                <label> Tipo de Reimpresión </label>
                            </div>
                            <div>
                                <input name="tipo" id="tipo" value=""/> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" id="div_obs" style="display:none;">
                            <div class="etiqueta">
                                <label>Motivo de Reimpresión</label><br/>
                                <textarea class="combo" rows="1" cols="50" name="txt_observacion" id="txt_observacion" value=""> </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" id="div_emp" style="display:none;">
                            <div class="etiqueta">
                                <label>Empresa</label><br/>
                                <input name="cmb_empresa" id="cmb_empresa" value="" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila">
                        <div class="celda"id="div_cc" style="display:none;">
                            <div class="etiqueta">
                                <label> Centro de Costo </label>
                            </div>
                            <div>
                                <input name="cmb_cc" id="cmb_cc" value=""/> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" id="div_idn" style="display:none;">
                            <div class="etiqueta">
                                <label>Identificación/Código Empleado</label><br/>
                                <textarea class="combo" rows="7" cols="20" name="txt_identificacion" id="txt_identificacion" value=""> </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="fila">
                        <div class="celda" style="text-align: center;padding-top: 10px;">
                            <a id="btnImprimir" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-tip'">Validar Registros</a>  
                        </div>
                    </div>
                </fieldset>
            </form>
            <div id="win_imp_indent" style="display: none;">
                <div class="fila">
                    <div class="celda" style="width: 70%">
                        <div class="etiqueta">
                            <label>
                                <center><b>Nota:</b>
                                    <i>Solo se generarán adhesivos confirmados</i>
                                <img src="./etc/img/icon-true.png"/></label></center>
                        </div>
                    </div>
                    <div class="celda" style="width: 30%">
                        <a id="btnCerrar" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-no'">Cerrar Ventana</a>
                    </div>
                </div>
                <div class="fila">
                    <div class="celda" style="width: 70%"> 
                        <fieldset class="clsFieldset" style="width:90%; text-align:left; padding-left: 10px; padding-top: 10px;">
                            <div id="div_html" style="overflow-x: hidden;overflow-y: auto;height: 220px;">
                            </div>
                        </fieldset>
                    </div>
                    <div class="celda" style="width: 30%"> 
                        <div id="div_print"></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>