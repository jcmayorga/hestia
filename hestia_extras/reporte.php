<?php
include_once dirname(__FILE__) . '/etc/conf.php';
include_once dirname(__FILE__) . '/etc/reporte_bl.php';
include_once dirname(__FILE__) . '/etc/lib/PHPExcel/Classes/PHPExcel.php';
include_once dirname(__FILE__) . '/etc/lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
include_once EXCEL_CLASE_FAST;



if (!empty($_REQUEST['accion'])) {
    $op = $_REQUEST['accion'];
} else {
    $op = '';
}
switch ($op) {
    case 'generaReporte':
        $dataRep = new Bl_Reporte();
        $data = $dataRep->reporteTipo($_REQUEST);
        if ($_REQUEST["tipo"] == 3) {
            echo json_encode($data);
        } else if ($_REQUEST["tipo"] == 1) { //Empresa - Contrato
            echo json_encode($data);
        } else {
            $array = array();
            foreach ($data as $key => $value) {
                //unset($arrayPlazas[$key]['numerofila'],$arrayPlazas[$key]['n_id_unidad_operativa']);
                $array[] = $data [$key];
            }
            $e = new Excel("Reporte");
            $e->setHoja("Rep", $array);
            $e->generaExcel();
        }
        exit();
        break;

    case 'generaReporte2':
        $dataRep = new Bl_Reporte();
        $inicio = $_REQUEST["fin"];
        $fin = $_REQUEST["ffi"];
        $emp = 0;
        if (isset($_REQUEST["emp"]))
            $emp = $_REQUEST["emp"];

        $data = $dataRep->reporteTipo($inicio, $fin, $emp);
        $array = array();
        foreach ($data as $key => $value) {
            //unset($arrayPlazas[$key]['numerofila'],$arrayPlazas[$key]['n_id_unidad_operativa']);
            $array[] = $data [$key];
        }
        $e = new Excel("Reporte");
        $e->setHoja("Rep", $array);
        $e->generaExcel();
        exit();
        break;

    case 'catalogoEmpresa':
        $cat = new Bl_Reporte();
        $rs = $cat->catalogoEmpresa($_REQUEST);
        echo json_encode($rs);
        exit();
        break;

    case 'catalogoContrato':
        $cat = new Bl_Reporte();
        $rs = $cat->catalogoContrato();
        echo json_encode($rs);
        exit();
        break;
}
?>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>HESTIA</title>
        <link rel="stylesheet" href="etc/lib/css/estilo.css"/>

        <!-- jquery.easy -->
        <link rel="stylesheet" type="text/css" href="etc/lib/jquery-easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="etc/lib/jquery-easyui/themes/icon.css">
        <script src="etc/lib/jquery-easyui/jquery-1.12.4.min.js" type="text/javascript"></script>
        <script src="etc/lib/jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>
        <script src="etc/lib/jquery-easyui/locale/easyui-lang-es.js" type="text/javascript"></script>


        <!-- extenciones para validar-->
        <script src="etc/lib/js/valida.js" type="text/javascript" ></script>
        <script src="etc/lib/js/hestia.js" type="text/javascript" ></script>

        <script type="text/javascript">
            $(function () {
                function frm(rec) {
                    $("#div_din").hide();
                    $("#div_con").hide();
                    $("#div_empcon").hide();
                    $("#div_idn").hide();
                    $("#div_emp").hide();
                    $("#div_ser").hide();
                    $('#txt_identificacion').val("");
                    $('#cmb_empresa').combobox({
                        required: false
                    });
                    $('#cmb_empresacon').combobox({
                        url: 'reporte.php',
                        required: false
                    });
                    $('#cmb_servicio').combobox({
                        required: false
                    });
                    $('#cmb_dinner').combobox({
                        required: false
                    });
                    $('#cmb_contrato').combobox({
                        required: false
                    });
                    $('#txt_identificacion').validatebox({
                        required: false
                    });
                    if (rec.value == 1) { //Contrato->Empresa
                        $("#div_empcon").show();
                        $("#div_con").show();
                        $("#div_ser").show();
                        $('#cmb_empresacon').combobox({
                            required: true
                        });
                        $('#cmb_contrato').combobox({
                            required: true
                        });
                        $('#cmb_servicio').combobox({
                            required: true
                        });
                    } else if (rec.value == 2) { //Comedor
                        $("#div_din").show();
                        $('#cmb_dinner').combobox({
                            required: true
                        });
                    } else if (rec.value == 3) { //Servicio
                        $("#div_emp").show();
                        $('#cmb_empresa').combobox({
                            required: true
                        });
                        $("#div_ser").show();
                        $('#cmb_servicio').combobox({
                            required: true
                        });
                    } else if (rec.value == 4) { //Identificacion
                        $("#div_idn").show();
                        $('#txt_identificacion').validatebox({
                            required: true
                        });
                    }

                    $('#fecha_inicio').datebox('setValue', '');
                    $('#fecha_fin').datebox('setValue', '');
                }


                $('#tipo').combobox({
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 100,
                    width: 250,
                    editable: false,
                    required: true,
                    data: [{
                            label: 'Bitácora General',
                            value: '0'
                        }


                        , {
                            label: 'Consolidado por Empresa',
                            value: '1'
                        }
//                        , {
//                            label: 'Por Comedor',
//                            value: '2'
//                        }
//                        , {
//                            label: 'Por Servicio',
//                            value: '3'
//                        }
                        , {
                            label: 'Por Identificación/Código Empleado',
                            value: '4'
                        }
                        , {
                            label: 'Producción Comedores',
                            value: '5'
                        }
                    ],
                    onSelect: function (rec) {
                        frm(rec);
                    }
                });
                $('#cmb_empresa').combobox({
                    url: 'reporte.php?accion=catalogoEmpresa',
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 150,
                    width: 350,
                    editable: false,
                    required: true

                });
                $('#cmb_empresacon').combobox({
                    url: 'reporte.php',
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 150,
                    width: 350,
                    multiple: true,
                    editable: false,
                    required: true

                });
                $('#cmb_contrato').combobox({
                    url: 'reporte.php?accion=catalogoContrato',
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 100,
                    width: 150,
                    editable: false,
                    required: true,
                    onChange: function () {
                        $('#cmb_empresacon').combobox('reload', 'reporte.php?accion=catalogoEmpresa&catEmpTip=2&cont=' + $('#cmb_contrato').combo('getValue'));
                        $('#cmb_empresacon').combobox({
                            required: true
                        });
                    }
                });
                $('#cmb_servicio').combobox({
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 200,
                    width: 250,
                    editable: false,
                    required: true,
                    data: [
                        {
                            label: 'Desayuno Reforzado 03:20 - 07:49',
                            value: 'DR'
                        },
                        {
                            label: 'Desayuno 07:50 - 11:49',
                            value: 'D'
                        },
                        {
                            label: 'Almuerzo 11:50 - 14:45',
                            value: 'A'
                        }
                        , {
                            label: 'Merienda 18:50 - 21:00',
                            value: 'M'
                        }
                        , {
                            label: 'Cena 22:00 - 23:59',
                            value: 'C'
                        }
                    ]
                });
                $('#cmb_dinner').combobox({
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 100,
                    width: 250,
                    editable: false,
                    required: true,
                    data: [{
                            label: 'Dinn 01',
                            value: '1'
                        }, {
                            label: 'Dinn 02',
                            value: '2'
                        }]
                });
                $('#fecha_inicio').datebox({
                    editable: false,
                    required: true,
                    formatter: formatoSalud,
                    parser: parserSalud
                });
                $('#fecha_fin').datebox({
                    editable: false,
                    required: true,
                    formatter: formatoSalud,
                    parser: parserSalud
                });
                $('#txt_identificacion').validatebox({
                    required: true
                });
                $('#btnIngresar').click(function () {
                    if ($("#frmDatos").form('validate')) {
                        if ($('#tipo').combo('getValue') == 3) {
                            var win2 = window.open("./reporte.php?accion=generaReporte&tipo=" + $('#tipo').combo('getValue') + "&fin=" + $('#fecha_inicio').combo('getText') + "&ffi=" + $('#fecha_fin').combo('getText') + "&emp=" + $('#cmb_empresa').combo('getValue') + "&se=" + $('#cmb_servicio').combo('getValue'), "Juk", "width=600,height=300");
                            //$.post('index.php', {accion: 'generaReporte', tipo: , fin: $('#fecha_inicio').combo('getText'), ffi: $('#fecha_fin').combo('getText'), emp: $('#cmb_empresa').combo('getValue')},
                            //function(resultado) {
                            //   $('#div_html').html(resultado)
                            //}, 'html');
                        } else if ($('#tipo').combo('getValue') == 1) { //Empresa - Contrato
                            var win1 = window.open("./reporte.php?accion=generaReporte&tipo=" + $('#tipo').combo('getValue') + "&fin=" + $('#fecha_inicio').combo('getText') + "&ffi=" + $('#fecha_fin').combo('getText') + "&emp=" + $('#cmb_empresacon').combo('getValues') + "&se=" + $('#cmb_servicio').combo('getValue'), "Juk2", "");
                            //$.post('reporte.php', {accion: 'generaReporte', tipo: , fin: $('#fecha_inicio').combo('getText'), ffi: $('#fecha_fin').combo('getText'), emp: $('#cmb_empresa').combo('getValue')},
                            //function(resultado) {
                            //   $('#div_html').html(resultado)
                            //}, 'html');
                        } else if ($('#tipo').combo('getValue') == 4) { //Por identificación Empleado
                            var win = window.open("./reporte.php?accion=generaReporte&tipo=" + $('#tipo').combo('getValue') + "&fin=" + $('#fecha_inicio').combo('getText') + "&ffi=" + $('#fecha_fin').combo('getText') + "&ident=" + $('#txt_identificacion').val(), "Juk", "width=200,height=100");
                        } else {
                            var win = window.open("./reporte.php?accion=generaReporte&tipo=" + $('#tipo').combo('getValue') + "&fin=" + $('#fecha_inicio').combo('getText') + "&ffi=" + $('#fecha_fin').combo('getText') + "&emp=" + $('#cmb_empresa').combo('getValue'), "Juk", "width=200,height=100");
                            setTimeout(function () {
                                win.close();
                            }, 30000);
                        }


                    } else {
                        $.messager.show({
                            title: 'Atención',
                            msg: 'Por favor, llene todos los campos requeridos para poder generar el reporte'
                        });
                    }
                });

                function fnExcelReport() {
                    var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
                    tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';
                    tab_text = tab_text + '<x:Name>Test Sheet</x:Name>';
                    tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
                    tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
                    tab_text = tab_text + "<table border='1px'>";
                    tab_text = tab_text + $('#myRep01').html();
                    tab_text = tab_text + '</table></body></html>';
                    var data_type = 'data:application/vnd.ms-excel';
                    var ua = window.navigator.userAgent;
                    var msie = ua.indexOf("MSIE ");
                    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                        if (window.navigator.msSaveBlob) {
                            var blob = new Blob([tab_text], {
                                type: "application/csv;charset=utf-8;"
                            });
                            navigator.msSaveBlob(blob, 'Test file.xls');
                        }
                    } else {
                        $('#test').attr('href', data_type + ', ' + encodeURIComponent(tab_text));
                        $('#test').attr('download', 'Test file.xls');
                    }

                }


                var tablesToExcel = (function () {
                    var uri = 'data:application/vnd.ms-excel;base64,'
                            , tmplWorkbookXML = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">'
                            + '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>'
                            + '<Styles>'
                            + '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>'
                            + '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>'
                            + '</Styles>'
                            + '{worksheets}</Workbook>'
                            , tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>'
                            , tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>'
                            , base64 = function (s) {
                                return window.btoa(unescape(encodeURIComponent(s)))
                            }
                    , format = function (s, c) {
                        return s.replace(/{(\w+)}/g, function (m, p) {
                            return c[p];
                        })
                    }
                    return function (tables, wsnames, wbname, appname) {
                        var ctx = "";
                        var workbookXML = "";
                        var worksheetsXML = "";
                        var rowsXML = "";

                        for (var i = 0; i < tables.length; i++) {
                            if (!tables[i].nodeType)
                                tables[i] = document.getElementById(tables[i]);
                            for (var j = 0; j < tables[i].rows.length; j++) {
                                rowsXML += '<Row>'
                                for (var k = 0; k < tables[i].rows[j].cells.length; k++) {
                                    var dataType = tables[i].rows[j].cells[k].getAttribute("data-type");
                                    var dataStyle = tables[i].rows[j].cells[k].getAttribute("data-style");
                                    var dataValue = tables[i].rows[j].cells[k].getAttribute("data-value");
                                    dataValue = (dataValue) ? dataValue : tables[i].rows[j].cells[k].innerHTML;
                                    var dataFormula = tables[i].rows[j].cells[k].getAttribute("data-formula");
                                    dataFormula = (dataFormula) ? dataFormula : (appname == 'Calc' && dataType == 'DateTime') ? dataValue : null;
                                    ctx = {attributeStyleID: (dataStyle == 'Currency' || dataStyle == 'Date') ? ' ss:StyleID="' + dataStyle + '"' : ''
                                        , nameType: (dataType == 'Number' || dataType == 'DateTime' || dataType == 'Boolean' || dataType == 'Error') ? dataType : 'String'
                                        , data: (dataFormula) ? '' : dataValue
                                        , attributeFormula: (dataFormula) ? ' ss:Formula="' + dataFormula + '"' : ''
                                    };
                                    rowsXML += format(tmplCellXML, ctx);
                                }
                                rowsXML += '</Row>'
                            }
                            ctx = {rows: rowsXML, nameWS: wsnames[i] || 'Sheet' + i};
                            worksheetsXML += format(tmplWorksheetXML, ctx);
                            rowsXML = "";
                        }

                        ctx = {created: (new Date()).getTime(), worksheets: worksheetsXML};
                        workbookXML = format(tmplWorkbookXML, ctx);

                        console.log(workbookXML);

                        var link = document.createElement("A");
                        link.href = uri + base64(workbookXML);
                        link.download = wbname || 'Workbook.xls';
                        link.target = '_blank';
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    }
                })();

            });
        </script>
    </head>

    <body>
        <div id="header">
            <div class="fila" >
                <div id="divNombreAplicacion" style="float:right;"> 
                    <img src="./etc/lib/img/goddard.gif" style="width: 110px; height: 50px"/>
                    &nbsp;
                    Reportería "Sistema de Gestión de Alimentos"
                </div>

            </div>
        </div>
        <div id="cuerpo" style="background-color: #FFFFFF; padding: 20px;">

            <form id="frmDatos" method="post" >
                <fieldset class="clsFieldset" style="width:90%; text-align:left; padding-left: 10px; padding-top: 10px;">
                    <div class="fila">
                        <div class="celda">
                            <div class="etiqueta">
                                <label> Tipo de Reporte </label>
                            </div>
                            <div>
                                <input name="tipo" id="tipo" value=""/> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" id="div_con" style="display:none;">
                            <div class="etiqueta">
                                <label>Contrato</label><br/>
                                <input name="cmb_contrato" id="cmb_contrato" value="" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" id="div_empcon" style="display:none;">
                            <div class="etiqueta">
                                <label>Empresas</label><br/>
                                <input name="cmb_empresacon" id="cmb_empresacon" value="" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" id="div_emp" style="display:none;">
                            <div class="etiqueta">
                                <label>Empresa</label><br/>
                                <input name="cmb_empresa" id="cmb_empresa" value="" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" id="div_ser" style="display:none;">
                            <div class="etiqueta">
                                <label>Servicio</label><br/>
                                <input name="cmb_servicio" id="cmb_servicio" value="" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" id="div_din" style="display:none;">
                            <div class="etiqueta">
                                <label>Comedor</label><br/>
                                <input name="cmb_dinner" id="cmb_dinner" value="" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" id="div_idn" style="display:none;">
                            <div class="etiqueta">
                                <label>Identificación/Código Empleado</label><br/>
                                <input name="txt_identificacion" id="txt_identificacion" maxlength="80" size="30" class="combo" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" style="width:50%;">
                            <div class="etiqueta">
                                <label>Fecha Desde</label><br/>
                                <input name="fecha_inicio" id="fecha_inicio" value="" /> 
                            </div>
                        </div>
                        <div class="celda" style="width:50%;">
                            <div class="etiqueta">
                                <label>Fecha Hasta</label><br/>
                                <input name="fecha_fin" id="fecha_fin" value="" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila">
                        <div class="celda" style="text-align: center;padding-top: 10px;">
                            <a id="btnIngresar" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-ok'">Generar Reporte</a>  
                        </div>
                    </div>
                </fieldset>
            </form>
            <div id="div_html"></div>
        </div>
        <div id="footer" >
            <div>
                <br/><br/>
                <hr>
                <p style="text-align: center;">&copy; Derechos reservados 2016 - Goddard Catering Group</p>
            </div>
        </div>

    </body>
</html>