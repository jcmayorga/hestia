<?php
include_once dirname(__FILE__) . '/conf.php';
include_once dirname(__FILE__) . '/reporte_bl.php';

    

if (!empty($_REQUEST['accion'])) {
    $op = $_REQUEST['accion'];
} else {
    $op = '';
}
switch ($op) {
    case 'generaReporte':
        $dataRep = new Bl_Reporte();
        $data = $dataRep->reporteTipo($_REQUEST);
        if ($_REQUEST["tipo"] == 3) {
            echo json_encode($data);
        } else {


            $array = array();
            foreach ($data as $key => $value) {
                //unset($arrayPlazas[$key]['numerofila'],$arrayPlazas[$key]['n_id_unidad_operativa']);
                $array[] = $data [$key];
            }
            $e = new Excel("Reporte");
            $e->setHoja("Rep", $array);
            $e->generaExcel();
        } 
        exit();
        break;

    case 'generaReporte2':
        $dataRep = new Bl_Reporte();
        $inicio = $_REQUEST["fin"];
        $fin = $_REQUEST["ffi"];
        $emp = 0;
        if (isset($_REQUEST["emp"]))
            $emp = $_REQUEST["emp"];

        $data = $dataRep->reporteTipo($inicio, $fin, $emp);
        $array = array();
        foreach ($data as $key => $value) {
            //unset($arrayPlazas[$key]['numerofila'],$arrayPlazas[$key]['n_id_unidad_operativa']);
            $array[] = $data [$key];
        }
        $e = new Excel("Reporte");
        $e->setHoja("Rep", $array);
        $e->generaExcel();
        exit();
        break;
    case 'catalogoEmpresa':
        $cat = new Bl_Reporte();
        $rs = $cat->catalogoEmpresa();
        echo json_encode($rs);
        exit();
        break;

    case 'ObtenerProvincia':
        $blDpa = new Bl_dpa;
        $arrayProvincias = $blDpa->provincia();
        echo json_encode($arrayProvincias);
        exit();
        break;
}
?>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>HESTIA - Generación de Códigos</title>
        <link rel="stylesheet" href="lib/css/estilo.css"/>

        <!-- jquery.easy -->
        <link rel="stylesheet" type="text/css" href="lib/jquery-easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="lib/jquery-easyui/themes/icon.css">
        <script src="lib/jquery-easyui/jquery-1.12.4.min.js" type="text/javascript"></script>
        <script src="lib/jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>
        <script src="lib/jquery-easyui/locale/easyui-lang-es.js" type="text/javascript"></script>


        <!-- extenciones para validar-->
        <script src="lib/js/valida.js" type="text/javascript" ></script>
        <script src="lib/js/hestia.js" type="text/javascript" ></script>

        <script type="text/javascript">
            $(function() {
                function frm(rec) {
                    $("#div_din").hide();
                    $("#div_idn").hide();
                    $("#div_emp").hide();
                    $("#div_ser").hide();
                    $('#txt_identificacion').val("");
                    $('#cmb_empresa').combobox({
                        required: false
                    });
                    $('#cmb_servicio').combobox({
                        required: false
                    });
                    $('#cmb_dinner').combobox({
                        required: false
                    });
                    $('#txt_identificacion').validatebox({
                        required: false
                    });
                    if (rec.value == 1) { //Empresa
                        $("#div_emp").show();
                        $('#cmb_empresa').combobox({
                            required: true
                        });
                    } else if (rec.value == 2) { //Comedor
                        $("#div_din").show();
                        $('#cmb_dinner').combobox({
                            required: true
                        });
                    } else if (rec.value == 3) { //Servicio
                        $("#div_emp").show();
                        $('#cmb_empresa').combobox({
                            required: true
                        });
                        $("#div_ser").show();
                        $('#cmb_servicio').combobox({
                            required: true
                        });
                    } else if (rec.value == 4) { //Identificacion
                        $("#div_idn").show();
                        $('#txt_identificacion').validatebox({
                            required: true
                        });
                    }

                    $('#fecha_inicio').datebox('setValue', '');
                    $('#fecha_fin').datebox('setValue', '');
                }


                $('#tipo').combobox({
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 100,
                    width: 250,
                    editable: false,
                    required: true,
                    data: [{
                            label: 'E401100001 | Reporte de Negocio',
                            value: '0'
                        }


                        , {
                            label: 'E409100001 | Procesos',
                            value: '1'
                        }
                        , {
                            label: 'E404300001 | Seguridad',
                            value: '2'
                        }
                        , {
                            label: 'E404200001 | Seguridad Industrial',
                            value: '3'
                        }
                        //, {
//                            label: 'Por Identificación/Código Empleado',
//                            value: '4'
//                        }
//                    

                    ],
                    onSelect: function(rec) {
                        //frm(rec);
                    }
                });
                $('#cmb_empresa').combobox({
                    url: 'index.php?accion=catalogoEmpresa',
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 150,
                    width: 350,
                    editable: false,
                    required: true

                });
                $('#cmb_servicio').combobox({
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 100,
                    width: 250,
                    editable: false,
                    required: true,
                    data: [
                        {
                            label: 'Desayuno 08:00 - 11:00',
                            value: '1'
                        }, 
                        {
                            label: 'Almuerzo 12:00 - 15:00',
                            value: '2'
                        }
                        , {
                            label: 'Merienda 18:00 - 21:00',
                            value: '3'
                        }
                    ]
                });
                $('#cmb_dinner').combobox({
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 100,
                    width: 250,
                    editable: false,
                    required: true,
                    data: [{
                            label: 'Dinn 01',
                            value: '1'
                        }, {
                            label: 'Dinn 02',
                            value: '2'
                        }]
                });
                $('#fecha_inicio').datebox({
                    editable: false,
                    required: true,
                    formatter: formatoSalud,
                    parser: parserSalud
                });
                $('#fecha_fin').datebox({
                    editable: false,
                    required: true,
                    formatter: formatoSalud,
                    parser: parserSalud
                });
                $('#txt_identificacion').validatebox({
                    required: true
                });
                $('#txt_detalle').validatebox({
                    required: true
                });
                
                $('#btnIngresar').click(function() {
                    if ($("#frmDatos").form('validate')) {
                        if ($('#tipo').combo('getValue') == 3) {

                            var win2 = window.open("./index.php?accion=generaReporte&tipo=" + $('#tipo').combo('getValue') + "&fin=" + $('#fecha_inicio').combo('getText') + "&ffi=" + $('#fecha_fin').combo('getText') + "&emp=" + $('#cmb_empresa').combo('getValue'), "Juk", "width=600,height=300");



                            //$.post('index.php', {accion: 'generaReporte', tipo: , fin: $('#fecha_inicio').combo('getText'), ffi: $('#fecha_fin').combo('getText'), emp: $('#cmb_empresa').combo('getValue')},
                            //function(resultado) {
                            //   $('#div_html').html(resultado)
                            //}, 'html');
                        }
                        else {
                            var win = window.open("./index.php?accion=generaReporte&fin=" + $('#fecha_inicio').combo('getText') + "&ffi=" + $('#fecha_fin').combo('getText') + "&emp=" + $('#cmb_empresa').combo('getValue'), "Juk", "width=200,height=100");
                            setTimeout(function() {
                                win.close();
                            }, 30000);
                        }


                    } else {
                        $.messager.show({
                            title: 'Atención',
                            msg: 'Por favor, llene todos los campos requeridos para poder generar el reporte'
                        });
                    }
                });


                function fnExcelReport() {
                    var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
                    tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';
                    tab_text = tab_text + '<x:Name>Test Sheet</x:Name>';
                    tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
                    tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
                    tab_text = tab_text + "<table border='1px'>";
                    tab_text = tab_text + $('#myRep01').html();
                    tab_text = tab_text + '</table></body></html>';
                    var data_type = 'data:application/vnd.ms-excel';
                    var ua = window.navigator.userAgent;
                    var msie = ua.indexOf("MSIE ");
                    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                        if (window.navigator.msSaveBlob) {
                            var blob = new Blob([tab_text], {
                                type: "application/csv;charset=utf-8;"
                            });
                            navigator.msSaveBlob(blob, 'Test file.xls');
                        }
                    } else {
                        $('#test').attr('href', data_type + ', ' + encodeURIComponent(tab_text));
                        $('#test').attr('download', 'Test file.xls');
                    }

                }

            });
        </script>
    </head>

    <body>
        <div id="header">
            <div class="fila" >
                <div id="divNombreAplicacion" style="float:right;"> 
                    <img src="./lib/img/goddard.gif" style="width: 110px; height: 50px"/>
                    &nbsp;
                    Generación de Códigos para Visitas
                </div>

            </div>
        </div>
        <div id="cuerpo" style="background-color: #FFFFFF; padding: 20px;">

            <form id="frmDatos" method="post" >
                <fieldset class="clsFieldset" style="width:90%; text-align:left; padding-left: 10px; padding-top: 10px;">
                    
                    <div class="fila" >
                        <div class="celda" id="div_emp" >
                            <div class="etiqueta">
                                <label>Empresa</label><br/>
                                <input name="cmb_empresa" id="cmb_empresa" value="" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila">
                        <div class="celda">
                            <div class="etiqueta">
                                <label> Centro de Costo* </label>
                            </div>
                            <div>
                                <input name="tipo" id="tipo" value=""/> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" id="div_ser">
                            <div class="etiqueta">
                                <label>Servicio</label><br/>
                                <input name="cmb_servicio" id="cmb_servicio" value="" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" id="div_din" style="display:none;">
                            <div class="etiqueta">
                                <label>Comedor</label><br/>
                                <input name="cmb_dinner" id="cmb_dinner" value="" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" id="div_idn">
                            <div class="etiqueta">
                                <label>Cantidad</label><br/>
                                <input name="txt_identificacion" id="txt_identificacion" maxlength="80" size="5" class="combo" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" style="width:50%;">
                            <div class="etiqueta">
                                <label>Fecha Desde</label><br/>
                                <input name="fecha_inicio" id="fecha_inicio" value="" /> 
                            </div>
                        </div>
                        <div class="celda" style="width:50%;">
                            <div class="etiqueta">
                                <label>Fecha Hasta</label><br/>
                                <input name="fecha_fin" id="fecha_fin" value="" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda" id="div_emp" >
                            <div class="etiqueta">
                                <label>Observación</label><br/>
                                <textarea class="combo" rows="2" cols="55" name="txt_detalle" id="txt_detalle" value=""> </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="fila">
                        <div class="celda" style="text-align: center;padding-top: 10px;">
                            <a id="btnIngresar" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-ok'">Generar Códigos</a>  
                        </div>
                    </div>
                </fieldset>
            </form>
            <div id="div_html"></div>
        </div>
        <div id="footer" >
            <div>
                <br/><br/>
                <hr>
                <p style="text-align: center;">&copy; Derechos reservados 2016 - Goddard Catering Group</p>
            </div>
        </div>

    </body>
</html>