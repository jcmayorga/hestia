<?php
include_once dirname(__FILE__) . '/etc/conf.php';
include_once dirname(__FILE__) . '/etc/adm_nomina_bl.php';
include_once dirname(__FILE__) . '/etc/lib/PHPExcel/Classes/PHPExcel.php';
include_once dirname(__FILE__) . '/etc/lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
include_once EXCEL_CLASE_FAST;
session_start();

if (!empty($_REQUEST['accion'])) {
    $op = $_REQUEST['accion'];
} else {
    $op = '';
}
switch ($op) {

    case 'cargaNomina':
        $obj = new bl_adm_nomina();
        $rows = $obj->nomina($_REQUEST);
        $cantidad = $obj->cantidadNomina($_REQUEST);
        $rs = array('total' => $cantidad, 'rows' => $rows);
        echo json_encode($rs);
        exit();
        break;
    case 'creaNomina':
        $cat = new bl_adm_nomina();
        $rs = $cat->creaNomina($_REQUEST);
        echo json_encode($rs);
        exit();
        break;
    case 'actualizaNomina':
        $cat = new bl_adm_nomina();
        $rs = $cat->actualizaNomina($_REQUEST);
        echo json_encode($rs);
        exit();
        break;

    case 'getCompany':
        $cat = new bl_adm_nomina();
        $rs = $cat->getCompany();
        echo json_encode($rs);
        exit();
        break;
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>HESTIA</title>
        <link rel="stylesheet" href="etc/lib/css/estilo.css"/>
        <!-- jquery.easy -->
        <link rel="stylesheet" type="text/css" href="etc/lib/jquery-easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="etc/lib/jquery-easyui/themes/icon.css">
        <script src="etc/lib/jquery-easyui/jquery-1.12.4.min.js" type="text/javascript"></script>
        <script src="etc/lib/jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>
        <script src="etc/lib/jquery-easyui/jquery.edatagrid.js" type="text/javascript"></script>
        <script src="etc/lib/jquery-easyui/locale/easyui-lang-es.js" type="text/javascript"></script>
        <!-- extensiones para validar-->
        <script src="etc/lib/js/valida.js" type="text/javascript" ></script>
        <script src="etc/lib/js/hestia.js" type="text/javascript" ></script>

        <script type="text/javascript">

            function buscarRoster() {
                $('#dtg_nomina').datagrid('gotoPage', 1);
                $('#dtg_nomina').edatagrid('reload', {'accion': 'cargaNomina', 'txtbuscar': $('#txtbuscar').searchbox('getValue')});
            }
            function crearRoster() {
                var dg = $("#dtg_nomina");
                var opts = $("#dtg_nomina").edatagrid('options')
                dg.edatagrid('appendRow', {isNewRecord: true});
                var rows = dg.edatagrid('getRows');
                opts.editIndex = rows.length - 1;
                dg.edatagrid('beginEdit', opts.editIndex);
                dg.edatagrid('selectRow', opts.editIndex);
                $('#btnNuevoReg').hide();
                $('#btnSaveTmp').show();
            }
            function guardarRoster() {
                $('#dtg_nomina').edatagrid('saveRow');
            }
            $(function () {
                var path = "adm_nomina.php";
<?php
$pd = new bl_adm_nomina();
$rsDesc = $pd->getCompany();
echo "var companyy=" . json_encode($rsDesc) . ";";
$rsDesc = $pd->getEstadoNomina();
echo "var estadoo=" . json_encode($rsDesc) . ";";
?>


                $('#btnNuevoReg').show();
                $('#btnSaveTmp').hide();


                $('#dtg_nomina').edatagrid({
                    url: path + '?accion=cargaNomina',
                    saveUrl: path + '?accion=creaNomina',
                    updateUrl: path + '?accion=actualizaNomina',
                    idField: "id",
                    title: "Administración de Nómina",
                    fitColumns: false,
                    pagination: true,
                    pageList: [13, 50, 100, 200],
                    pageSize: 13,
                    singleSelect: true,
                    rownumbers: true,
                    striped: true,
                    width: 970,
                    height: 450,
                    columns: [
                        [
                            {field: 'identification', align: 'center', resizable: true, width: 90, title: 'Identificación',
                                editor: {type: 'textbox',
                                    options: {required: true
                                    }}
                            },
                            {field: 'fullname', align: 'left', resizable: true, width: 170, title: 'Nombres y Apellidos',
                                editor: {
                                    type: 'textbox',
                                    options: {required: true,
                                    }
                                },
                                styler: function (value, row, index) {
                                    return 'color:#0033cc;';
                                }
                            },
//                            {field: 'employeecode', align: 'center', resizable: true, width: 90, title: 'Código<br>Empleado',
//                                editor: {type: 'textbox',
//                                }
//                            },
                            {field: 'company_id', align: 'left', resizable: true, width: 170, title: 'Empresa',
                                formatter: function (value) {
                                    for (var i = 0; i < companyy.length; i++) {
                                        if (companyy[i].id == value)
                                            return companyy[i].businessname;
                                    }
                                    return value;
                                }, editor: {
                                    type: 'combobox',
                                    options: {
                                        data: companyy,
                                        valueField: 'id',
                                        panelHeight: 150,
                                        panelWidth: 270,
                                        textField: 'businessname', required: true, editable: false}
                                }},
                            {field: 'unit', align: 'left', resizable: true, width: 120, title: 'Unidad',
                                editor: {
                                    type: 'textbox'
                                }},
                            {field: 'costcenter', align: 'left', resizable: true, width: 90, title: 'Código<br/>Centro Costo',
                                editor: {
                                    type: 'textbox'
                                }},
                            {field: 'namecenter', align: 'left', resizable: true, width: 120, title: 'Nombre<br/>Centro Costo',
                                editor: {
                                    type: 'textbox'
                                }},
                            {field: 'ctstatus_id', align: 'left', resizable: true, width: 170, title: 'Estado',
                                formatter: function (value) {
                                    for (var i = 0; i < estadoo.length; i++) {
                                        if (estadoo[i].id == value)
                                            return estadoo[i].name;
                                    }
                                    return value;
                                }, styler: function (value, row, index) {
                                    if (row.ctstatus_id == "505") { //Activo
                                        return 'color:black;background-color: #00ff00';
                                    } else if (row.ctstatus_id == "506") { //Vacaciones
                                        return 'color:black;background-color: #6699ff';
                                    } else if (row.ctstatus_id == "507") { //Salida de la Empresa
                                        return 'color:white;background-color: #660033';
                                    } else if (row.ctstatus_id == "508") { //Bloqueado
                                        return 'color:black;background-color: #ff3300';
                                    } else if (row.ctstatus_id == "520") { //Bloqueado - Quifatex Actualización
                                        return 'color:black;background-color: #ff00ff';
                                    }

                                }, editor: {
                                    type: 'combobox',
                                    options: {
                                        data: estadoo,
                                        valueField: 'id',
                                        panelHeight: 150,
                                        panelWidth: 250,
                                        textField: 'name', required: true, editable: false}
                                }
                            }
                        ]],
                    toolbar: '#toolb',
                    onSuccess: function (index, row) {
                        $('#dtg_nomina').edatagrid('reload');
                        $('#btnNuevoReg').show();
                        $('#btnSaveTmp').hide();
                    },
                    onError: function (index, row) {
                        alert(row.msg);
                    },
                    onBeginEdit: function (index, row) {
                        $('#btnSaveTmp').show();
                        $('#btnNuevoReg').hide();
                    },
                    onLoadSuccess: function (data) {
                    },
                    destroyMsg: {
                        norecord: {// when no record is selected
                            title: 'Advertencia',
                            msg: 'No se ha seleccionado registro para eliminar.'
                        },
                        confirm: {// when select a row
                            title: 'Por favor confirme',
                            msg: 'Esta seguro que desea eliminar el registro seleccionado?'
                        }
                    }
                });

            });
        </script>
    </head>
    <body>
        <!--        <div id="header">
                    <div class="fila" >
                        <div id="divNombreAplicacion" style="float:right;"> 
                            <img src="./etc/lib/img/goddard.gif" style="width: 110px; height: 50px"/>
                            &nbsp;
                            Administración de Nómina
                        </div>
                    </div>
                </div>-->
        <div id="cuerpo" style="background-color: #FFFFFF; padding: 10px;width:970px;height: 470px;">
            <div class="fila">
                <div class="celda">
                    <div style="width: 98%">  
                        <table id="dtg_nomina" name="dtg_nomina"></table> 
                    </div>
                </div>
            </div>

            <div id="toolb" style="text-align: left;"> 
                <div class="fila" style="height: 30px;margin: 0">  
                    <div class="celda" style="width: 50%">
                        <a href="#" id="btnNuevoReg" onclick="crearRoster()" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">Nuevo</a>  
                        <a href="#" id="btnSaveTmp" onclick="guardarRoster();" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true">Guardar</a>  
                    </div>
                    <div class="celda" style="width: 50%;text-align: right">
                        <input name="txtbuscar" id="txtbuscar" class="combo easyui-searchbox" data-options="prompt:'Por favor ingrese un valor',searcher:buscarRoster"  type="text"   />
                    </div>
                </div>  
            </div>
        </div>
    </body>
</html>