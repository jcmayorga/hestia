<?php

class bl_adm_nomina {

    public function __construct() {
        include_once dirname(__FILE__) . '/conf.php';
        include_once dirname(__FILE__) . '/adm_nomina_da.php';
    }

    function nomina($request) {
        $condicion = $this->getCondNomina($request);
        $pageSize = '';
        if (!empty($request['page'])) {
            if ($request['page'] > 1) {
                $pageSize = (($request['page'] - 1) * $request['rows']) + 1;
            } else {
                $pageSize = 1;
            }
        } $pageNumber = '';
        if (!empty($request['rows']))
            $pageNumber = $pageSize + ($request['rows'] - 1);

        $da = new da_adm_nomina();
        return $da->nomina($condicion, $pageSize, $pageNumber);
    }

    function cantidadNomina($request) {
        $condicion = $this->getCondNomina($request);
        $obj_db = new da_adm_nomina();
        $tmp = $obj_db->countNomina($condicion);
        return $tmp[0]['total'];
    }

    function getCondNomina($request) {
        $condicion = array();
        if (isset($request['txtbuscar'])) {
            if (!empty($request['txtbuscar'])) {
                $condicion[] = " upper(identification) like upper('%{$request['txtbuscar']}%') or upper(fullname) like upper('%{$request['txtbuscar']}%') or upper(namecenter) like upper('%{$request['txtbuscar']}%')  or upper(costcenter) like upper('%{$request['txtbuscar']}%')";
            }
        }
        return $condicion;
    }

    function getNomina(
    $request) {
        $catalogo = new da_adm_nomina();
        $rs = $catalogo->getNomina($request);
        return $rs;
    }

    function getCompany() {


        $catalogo = new da_adm_nomina();
        $rs = $catalogo->getCompany($request);
        return $rs;
    }

    function getEstadoNomina() {


        $catalogo = new da_adm_nomina();
        $rs = $catalogo->getEstadoNomina($request);
        return $rs;
    }

    function getServiceDinner(
    $request) {
        $catalogo = new da_adm_nomina();
        $condicion = array();
        $condicion[] = " and dateregistry = '" . $request["date"] . "' ";
        $condicion[] = " b.dinner_id = " . $request["dinner"] . " ";
        $rs = $catalogo->getServiceDinner($condicion);
        return $rs;
    }

    function getCompanyDinner(
    $request) {
        $catalogo = new da_adm_nomina();
        $rs = $catalogo->getCompanyDinner($request);
        return $rs;
    }

    function creaNomina($request) {
        $obj_db = new da_adm_nomina();
        $rs_CCDS = $obj_db->getNominaIdentification(strtoupper(trim($_REQUEST["identification"])));
        if ($rs_CCDS) {
            return array('isError' => true, 'msg' => "La identificación ingresada ya se encuentra registrada, por favor verifique este parámetro.");
            exit;
        }
        $datos["id"] = " (select max(id)+1  from administration.roster) ";
        $datos["identification"] = "'" . strtoupper(trim($_REQUEST["identification"])) . "'";
        $datos["fullname"] = "'" . trim(strtoupper($_REQUEST["fullname"])) . "'";
        $datos["company_id"] = $_REQUEST["company_id"];
        $datos["ctstatus_id"] = $_REQUEST["ctstatus_id"];
        $datos["created_at"] = "now()";
        $datos["usucrud_id"] = $_SESSION['_sf2_attributes']['userdata']['id'];
        $datos["unit"] = "'" . trim(strtoupper($_REQUEST["unit"])) . "'";
        $datos["costcenter"] = "'" . trim(strtoupper($_REQUEST["costcenter"])) . "'";
        $datos["namecenter"] = "'" . trim($_REQUEST["namecenter"]) . "'";
        $datos["ctidentification_id"] = 502;
        $obj_db->creaNomina($datos);
        exec("php codbarras.php");
        $rs_CCDS = $obj_db->getNominaIdentification(strtoupper(trim($_REQUEST["identification"])));
        return $rs_CCDS;
    }

    function actualizaNomina($request) {
        $obj_db = new da_adm_nomina();
        $datos["fullname"] = "'" . trim(strtoupper($_REQUEST["fullname"])) . "'";
        $rs_CCDS = $obj_db->getNominaID($_REQUEST["id"]);
        if (strtoupper(trim($rs_CCDS[0]['identification'])) != strtoupper(trim($_REQUEST["identification"]))) {
            $rs_CCDS = $obj_db->getNominaIdentification(strtoupper(trim($_REQUEST["identification"])));
            if ($rs_CCDS) {
                return array('isError' => true, 'msg' => "La identificación ingresada ya se encuentra registrada, por favor verifique este parámetro.");
                exit;
            } else {
                $datos["identification"] = "'" . strtoupper(trim($_REQUEST["identification"])) . "'";
            }
        }
        $datos["company_id"] = $_REQUEST["company_id"];
        $datos["ctstatus_id"] = $_REQUEST["ctstatus_id"];
        $datos["updated_at"] = "now()";
        $datos["usucrud_id"] = $_SESSION['_sf2_attributes']['userdata']['id'];
        $datos["unit"] = "'" . trim(strtoupper($_REQUEST["unit"])) . "'";
        $datos["costcenter"] = "'" . trim(strtoupper($_REQUEST["costcenter"])) . "'";
        $datos["namecenter"] = "'" . trim($_REQUEST["namecenter"]) . "'";
        $condicion[] = "id = " . $_REQUEST["id"];
        $obj_db->actualizaNomina($datos, $condicion);
        $rs_CCDS = $obj_db->getNominaID($_REQUEST["id"]);
        return $rs_CCDS;
    }

    /**
     * Retorna el catálogo Institución Referencia
     * @return array el catálogo Institución Referencia
     */
    function catalogoEmpresa($request) {
        $catalogo = new da_reporte();
        if (isset($request['catEmpTip']) && $request['catEmpTip'] == 2) {
            $rs = $catalogo->catalogoEmpresaCont($request['cont']);
        } else
            $rs = $catalogo->catalogoEmpresa();
        return $rs;
    }

}

?>