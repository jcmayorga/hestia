<?php

//funciones de cotrol de acceso
function pre_r($rs) {
    echo '<div style="text-align:left">';
    echo '<pre>';
    print_r($rs);
    echo "</div>";
}

/*
 *  configuraciones para cambio de ambiente
 */

/**
 * path del directorio 
 */
define('INICIO', dirname(dirname(__FILE__)));


/**
 * determinar el idioma para las funcion es php system('locale -a')
 */
setlocale(LC_ALL, 'es_EC.utf8');

/**
 * Define el tiempo de vida de la sesión 
 */
define('TIME_SESSION', 4800);


/**
 * Formato de fecha de la app
 */
define("FORMATO_FECHA", "YYYY-MM-DD");

/**
 * Formato de fecha con horas de la app
 */
define("FORMATO_FECHA_HORA", "YYYY-MM-DD HH24:MI:SS");


/* * *********************************************************************** */


/*
 * BD acceso a la conexión base de datos 
 */
define('BD', dirname(__FILE__) . '/das/PGSQL.php');



/**
 * LIB directorio lib
 */
define('LIB', dirname(__FILE__) . '/lib/');

/**
 * PDF 
 */
define('PDF', LIB . 'php/pdf/pdf.php');

/**
 * EXCEL DE TERCEROS LIBRERIA NATIVA
 */
define('EXCEL', LIB . 'PHPExcel/Classes/PHPExcel.php');
/**
 * LIBRERIA EXCEL DESARROLLADO RAPIDO 
 */
define('EXCEL_CLASE_FAST', LIB . 'Excel.php');
