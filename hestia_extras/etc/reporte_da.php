<?php

class da_reporte {

    public function __construct() {
        include_once dirname(__FILE__) . '/conf.php';
        include_once BD;
    }

    function produccionDinner($condicion = array(), $condicion2 = array()) {
        $sql = "(select c.name as Comedor, count(*) as Despachado, d.description as Servicio, e.businessname as Empresa, 'Tickets Emitidos' as tipo from business.transaction a, administration.companydinner b, administration.dinner c, business.service d, administration.company e
                where a.companydinner_id=b.id
                and b.company_id=e.id
                and b.dinner_id=c.id
                and a.cttransaction_id in (510, 511, 529)
                and a.service_id=d.id ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "group by c.name, d.description, e.businessname
                order by c.name, d.description,  e.businessname )";
        $sql.="union
                (select e.name as Comedor, count(*) as Despachado, d.description as Servicio, c.businessname as Empresa, 'Firmas Registradas' as Tipo
                from business.closedetail a, business.close b, administration.company c, 
                business.service d, administration.dinner e
                where a.close_id=b.id
                and a.company_id=c.id
                and a.service_id=d.id
                and b.dinner_id=e.id
                and a.ctstatus_id=526
                and a.roster_id is null ";
        if (count($condicion2) > 0)
            $sql .= implode(' AND ', $condicion2);
        $sql.="group by e.name, d.description, c.businessname
                order by e.name, d.description, c.businessname)
                order by 1,3,4";
        $bd = new PGSQL();
//        echo $sql;
//        die;
        return $bd->query($sql);
    }

    function juk($condicion = array()) {
        $sql = "select a.id, d.businessname, c.unit, c.costcenter, c.namecenter,  c.identification, g.name, c.employeecode, c.firstname, c.lastname, c.fullname, f.name, i.name as dinner, i.location, i.address, b.description, b.acronym, a.dateregistry, a.timeregistry
                from business.transaction a, business.service b, administration.roster c, administration.company d, 
                application.catalog f, application.catalog g, administration.companydinner h, administration.dinner i
                where a.service_id=b.id
                and a.roster_id=c.id
                and c.company_id=d.id
                and a.cttransaction_id=f.id
                and c.ctidentification_id=g.id
                and a.companydinner_id=h.id
                and h.dinner_id=i.id
                and a.ctstatus_id in (513, 514, 528) ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "order by a.dateregistry, a.timeregistry";
        $bd = new PGSQL();

//      echo $sql; die;
        return $bd->query($sql);
    }

    function jukDetalle($condicion = array()) {
        $sql = "select c.id, d.businessname, c.unit, c.costcenter, c.namecenter,  c.identification, g.name, c.employeecode, c.firstname, c.lastname, c.fullname, f.name, i.name as dinner, i.location, i.address, b.description, b.acronym, a.dateregistry, a.timeregistry
                from business.transaction a, business.service b, administration.roster c, administration.company d, 
                application.catalog f, application.catalog g, administration.companydinner h, administration.dinner i
                where a.service_id=b.id
                and a.roster_id=c.id
                and c.company_id=d.id
                and a.cttransaction_id=f.id
                and c.ctidentification_id=g.id
                and a.companydinner_id=h.id
                and h.dinner_id=i.id
                and a.ctstatus_id in (513, 514, 528) ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "order by a.dateregistry, a.timeregistry";
        $bd = new PGSQL();

//        echo $sql; die;
        return $bd->query($sql);
    }

    function extrasDetalle($condicion = array()) {
        $sql = "select a.identification, a.company_id, a.fullname, b.dateclose, a.costcentername
                from business.closedetail a, business.close b, administration.company c
                where a.close_id=b.id
                and a.company_id=c.id
                and a.ctstatus_id=526
                and a.roster_id is null ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $bd = new PGSQL();

//        echo $sql; die;
        return $bd->query($sql);
    }

    function jukDisctinct($condicion = array()) {
        $sql = "select distinct c.id, d.businessname, c.unit, c.costcenter, c.namecenter,  c.identification, c.employeecode, c.firstname, c.lastname, c.fullname
                from business.transaction a, business.service b, administration.roster c, administration.company d, 
                application.catalog f, application.catalog g, administration.companydinner h, administration.dinner i
                where a.service_id=b.id
                and a.roster_id=c.id
                and c.company_id=d.id
                and a.cttransaction_id=f.id
                and c.ctidentification_id=g.id
                and a.companydinner_id=h.id
                and h.dinner_id=i.id
                and a.ctstatus_id in (513, 514, 528) ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "order by c.fullname, c.costcenter, c.namecenter ";
        //   echo $sql;
        $bd = new PGSQL();

//        echo $sql; die;
        return $bd->query($sql);
    }

    function extrasDisctinct($condicion = array()) {
        $sql = "select distinct identification, businessname, company_id from (select distinct a.identification, c.businessname, costcentername, fullname, a.company_id
                from business.closedetail a, business.close b, administration.company c
                where a.close_id=b.id
                and a.company_id=c.id
                and a.ctstatus_id=526
                and a.roster_id is null ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "order by a.fullname, a.costcentername) f ";
        //   echo $sql;
        $bd = new PGSQL();

//        echo $sql; die;
        return $bd->query($sql);
    }

    function getService($cond) {
        $sql = "select description from business.service
            where acronym='$cond'
            limit  1";

        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function jukCount($condicion = array()) {
        $sql = "select distinct c.id, count(*) as total
                from business.transaction a, business.service b, administration.roster c, administration.company d, 
                application.catalog f, application.catalog g, administration.companydinner h, administration.dinner i
                where a.service_id=b.id
                and a.roster_id=c.id
                and c.company_id=d.id
                and a.cttransaction_id=f.id
                and c.ctidentification_id=g.id
                and a.companydinner_id=h.id
                and h.dinner_id=i.id
                and a.ctstatus_id in (513, 514, 528) ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "group by c.id";
        $bd = new PGSQL();

//        echo $sql; die;
        return $bd->query($sql);
    }

    function extrasCount($condicion = array()) {
        $sql = "select a.identification, a.company_id,count(*) as total
                from business.closedetail a, business.close b, administration.company c
                where a.close_id=b.id
                and a.company_id=c.id
                and a.ctstatus_id=526
                and a.roster_id is null";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "group by a.identification, a.company_id";
        $bd = new PGSQL();

//        echo $sql; die;
        return $bd->query($sql);
    }

    function catalogoEmpresa() {
        $sql = "select id as value, businessname as label from administration.company where id in (select distinct (company_id) from business.transaction a, administration.companydinner b
                where a.companydinner_id=b.id)
                order by label";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function getEmpresa($condicion = array()) {
        $sql = "select id as value, businessname as label from administration.company where 1=1";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql.="order by label";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function all() {
        $sql = "select a.ctstatus_id, a.id, identification, length(identification) as len, barcode, b.name as estado, businessname, firstname, lastname, fullname, company_id, barcode, unit, costcenter, namecenter 
                from administration.roster a,  application.catalog b, administration.company c
                where a.ctstatus_id=b.id
                and c.id=6
                and a.company_id=c.id
                order by company_id";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function allcd() {
        $sql = "select a.ctstatus_id, a.id, identification, length(identification) as len, barcode, b.name as estado, businessname, firstname, lastname, fullname, company_id, barcode, unit, costcenter, namecenter 
                from administration.roster a,  application.catalog b, administration.company c
                where a.ctstatus_id=b.id
                and c.id=12
                and a.company_id=c.id
                order by company_id";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function catalogoEmpresaCont($comp) {
        $sql = "select id as value, businessname as label from administration.company
where contract_id=$comp
order by 2";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function catalogoContrato() {
        $sql = "(select 0 as value, '[Seleccione ...]' as label ) 
            union
            (select id as value, name as label from administration.contract)
                order by 1  ";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

}

?>