<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once dirname(__FILE__) . '/conf.php';


$arrdias[] = "";
$arrdias[1] = "L";
$arrdias[2] = "M";
$arrdias[3] = "M";
$arrdias[4] = "J";
$arrdias[5] = "V";
$arrdias[6] = "S";
$arrdias[7] = "D";

function createDateRangeArray($strDateFrom, $strDateTo) {
    // takes two dates formatted as YYYY-MM-DD and creates an
    // inclusive array of the dates between the from and to dates.
    // could test validity of dates here but I'm already doing
    // that in the main script

    $aryRange = array();

    $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
    $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));


    if ($iDateTo >= $iDateFrom) {
        array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
        while ($iDateFrom < $iDateTo) {
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange, date('Y-m-d', $iDateFrom));
        }
    }
    return $aryRange;
}

$td_dia = "<td rowspan='2'>#</td><td rowspan='2'>Apellidos y Nombres</td><td rowspan='2'>CECO</td><td rowspan='2'>Centro de Costo</td>";
$td_num = "";
$xx = createDateRangeArray('2016-06-01', '2016-06-20');
foreach ($xx as $value) {
    $date = new DateTime($value);
    $td_num.="<td>" . $date->format('d') . "</td>";
    $td_dia.="<td>" . $arrdias[$date->format('N')] . "</td>";
}
$td_dia.="<td rowspan='2'>Total Almuerzos</td><td rowspan='2'>Valor</td><td rowspan='2'>IVA</td><td rowspan='2'>TOTAL</td>";

echo'<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>HESTIA</title>
        <link rel="stylesheet" href="lib/css/estilo.css"/>

        <!-- jquery.easy -->
        <link rel="stylesheet" type="text/css" href="lib/jquery-easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="lib/jquery-easyui/themes/icon.css">
        <script src="lib/jquery-easyui/jquery-1.12.4.min.js" type="text/javascript"></script>
        <script src="lib/jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>
        <script src="lib/jquery-easyui/locale/easyui-lang-es.js" type="text/javascript"></script>
        
<script type="text/javascript">
function fnExcelReport() {
    var tab_text = \'<html xmlns:x="urn:schemas-microsoft-com:office:excel">\';
    tab_text = tab_text + \'<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>\';

    tab_text = tab_text + \'<x:Name>Test Sheet</x:Name>\';

    tab_text = tab_text + \'<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>\';
    tab_text = tab_text + \'</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>\';

    tab_text = tab_text + "<table border=\'1px\'>";
    tab_text = tab_text + $(\'#myRep01\').html();
    tab_text = tab_text + \'</table></body></html>\';

    var data_type = \'data:application/vnd.ms-excel\';
    
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        if (window.navigator.msSaveBlob) {
            var blob = new Blob([tab_text], {
                type: "application/csv;charset=utf-8;"
            });
            navigator.msSaveBlob(blob, \'Test file.xls\');
        }
    } else {
        $(\'#test\').attr(\'href\', data_type + \', \' + encodeURIComponent(tab_text));
        $(\'#test\').attr(\'download\', \'Test file.xls\');
    }

}
</script>


</head>
';

echo "<table border='1' id='myRep01'>"
. "<tr>"
 ."<td colspan='4'>Desde: 2016-06-01 Hasta: 2016-06-20 </td>"
 . "</tr>"
 . "<tr>"
 . $td_dia
 . "</tr><tr>"
 . $td_num
 . "</tr></table>".'<a href="#" id="test" onClick="javascript:fnExcelReport();">download</a>';
//pre_r($xx);
//pre_r($arrdias);