/**
 * @author Manuel Haro 
 * @version 1.0
 * @package salud 
 */

/*
 * completa con ceros a la izquierda
 */
$.strPad = function (i, l, s) {
    var o = i.toString();
    if (!s) {
        s = '0';
    }
    while (o.length < l) {
        o = s + o;
    }
    return o;
};

/*
 *formate la fecha
 */
function formatoSalud(date) {
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    return y + '-' + (m < 10 ? ('0' + m) : m) + '-' + (d < 10 ? ('0' + d) : d);
    // return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;  
}

function formatoSaludRdacaa(date) {
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    return (d < 10 ? ('0' + d) : d) + '/' + (m < 10 ? ('0' + m) : m) + '/' + y;
}

function parserSalud(s) {
    var ss = (s.split('-'));
    var y = parseInt(ss[0], 10);
    var m = parseInt(ss[1], 10);
    var d = parseInt(ss[2], 10);
    if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
        return new Date(y, m - 1, d);
    } else {
        return new Date();
    }
}


/*
 *configuración de ajax 
 */
$.ajaxSetup({
    type: 'POST',
    async: false,
    cache: false,
    beforeSend: function () {

    },
    complete: function (data) {
        controlSesion(data.responseText);
    },
    error: function () {
        alert("Error de conexión");
    }
});

/*
 * actualiza contenedor
 * p objeto de parametros url, type:post, parametros:'', contenedor 
 */
function cargarContenido(p) {
    var type, parametros;
    if (!(p.url)) {
        $('#' + p.contenedor).html('url no especificado');
        return;
    }
    if (!(p.type))
        type = 'POST';
    else
        type = p.type;
    if (!(p.parametros))
        parametros = {};
    else
        parametros = p.parametros;
    $('#' + p.contenedor).empty();
    $.ajax({
        url: p.url,
        type: type,
        data: parametros,
        dataType: 'html',
        async: false,
        success: function (data) {
            $('#' + p.contenedor).html(data);
        }
    });
    return;
}

/*
 * control de la sesión
 */
function controlSesion(resultado) {
    if (resultado == "ACCESO DENEGADO") {
        alert('La sesión ha expirado, o usted no tiene permisos para acceder a este recurso');
        document.location.href = '../index.php';
    }
}

//transforma a mayusculas los campos text y textarea
function mayusculas() {
    //transforma a mayusculas los objetos que tiene clase mayuscula
    $('input[type=text].mayusculas').blur(function () {
        $(this).val($.trim($(this).val().toUpperCase()));
    })
    $('textarea.mayusculas').blur(function () {
        $(this).val($.trim($(this).val().toUpperCase()));
    })
}

//quita los espacios en blanco del inicio y final del campo text y textarea
function trim() {
    //quita los espacios en blanco
    $('input[type=text]').blur(function () {
        $(this).val($.trim($(this).val()));
    })
    $('textarea').blur(function () {
        $(this).val($.trim($(this).val()));
    })
}

//limpia la página para realizar una nueva carga en el content
function limpiarPagina() {
    var objetos = $('body > div');
    $.each(objetos, function () {
        if ($(this).attr('id') != 'pagina') {
            $(this).remove();
        }
    });
}


//ingresa una función a jquery
/*!
 * jQuery serializeObject - v0.2 - 1/20/2010
 * http://benalman.com/projects/jquery-misc-plugins/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */

// Whereas .serializeArray() serializes a form into an array, .serializeObject()
// serializes a form into an (arguably more useful) object.

(function ($, undefined) {
//  '$:nomunge'; // Used by YUI compressor.

    $.fn.serializeObject = function () {
        var obj = {};

        $.each(this.serializeArray(), function (i, o) {
            var n = o.name,
                    v = o.value;

            obj[n] = obj[n] === undefined ? v
                    : $.isArray(obj[n]) ? obj[n].concat(v)
                    : [obj[n], v];
        });

        return obj;
    };

})(jQuery);



/*
 *Obtiene el mes en letras
 */
function obtenerMes(mes) {
    mes--;
    var arrayMes = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    return arrayMes[mes];

}


function setearAlturaPanelPrincipal() {
    var c = $('#layoutPrincipal');
    var p = c.layout('panel', 'center'); // get the center panel  
    var oldHeight = p.panel('panel').outerHeight();
    p.panel('resize', {height: 'auto'});
    var newHeight = p.panel('panel').outerHeight();
    c.height(c.height() + newHeight - oldHeight);
    c.layout('resize');
}

/**
 * parserSaludDatetimebox
 * función para el parse en los objetos tipo Datetimebox (yyyy-mm-dd h:m) 24 horas
 * @param {type} s
 * @returns {Date}
 */
function parserSaludDatetimebox(s) {
    if (!s) {
        return new Date();
    }
    var ss = (s.split('-'));
    var hora = (s.split(' '));
    var hora = (hora[1].split(':'));
    var h = (hora[0]);
    var min = (hora[1]);
    var y = parseInt(ss[0], 10);
    var m = parseInt(ss[1], 10);
    var d = parseInt(ss[2], 10);
    if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
        return new Date(y, m - 1, d, h, min);
    } else {
        return new Date();
    }
}

/**
 * formatoSaludDatetimebox
 *  función para dar formato en los objetos tipo Datetimebox (yyyy-mm-dd h:m) 24 horas
 * @param {type} s
 * @returns {Date}
 */
function formatoSaludDatetimebox(date) {
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    var h = date.getHours();
    var min = date.getMinutes();
    return y + '-' + (m < 10 ? ('0' + m) : m) + '-' + (d < 10 ? ('0' + d) : d) + ' ' + (h < 10 ? ('0' + h) : h) + ':' + (min < 10 ? ('0' + min) : min);
}
