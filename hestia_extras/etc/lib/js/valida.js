/**
 * extenciones de validación
 */


// extend the 'equals' rule  
$.extend($.fn.validatebox.defaults.rules, {
    equals: {
        validator: function(value, param) {
            return value == $(param[0]).val();
        },
        message: 'Las claves no son iguales'
    }
});

$.extend($.fn.validatebox.defaults.rules, {
    fecha1mayorfecha2: {
        validator: function(fecha1, fecha2) {
            var elema = fecha1.split('-');
            var anio_a = parseInt(elema[0]);
            var mes_a = parseInt(elema[1]);
            var dia_a = elema[2] * 1;
            var f1 = anio_a * 10000 + mes_a * 100 + dia_a;

            var elemb = fecha2[0].split('-');
            var anio_b = parseInt(elemb[0]);
            var mes_b = parseInt(elemb[1]);
            var dia_b = elemb[2] * 1;
            var f2 = anio_b * 10000 + mes_b * 100 + dia_b;
            return f1 >= f2;
        },
        message: 'La fecha inicial no vebe ser mayor a la final'
    }
});


$.extend($.fn.validatebox.defaults.rules, {
    fechaMenorActual: {
        validator: function(fecha) {
            var x = new Date();
            var f = fecha.split('-');
            x.setFullYear(f[0], f[1] - 1, f[2]);
            var today = new Date();

            if (x > today)
                return false
            else
                return true
        },
        message: 'La fecha no debe ser mayor a la fecha actual'
    }
});



$.extend($.fn.validatebox.defaults.rules, {
    fechaMayorIgualActual: {
        validator: function(fecha) {
            var x = new Date();
            var f = fecha.split('-');
            x.setFullYear(f[0], f[1] - 1, f[2]);
            var today = new Date();

            if (x >= today)
                return true
            else
                return false
        },
        message: 'La fecha no debe ser Menor a la fecha actual'
    }
});

$.extend($.fn.validatebox.defaults.rules, {
    fechaMayorseisMeses: {
        validator: function(fecha) {
            var x = new Date();
            var f = fecha.split('-');
            x.setFullYear(f[0], f[1] - 1, f[2]);
            var today = new Date();
            today.setMonth(today.getMonth() - 6);
            if (x > today)
                return false
            else
                return true
        },
        message: 'La fecha debe ser menor al menos 6 meses de antiguedad'
    }
});

$.extend($.fn.validatebox.defaults.rules, {
    nacimientoMayorCienAnios: {
        validator: function(fecha) {
            var x = new Date();
            var f = fecha.split('-');
            x.setFullYear(f[0], f[1] - 1, f[2]);
            var today = new Date();
            today.setYear(today.getYear() - 110);
            if (x < today)
                return false
            else
                return true
        },
        message: 'La fecha de nacimiento no puede ser mayor a 110 años'
    }
});




$.extend($.fn.validatebox.defaults.rules, {
    certificadoVotacion: {
        validator: function(value) {
            return /^[0-9-]*$/.test(value)
        },
        message: 'EL valor ingresado no es un número de certificado válido'
    }
});


$.extend($.fn.validatebox.defaults.rules, {
    numeroDecimalPositivo: {
        validator: function(value) {
            return /^([0-9])*[.]?[0-9]*$/.test(value)
        },
        message: 'Solo se permite el ingreso de números positivos'
    }
});


$.extend($.fn.validatebox.defaults.rules, {
    numeroPositivo: {
        validator: function(value) {
            return /^\d+$/.test(value)
        },
        message: 'Solo se permite el ingreso de números positivos'
    }
});


$.extend($.fn.validatebox.defaults.rules, {
    numeroMayorCero: {
        validator: function(value) {
            if (/^\d+$/.test(value))
                if (value > 0)
                    return true
            return false
        },
        message: 'Solo se permite el ingreso de números positivos mayores a cero'
    }
});


$.extend($.fn.validatebox.defaults.rules, {
    numeroceroaquince: {
        validator: function(value) {
            if (/^\d+$/.test(value))
                if (value > -1 && value<16)
                    return true
            return false
        },
        message: 'Solo se permite el ingreso de números entre 0 y 15'
    }
});

$.extend($.fn.validatebox.defaults.rules, {
    numeroPositivoPorcentaje: {
        validator: function(value) {
            if (/^\d+$/.test(value)) {
                if (value > 0 && value <= 100) {
                    return true
                }
            } else {
                return false
            }
        },
        message: 'Solo se permite el ingreso de números positivos mayores a cero y menores de 100'
    }
});

$.extend($.fn.validatebox.defaults.rules, {
    numeroPositivoPorcentajeEspeciales: {
        validator: function(value) {
            if (/^\d+$/.test(value)) {
                if (value >= 65 && value <= 100) {
                    return true
                }
            } else {
                return false
            }
        },
        message: 'Solo se permite el ingreso de números positivos mayores a sesenta y cinco y menores de 100'
    }
});

$.extend($.fn.validatebox.defaults.rules, {
    numeroPositivoHastaSieteUno: {
        validator: function(value) {
            if (/^\d+$/.test(value)) {
                if (value > 0 && value <= 71) {
                    return true
                }
            } else {
                return false
            }
        },
        message: 'Solo se permite el ingreso de números positivos mayores a cero y menores de 71'
    }
});

$.extend($.fn.validatebox.defaults.rules, {
    historiaClinica: {
        validator: function(value) {
            return /^[0-9-]*$/.test(value)
        },
        message: 'EL valor ingresado no es una historia clínica válida'
    }
});


$.extend($.fn.validatebox.defaults.rules, {
    pasaporte: {
        validator: function(value) {
            return /^[a-zA-Z0-9-\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\u0020]*$/.test(value)
        },
        message: 'EL valor ingresado no es un pasaporte válido'
    }
});



$.extend($.fn.validatebox.defaults.rules, {
    alfabetico: {
        validator: function(value) {
            return /^[a-zA-Z-\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*$/.test(value)
        },
        message: 'Solo se permite el ingreso de caracteres alfabéticos'
    }
});


$.extend($.fn.validatebox.defaults.rules, {
    alfabeticoEspacio: {
        validator: function(value) {
            return /^[a-zA-Z-\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\u0020]*$/.test(value)
        },
        message: 'Solo se permite el ingreso de caracteres alfabéticos'
    }
});


$.extend($.fn.validatebox.defaults.rules, {
    alfanumerico: {
        validator: function(value) {
            return /^[a-zA-Z0-9-\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*$/.test(value)
        },
        message: 'Solo se permite el ingreso de caracteres alfanuméricos'
    }
});


$.extend($.fn.validatebox.defaults.rules, {
    alfanumericoEspacio: {
        validator: function(value) {
            return /^[\/a-zA-Z0-9-\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\u0020]*$/.test(value)
        },
        message: 'Solo se permite el ingreso de caracteres alfanuméricos'
    }
});


$.extend($.fn.validatebox.defaults.rules, {
    cedula: {
        validator: function(value) {
            return cedula(value);
        },
        message: 'El valor ingresado no es una cédula válida'
    }
});


$.extend($.fn.validatebox.defaults.rules, {
    path: {
        validator: function(value) {
            return /^[./a-zA-Z0-9-\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF=?]*$/.test(value)
        },
        message: 'Solo se permite el ingreso de caracteres alfabéticos y (/y.)'
    }
});

$.extend($.fn.validatebox.defaults.rules, {
    observacion: {
        validator: function(value) {
            return /^[\/a-zA-Z0-9-\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\u0020@\.\,]*$/.test(value)
        },
        message: 'Solo se permite el ingreso de caracteres alfanuméricos y los siguientes caracteres .,@'
    }
});

//valida la cédula con el dígito verificador
function cedula(cedula) {
    if (!(/^\d+$/.test(cedula))) {
        return false;
    }
    if (cedula.length == 10) {
        var arrayCedula = cedula.split("");
        var suma = 0;
        var i = 0
        while (i < 9) {
            if ((i + 1) % 2 == 0)
                suma += Number(arrayCedula[i]);
            else {
                if (arrayCedula[i] * 2 > 9)
                    suma += Number(arrayCedula[i]) * 2 - 9
                else
                    suma += Number(arrayCedula[i]) * 2
            }
            i++;
        }
        var digito = 0;
        if (suma % 10 != 0)
            digito = (Math.floor(suma / 10) + 1) * 10 - Number(suma);
        if (arrayCedula[9] == digito)
            return true;
        else
            return false;
    }
    else
        return false;
}



/**
 * valida los caracteres que envie como parametro 
 * EJM:
 * Para escribir solo letras
 * $('#miCampo1').validaCampo(' abcdefghijklmnñopqrstuvwxyzáéiou');
 *Para escribir solo numeros    
 * $('#miCampo2').validaCampo('0123456789');    
 * @param {type} 
 * @returns {undefined}
 */
(function(a) {
    a.fn.validaCampo= function(b) {
        a(this).on({keypress: function(a) {
                var c = a.which, d = a.keyCode, e = String.fromCharCode(c).toLowerCase(), f = b;
                (-1 != f.indexOf(e) || 9 == d || 37 != c && 37 == d || 39 == d && 39 != c || 8 == d || 46 == d && 46 != c) && 161 != c || a.preventDefault()
            }})
    }
})(jQuery);










//    
//    //valida el correo electrónico
//    correo:{expresion:/^[A-Za-z][A-Za-z0-9._-]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/,descripcion:'Correo electrónico incorrecto (correo@dominio.com)'}
//    
//    //valida que ingrese únicamente caracteres alfabéticos, numéricos, puntos, espacios, guión medio y guion bajo
//    ,alfanumerico_plus:{expresion:/^[a-zA-Z0-9-\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\.\s\-\_\/\\]*$/,descripcion:'Únicamente caracteres alfanuméricos, puntos, guiones y espacios'}
//    
//    //valida que ingrese únicamente caracteres alfabéticos, numéricos, puntos, espacios, guión medio y guion bajo
//    ,alfabetico_plus:{expresion:/^[a-zA-Z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\.\s\-\_\/\\]*$/,descripcion:'Únicamente caracteres alfabéticos, puntos, guiones y espacios'}
//    
//    //valida que ingrese únicamente caracteres alfabéticos y numéricos (no se debe modificar)
//    ,alfanumerico:{expresion:/^[a-zA-Z0-9-\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*$/,descripcion:'Únicamente caracteres alfanuméricos'}
//    
//    //valida que ingrese únicamente caracteres alfabéticos (no se debe moficar)
//    ,alfabetico:{expresion:/^[a-zA-Z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*$/,descripcion:'Únicamente caracteres alfabéticos'} 
//    
//    //valida que ingrese un número telefónico
//    ,telefono:{expresion:/^[0][1-9][0-9]{7}$/,descripcion:'Debe iniciar con un 0, y debe tener 9 caracteres'} 
//    
//    //valida que ingrese únicamente números
//    ,numerico:{expresion:/^\d+$/,descripcion:'Únicamente números'}
//    
//    //valida que ingrese una cédula
//    ,cedula:{expresion:/^[0-9]{10}$/,descripcion:'Número de cédula incorrecto'}
//    
//    //valida que ingrese únicamente números y guiones medios
//    ,numero_guion:{expresion:/^[0-9-]*$/,descripcion:'Únicamente números y guiones'}
//    
//    //valida que no sea vacío
//    ,obligatorio:{expresion:/^[^\s]+/,descripcion:'Campo obligatorio'}
//    
//    //valida un numero flotante con punto
//    ,decimal:{expresion:/^([0-9])*[.]?[0-9]*$/,descripcion:'Debe ser un número decimal 10.5'}
//    
//    //valida pasaporte
//    ,pasaporte:{expresion:/^[a-zA-Z0-9-\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\s\.-]*$/,descripcion:'Pasaporte incorrecto'}
//}
//
     