<?php

require_once  (dirname(__FILE__)) . "/PHPExcel/Classes/PHPExcel.php";

class Excel {

    private $_hojaArray = array();
    private $_objPHPExcel;
    private $_nombreArchivoSalida;
    private $_rutaParaGuardar;
    private $_aliasCatalogo;
    private $_catalgo;
    private $_tamano;

    function __construct($nombreArchivoSalida, $rutaParaGuardar = "") {
        $this->_tamano = ini_get("memory_limit");;
        ini_set('memory_limit','-1');
        
        $this->_catalgo = false;
        $this->_nombreArchivoSalida = $nombreArchivoSalida;
        $this->_rutaParaGuardar = $rutaParaGuardar;
        $this->_objPHPExcel = new PHPExcel();
        $this->_objPHPExcel->getProperties()->setCreator("QuitoWebStyle")
                ->setLastModifiedBy("QuitoWebStyle")
                ->setTitle("Office 2007 XLSX Test Document")
                ->setSubject("Office 2007 XLSX Test Document")
                ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                ->setKeywords("office 2007 openxml php")
                ->setCategory("Test result file");
    }

    function setActivaCatalogo($sinCatalogo = false) {
        $this->_catalgo = $sinCatalogo;
    }

    function setAliasCatalogo($nombreHoja, $campo, $alias, $descripcion) {
        $this->_aliasCatalogo[$nombreHoja][] = array("nombre_columna" => $campo, "alias" => $alias, "descripcion" => $descripcion);
    }

    function setHoja($nombreHoja, $rs) {
        $this->_hojaArray[$nombreHoja] = $rs;
    }

    private function generaCatalogo() {
        if (count($this->_hojaArray) > 0) {
            $indice = count($this->_hojaArray) - 1;

            foreach ($this->_aliasCatalogo as $nombreHoja => $catalogo) {
                $this->_objPHPExcel->createSheet($indice);
                $this->_objPHPExcel->setActiveSheetIndex($indice);
                $this->_objPHPExcel->getActiveSheet()->setTitle("catalogo_" . $nombreHoja);

                $cuentaFila = 1;
                foreach ($catalogo as $data) {
                    $cuentaColuma = 0;
                    unset($data["nombre_columna"]);
                    foreach ($data as $informacion) {

                        $type = PHPExcel_Cell_DataType::TYPE_STRING;

                        if ($cuentaFila == 1) {
                            $this->_objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(
                                    0, 1, "Columna", $type);

                            $this->_objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(
                                    1, 1, "Descripcion", $type);
                            $cuentaFila++;
                        }


                        $this->_objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(
                                $cuentaColuma, $cuentaFila, $informacion, $type);
                        $cuentaColuma++;
                    }
                    $cuentaFila++;
                }
            }
        }
    }

    function generaExcel() {

        $indice = 0;
        foreach ($this->_hojaArray as $nombreHoja => $datosHoja) {

            if ($indice != 0) {
                $this->_objPHPExcel->createSheet($indice);
            }
            $this->_objPHPExcel->setActiveSheetIndex($indice);
            $this->_objPHPExcel->getActiveSheet()->setTitle($nombreHoja);


            /////cabeceras
            $cabeceras = array();
            foreach ($datosHoja as $datH) {
                foreach ($datH as $k => $v) {
                    if (!is_numeric($k)) {
                        $cabeceras[$k] = $k;
                    }
                }
                break;
            }


            $cuenta = 0;
            foreach ($cabeceras as $cabe) {
                $this->_objPHPExcel->getActiveSheet()
                        ->setCellValueExplicitByColumnAndRow($cuenta, 1, $cabe);
                $cuenta++;
            }


            /////data

            $cuentaFila = 1;
            foreach ($datosHoja as $data) {

                $cuentaFila++;
                $cuentaColuma = 0;
                foreach ($data as $ll => $dato) {
                    if (!is_numeric($ll)) {

                        //$this->_objPHPExcel->getActiveSheet()->setCellValue(columnLetter($cuentaColuma). $cuentaFila, $dato);
                        $type = PHPExcel_Cell_DataType::TYPE_STRING;
                        $this->_objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($cuentaColuma, $cuentaFila, $dato, $type);
                        $cuentaColuma++;
                    }
                }
            }

            ////
            $indice++;
        }

        if ($this->_catalgo) {
            $this->generaCatalogo();
        }
        $this->_objPHPExcel->setActiveSheetIndex(0);



        // Save Excel 2007 file

        if ($this->_rutaParaGuardar == "") {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $this->_nombreArchivoSalida . '.xlsx"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($this->_objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
        } else {
            $objWriter = PHPExcel_IOFactory::createWriter($this->_objPHPExcel, 'Excel2007');
            echo $this->_rutaParaGuardar . "/" . $this->_nombreArchivoSalida . ".xlsx";
            $objWriter->save($this->_rutaParaGuardar . "/" . $this->_nombreArchivoSalida . ".xlsx");
        }
    }

    private function columnLetter($c) {
        $c = intval($c);
        if ($c <= 0)
            return '';
        $letter = "";
        while ($c != 0) {
            $p = ($c - 1) % 26;
            $c = intval(($c - $p) / 26);
            $letter = chr(65 + $p) . $letter;
        }
        return $letter;
    }
   
    function __destruct() {
        ini_set('memory_limit', $this->_tamano);
        
    }

}
