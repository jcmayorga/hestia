<?php

class Bl_Reporte {

    public function __construct() {
        include_once dirname(__FILE__) . '/conf.php';
        include_once dirname(__FILE__) . '/reporte_da.php';
    }

    function reporteTipo($request) {
        $inicio = $request["fin"];
        $fin = $request["ffi"];
        $emp = 0;
        if (isset($request["emp"]))
            $emp = $request["emp"];
        $ident = 0;
        if (isset($request["ident"]))
            $ident = $request["ident"];
        $da_recalificacion = new da_reporte();
        $condicion = array();
        $condicion[] = "and a.dateregistry>='$inicio' ";
        $condicion[] = " a.dateregistry<='$fin' ";


        if ($request["tipo"] == 1) { //Empresa - Contrato
            $dd = $this->reporteTipo1HTML($request);
            exit;
        } else if ($request["tipo"] == 3) {
            $dd = $this->reporteTipo3HTML($request);
            exit;
        } else if ($request["tipo"] == 4) {
                $condicion[] = " (c.identification in ('$ident','-$ident' ) or c.employeecode in ('$ident'))";
            $datos = $da_recalificacion->juk($condicion);
            return $datos;
        } else if ($request["tipo"] == 5) { //Produccipn Dinner
            unset($condicion);
            $condicion[] = " and a.dateregistry BETWEEN '$inicio' and '$fin' ";
            $condicion2[] = " and b.dateclose BETWEEN '$inicio' and '$fin' ";
            $datos = $da_recalificacion->produccionDinner($condicion, $condicion2);
            return $datos;
        } else {
            if ($emp > 0)
                $condicion[] = " d.id=$emp ";
            $datos = $da_recalificacion->juk($condicion);
            return $datos;
        }
    }

    function createDateRangeArray($strDateFrom, $strDateTo) {
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.
        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange = array();

        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));


        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom+=86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        return $aryRange;
    }

    function reporteTipo1HTML($request) {
        $inicio = $request["fin"];
        $fin = $request["ffi"];
        $emp = 0;
        if (isset($request["emp"]))
            $emp = $request["emp"];
        $da_reporte = new da_reporte();


        $arrdias[] = "";
        $arrdias[1] = "L";
        $arrdias[2] = "M";
        $arrdias[3] = "M";
        $arrdias[4] = "J";
        $arrdias[5] = "V";
        $arrdias[6] = "S";
        $arrdias[7] = "D";
        echo'<html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <title>HESTIA</title>
                    <link rel="stylesheet" href="etc/lib/css/estilo.css"/>

                    <!-- jquery.easy -->
                    <link rel="stylesheet" type="text/css" href="etc/lib/jquery-easyui/themes/default/easyui.css">
                    <link rel="stylesheet" type="text/css" href="etc/lib/jquery-easyui/themes/icon.css">
                    <script src="etc/lib/jquery-easyui/jquery-1.12.4.min.js" type="text/javascript"></script>
                    <script src="etc/lib/jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>
                    <script src="etc/lib/jquery-easyui/locale/easyui-lang-es.js" type="text/javascript"></script>
            
            </head>
            ';



        echo '<script type="text/javascript"> 
var tablesToExcel = (function () {
        var uri = "data:application/vnd.ms-excel;base64,"
                , tmplWorkbookXML = "<?xml version=\"1.0\"?><?mso-application progid=\"Excel.Sheet\"?><Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\">"
                + "<DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\"><Author>QuitoWebStyle</Author><Created>{created}</Created></DocumentProperties>"
                + "<Styles>"
                + "<Style ss:ID=\"Currency\"><NumberFormat ss:Format=\"Currency\"></NumberFormat></Style>"
                + "<Style ss:ID=\"Date\"><NumberFormat ss:Format=\"Medium Date\"></NumberFormat></Style>"
                + "</Styles>"
                + "{worksheets}</Workbook>"
                , tmplWorksheetXML = "<Worksheet ss:Name=\"{nameWS}\"><Table>{rows}</Table></Worksheet>"
                , tmplCellXML = "<Cell{attributeStyleID}{attributeFormula}><Data ss:Type=\"{nameType}\">{data}</Data></Cell>"
                , base64 = function (s) {
                    return window.btoa(unescape(encodeURIComponent(s)))
                }
        , format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            })
        }
        return function (tables, wsnames, wbname, appname) {
            var ctx = "";
            var workbookXML = "";
            var worksheetsXML = "";
            var rowsXML = "";

            for (var i = 0; i < tables.length; i++) {
                if (!tables[i].nodeType)
                    tables[i] = document.getElementById(tables[i]);
                for (var j = 0; j < tables[i].rows.length; j++) {
                    rowsXML += "<Row>"
                    for (var k = 0; k < tables[i].rows[j].cells.length; k++) {
                        var dataType = tables[i].rows[j].cells[k].getAttribute("data-type");
                        var dataStyle = tables[i].rows[j].cells[k].getAttribute("data-style");
                        var dataValue = tables[i].rows[j].cells[k].getAttribute("data-value");
                        dataValue = (dataValue) ? dataValue : tables[i].rows[j].cells[k].innerHTML;
                        var dataFormula = tables[i].rows[j].cells[k].getAttribute("data-formula");
                        dataFormula = (dataFormula) ? dataFormula : (appname == "Calc" && dataType == "DateTime") ? dataValue : null;
                        ctx = {attributeStyleID: (dataStyle == "Currency" || dataStyle == "Date") ? " ss:StyleID=\"" + dataStyle + "\"" : ""
                            , nameType: (dataType == "Number" || dataType == "DateTime" || dataType == "Boolean" || dataType == "Error") ? dataType : "String"
                            , data: (dataFormula) ? "" : dataValue
                            , attributeFormula: (dataFormula) ? " ss:Formula=\"" + dataFormula + "\"" : ""
                        };
                        rowsXML += format(tmplCellXML, ctx);
                    }
                    rowsXML += "</Row>"
                }
                ctx = {rows: rowsXML, nameWS: wsnames[i] || "Sheet" + i};
                worksheetsXML += format(tmplWorksheetXML, ctx);
                rowsXML = "";
            }

            ctx = {created: (new Date()).getTime(), worksheets: worksheetsXML};
            workbookXML = format(tmplWorkbookXML, ctx);
            var link = document.createElement("A");
            link.href = uri + base64(workbookXML);
            link.download = wbname || "Workbook.xls";
            link.target = "_blank";
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    })();</script>';



        echo "<br/><br/><a href='#' class='easyui-linkbutton' id='test' onClick='javascript:descargaReport();'  data-options=\"iconCls:'icon-save'\">Descargar Reporte</a><br/><br/>";
        $empresasarr = explode(",", $emp);
        $js = "";
        $js_xl_tb = "";
        $js_xl_tb_name = "";
        foreach ($empresasarr as $value_emparr) {


            unset($condicion);
            unset($datos);
            unset($datos2);
            unset($datos3);
            unset($td_dia);
            unset($td_num);
            unset($xx);
            unset($subtd_dia);

            $condicion = array();
            $condicion[] = "and a.dateregistry>='$inicio' ";
            $condicion[] = " a.dateregistry<='$fin' ";
            if ($emp != 0 and $emp != null)
                $condicion[] = " d.id in ($value_emparr) ";
            $condicion[] = " b.acronym='" . $request["se"] . "' ";

            $datos = $da_reporte->jukDisctinct($condicion);
            $obj_rep = new da_reporte();
            $condservc = $request["se"];
            $daata_serv = $obj_rep->getService($condservc);


            $td_dia = "<td>#</td><td>Apellidos y Nombres</td><td>Identificador</td><td>CECO</td><td>Centro de Costo</td><td>Empresa</td>";
            $td_num = "";
            $xx = $this->createDateRangeArray($request["fin"], $request["ffi"]);
            foreach ($xx as $value) {
                $date = new DateTime($value);
                $td_num.="<td>" . $date->format('d') . "</td>";
                $td_dia.="<td>" . $arrdias[$date->format('N')] . "\t " . $date->format('d') . "</td>";
            }
            $td_dia.="<td>TOTAL " . strtoupper($daata_serv[0]['description']) . "</td>";

            $cond_em = array();
            $cond_em[] = "and id=$value_emparr ";
            $rs_empr = $da_reporte->getEmpresa($cond_em);



            if ($js_xl_tb == "") {
                $js_xl_tb = '"id_' . $value_emparr . '"';
                $js_xl_tb_name = '"' . $rs_empr[0]['label'] . '"';
            } else {
                $js_xl_tb.=',"id_' . $value_emparr . '"';
                $js_xl_tb_name.=',"' . $rs_empr[0]['label'] . '"';
            }
            echo "<br/><br/><br/><br/><br/><table border='1' id='id_$value_emparr' class=class='table2excel'><tr>"
            . "<td colspan='10'>Servicio: [" . $daata_serv[0]['description'] . "] Desde: " . $request["fin"] . " Hasta: " . $request["ffi"] . " </td>"
            . "</tr>"
            . "<tr style='color:red;text-align:center;font-weight:bold;'>"
            . $td_dia
            . "</tr>";
            $i = 0;

            foreach ($datos as $value) {
                $i++;
                $subtd_dia = "";
                foreach ($xx as $value2) {
                    $date = new DateTime($value2);
                    $subtd_dia.="<td id='" . $value['id'] . $date->format('ymd') . "' style=;text-align:center;'></td>";
                }
                echo"<tr><td>$i</td><td>" . $value['fullname'] . "</td><td>" . $value['identification'] . "</td><td>" . $value['costcenter'] . "</td><td>" . $value['namecenter'] . "</td><td>" . $value['businessname'] . "</td>$subtd_dia<td id='t" . $value['id'] . "' style='text-align:center;font-weight:bold;'></td></tr>";
            }
            echo"</table>";
            $datos2 = $da_reporte->jukDetalle($condicion);
            foreach ($datos2 as $value3) {
                $date2 = new DateTime($value3['dateregistry']);
                $js.= '<script type="text/javascript">
                var elem="";
                elem=parseInt(document.getElementById("' . $value3['id'] . $date2->format('ymd') . '").innerHTML);
                if (elem>0)
                  document.getElementById("' . $value3['id'] . $date2->format('ymd') . '").innerHTML =elem+1;
                  else
                    document.getElementById("' . $value3['id'] . $date2->format('ymd') . '").innerHTML =1;
                 </script>';
            }
            $datos3 = $da_reporte->jukCount($condicion);
            foreach ($datos3 as $value4) {
                $js.= '<script type="text/javascript">
                        document.getElementById("t' . $value4['id'] . '").innerHTML =' . $value4['total'] . ';
                     </script>';
            }
        }

        echo $js;



        //Inicia Extras

        $js = "";


//$value_emparr Empresa
        unset($condicion);
        unset($datos);
        unset($datos2);
        unset($datos3);
        unset($td_dia);
        unset($td_num);
        unset($xx);
        unset($subtd_dia);

        $condicion = array();
        $condicion[] = " and b.dateclose BETWEEN '$inicio' and '$fin'";
        $condicion[] = " b.serviceacronym='" . $request["se"] . "' ";
	$condicion[] = " c.id in (" . $request["emp"] . ") ";

        $datos = $da_reporte->extrasDisctinct($condicion);
        $obj_rep = new da_reporte();
        $condservc = $request["se"];
        $daata_serv = $obj_rep->getService($condservc);


        $td_dia = "<td>#</td><td>Apellidos y Nombres (Extras)</td><td>Identificador</td><td>Centro de Costo</td><td>Empresa</td>";
        $td_num = "";
        $xx = $this->createDateRangeArray($request["fin"], $request["ffi"]);
        foreach ($xx as $value) {
            $date = new DateTime($value);
            $td_num.="<td>" . $date->format('d') . "</td>";
            $td_dia.="<td>" . $arrdias[$date->format('N')] . "\t " . $date->format('d') . "</td>";
        }
        $td_dia.="<td>TOTAL " . strtoupper($daata_serv[0]['description']) . "</td>";

        $cond_em = array();
        $cond_em[] = "and id=$value_emparr ";
        $rs_empr = $da_reporte->getEmpresa($cond_em);



        $js_xl_tb.=',"id_extras"';
        $js_xl_tb_name.=',"EXTRAS"';
        echo "<br/><br/><br/><br/><br/><table border='1' id='id_extras' class=class='table2excel'><tr>"
        . "<td colspan='10'>EXTRAS -> Servicio: [" . $daata_serv[0]['description'] . "] Desde: " . $request["fin"] . " Hasta: " . $request["ffi"] . " </td>"
        . "</tr>"
        . "<tr style='color:red;text-align:center;font-weight:bold;'>"
        . $td_dia
        . "</tr>";
        $i = 0;

        foreach ($datos as $value) {
            $i++;
            $subtd_dia = "";
            foreach ($xx as $value2) {
                $date = new DateTime($value2);
                $subtd_dia.="<td id='" . $value['identification'] . $date->format('ymd') . $value['company_id'] . "' style=;text-align:center;'></td>";
            }
            echo"<tr><td>$i</td><td id='name_" . $value['identification'] . $value['company_id'] . "'> </td><td>" . $value['identification'] . "</td><td id='cc_" . $value['identification'] . $value['company_id'] . "'></td><td>" . $value['businessname'] . "</td>$subtd_dia<td id='t" . $value['identification'] . $value['company_id'] . "' style='text-align:center;font-weight:bold;'></td></tr>";
        }
        echo"</table>";
        $datos2 = $da_reporte->extrasDetalle($condicion);
        foreach ($datos2 as $value3) {
            $date2 = new DateTime($value3['dateclose']);
            $js.= '<script type="text/javascript">
                var elem="";
                document.getElementById("name_' . $value3['identification'] . $value3['company_id'] . '").innerHTML ="' . $value3['fullname'] . '" ;
                document.getElementById("cc_' . $value3['identification'] . $value3['company_id'] . '").innerHTML ="' . $value3['costcentername'] . '" ;    
                elem=parseInt(document.getElementById("' . $value3['identification'] . $date2->format('ymd') . $value3['company_id'] . '").innerHTML);
                if (elem>0)
                  document.getElementById("' . $value3['identification'] . $date2->format('ymd') . $value3['company_id'] . '").innerHTML =elem+1;
                  else
                    document.getElementById("' . $value3['identification'] . $date2->format('ymd') . $value3['company_id'] . '").innerHTML =1;
                 </script>';
        }
        $datos3 = $da_reporte->extrasCount($condicion);
        foreach ($datos3 as $value4) {
            $js.= '<script type="text/javascript">
                        document.getElementById("t' . $value4['identification'] . $value4['company_id'] . '").innerHTML =' . $value4['total'] . ';
                     </script>';
        }
        $js.='<script type="text/javascript">
                function descargaReport(){
                tablesToExcel([' . $js_xl_tb . '], [' . $js_xl_tb_name . '], "Consolidado' . $request["se"] . '_' . $inicio . '_to_' . $fin . '.xls", "Excel");
                }
              </script>';
        echo $js;





        return;
    }

    function reporteTipo3HTML($request) {
        $inicio = $request["fin"];
        $fin = $request["ffi"];
        $emp = 0;
        if (isset($request["emp"]))
            $emp = $request["emp"];
        $da_reporte = new da_reporte();
        $condicion = array();
        $condicion[] = "and a.dateregistry>='$inicio' ";
        $condicion[] = " a.dateregistry<='$fin' ";
        $condicion[] = " b.acronym='" . $request["se"] . "' ";
        if ($emp > 0)
            $condicion[] = " d.id=$emp ";
        $datos = $da_reporte->jukDisctinct($condicion);
        $arrdias[] = "";
        $arrdias[1] = "L";
        $arrdias[2] = "M";
        $arrdias[3] = "M";
        $arrdias[4] = "J";
        $arrdias[5] = "V";
        $arrdias[6] = "S";
        $arrdias[7] = "D";
        echo'<html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <title>HESTIA</title>
                    <link rel="stylesheet" href="lib/css/estilo.css"/>

                    <!-- jquery.easy -->
                    <link rel="stylesheet" type="text/css" href="lib/jquery-easyui/themes/default/easyui.css">
                    <link rel="stylesheet" type="text/css" href="lib/jquery-easyui/themes/icon.css">
                    <script src="lib/jquery-easyui/jquery-1.12.4.min.js" type="text/javascript"></script>
                    <script src="lib/jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>
                    <script src="lib/jquery-easyui/locale/easyui-lang-es.js" type="text/javascript"></script>

            <script type="text/javascript">
            function fnExcelReport() {
                var tab_text = \'<html xmlns:x="urn:schemas-microsoft-com:office:excel">\';
                tab_text = tab_text + \'<head><xml><x:ExcelWorkbook><x:ExcelWorksheets>\';

                tab_text = tab_text + \'<x:ExcelWorksheet><x:Name>Consolidado</x:Name>\';
                tab_text = tab_text + \'<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>\';

                tab_text = tab_text + \'</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>\';

                tab_text = tab_text + "<table border=\'1px\'>";
                tab_text = tab_text + $(\'#myRep01\').html();
                tab_text = tab_text + \'</table></body></html>\';


             var data_type = \'data:application/vnd.ms-excel\';

                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE ");

                if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                    if (window.navigator.msSaveBlob) {
                        var blob = new Blob([tab_text], {
                            type: "application/csv;charset=utf-8;"
                        });
                        navigator.msSaveBlob(blob, \'CONSOLIDADO.xls\');
                    }
                } else {
                    $(\'#test\').attr(\'href\', data_type + \', \' + encodeURIComponent(tab_text));
                    $(\'#test\').attr(\'download\', \'CONSOLIDADO.xls\');
                }

            }
            </script>
            </head>
            ';
        $obj_rep = new da_reporte();
        $cond = $request["se"];
        $daata_serv = $obj_rep->getService($cond);

        $td_dia = "<td rowspan='2'>#</td><td rowspan='2'>Empresa</td><td rowspan='2'>Identificador</td><td rowspan='2'>Apellidos y Nombres</td><td rowspan='2'>CECO</td><td rowspan='2'>Centro de Costo</td>";
        $td_num = "";
        $xx = $this->createDateRangeArray($request["fin"], $request["ffi"]);
        foreach ($xx as $value) {
            $date = new DateTime($value);
            $td_num.="<td>" . $date->format('d') . "</td>";
            $td_dia.="<td>" . $arrdias[$date->format('N')] . "</td>";
        }
        $td_dia.="<td rowspan='2'>TOTAL</td>";

        echo "<br/><br/><a href='#' class='easyui-linkbutton' id='test' onClick='javascript:fnExcelReport();'  data-options=\"iconCls:'icon-save'\">descarga</a><br/><br/><br/><table border='1' id='myRep01'><tr>"
        . "<td colspan='10'>Servicio: <h1>" . $daata_serv[0]['description'] . "</h1>Desde: " . $request["fin"] . " Hasta: " . $request["ffi"] . " </td>"
        . "</tr>"
        . "<tr style='color:red;text-align:center;font-weight:bold;'>"
        . $td_dia
        . "</tr><tr style='color:red;text-align:center;font-weight:bold;'>"
        . $td_num
        . "</tr>";
        $i = 0;

        foreach ($datos as $value) {
            $i++;
            $subtd_dia = "";
            foreach ($xx as $value2) {
                $date = new DateTime($value2);
                $subtd_dia.="<td id='" . $value['id'] . $date->format('ymd') . "' style=;text-align:center;'></td>";
            }
            echo"<tr><td>$i</td><td>" . $value['businessname'] . "</td><td>" . $value['identification'] . "</td><td>" . $value['fullname'] . "</td><td>" . $value['costcenter'] . "</td><td>" . $value['namecenter'] . "</td>$subtd_dia<td id='t" . $value['id'] . "' style='text-align:center;font-weight:bold;'></td></tr>";
        }
        echo"</table>";
        $datos2 = $da_reporte->jukDetalle($condicion);
        foreach ($datos2 as $value3) {
            $date2 = new DateTime($value3['dateregistry']);
            echo '<script type="text/javascript">
                var elem="";
                elem=parseInt(document.getElementById("' . $value3['id'] . $date2->format('ymd') . '").innerHTML);
                if (elem>0)
                  document.getElementById("' . $value3['id'] . $date2->format('ymd') . '").innerHTML =elem+1;
                  else
                    document.getElementById("' . $value3['id'] . $date2->format('ymd') . '").innerHTML =1;
                 </script>';
        }
        $datos3 = $da_reporte->jukCount($condicion);
        foreach ($datos3 as $value4) {
            echo '<script type="text/javascript">
    document.getElementById("t' . $value4['id'] . '").innerHTML =' . $value4['total'] . ';
 </script>';
        }

        return;
    }

    function reporteTipo2($inicio, $fin, $emp = 0) {
        $da_recalificacion = new da_reporte();
        $condicion = array();
        $condicion[] = "and a.dateregistry>='$inicio' ";
        $condicion[] = " a.dateregistry<='$fin' ";
        if ($emp > 0)
            $condicion[] = " d.id=$emp ";
        $datos = $da_recalificacion->juk($condicion);
        return $datos;
    }

    /**
     * Retorna el catálogo Institución Referencia
     * @return array el catálogo Institución Referencia
     */
    function catalogoEmpresa($request) {
        $catalogo = new da_reporte();
        if (isset($request['catEmpTip']) && $request['catEmpTip'] == 2) {
            $rs = $catalogo->catalogoEmpresaCont($request['cont']);
        } else
            $rs = $catalogo->catalogoEmpresa();
        return $rs;
    }

    /**
     * Retorna el catálogo de los contratos
     * @return array el catálogo Institución Referencia
     */
    function catalogoContrato() {
        $catalogo = new da_reporte();
        $rs = $catalogo->catalogoContrato();
        return $rs;
    }

    /**
     * Retorna el catálogo Tipo Identificación
     * @return array el catálogo Tipo Identificación
     */
    function catalogo_TipoIdentificacion() {
        $catalogo = new Bl_Catalogo();
        $rs = $catalogo->recuperarCatalogo("TIPO_IDENTIFICACION");
        return $rs;
    }

    /**
     * Retorna el catálogo Estado Civil
     * @return array el catálogo Estado Civil
     */
    function catalogo_TipoEstadoCivil() {
        $catalogo = new Bl_Catalogo();
        $rs = $catalogo->recuperarCatalogo("ESTADO_CIVIL");
        return $rs;
    }

    /**
     * Retorna el catálogo-discapacidad "Con quien vive"
     * @return array el catálogo-discapacidad "Con quien vive"
     */
    function catalogo_TipoViveCon() {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarCatalogo("DIS-CON_QUIEN_VIVE");
        return $rs;
    }

    /**
     * Retorna el catálogo-discapacidad "Nivel de instrucción"
     * @return array el catálogo-discapacidad "Nivel de instrucción"
     */
    function catalogo_TipoInstruccion() {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarCatalogo("DIS-NIVEL-INSTRUCCION");
        return $rs;
    }

    /**
     * Retorna el catálogo-discapacidad "Vivienda"
     * @return array el catálogo-discapacidad "Vivienda"
     */
    function catalogo_TipoVivienda() {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarCatalogo("DIS-VIVIENDA");
        return $rs;
    }

    /**
     * Retorna el catálogo "SI/NO"
     * @return array el catálogo"SI/NO"
     */
    function catalogo_TipoSiNo() {
        $catalogo = new Bl_Catalogo();
        $rs = $catalogo->recuperarCatalogo("SI/NO");
        return $rs;
    }

    /**
     * Retorna el catálogo-discapacidad "SI Trabaja"
     * @return array el catálogo-discapacidad "SI Trabaja"
     */
    function catalogo_TipoSiTrabajo() {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarCatalogo("DIS-SI-TRABAJA");
        return $rs;
    }

    /**
     * Retorna el catálogo-discapacidad "NO trabaja"
     * @return array el catálogo-discapacidad "NO trabaja"
     */
    function catalogo_TipoNoTrabajo() {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarCatalogo("DIS-NO-TRABAJA");
        return $rs;
    }

    /**
     * Retorna el catálogo-discapacidad "Período de Adquisición"
     * @return array el catálogo-discapacidad "Período de Adquisición"
     */
    function catalogo_TipoPeriodoAdquisicion() {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarCatalogo("DIS-PERIODO-ADQUISICION");
        return $rs;
    }

    /**
     * Retorna el recursivo catálogo-discapacidad "Periódo de Adquisición"
     * @param int $padre ID catálogo padre
     * @return array el catálogo-discapacidad "Período de Adquisición"
     */
    function catalogo_TipoPeriodoAdquisicionTipo($padre) {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarCatalogo("DIS-PERIODO-ADQUISICION", $padre);
        return $rs;
    }

    /**
     * Retorna el recursivo catálogo-discapacidad "Periódo de Adquisición"
     * @param int $padre ID catálogo padre
     * @return array el catálogo-discapacidad "Período de Adquisición"
     */
    function catalogo_TipoPeriodoAdquisicionTipoValor($valor) {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarCatalogoModificado("DIS-PERIODO-ADQUISICION", $valor);
        return $rs;
    }

    /**
     * Retorna el nombre del rango que le corresponde de acuerdo a puntaje
     * @param int $val valor a ser calculado en el rango
     * @return array el nombre de rango
     */
    function rango_PuntajeAyuda($val) {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarRango("DIS-RANGO-PUNTAJE", $val);
        return $rs;
    }

    /**
     * Retorna el catálogo-discapacidad "Grado de Discapacidad"
     * @return array el catálogo-discapacidad "Grado de Discapacidad"
     */
    function catalogo_TipoGradoDiscapacidad() {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarCatalogo("DIS-GRADO-DISCAPACIDAD");
        return $rs;
    }

    /**
     * Retorna el nombre del rango del grado de discapacidad
     * @param int $val porcentaje global de discapacidad
     * @param int $val3 porcentaje factores sociales
     * @return array el catálogo-discapacidad "Nivel de instrucción"
     */
    function rango_GradoDiscapacidad($val, $val2) {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $val = $val + $val2;
        $rs = $catalogo->recuperarRango("DIS-GRADO-DISCAPACIDAD", $val);
        return $rs;
    }

    /**
     * Retorna el catálogo de discapacidades de acuerdo a selección enviada
     * @param array $request seleccion de discapacidades
     * @return array catálogo Tipo de Discapacidad
     */
    function catalogo_TipoDiscapacidadComboM() {
        $da_recalificacion = new da_recalificacion();
        $condicion = array();
        $condicion[] = "c_catalogo = 'DIS-TIPO-DISCAPACIDAD'";
        $condicion[] = "c_estado = 'A'";
        $datos = $da_recalificacion->catalogo($condicion);
        return $datos;
    }

    function catalogo_TipoDiscapacidadComboModificado($request) {
        if (!empty($request['id'])) {

            $id_in = $request['id'];
            $da_recalificacion = new da_recalificacion();
            $condicion = array();
            $condicion[] = "c_catalogo = 'DIS-TIPO-DISCAPACIDAD'";
            $condicion[] = "c_estado = 'A'";
            $condicion[] = "c_valor in ($id_in)";
            $datos = $da_recalificacion->catalogo($condicion);
            return $datos;
        } else {
            return "";
        }
    }

//     function catalogo_TipoDiscapacidadCombo() {        
//        if (!empty($request['id'])) {
//            
//            $id_in = $request['id'];
//            $da_recalificacion = new da_recalificacion();
//            $condicion = array();
//            $condicion[] = "c_catalogo = 'DIS-TIPO-DISCAPACIDAD'";
//            $condicion[] = "c_estado = 'A'";
//            $condicion[] = "c_valor in ($id_in)";
//            $datos = $da_recalificacion->catalogo($condicion);
//            return $datos;
//        } else {
//            return "";
//        }
//    }
    function catalogo_TipoDiscapacidadCombo($request) {
        if (!empty($request['id'])) {
            $in = "";
            foreach ($request['id'] as $valor) {
                if ($valor)
                    $in.="'" . $valor . "', ";
            }
            $id_in = substr($in, 0, (strlen($in) - 2));
            $da_recalificacion = new da_recalificacion();
            $condicion = array();
            $condicion[] = "c_catalogo = 'DIS-TIPO-DISCAPACIDAD'";
            $condicion[] = "c_estado = 'A'";
            $condicion[] = "c_valor in ($id_in)";
            $datos = $da_recalificacion->catalogo($condicion);
            return $datos;
        } else {
            return "";
        }
    }

    /**
     * Retorna el catálogo-discapacidad "Terapias"
     * @return array el catálogo-discapacidad "Terapias"
     */
    function catalogo_TipoTerapia() {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarCatalogo("DIS-TERAPIA");
        return $rs;
    }

    /**
     * Retorna el catálogo-discapacidad "Ayuda Técnica"
     * @return array el catálogo-discapacidad "Ayuda Técnica"
     */
    function catalogo_TipoAyudaTecnica() {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarCatalogo("DIS-AYUDA-TECNICA");
        return $rs;
    }

    /**
     * Retorna el catálogo-discapacidad "Mando de Vehículo Ortopédico"
     * @return array el catálogo-discapacidad "Mando de Vehículo Ortopédico"
     */
    function catalogo_TipoMandoVehiculoOrtopedico() {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarCatalogo("DIS-MANDO-VEHICULO-ORTOPEDICO");
        return $rs;
    }

    /**
     * Retorna el catálogo-discapacidad "Aprueba o No Aprueba"
     * @return array el catálogo-discapacidad "Aprueba o No Aprueba"
     */
    function catalogo_TipoApruebaNoAprueba() {
        $catalogo = new Bl_CatalogoDiscapacidad();
        $rs = $catalogo->recuperarCatalogo("DIS-APREBA/NOAPRUEBA");
        return $rs;
    }

    /**
     * Retorna el catálogo Sexo
     * @return array el catálogo Sexo
     */
    function catalogo_TipoSexo() {
        $catalogo = new Bl_Catalogo();
        $rs = $catalogo->recuperarCatalogo("SEXO");
        return $rs;
    }

    /**
     * asigna y ordena los datos en un array de la persona para agregar una nueva
     * @param array $datos request del gui
     * @return array ordenado para agregar una nueva persona
     */
    function asignaDataPersona($datos) {
        $datos_persona['c_identificacion'] = $datos['cedula'];
//        $datos_persona['c_tipo_identificacion'] = $datos['c_tipo_identificacion'];
        $datos_persona['c_tipo_identificacion'] = 'C';
        $datos_persona['c_nombres'] = strtoupper($datos['nombre']);
        $datos_persona['c_apellidos'] = strtoupper($datos['apellido']);
        $datos_persona['c_sexo'] = $datos['sexo'];
        $datos_persona['f_fecha_nacimiento'] = $datos['fecha_nacimiento'];
        $datos_persona['c_estado_civil'] = $datos['estado_civil'];
        $datos_persona['discapacidad'] = "S";
        $datos_persona['c_validado'] = "S";
        $datos_persona['c_telefonos'] = $datos['telefono_convencional'] . ";" . $datos['telefono_celular'];
        return $datos_persona;
    }

    /**
     * asigna y ordena los datos en un array de la persona para actualizarla
     * @param array $datos request del gui
     * @return array ordenado para actualizar a una persona
     */
    function asignaDataPersonaActualizar($datos) {
        $datos_persona['c_identificacion'] = $datos['cedula'];
        $datos_persona['c_tipo_identificacion'] = 'C';
        $datos_persona['c_nombres'] = strtoupper($datos['nombre']);
        $datos_persona['c_apellidos'] = strtoupper($datos['apellido']);
        $datos_persona['c_estado_civil'] = $datos['estado_civil'];
        $datos_persona['c_sexo'] = $datos['sexo'];
        $datos_persona['f_fecha_nacimiento'] = $datos['fecha_nacimiento'];
        $datos_persona['c_discapacidad'] = "S";
        $datos_persona['c_validado'] = "S";
        $datos_persona['c_telefonos'] = $datos['telefono_convencional'] . ";" . $datos['telefono_celular'];
        return $datos_persona;
    }

    /**
     * asigna y ordena los datos en un array de la persona con discapacidad
     * @param array $datos request del gui
     * @return array ordenado para añadir a una persona con discapacidad
     */
    function asignaDataPersonaDiscapacidad01($datos) {

        $data01['dis_institucion_referencia'] = $datos['dis_institucion_referencia'];
        $data01['dis_cedula_calificador_referente'] = $datos['dis_cedula_calificador_referente'];
        $data01['dis_codigo_calificador_referente'] = $datos['dis_codigo_calificador_referente'];
        $data01['dis_nombre_calificador_referente'] = $datos['dis_nombre_calificador_referente'];

        $data01['dis_cedula'] = $datos['cedula'];
//        $data01['codigo_conadis'] = $datos['codigo_conadis'];
        $data01['dis_id_estado_civil'] = $datos['estado_civil'];
        $data01['dis_direccion'] = $datos['direccion'];
        $data01['dis_referencia'] = $datos['referencia'];
        $data01['dis_telefono_convencional'] = $datos['telefono_convencional'];
        $data01['dis_telefono_celular'] = $datos['telefono_celular'];
        $data01['dis_cod_provincia'] = $datos['c_cod_provincia'];
        $data01['dis_cod_canton'] = $datos['c_cod_canton'];
        $data01['dis_cod_parroquia'] = $datos['c_cod_parroquia'];
        $data01['dis_latlong_gmaps'] = $datos['latlong_gmaps'];
        $data01['dis_georeferencia'] = $datos['georeferencia_aproximada'];
        $data01['dis_contacto_apellido'] = $datos['contacto_apellido'];
        $data01['dis_contacto_nombre'] = $datos['contacto_nombre'];
        $data01['dis_contacto_direccion'] = $datos['contacto_direccion'];
        $data01['dis_contacto_telefono'] = $datos['contacto_telefono'];
        $data01['dis_vive_con'] = $datos['vive_con'];
        $data01['dis_vivienda'] = $datos['vivienda'];
        $data01['dis_nivel_instruccion'] = $datos['nivel_instruccion'];
        $data01['dis_preg_formacion_ocupacional'] = $datos['preg_formacion_ocupacional'];
        $data01['dis_formacion_ocupacional'] = $datos['formacion_ocupacional'];
        $data01['dis_profesion'] = $datos['ciiu'];
        $data01['dis_preg_trabaja'] = $datos['preg_trabaja'];
        $data01['dis_si_trabajo'] = $datos['si_trabajo'];
        $data01['dis_no_trabajo'] = $datos['no_trabajo'];
        $data01['dis_periodo_adquisicion'] = $datos['periodo_adquisicion'];
        $data01['dis_periodo_adquisiciontipo'] = $datos['periodo_adquisicionTipo'];
        $data01['dis_periodo_adquisicionsubtipo'] = $datos['periodo_adquisicionSubTipo'];
        $data01['dis_fecha_adquisicion'] = $datos['fecha_adquisicion'];
        $data01['dis_puntaje_ayuda_permanente'] = $datos['dis_puntaje_ayuda_permanente'];
        $data01['dis_result_ayuda_permanente'] = $datos['dis_result_ayuda_permanente'];
        $data01['dis_deficiencia_predomina'] = $datos['deficiencia_predomina'];
        $data01['dis_porcentaje_global_dis'] = $datos['dis_porcentaje_global_discapacidad'];
        $data01['dis_puntaje_factores_sociales'] = $datos['dis_puntaje_factores_sociales'];
        $data01['dis_porcentaje_discapacidad'] = $datos['dis_porcentaje_global_discapacidad'] + $datos['dis_puntaje_factores_sociales'];
        if ($data01['dis_porcentaje_discapacidad'] > 100) {
            $data01['dis_porcentaje_discapacidad'] = 100;
        }
        $data01['dis_grado_discapacidad'] = $datos['dis_grado_discapacidad'];

        if (isset($datos['terapia']))
            $data01['dis_terapia'] = implode(",", $datos['terapia']);
        else
            $data01['dis_terapia'] = "0";

        if (isset($datos['ayuda_tecnica']))
            $data01['dis_ayuda_tecnica'] = implode(",", $datos['ayuda_tecnica']);
        else
            $data01['dis_ayuda_tecnica'] = "0";

        $data01['dis_preg_vehiculo_ortopedico'] = $datos['preg_vehiculo_ortopedico'];
        $data01['dis_mando_vehiculo_ortopedico'] = $datos['mando_vehiculo_ortopedico'];
        $data01['dis_aditamento_vehiculo_ortopedico'] = $datos['aditamento_vehiculo_ortopedico'];
        $data01['dis_preg_ant'] = $datos['preg_ant'];
        $data01['dis_evaluacion_psicologica'] = $datos['evaluacion_psicologica'];
        $data01['dis_evaluacion_medica'] = $datos['evaluacion_medica'];
        $data01['dis_result_ant'] = $datos['result_ant'];
        $data01['dis_id_usuario'] = trim($_SESSION['usuario']['c_usuario'] . " ");

        if ($_SESSION['usuario']['c_id_externo_entidad'] == "")
            $data01['dis_unidad'] = 0;
        else
            $data01['dis_unidad'] = $_SESSION['usuario']['c_id_externo_entidad'];
        return $data01;
    }

    /**
     * Valida si existe una persona, si existe, cambia los estados, si no existe
     * crea una nueva persona, adicional a esto almacena una primera parte del 
     * formulario principal
     * @param array $datos request del gui
     * @return array Status de transacción del primer formulario
     */
    function enviaRecalificacion($datos) {
        $bl_persona = new Bl_persona();
        $persona_discapacidad = new Bl_personaDiscapacidad();
        $rs = "";
        //Asigna data a arrays persona
        $datos_persona = $this->asignaDataPersona($datos);
        $datos_persona_act = $this->asignaDataPersonaActualizar($datos);

        //Verifica si existe o no la persona
        if (count($bl_persona->persona($datos['cedula'])) > 0) {
            //Existe persona -> actualiza             
            if ($bl_persona->actualizar($datos_persona_act)) {
                $rs = array("error" => 0, "Mensaje" => "Datos ACTUALIZADOS correctamente");
            } else {
                return array("error" => 1, "Mensaje" => "ERROR al actualizar persona");
                exit;
            }
        } else {
            //No existe persona -> crea nueva persona
            if ($bl_persona->crear($datos_persona)) {
                $rs = array("error" => 0, "Mensaje" => "Persona creada correctamente");
            } else {
                return array("error" => 1, "Mensaje" => "ERROR al crear Persona");
                exit;
            }
        }
        $datos_personaDiscapacidad = $this->asignaDataPersonaDiscapacidad01($datos);
        //Crea registro
        if ($persona_discapacidad->crearPersonaDiscapacitada($datos_personaDiscapacidad)) {
            $rs = array("error" => 0, "Mensaje" => "Almacenamiento formulario OK");
            $da_recalificacion = new da_recalificacion();
            $bl_certificado = new bl_certificadoDiscapacidad();

            //Almacena los tipos y detalles de discapacidad
            $rs = $this->almacenaDetalleDiscapacidad($datos);
            //Discrimina si genera o no el certificado de acuerdo al porcentaje de discapacidad
            $rs_minimo_calificacion = $da_recalificacion->obtenerPorcentajeMinimo();
            if ($datos_personaDiscapacidad['dis_porcentaje_discapacidad'] >= $rs_minimo_calificacion[0]['c_valor']) {
                //Genera Certificado
                if ($bl_certificado->almacenaCertificado($datos_personaDiscapacidad)) {
                    $rs = array("error" => 0, "Mensaje" => "Certificado OK", "certificado" => 1);
                } else {
                    return array("error" => 1, "Mensaje" => "ERROR al crear el certificado");
                    exit;
                }
            } else {
                $rs = array("error" => 0, "Mensaje" => "Se almacena el registro, pero no genera certificado por el porcentaje", "certificado" => 0);
            }
        } else {
            return array("error" => 1, "Mensaje" => "Existió un error al momento de almacenar el fumulario JUK01");
            exit;
        }
        return $rs;
    }

    /**
     * Almacena el detalle de los tipos de discapacidad del segundo formulario
     * @param array $datos request del gui
     * @return array status del registro de los detalles de discapacidad
     */
    function almacenaDetalleDiscapacidad($datos) {
        $persona_discapacidad = new Bl_personaDiscapacidad();
        $data02 = array();
        $rs = "";
        for ($i = 1; $i < 7; $i++) {
            if (isset($datos['tipo_discapacidad_' . $i])) {
                $data02['c_dis_cal_cedula'] = $datos['cedula'];
                $data02['n_dis_cal_tipo_discapacidad'] = $i;
                $data02['c_dis_cal_cie10'] = $datos['cie10_' . $i];
                $data02['n_dis_cal_porcentaje_dis'] = $datos['porcentaje_discapacidad_' . $i];
                $data02['c_dis_cal_id_usuario'] = $_SESSION['usuario']['c_usuario'];
                $data02['n_dis_cal_id_unidad'] = $_SESSION['usuario']['c_id_externo_entidad'];
                $data02['c_dis_cal_estado'] = "A";
                if ($persona_discapacidad->crearDiscapacidad($data02)) {
                    $rs = array("error" => 0, "Mensaje" => "Detalle $i almacenado correctamente");
                } else {
                    return array("error" => 1, "Mensaje" => "ERROR al momento de almacenar detalle discapacidad $i");
                    exit;
                }
            }
        }
        return $rs;
    }

    /**
     * Almacena el detalle de los tipos de discapacidad del segundo formulario
     * @param array $datos request del gui
     * @return array status del registro de los detalles de discapacidad
     */
    function almacenaDetalleDiscapacidadModificacion($datos) {
        $persona_discapacidad = new Bl_personaDiscapacidad();
        $data02 = array();
        $rs = "";
        for ($i = 1; $i < 7; $i++) {
            if (isset($datos['tipo_discapacidad_' . $i])) {
                $data02['c_dis_cal_cedula'] = $datos['c_dis_cedula'];
                $data02['n_dis_cal_tipo_discapacidad'] = $i;
                $data02['c_dis_cal_cie10'] = $datos['cie10_' . $i];
                $data02['n_dis_cal_porcentaje_dis'] = $datos['porcentaje_discapacidad_' . $i];
                $data02['c_dis_cal_id_usuario'] = $_SESSION['usuario']['c_usuario'];
                $data02['n_dis_cal_id_unidad'] = $_SESSION['usuario']['c_id_externo_entidad'];
                $data02['c_dis_cal_estado'] = "A";
                if ($persona_discapacidad->crearDiscapacidadModificacion($data02)) {
                    $rs = array("error" => 0, "Mensaje" => "Detalle $i almacenado correctamente");
                } else {
                    return array("error" => 1, "Mensaje" => "ERROR al momento de almacenar detalle discapacidad $i");
                    exit;
                }
            }
        }

        return $rs;
    }

    /* Para los CIE10 */

    function cie10PorDescripcionOCodigoModificado($val) {
        $rs = $cie->cie10PorDescripcionOCodigo($descripcion, $condicion);
        foreach ($rs as $k => $vec) {
            $rs[$k]['c_descripcion'] = "[" . $vec['c_codigo'] . "] - " . $vec['c_descripcion'];
        }
        echo json_encode($rs);


        $condicion[] = "concat(trim(replace(replace(replace(replace(replace(lower(c_descripcion),'á','a'),'é','e'),'í','i' ),'ó','o'),'ú','u')), lower(c_codigo) ) like trim(replace(replace(replace(replace(replace(lower(lower('%$descripcion%')),'á','a'),'é','e'),'í','i' ),'ó','o'),'ú','u'))";
        $da = new Da_cie10();

        return $da->cie10($condicion);
    }

    /**
     * Valida si existe una persona, si existe, cambia los estados, si no existe
     * crea una nueva persona, adicional a esto almacena una primera parte del 
     * formulario principal
     * @param array $datos request del gui
     * @return array Status de transacción del primer formulario
     */
    function modificarRecalificacion($datos) {
        $persona = array();
        $persona['c_dis_cedula'] = (empty($datos['c_dis_cedula'])) ? "null" : "'" . $datos['c_dis_cedula'] . "'";
        $persona['n_dis_puntaje_ayuda_permanente'] = (empty($datos['n_dis_puntaje_ayuda_permanente'])) ? "null" : "'" . $datos['n_dis_puntaje_ayuda_permanente'] . "'";
        $persona['c_dis_result_ayuda_permanente'] = (empty($datos['c_dis_result_ayuda_permanente'])) ? "null" : "'" . $datos['c_dis_result_ayuda_permanente'] . "'";

        if ($datos['c_dis_deficiencia_predomina'] > 0) {
            $persona['c_dis_deficiencia_predomina'] = "'" . $datos['c_dis_deficiencia_predomina'] . "'";
        }

        if ($datos['c_dis_deficiencia_predomina2'] > 0) {
            $persona['c_dis_deficiencia_predomina'] = "'" . $datos['c_dis_deficiencia_predomina2'] . "'";
        }

        $persona['n_dis_puntaje_factor_social'] = (empty($datos['n_dis_puntaje_factor_social'])) ? "0" : "'" . $datos['n_dis_puntaje_factor_social'] . "'";
        $persona['n_dis_porcentaje_global_dis'] = (empty($datos['n_dis_porcentaje_global_dis'])) ? "null" : "'" . $datos['n_dis_porcentaje_global_dis'] . "'";
        $persona['n_dis_grado_discapacidad'] = (empty($datos['n_dis_grado_discapacidad'])) ? "null" : "'" . $datos['n_dis_grado_discapacidad'] . "'";

        $data01['dis_porcentaje_discapacidad'] = $datos['n_dis_porcentaje_global_dis'] + $datos['n_dis_puntaje_factor_social'];
        if ($data01['dis_porcentaje_discapacidad'] > 100) {
            $data01['dis_porcentaje_discapacidad'] = 100;
        }
        $persona['n_dis_porcentaje_discapacidad'] = (empty($data01['dis_porcentaje_discapacidad'])) ? "null" : "'" . $data01['dis_porcentaje_discapacidad'] . "'";

        $cont = 0;
        if (isset($datos['c_dis_terapia'])) {
            $totTerapia = count($datos['c_dis_terapia']);
            for ($i = 0; $i < $totTerapia; $i++) {
                if ($datos['c_dis_terapia'][$i] != ',') {
                    $data01['c_dis_terapia'][$cont] = $datos['c_dis_terapia'][$i];
                    $cont++;
                }
            }
            if (isset($data01['c_dis_terapia'])) {
                $data02['c_dis_terapia'] = implode(",", $data01['c_dis_terapia']);
                $persona['c_dis_terapia'] = (empty($data02['c_dis_terapia'])) ? "null" : "'" . $data02['c_dis_terapia'] . "'";
            }
        } else {
            $data01['c_dis_terapia'] = "0";
        }

        $cont2 = 0;
        if (isset($datos['c_dis_ayuda_tecnica'])) {
            $totTerapia2 = count($datos['c_dis_ayuda_tecnica']);
            for ($i = 0; $i < $totTerapia2; $i++) {
                if ($datos['c_dis_ayuda_tecnica'][$i] != ',') {
                    $data01['c_dis_ayuda_tecnica'][$cont2] = $datos['c_dis_ayuda_tecnica'][$i];
                    $cont2++;
                }
            }
            if (isset($data01['c_dis_ayuda_tecnica'])) {
                $data02['c_dis_ayuda_tecnica'] = implode(",", $data01['c_dis_ayuda_tecnica']);
                $persona['c_dis_ayuda_tecnica'] = (empty($data02['c_dis_ayuda_tecnica'])) ? "null" : "'" . $data02['c_dis_ayuda_tecnica'] . "'";
            }
        } else {
            $data01['c_dis_ayuda_tecnica'] = "0";
            $persona['c_dis_ayuda_tecnica'] = (empty($data01['c_dis_ayuda_tecnica'])) ? "null" : "'" . $data01['c_dis_ayuda_tecnica'] . "'";
        }
        $persona['c_dis_justificacionModifica '] = (empty($datos['c_dis_justificacionModifica'])) ? "null" : "'" . $datos['c_dis_justificacionModifica'] . "'";

        $persona['f_dis_fechahora_modificacion'] = "current_timestamp";

        $persona['c_dis_id_usuariomodifica'] = trim($_SESSION['usuario']['c_usuario'] . " ");

        $condicion = array();
        $condicion[] = "c_dis_cedula = '$datos[c_dis_cedula]' ";

        //Primero Modifico registro        
        $persona_discapacidad = new Da_personaDiscapacidad();

        if ($persona_discapacidad->modificaPersonaDiscapacitada($persona, $condicion)) {
            $rs = array("error" => 0, "Mensaje" => "Almacenamiento formulario OK");
            $da_recalificacion = new da_recalificacion();
            $bl_certificado = new bl_certificadoDiscapacidad();


            //Almacena los tipos y detalles de discapacidad   
            $data02 = array();
            $persona_discapacidad = new Bl_personaDiscapacidad();
            if (isset($datos['c_dis_cedula'])) {
                $data02['c_dis_cal_cedula'] = $datos['c_dis_cedula'];
                $modEst = $persona_discapacidad->modificaDiscapacidadEstado($data02);
                $rs = $this->almacenaDetalleDiscapacidadModificacion($datos);
            }

            //Discrimina si genera o no el certificado de acuerdo al porcentaje de discapacidad
            $rs_minimo_calificacion = $da_recalificacion->obtenerPorcentajeMinimo();
            if ($data01['dis_porcentaje_discapacidad'] >= $rs_minimo_calificacion[0]['c_valor']) {
                //Genera Certificado
                if ($bl_certificado->almacenaCertificadoModificado($persona)) {
                    $rs = array("error" => 0, "Mensaje" => "Certificado OK", "certificado" => 1);
                } else {
                    return array("error" => 1, "Mensaje" => "ERROR al crear el certificado");
                    exit;
                }
            } else {
                $rs = array("error" => 0, "Mensaje" => "Se almacena el registro, pero no genera certificado por el porcentaje", "certificado" => 0);
            }
        } else {
            return array("error" => 1, "Mensaje" => "Existió un error al momento de almacenar el fumulario JUK01");
            exit;
        }
        return $rs;
    }

    /**
     * Modifica la información de la tabla persona
     * @param array $datos request del gui
     * @return array Status de transacción del primer formulario
     */
    function modificarPersona($datos) {
        $persona = array();
        $persona['c_apellidos'] = strtoupper("'$datos[c_apellidos]'");
        $persona['c_nombres'] = strtoupper("'$datos[c_nombres]'");
        $persona['f_fecha_nacimiento'] = "'$datos[f_fecha_nacimiento]'";
        $persona['c_estado_civil'] = "'$datos[c_dis_id_estado_civil]'";
        $persona['c_sexo'] = "'$datos[c_sexo]'";

        $condicion = array();
        $condicion[] = "c_identificacion = '$datos[c_dis_cedula]' ";

        $persona_discapacidad = new Da_personaDiscapacidad();
        if ($persona_discapacidad->modificaPersona($persona, $condicion)) {
            return array("success" => true, "message" => "Datos almacenados correctamente");
        } else {
            return array("success" => false, "message" => "Existió un error al momento de almacenar la información");
        }
    }

    function all() {
        $da_reporte = new da_reporte();
        $datos = $da_reporte->all();

        echo "<table border='1'><tr><td>id</td><td>identificacion</td><td>conteo</td><td>empresa</td><td>barcode</td><td>estado</td><td>fullname</td><td>unidad</td><td>cod cc</td><td>centro de costo</td></tr>";
        $i = 0;

        foreach ($datos as $value) {
            $color = "";
            if ($value['ctstatus_id'] == 505) {
                $color = "#00ff00";
            } else if ($value['ctstatus_id'] == 506) {
                $color = "#3399ff";
            } else if ($value['ctstatus_id'] == 520) {
                $color = "#ff99ff";
            } else if ($value['ctstatus_id'] == 508) {
                $color = "#ff0000";
            } else if ($value['ctstatus_id'] == 521) {
                $color = "#9933ff";
            }

            echo"<tr style='background-color: $color;'><td>" . $value['id'] . "</td><td>" . $value['identification'] . "</td><td>" . $value['len'] . "</td><td>" . $value['businessname'] . "</td>"
            . "<td>" . $value['barcode'] . "</td><td>" . $value['estado'] . "</td><td>" . $value['fullname'] . "</td><td>" . $value['unit'] . "</td><td>" . $value['costcenter'] . "</td><td>" . $value['namecenter'] . "</td></tr>";
        }
        echo"</table>";
    }

    function allcd() {
        $da_reporte = new da_reporte();
        $datos = $da_reporte->allcd();

        echo "<table border='1'><tr><td>id</td><td>identificacion</td><td>conteo</td><td>empresa</td><td>barcode</td><td>estado</td><td>fullname</td><td>unidad</td><td>cod cc</td><td>centro de costo</td></tr>";
        $i = 0;

        foreach ($datos as $value) {
            $color = "";
            if ($value['ctstatus_id'] == 505) {
                $color = "#00ff00";
            } else if ($value['ctstatus_id'] == 506) {
                $color = "#3399ff";
            } else if ($value['ctstatus_id'] == 520) {
                $color = "#ff99ff";
            } else if ($value['ctstatus_id'] == 508) {
                $color = "#ff0000";
            } else if ($value['ctstatus_id'] == 521) {
                $color = "#9933ff";
            }

            echo"<tr style='background-color: $color;'><td>" . $value['id'] . "</td><td>" . $value['identification'] . "</td><td>" . $value['len'] . "</td><td>" . $value['businessname'] . "</td>"
            . "<td>" . $value['barcode'] . "</td><td>" . $value['estado'] . "</td><td>" . $value['fullname'] . "</td><td>" . $value['unit'] . "</td><td>" . $value['costcenter'] . "</td><td>" . $value['namecenter'] . "</td></tr>";
        }
        echo"</table>";
    }

}

?>
