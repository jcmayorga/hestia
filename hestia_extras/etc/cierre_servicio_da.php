<?php

class da_cierre_servicio {

    public function __construct() {
        include_once dirname(__FILE__) . '/conf.php';
        include_once BD;
    }

    function getDinnerUser($user) {
        $sql = "select  a.id, a.name from administration.dinner a, administration.dinneruser b
                where a.id=b.dinner_id
                and b.ctstatus_id=500
                and user_id=$user";
        $sql.="order by name";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function getServiceDinner($condicion = array()) {
        $sql = "select c.acronym as acron, c.description from business.transaction a, administration.companydinner b, business.service c
                where a.companydinner_id=b.id
                and a.service_id=c.id ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql.="group by c.acronym, c.description "
                . "order by c.description";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function getCompanyDinner($dinner) {
        $sql = "select b.id, b.businessname
                from administration.companydinner a, administration.company b
                where a.company_id=b.id
                and a.dinner_id=$dinner
		and a.ctstatus_id=500
                order by b.businessname";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function getCompany() {
        $sql = "select b.id, b.businessname
                from administration.company b
                order by b.businessname";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function getCountCloseDinnerService($condicion) {
        $sql = "select count(*) 
                from business.close 
                where 1=1 ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function createCloseDinnerService($datos) {
        $bd = new PGSQL();
        $bd->prepara("INSERT", "business.close", $datos, '', "id");
        $sql = $bd->sql();
        return $bd->query($sql);
    }

    function creaFirmasTMP($datos) {
        $bd = new PGSQL();
        $bd->prepara("INSERT", "business.closedetail", $datos, '', "id");
        $sql = $bd->sql();
        return $bd->query($sql);
    }
    function creaTicketFirmaRoster($datos) {
        $bd = new PGSQL();
        $bd->prepara("INSERT", "business.transaction", $datos, '', "id");
        $sql = $bd->sql();
        return $bd->query($sql);
    }
    function actualizaFirmasTMP($datos, $condicion) {
        $bd = new PGSQL();
        $bd->prepara("UPDATE", "business.closedetail", $datos, $condicion);
        $sql = $bd->sql();
        return $bd->query($sql);
    }
    function actualizaCloseDinnerService($datos,$condicion) {
        $bd = new PGSQL();
        $bd->prepara("UPDATE", "business.close", $datos, $condicion);
        $sql = $bd->sql();
        return $bd->query($sql);
    }

    function getMaxFirmaTMPClose($close) {
        $sql = "select max(id) as id
                from business.closedetail
                where close_id=$close";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function getFirmaTMPCloseID($id) {
        $sql = "select a.id, a.close_id, a.identification, a.fullname, a.costcentername, a.company_id, 
                a.service_id, a.roster_id, a.ctstatus_id, a.created_at, a.updated_at, a.usucrud_id, b.name as estado
                from business.closedetail a, application.catalog b
                where a.id=$id"
                . " and a.ctstatus_id=b.id";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function getRosterID($id) {
        $sql = "SELECT id, identification, ctidentification_id, firstname, lastname, 
                fullname, email, company_id, employeecode, barcode, rfidcode, 
                biometriccode, ctstatus_id, created_at, updated_at, usucrud_id, 
                unit, costcenter, namecenter
                FROM administration.roster
                where identification='$id'";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function getIdCloseDinnerService($condicion) {
        $sql = "select *
                from business.close 
                where 1=1 ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function getDetailClose($padre) {
        $sql = "select a.id, a.close_id, a.identification, a.fullname, a.costcentername, a.company_id, 
                a.service_id, a.roster_id, a.ctstatus_id, a.created_at, a.updated_at, a.usucrud_id, b.name as estado
                from business.closedetail a, application.catalog b
                where close_id=" . $padre . " "
                . " and a.ctstatus_id=b.id order by id";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function eliminaFirmasporIdTMP($condicion) {
        $bd = new PGSQL();
        $bd->prepara("DELETE", "business.closedetail", '', $condicion);
        $sql = $bd->sql();
        return $bd->query($sql);
    }

    function produccionDinner($condicion = array()) {
        $sql = "select c.name as Comedor, count(*) as Despachado, d.description as Servicio, e.businessname as Empresa from business.transaction a, administration.companydinner b, administration.dinner c, business.service d, administration.company e
                where a.companydinner_id=b.id
                and b.company_id=e.id
                and b.dinner_id=c.id
                and a.service_id=d.id ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "group by c.name, d.description, e.businessname
                order by c.name, d.description,  e.businessname";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function juk($condicion = array()) {
        $sql = "select a.id, d.businessname, c.unit, c.costcenter, c.namecenter,  c.identification, g.name, c.employeecode, c.firstname, c.lastname, c.fullname, f.name, i.name as dinner, i.location, i.address, b.description, b.acronym, a.dateregistry, a.timeregistry
                from business.transaction a, business.service b, administration.roster c, administration.company d, 
                application.catalog f, application.catalog g, administration.companydinner h, administration.dinner i
                where a.service_id=b.id
                and a.roster_id=c.id
                and c.company_id=d.id
                and a.cttransaction_id=f.id
                and c.ctidentification_id=g.id
                and a.companydinner_id=h.id
                and h.dinner_id=i.id
                and a.ctstatus_id in (513, 514) ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "order by a.dateregistry, a.timeregistry";
        $bd = new PGSQL();

//        echo $sql; die;
        return $bd->query($sql);
    }

    function jukDetalle($condicion = array()) {
        $sql = "select c.id, d.businessname, c.unit, c.costcenter, c.namecenter,  c.identification, g.name, c.employeecode, c.firstname, c.lastname, c.fullname, f.name, i.name as dinner, i.location, i.address, b.description, b.acronym, a.dateregistry, a.timeregistry
                from business.transaction a, business.service b, administration.roster c, administration.company d, 
                application.catalog f, application.catalog g, administration.companydinner h, administration.dinner i
                where a.service_id=b.id
                and a.roster_id=c.id
                and c.company_id=d.id
                and a.cttransaction_id=f.id
                and c.ctidentification_id=g.id
                and a.companydinner_id=h.id
                and h.dinner_id=i.id
                and a.ctstatus_id in (513, 514) ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "order by a.dateregistry, a.timeregistry";
        $bd = new PGSQL();

//        echo $sql; die;
        return $bd->query($sql);
    }

    function jukDisctinct($condicion = array()) {
        $sql = "select distinct c.id, d.businessname, c.unit, c.costcenter, c.namecenter,  c.identification, c.employeecode, c.firstname, c.lastname, c.fullname
                from business.transaction a, business.service b, administration.roster c, administration.company d, 
                application.catalog f, application.catalog g, administration.companydinner h, administration.dinner i
                where a.service_id=b.id
                and a.roster_id=c.id
                and c.company_id=d.id
                and a.cttransaction_id=f.id
                and c.ctidentification_id=g.id
                and a.companydinner_id=h.id
                and h.dinner_id=i.id
                and a.ctstatus_id in (513, 514) ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "order by c.fullname, c.costcenter, c.namecenter ";
        //   echo $sql;
        $bd = new PGSQL();

//        echo $sql; die;
        return $bd->query($sql);
    }

    function getService($cond) {
        $sql = "select description from business.service
            where acronym='$cond'
            limit  1";

        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function getFullService($condicion) {
        $sql = "select id, description, company_id, created_at, updated_at, usucrud_id, 
                hoursince, houruntil, ctstatus_id, acronym
                from business.service
                     where 1=1 ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        //pre_r($sql); die;
        $bd = new PGSQL();
        return $bd->query($sql);
    }
    
        function getFullCompanyDinner($condicion) {
        $sql = "select * from administration.companydinner 
                where 1=1 ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function jukCount($condicion = array()) {
        $sql = "select distinct c.id, count(*) as total
                from business.transaction a, business.service b, administration.roster c, administration.company d, 
                application.catalog f, application.catalog g, administration.companydinner h, administration.dinner i
                where a.service_id=b.id
                and a.roster_id=c.id
                and c.company_id=d.id
                and a.cttransaction_id=f.id
                and c.ctidentification_id=g.id
                and a.companydinner_id=h.id
                and h.dinner_id=i.id
                and a.ctstatus_id in (513, 514) ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "group by c.id";
        $bd = new PGSQL();

//        echo $sql; die;
        return $bd->query($sql);
    }

    function catalogoEmpresa() {
        $sql = "select id as value, businessname as label from administration.company where id in (select distinct (company_id) from business.transaction a, administration.companydinner b
                where a.companydinner_id=b.id)
                order by label";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function getEmpresa($condicion = array()) {
        $sql = "select id as value, businessname as label from administration.company where 1=1";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql.="order by label";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function all() {
        $sql = "select a.ctstatus_id, a.id, identification, length(identification) as len, barcode, b.name as estado, businessname, firstname, lastname, fullname, company_id, barcode, unit, costcenter, namecenter 
                from administration.roster a,  application.catalog b, administration.company c
                where a.ctstatus_id=b.id
                and c.id=6
                and a.company_id=c.id
                order by company_id";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function allcd() {
        $sql = "select a.ctstatus_id, a.id, identification, length(identification) as len, barcode, b.name as estado, businessname, firstname, lastname, fullname, company_id, barcode, unit, costcenter, namecenter 
                from administration.roster a,  application.catalog b, administration.company c
                where a.ctstatus_id=b.id
                and c.id=12
                and a.company_id=c.id
                order by company_id";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function catalogoEmpresaCont($comp) {
        $sql = "select id as value, businessname as label from administration.company
where contract_id=$comp
order by 2";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function catalogoContrato() {
        $sql = "(select 0 as value, '[Seleccione ...]' as label ) 
            union
            (select id as value, name as label from administration.contract)
                order by 1  ";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function obtenerPorcentajeMinimo() {
        $sql = "select                    
                    c_valor
                 from sistema.tb_discapacidades_catalogo
                 WHERE 
                 c_estado='A' 
                 and c_catalogo='DIS-MINIMO-CALIFICACION'";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

}

?>
