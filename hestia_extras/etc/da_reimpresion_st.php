<?php

class da_reimpresion_st {

    public function __construct() {
        include_once dirname(__FILE__) . '/conf.php';
        include_once BD;
    }

    function revisa($ident) {
        $sql = "select count(*), b.name, a.ctstatus_id, a.id from administration.roster a, application.catalog b
                where identification='$ident'
                and a.ctstatus_id=b.id
                group by b.name, a.ctstatus_id, a.id";

        $bd = new PGSQL();
        return $bd->query($sql);
    }

  function crearPadreSticker($datos) {
        $bd = new PGSQL();
        $bd->prepara("INSERT", "business.sticker", $datos, '', 'id');
         $sql= $bd->sql();
         return $bd->query($sql);
    }
    function ultimoPadreSticker() {
        $sql = "select max(id) as id from business.sticker";
        $bd = new PGSQL();
        return $bd->query($sql);
    }
    
    
  function crearGrupoStickerRoster($datos) {
        $bd = new PGSQL();
        $bd->prepara("INSERT", "business.stickerroster", $datos, '', 'id');
         $sql= $bd->sql();
         return $bd->query($sql);
    }

    function countGrupoSticker($padre) {
        $sql = "select count(*) as conteo from business.stickerroster where sticker_id=$padre group by sticker_id";
        $bd = new PGSQL();
        return $bd->query($sql);
    }
    
    function jukDisctinct($condicion = array()) {
        $sql = "select distinct c.id, d.businessname, c.unit, c.costcenter, c.namecenter,  c.identification, c.employeecode, c.firstname, c.lastname, c.fullname
                from business.transaction a, business.service b, administration.roster c, administration.company d, 
                application.catalog f, application.catalog g, administration.companydinner h, administration.dinner i
                where a.service_id=b.id
                and a.roster_id=c.id
                and c.company_id=d.id
                and a.cttransaction_id=f.id
                and c.ctidentification_id=g.id
                and a.companydinner_id=h.id
                and h.dinner_id=i.id
                and a.ctstatus_id in (513, 514) ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "order by c.costcenter, c.namecenter, c.fullname";
        //   echo $sql;
        $bd = new PGSQL();

//        echo $sql; die;
        return $bd->query($sql);
    }

    function getService($cond) {
        $sql = "select description from business.service
            where acronym='$cond'
            limit  1";

        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function jukCount($condicion = array()) {
        $sql = "select distinct c.id, count(*) as total
                from business.transaction a, business.service b, administration.roster c, administration.company d, 
                application.catalog f, application.catalog g, administration.companydinner h, administration.dinner i
                where a.service_id=b.id
                and a.roster_id=c.id
                and c.company_id=d.id
                and a.cttransaction_id=f.id
                and c.ctidentification_id=g.id
                and a.companydinner_id=h.id
                and h.dinner_id=i.id
                and a.ctstatus_id in (513, 514) ";
        if (count($condicion) > 0)
            $sql .= implode(' AND ', $condicion);
        $sql .= "group by c.id";
        $bd = new PGSQL();

//        echo $sql; die;
        return $bd->query($sql);
    }

    function catalogoEmpresa() {
        $sql = "select id as value, businessname as label from administration.company where id in (select distinct (company_id) from business.transaction a, administration.companydinner b
                where a.companydinner_id=b.id)
                order by label";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function all() {
        $sql = "select a.ctstatus_id, a.id, identification, length(identification) as len, barcode, b.name as estado, businessname, firstname, lastname, fullname, company_id, barcode, unit, costcenter, namecenter 
                from administration.roster a,  application.catalog b, administration.company c
                where a.ctstatus_id=b.id
                and c.id=6
                and a.company_id=c.id
                order by company_id";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function allcd() {
        $sql = "select a.ctstatus_id, a.id, identification, length(identification) as len, barcode, b.name as estado, businessname, firstname, lastname, fullname, company_id, barcode, unit, costcenter, namecenter 
                from administration.roster a,  application.catalog b, administration.company c
                where a.ctstatus_id=b.id
                and c.id=12
                and a.company_id=c.id
                order by company_id";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function catalogoEmpresaCont($comp) {
        $sql = "select id as value, businessname as label from administration.company
where contract_id=$comp
order by 2";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function catalogoContrato() {
        $sql = "(select 0 as value, '[Seleccione ...]' as label ) 
            union
            (select id as value, name as label from administration.contract)
                order by 1  ";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

    function obtenerPorcentajeMinimo() {
        $sql = "select                    
                    c_valor
                 from sistema.tb_discapacidades_catalogo
                 WHERE 
                 c_estado='A' 
                 and c_catalogo='DIS-MINIMO-CALIFICACION'";
        $bd = new PGSQL();
        return $bd->query($sql);
    }

}

?>