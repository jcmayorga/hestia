<?php

class PGSQL {

    /**
     * Identifica al tipo de conexión
     * @var int
     * 
     *  0= Oracle
     *  1= Postgresql
     * 
     */
    private $tipo_db;




// =======================
// POSTGRESQL
// =======================    

    /**
     * ip o nombre del servidor de base de datos
     * @var string 
     */
    private $host_1 = 'localhost';

    /**
     * nombre de la base de datos
     * @var string 
     */
    private $bd_1 = 'hestia';

    /**
     * usuario de la base de datos
     * @var string 
     */
    private $usuario_1 = 'postgres';

    /**
     * clave del usuario de la base de datos
     * @var string 
     */
    private $clave_1 = 'qws12345';

    /**
     * cadena sql creada según la petición del usuario
     * @var type 
     */
    private $sql = '';

    /**
     * almacena la tabla sobre la que se va ha trabajar
     * @var string 
     */
    private $tabla;

    /**
     * almacena el nombre del campo id de la tabla
     * @var type 
     */
    private $id_nombre;

    /**
     * almacena la acción en caso de ser insert
     * @type string 
     */
    private $accion = "";

    /**
     * Objeto pdo que contiene la conexión a la base de datos
     * @var objeto 
     */
    public $conexion = null;

    /**
     * Objeto  que contiene el valor si la conexion se encuentra en transaccion
     * @var objeto 
     */
    public $en_transaccion = false;

    /**
     * Objeto  que contiene el número de transacciones realizadas
     *  0 = Oracle
     *  1 = Postgresql
     */
    function __construct($tipo = 1) {
        include_once dirname(__FILE__) . '/conf.php';
        include_once dirname(__FILE__) . '/PDOT.php';
        $this->tipo_db = $tipo;
        $this->conexion = $this->conexion();
    }

    /**
     * Retorna el objeto de conexión con la base de datos
     * @return object
     */
    function getConexion() {
        return $this->conexion;
    }

    /**
     * Inicial la transacción
     */
    function iniciarTransaccion() {
//Postgresql            
            if ($this->conexion->beginTransaction()) {
                return array("success" => true, "message" => "Transacción iniciada correctamente");
            } else {
                $this->errorInfo();
                return array("success" => false, "message" => "La transacción no se inició correctamente");
            }
    }

    /**
     * Finaliza la transacción, si el $success es true realiza un commint caso contrario hace un rollback
     * @param boolean $success
     */
    function finalizarTransaccion($success = false) {
        if (!is_null($this->conexion)) {
//Postgresql                
                if ($this->conexion->inTransaction()) {
                    if ($success) {
                        if ($this->conexion->commit())
                            return array("success" => true, "message" => "Commit correcto");
                        else {
                            $this->errorInfo();
                            return array("success" => false, "message" => "No se realizó el commit");
                        }
                    } else {
                        if ($this->conexion->rollBack())
                            return array("success" => true, "message" => "Rollback correcto");
                        else {
                            $this->errorInfo();
                            return array("success" => false, "message" => "No se realizó el rollback");
                        }
                    }
                } else {
                    return array("success" => false, "message" => "No existe una transacción iniciada");
                }
        } else {
            return array("success" => false, "message" => "No existe una conexión disponible");
        }
    }

    /**
     * Crea y retorna el objeto PDO con la conexión
     * 0 = Oracle
     * 1 = Postgresql
     * */
    function conexion() {
//Posgresql
            try {
                return new PDOT("pgsql:host=" . $this->host_1 . ";dbname=" . $this->bd_1, $this->usuario_1, $this->clave_1);
            } catch (PDOException $e) {
                echo $e->getMessage();
                echo 'No hay conexión con la Base de Datos ';
                if (isset($_SERVER['SERVER_ADDR']))
                    echo $e->getMessage();
                if (($_SERVER['SERVER_ADDR'] == AMBIENTE_DESARROLLO_IP) || ($_SERVER['SERVER_ADDR'] == AMBIENTE_PRUEBAS_IP))
                    echo $e->getMessage();
                exit();
            }
    }

    /**
     * prepara una sentencia sql según la petición del usuario y asigna a la variable $sql
     * @param string $accion INSERT, UPDATE o DELETE 
     * @param string $tabla nombre de la tabla
     * @param array $datos array asociativo de los campos
     * @param array $condicion para UPDATE y DELETE array con las condiciones necesarias
     * @param string $id_nombre es el nombre del campo id de la tabla, en caso que desee obtener el id luego de la inserción
     */
    function prepara($accion, $tabla, $datos, $condicion = '', $id_nombre = "") {
        $accion = strtoupper($accion);
        switch ($accion) {
            case 'INSERT':
                $this->tabla = $tabla;
                $this->id_nombre = $id_nombre;
                $this->accion = $accion;
                $this->sql = $this->insert($tabla, $datos);
                //print_r( $this->sql = $this->insert($tabla, $datos) );
                break;
            case 'UPDATE':
                $this->accion = $accion;
                $this->sql = $this->update($tabla, $datos, $condicion);
                break;
            case 'DELETE':
                $this->accion = $accion;
                $this->sql = $this->delete($tabla, $condicion);
                break;
            default :
                exit;
                break;
        };
    }

    /**
     * prepara la sentencia para realizar un insert en la BD
     * @param string $tabla nombre de la tabla
     * @param array $datos array asociativo con los campos necesarios para la inserción 
     */
    function insert($tabla, $datos) {
        $campos = '';
        $valores = '';
        foreach ($datos as $campo => $valor) {
            $campos .= $campo . ',';
            $valores .= $valor . ',';
        }
        $campos = substr($campos, 0, (strlen($campos) - 1));
        $valores = substr($valores, 0, (strlen($valores) - 1));
        $sql = "INSERT INTO $tabla ($campos) VALUES($valores)";
        return $sql;
    }

    /**
     * prepara la sentencia para realizar un update en la BD
     * @param string $tabla nombre de la tabla
     * @param array $datos array asociativo de datos a actualizar
     * @param array $condicion array asociativo con las condiciones
     */
    function update($tabla, $datos, $condicion) {
        $sql = "UPDATE  " . $tabla . " SET ";
        foreach ($datos as $campo => $valor)
            $sql .= $campo . "=" . $valor . ",";
        $sql = substr($sql, 0, (strlen($sql) - 1));
        $sql .= " WHERE " . implode(" AND ", $condicion);
        return $sql;
    }

    /**
     * prepara la sentencia para realizar un delete en la BD
     * @param string
      function delete($ $tabla nombre de la tabla
     * @param array $condicion array asociativo con las condiciones para realizar el delete 
     */
    function delete($tabla, $condicion) {
        $sql = "DELETE FROM $tabla WHERE " . implode(' AND ', $condicion);
        return $sql;
    }

    /**
     * ejecuta una sentencia sql de INSERT, UPDATE o DELETE
     * @return boolean 
     */
    function exec($parametros = null) {
//Postgresql            
            $transaccionInterna = false;
            if (!$this->conexion->inTransaction()) {
                $transaccionInterna = $this->conexion->beginTransaction();
            }

            $stm = $this->conexion->prepare($this->sql);
            $id = true;
            if ($stm->execute()) {
                if ($this->accion == "INSERT" && $this->id_nombre != '') {
                    $sql = "select max($this->id_nombre) from $this->tabla ";
                    $stm1 = $this->conexion->prepare($sql);
                    $stm1->execute();
                    $tmp = $stm1->fetchAll(PDO::FETCH_ASSOC);
                    $id = $tmp[0]['max'];
                }
                if ($transaccionInterna) {
                    $this->conexion->commit();
                }
            } else {
                $this->errorInfo();
                $this->conexion->rollBack();
                $id = false;
            }
            if ($transaccionInterna) {
                $conexion = $this->conexion;
                unset($conexion);
            }
        return $id;
    }

    /**
     * ejectuta una sentencia slq de select
     * @param string $sql sentencia sql a ser ejecutado
     * @return array de arrays
     */
    function query($sql, $parametros = null) {
//postgresql            
            $this->sql = $sql;
            $stm = $this->conexion->prepare($sql);
            if ($stm->execute())
                return $stm->fetchAll(PDO::FETCH_ASSOC);
            else
                $this->errorInfo();
            return array();
    }

    /**
     * retorna la sentencia sql creada 
     */
    function sql() {

        return $this->sql;
    }

    function errorInfo($msg = '') {
        $str_mensaje_error = "";
//Postgresql                
            $error = $this->conexion->errorInfo();
            if (!empty($error[2])) {
                $error_bd = pre_r($error) . "<b>{$this->sql}</b>";
            }
            $error_php = error_get_last();
        $msg_app = "<b>ERROR APP:</b>" . $msg;
        $msg_bd = ($error_bd['message'] == "") ? "" : "<b>ERROR BD:</b>" . $error_bd['message'] . " <b>SQL DE ERROR:</b>" . $error_bd['sqltext'];
        $msg_php = ($error_php['message'] == "") ? "" : "<b>ERROR PHP:</b>" . $error_php['message'] . " <b>SQL PHP:</b>" . $this->sql;

        $str_mensaje_error = $msg_app . " " . $msg_bd . " " . $msg_php;
        pre_r($str_mensaje_error);
    }

    private function agregarLog($operacion, $error, $consulta) {
        $log = date('Y-m-d H:i:s') . " | " . $operacion . " | " . $error . " | " . $consulta;
        error_log($log);
    }

    public function getErrorInfo() {
        return error_get_last();
    }

}

?>
