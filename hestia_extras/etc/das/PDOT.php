<?php
/**
 * Clase PGSQL transaccional
 * 
 * @author ROSA CAMPANA
 * @version 1.0
 * @package das 
 */

class PDOT extends PDO{

    /**
    * El nivel de transacción actual
    */
    protected $transLevel = 0;
    
   /* protected function nestable() {          
        return in_array($this->getAttribute(PDO::ATTR_DRIVER_NAME), array("odbc"));
    }*/

    public function beginTransaction() {
        $error=array();
        /*
        if( $this->transLevel == 0 ) {            
            printf('\nBEGIN \n');
            $rs=parent::beginTransaction();                       
            $error = parent::errorInfo();
        } else {           
            printf('\nSAVEPOINT "'.$this->transLevel.'" \n');
            $rs=$this->exec('SAVEPOINT "'.$this->transLevel.'"');                        
            $error = $this->errorInfo();
        }*/
        if(!parent::inTransaction()){
            $rs=parent::beginTransaction();                       
            $error = parent::errorInfo();           
        }else{
            return false;
        }
                   
       
        if($error[0]=='00000'){
           // printf("\n+nivel");             
            // $this->transLevel++;
            return true;
        }else{             
             return false;
        }
    }
    
    public function endTransaction(){
        printf("\n entrando end \n");
        var_dump(parent::inTransaction());
           try{
                $this->commit();   
                return true;                
            }catch(Exception $e){
                return false;               
            }        
    }    
    
    public function commit() {
        try{
            //$this->transLevel--;    
            //printf("\n-nivel\n");
            $rs= parent::commit();
            $error = parent::errorInfo();
//            printf("\ncommit en  nivel".$this->transLevel."\n" );
//            print_r($error);
            if($error[0]=='00000'){                
                 return true;                 
            }else{
                throw new Exception("Error en commit");                 
            }            
        }catch (Exception $e) {   
            $rs= parent::rollBack();   
           // $this->exec('ROLLBACK TO SAVEPOINT "'.$this->transLevel.'"');            
//            printf('ROLLBACK TO SAVEPOINT "'.$this->transLevel.'" \n');                                                
//            printf("\n+nivel");
//            $this->transLevel++;
            return false;
        }
    }
    
    public function rollBack() {
        try{
            $this->transLevel--;
            printf("\n-nivel");
             $rs= parent::rollBack();   
            $error = parent::errorInfo();
//            $rs= $this->exec('ROLLBACK TO SAVEPOINT "'.$this->transLevel.'"');
//            printf( 'ROLLBACK TO SAVEPOINT "'.$this->transLevel.'" \n' );
//            print_r($error);
            if($error[0]=='00000'){                
                 return true;                 
            }else{
                throw new Exception("Error en commit");                 
            }  
        }catch(Exception $e){
//            printf("\n+nivel");
//            $this->transLevel++;
            return false;
        }
        
    }
} 