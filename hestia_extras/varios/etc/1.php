<?php
/**
 * Interfaz para la gestión de roles
 * 
 * @author Manuel Haro
 * @version 1.0
 * @package roles 
 * 
 * <code>
 * @include conf.php
 * @include bl_rol.php
 * @include ACCESO
 * </code>
 */
ini_set('default_charset', 'UTF-8');
include_once dirname(dirname(__FILE__)) . '/conf.php';
include_once dirname(dirname(__FILE__)) . '/bl/bl_rol.php';
include_once dirname(dirname(dirname(__FILE__))) . '/recursos/bl/bl_recurso.php';
include_once ACCESO;

ACCESO::seguridad('ADM-ROL', $_REQUEST);


$accion = '';
if (isset($_REQUEST))
    $accion = $_REQUEST['accion'];

switch ($accion) {
    case 'Buscar':
        $rol = new BL_Rol();
        $rows = $rol->rol($_REQUEST);
        $cantidad = $rol->cantidadRol($_REQUEST);
        $roles = array('total' => $cantidad, 'rows' => $rows);
        echo json_encode($roles);
        exit();
        break;
    case 'ArbolRecursos':
        $c_recurso = new BL_Recurso();
        
        echo json_encode(array($c_recurso->arbolRecursos(0)));
        exit();
        break;
    case 'Actualizar':
        $rol = new BL_Rol();
        $resultado = array();
        if ($rol->actualizar($_REQUEST)) {
            $resultado["message"] = "Actualización correcta";
            $resultado["success"] = true;
        } else {
            $resultado["message"] = "Error, no se actualizó";
            $resultado["success"] = false;
        }
        echo json_encode($resultado);
        exit();
        break;
    case 'Insertar':
        $rol = new BL_Rol();
        if ($rol->crear($_REQUEST)) {
            $resultado["message"] = "Creación correcta";
            $resultado["success"] = true;
        } else {
            $resultado["message"] = "Error, no se creó";
            $resultado["success"] = false;
        }
        echo json_encode($resultado);
        exit();
        break;
    case 'Eliminar':
        $rol = new BL_Rol();
        if ($rol->eliminar($_REQUEST)) {
            $resultado["message"] = "Eliminación correcta";
            $resultado["success"] = true;
        } else {
            $resultado["message"] = "Error, no se eliminó";
            $resultado["success"] = false;
        }
        echo json_encode($resultado);
        exit();
        break;
    case 'Visualizar':
        //carga el código html
        break;
    default :
        exit();
        break;
}
?>

<table id="dgRoles"></table>





<script type="text/javascript">
    
    var pathRol = "administracion/roles/gui/rol.php";
    
    function buscarRoles(){
        $('#dgRoles').datagrid('reload',{'accion':'Buscar','txtbuscar':$('#txtbuscar').searchbox('getValue') });
    }
    function crearRol(){
        $('#dlgRol').dialog('open').dialog('setTitle','Nuevo rol');
        $('#frmRol').form('clear');
        $('#frmRol #accion').val('Insertar');
    }
    
    function editarRol(row){
        if (row){
            $('#dlgRol').dialog('open').dialog('setTitle','Editar rol');
            $('#frmRol').form('clear');
            $('#frmRol').form('load',row);
            $('#frmRol #accion').val('Actualizar');
        }
        else{
            $.messager.show({
                title:"Atención",
                msg:"Seleccione un registro"
            })
        }
    }
    
    
    function eliminarRol(row){
        if (row){
            $.messager.confirm('Confirmación','¿Está seguro que desea eliminar este rol?',function(r){  
                if(r){
                    $.post(pathRol, 
                    {accion:'Eliminar',n_id_roll:row.n_id_roll}, 
                    function(result){
                        if(result.success)
                            $('#dgRoles').datagrid('reload');
                        $.messager.show({
                            title: 'Atención',
                            msg: result.message
                        }); 
                    }, 
                    'json'
                );
                }
            });
        }
        else{
            $.messager.show({
                title:"Atención",
                msg:"Seleccione un registro"
            })
        }
        return;
    }
    
    
    function guardarRol(){
        $("#frmRol").form('submit',{
            url: pathRol,
            onSubmit: function(){
                return $(this).form('validate');
            },
            success: function(result){
                controlSesion(result);
                var result = eval('('+result+')');
                if (result.success){
                    $("#dlgRol").dialog('close');
                    $('#dgRoles').datagrid('reload');
                } 
                $.messager.show({
                    title: 'Atención',
                    msg: result.message
                });
            }
        });  
    }
    
        
    $(function(){
        //grid de roles
        $('#dgRoles').datagrid({
            singleSelect:true,
            pagination:true,
            rownumbers:true,
            fitColumns:true,
            fit:true,
            pageList:[5,10,15,20,50,100],
            pageSize:15,
            url:pathRol+"?accion=Buscar",       
            title:"Roles", 
            toolbar:'#tb',
            nowrap:false,
            frozenColumns:[[  
                    {field:'c_nombre',title:'Nombre',width:200},  
                    {field:'c_descripcion',title:'Descripción',width:200} 
                ]],
            columns:[[  
                    {field:'c_recurso',title:'Recursos'}, 
                    {field:'c_estado',title:'Estado'}, 
                ]]
        })
        
        //árbol de recursos
        $('#frmRol #recursos').combotree({  
            url:pathRol+'?accion=ArbolRecursos',
            multiple:true,
            animate:true,
            separator:',',
            editable:false,
            checkbox:true,
            onlyLeafCheck:true,
            panelWidth:400,
            onCheck:function(node, checked){
                if(checked){
                    $('#frmRol #c_recurso').val($('#frmRol #c_recurso').val()+node.attributes.c_cod_recurso+';');
                }
                else{
                    var tmp = $('#frmRol #c_recurso').val();
                    var c_recurso = tmp.replace(node.attributes.c_cod_recurso+';','');
                    $('#frmRol #c_recurso').val(c_recurso);
                }
            }
        });
    
        //Dialogo rol
        $("#dlgRol").dialog({
            width: 400, 
            resizable:false,
            height: 300,  
            modal: true,
            closed:true,
            buttons:[{
                    text:'Guardar',
                    iconCls: 'icon-save',
                    handler:function(){
                        guardarRol();
                    }
                },
                {
                    text:'Cancelar',
                    iconCls: 'icon-cancel',
                    handler:function(){
                        $('#dlgRol').dialog('close');
                    }
                }
            ],
            onOpen:function(){
                $.parser.parse('#frmRol');
            }
        });
        
        mayusculas();
        trim();
        
        $.parser.parse('#content');
        
    })
</script>
