<?php

/**
 * Clase Bl_Rol
 * 
 * Gestiona los roles, básicamente crea, actualiza y elimina los roles
 * 
 * 
 * @autor creación:Manuel Haro
 * @package roles
 * @version 1.0
 * 
 * <code>
 * @include da_rol.php
 * </code>
 *  
 */
class BL_Rol {

    function __construct() {
        include_once '../da/da_rol.php';
    }

    /**
     * crear($post)
     * Crea un rol
     * 
     * @param array $post
     * @return boolean
     */
    function crear($post) {
        $datos = array();

        $datos['c_nombre'] = "'$post[c_nombre]'";
        $datos['c_descripcion'] = "'$post[c_descripcion]'";
        $datos['c_recurso'] = "'$post[c_recurso]'";
        $datos['c_estado'] = "'$post[c_estado]'";

        $da = new Da_Rol();
        return $da->crear($datos);
    }

    /**
     * actualizar($post)
     * Actualiza un rol
     * 
     * @param array $post
     * @return boolean
     */
    function actualizar($post) {
        $datos = array();

        $datos['c_nombre'] = "'$post[c_nombre]'";
        $datos['c_descripcion'] = "'$post[c_descripcion]'";
        $datos['c_recurso'] = "'$post[c_recurso]'";
        $datos['c_estado'] = "'$post[c_estado]'";

        $condicion = array();
        $condicion[] = " n_id_roll = $post[n_id_roll] ";

        $da = new Da_Rol();
        return $da->actualizar($datos, $condicion);
    }

    /**
     * eliminar($post)
     * Actializa el c_estado de un rol a 'I'= inactivo
     * 
     * @param array $post
     * @return boolean
     */
    function eliminar($post) {
        $datos = array();
        $datos['c_estado'] = "'I'";

        $condicion = array();
        $condicion[] = " n_id_roll = $post[n_id_roll] ";

        $da = new Da_Rol();
        return $da->actualizar($datos, $condicion);
    }

    /**
     * rol($post)
     * Recupera los roles activos
     *  
     * @param array $post
     * @return array
     */
    function rol($post) {
        $condicion = $this->obtenerCondicionRol($post);
        $pageSize = '';
        if (!empty($post['page'])){
            if($post['page']>1){
                $pageSize = (($post['page'] - 1) * $post['rows'])+1;
            }else{
                $pageSize =1;
            }
            
        }
            
        $pageNumber = '';
        if (!empty($post['rows']))
            $pageNumber =  $pageSize + ($post['rows']-1)  ;

        $da = new Da_Rol();
        return $da->rol($condicion, $pageSize, $pageNumber);
    }

    /**
     * cantidadRol($post)
     * Recupera la  cantidad de roles activos
     *  
     * @param array $post
     * @return array
     */
    function cantidadRol($post) {
        $condicion = $this->obtenerCondicionRol($post);
        $da = new Da_Rol();
        $tmp = $da->contarRol($condicion);
        return $tmp[0]['total'];
    }

    function obtenerCondicionRol($post) {
        $condicion = array();
        if (!empty($post['n_id_roll']))
            $condicion[] = " n_id_roll = $post[n_id_roll] ";
        if (isset($post['txtbuscar'])) {
            if (!empty($post['txtbuscar']))
                $condicion[] = " upper(c_nombre) like upper('%{$post['txtbuscar']}%')";
        }
        return $condicion;
    }

}

?>

