--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.7
-- Dumped by pg_dump version 9.5.1

-- Started on 2016-05-04 00:31:44

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = business, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 187 (class 1259 OID 16509)
-- Name: client; Type: TABLE; Schema: business; Owner: postgres
--

CREATE TABLE client (
    id integer NOT NULL,
    name character varying,
    logo character varying,
    contract_id integer,
    ctstatus_id integer,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    usucrud_id integer
);


ALTER TABLE client OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 16507)
-- Name: client_id_seq; Type: SEQUENCE; Schema: business; Owner: postgres
--

CREATE SEQUENCE client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_id_seq OWNER TO postgres;

--
-- TOC entry 3001 (class 0 OID 0)
-- Dependencies: 186
-- Name: client_id_seq; Type: SEQUENCE OWNED BY; Schema: business; Owner: postgres
--

ALTER SEQUENCE client_id_seq OWNED BY client.id;


--
-- TOC entry 177 (class 1259 OID 16414)
-- Name: company; Type: TABLE; Schema: business; Owner: postgres
--

CREATE TABLE company (
    id integer NOT NULL,
    ruc character varying,
    establishmentnumber integer,
    businessname character varying,
    legalrepresentative character varying,
    address character varying,
    phone character varying,
    logo character varying,
    contract_id integer,
    client_id integer,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    usucrud_id integer
);


ALTER TABLE company OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 16454)
-- Name: company_id_seq; Type: SEQUENCE; Schema: business; Owner: postgres
--

CREATE SEQUENCE company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE company_id_seq OWNER TO postgres;

--
-- TOC entry 3002 (class 0 OID 0)
-- Dependencies: 183
-- Name: company_id_seq; Type: SEQUENCE OWNED BY; Schema: business; Owner: postgres
--

ALTER SEQUENCE company_id_seq OWNED BY company.id;


--
-- TOC entry 191 (class 1259 OID 16579)
-- Name: companydinner; Type: TABLE; Schema: business; Owner: postgres
--

CREATE TABLE companydinner (
    id integer NOT NULL,
    client_id integer,
    company_id integer,
    dinner_id integer,
    ctstatus_id integer,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    usucrud_id integer
);


ALTER TABLE companydinner OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 16577)
-- Name: companydinner_id_seq; Type: SEQUENCE; Schema: business; Owner: postgres
--

CREATE SEQUENCE companydinner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE companydinner_id_seq OWNER TO postgres;

--
-- TOC entry 3003 (class 0 OID 0)
-- Dependencies: 190
-- Name: companydinner_id_seq; Type: SEQUENCE OWNED BY; Schema: business; Owner: postgres
--

ALTER SEQUENCE companydinner_id_seq OWNED BY companydinner.id;


--
-- TOC entry 182 (class 1259 OID 16451)
-- Name: contract; Type: TABLE; Schema: business; Owner: postgres
--

CREATE TABLE contract (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE contract OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 16466)
-- Name: contract_id_seq; Type: SEQUENCE; Schema: business; Owner: postgres
--

CREATE SEQUENCE contract_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contract_id_seq OWNER TO postgres;

--
-- TOC entry 3004 (class 0 OID 0)
-- Dependencies: 184
-- Name: contract_id_seq; Type: SEQUENCE OWNED BY; Schema: business; Owner: postgres
--

ALTER SEQUENCE contract_id_seq OWNED BY contract.id;


--
-- TOC entry 178 (class 1259 OID 16417)
-- Name: dinner; Type: TABLE; Schema: business; Owner: postgres
--

CREATE TABLE dinner (
    id integer NOT NULL,
    name character varying,
    address character varying,
    location character varying,
    phone character varying,
    ctstatus_id integer,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    usucrud_id integer
);


ALTER TABLE dinner OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16485)
-- Name: dinner_id_seq; Type: SEQUENCE; Schema: business; Owner: postgres
--

CREATE SEQUENCE dinner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dinner_id_seq OWNER TO postgres;

--
-- TOC entry 3005 (class 0 OID 0)
-- Dependencies: 185
-- Name: dinner_id_seq; Type: SEQUENCE OWNED BY; Schema: business; Owner: postgres
--

ALTER SEQUENCE dinner_id_seq OWNED BY dinner.id;


--
-- TOC entry 181 (class 1259 OID 16442)
-- Name: roster; Type: TABLE; Schema: business; Owner: postgres
--

CREATE TABLE roster (
    id integer NOT NULL,
    identification character varying,
    ctidentification_id integer,
    firstname character varying,
    lastname character varying,
    fullname character varying,
    email character varying,
    company_id integer,
    employeecode character varying,
    barcode character varying,
    rfidcode character varying,
    biometriccode character varying,
    ctstatus_id integer,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    usucrud_id integer
);


ALTER TABLE roster OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 16440)
-- Name: roster_id_seq; Type: SEQUENCE; Schema: business; Owner: postgres
--

CREATE SEQUENCE roster_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE roster_id_seq OWNER TO postgres;

--
-- TOC entry 3006 (class 0 OID 0)
-- Dependencies: 180
-- Name: roster_id_seq; Type: SEQUENCE OWNED BY; Schema: business; Owner: postgres
--

ALTER SEQUENCE roster_id_seq OWNED BY roster.id;


--
-- TOC entry 2872 (class 2604 OID 16512)
-- Name: id; Type: DEFAULT; Schema: business; Owner: postgres
--

ALTER TABLE ONLY client ALTER COLUMN id SET DEFAULT nextval('client_id_seq'::regclass);


--
-- TOC entry 2866 (class 2604 OID 16456)
-- Name: id; Type: DEFAULT; Schema: business; Owner: postgres
--

ALTER TABLE ONLY company ALTER COLUMN id SET DEFAULT nextval('company_id_seq'::regclass);


--
-- TOC entry 2874 (class 2604 OID 16582)
-- Name: id; Type: DEFAULT; Schema: business; Owner: postgres
--

ALTER TABLE ONLY companydinner ALTER COLUMN id SET DEFAULT nextval('companydinner_id_seq'::regclass);


--
-- TOC entry 2871 (class 2604 OID 16468)
-- Name: id; Type: DEFAULT; Schema: business; Owner: postgres
--

ALTER TABLE ONLY contract ALTER COLUMN id SET DEFAULT nextval('contract_id_seq'::regclass);


--
-- TOC entry 2868 (class 2604 OID 16487)
-- Name: id; Type: DEFAULT; Schema: business; Owner: postgres
--

ALTER TABLE ONLY dinner ALTER COLUMN id SET DEFAULT nextval('dinner_id_seq'::regclass);


--
-- TOC entry 2870 (class 2604 OID 16445)
-- Name: id; Type: DEFAULT; Schema: business; Owner: postgres
--

ALTER TABLE ONLY roster ALTER COLUMN id SET DEFAULT nextval('roster_id_seq'::regclass);


--
-- TOC entry 2877 (class 2606 OID 16464)
-- Name: pk_business; Type: CONSTRAINT; Schema: business; Owner: postgres
--

ALTER TABLE ONLY company
    ADD CONSTRAINT pk_business PRIMARY KEY (id);


--
-- TOC entry 2885 (class 2606 OID 16517)
-- Name: pk_client; Type: CONSTRAINT; Schema: business; Owner: postgres
--

ALTER TABLE ONLY client
    ADD CONSTRAINT pk_client PRIMARY KEY (id);


--
-- TOC entry 2887 (class 2606 OID 16585)
-- Name: pk_companydinner; Type: CONSTRAINT; Schema: business; Owner: postgres
--

ALTER TABLE ONLY companydinner
    ADD CONSTRAINT pk_companydinner PRIMARY KEY (id);


--
-- TOC entry 2883 (class 2606 OID 16473)
-- Name: pk_contract; Type: CONSTRAINT; Schema: business; Owner: postgres
--

ALTER TABLE ONLY contract
    ADD CONSTRAINT pk_contract PRIMARY KEY (id);


--
-- TOC entry 2879 (class 2606 OID 16495)
-- Name: pk_dinner; Type: CONSTRAINT; Schema: business; Owner: postgres
--

ALTER TABLE ONLY dinner
    ADD CONSTRAINT pk_dinner PRIMARY KEY (id);


--
-- TOC entry 2881 (class 2606 OID 16450)
-- Name: pk_roster; Type: CONSTRAINT; Schema: business; Owner: postgres
--

ALTER TABLE ONLY roster
    ADD CONSTRAINT pk_roster PRIMARY KEY (id);


-- Completed on 2016-05-04 00:32:07

--
-- PostgreSQL database dump complete
--

