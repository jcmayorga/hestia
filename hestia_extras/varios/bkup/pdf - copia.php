<?php

require_once('./tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Juk');
$pdf->SetTitle('Códigos de Barra Generados');
$pdf->SetSubject('Hestia');


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(5, 5);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$style = array(
    'position' => '',
    'align' => 'L',
    'stretch' => false,
    'fitwidth' => true,
    'cellfitalign' => '',
    'border' => false,
    'hpadding' => 'auto',
    'vpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255),
    'text' => false,
    'font' => 'helvetica',
    'fontsize' => 8,
    'stretchtext' => 4
);
$pdf->SetFont('helvetica', '', 5);
$x=-1;
$y=7;
$w=41;
$h=12;
// ---------------------------------------------------------
$dbconn = pg_connect("host=localhost dbname=hestia_dev user=postgres password=qws12345") or die('No se ha podido conectar: ' . pg_last_error());
$result = pg_query($dbconn, "SELECT id, firstname, lastname, barcode FROM administration.roster  order by 1 limit 3");
if (!$result) { echo "An error occurred.\n"; exit; }
while ($row = pg_fetch_row($result)) {
	
 // echo "id: $row[0] |  barcode: $row[2] ||> $barcode <|| fullname: $row[1]";
	//$pdf->AddPage('L','C10');
	
	$resolution= array(20, 40);
	$pdf->AddPage('L', $resolution);
	
	
	$barcode=$row[3];
	$apellidos =$row[2];
	$nombres =$row[1];
	$pdf->MultiCell(35, 5, $apellidos, 0, 'C', false, 1, 3, 3, true, 0, false, true, 0, 'T', false);
	$pdf->MultiCell(35, 5, $nombres, 0, 'C', false, 1, 3, 5, true, 0, false, true, 0, 'T', false);
	$pdf->write1DBarcode($barcode, 'C39', $x, $y, $w, $h, 0.4, $style, 'N');
	//$pdf->MultiCell(35, 5, $barcode, 0, 'C', false, 1, 3, 20, true, 0, false, true, 0, 'T', false);

}
pg_free_result($result);
pg_close($dbconn);
$pdf->Output('codigos.pdf', 'I');


