<?php


include_once dirname(__FILE__) . '/etc/conf.php';
include_once dirname(__FILE__) . '/etc/cierre_servicio_bl.php';
include_once dirname(__FILE__) . '/etc/lib/PHPExcel/Classes/PHPExcel.php';
include_once dirname(__FILE__) . '/etc/lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
include_once EXCEL_CLASE_FAST;
session_start();

if (!empty($_REQUEST['accion'])) {
    $op = $_REQUEST['accion'];
} else {
    $op = '';
}
switch ($op) {

    case 'getDinner':
        $cat = new bl_cierre_servicio();
        $rs = $cat->getDinnerUser($_SESSION['_sf2_attributes']['userdata'][id]);
        echo json_encode($rs);
        exit();
        break;
    case 'getServiceDinner':
        $cat = new bl_cierre_servicio();
        $rs = $cat->getServiceDinner($_REQUEST);
        echo json_encode($rs);
        exit();
        break;

    case 'getCompanyDinner':
        $cat = new bl_cierre_servicio();
        $rs = $cat->getCompanyDinner($_REQUEST['dinner']);
        echo json_encode($rs);
        exit();
        break;

    case 'generaPadreCierre':
        $cat = new bl_cierre_servicio();
        $rs = $cat->createCloseDinnerService($_REQUEST);
        echo json_encode($rs);
        exit();
        break;
    case 'cargaFirmas':
        $cat = new bl_cierre_servicio();
        $rs = $cat->getDetailClose($_REQUEST['padre']);
        echo json_encode($rs);
        exit();
        break;
    case 'creaFirmasTMP':
        $cat = new bl_cierre_servicio();
        $rs = $cat->creaFirmasTMP($_REQUEST);
        echo json_encode($rs);
        exit();
        break;
    case 'actualizaFirmasTMP':
        $cat = new bl_cierre_servicio();
        $rs = $cat->actualizaFirmasTMP($_REQUEST);
        echo json_encode($rs);
        exit();
        break;

    case 'eliminaFirmasTMP':
        $cat = new bl_cierre_servicio();
        $rs = $cat->eliminaFirmasporIdTMP($_REQUEST['id']);
        echo json_encode($rs);
        exit();
        break;

    case 'getCompany':
        $cat = new bl_cierre_servicio();
        $rs = $cat->getCompany();
        echo json_encode($rs);
        exit();
        break;

    case 'procesaCierre':
        $cat = new bl_cierre_servicio();
        $rs = $cat->procesaCierre($_REQUEST);
        echo json_encode($rs);
        exit();
        break;
}
?>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>HESTIA</title>
        <link rel="stylesheet" href="etc/lib/css/estilo.css"/>

        <!-- jquery.easy -->
        <link rel="stylesheet" type="text/css" href="etc/lib/jquery-easyui/themes/default/easyui.css">
        <link rel="stylesheet" type="text/css" href="etc/lib/jquery-easyui/themes/icon.css">
        <script src="etc/lib/jquery-easyui/jquery-1.12.4.min.js" type="text/javascript"></script>
        <script src="etc/lib/jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>
        <script src="etc/lib/jquery-easyui/jquery.edatagrid.js" type="text/javascript"></script>
        <script src="etc/lib/jquery-easyui/locale/easyui-lang-es.js" type="text/javascript"></script>


        <!-- extenciones para validar-->
        <script src="etc/lib/js/valida.js" type="text/javascript" ></script>
        <script src="etc/lib/js/hestia.js" type="text/javascript" ></script>

        <script type="text/javascript">
            $(function () {
                var path = "cierre_servicio.php";


<?php
$pd = new bl_cierre_servicio();
$rsDesc = $pd->getCompany();
//foreach ($rsDesc as $k => $vec) {
//    $rsDesc[$k]['valor_sexo'] = trim($rsDesc[$k]['valor']);
//    $rsDesc[$k]['descripcion_sexo'] = $rsDesc[$k]['descripcion'];
//}
echo "var comedor=" . json_encode($rsDesc) . ";";
?>

                $('#cmb_dinner').combobox({
                    url: path + '?accion=getDinner',
                    valueField: 'id',
                    textField: 'name',
                    panelHeight: 100,
                    width: 250,
                    editable: false,
                    required: true,
                    onSelect: function (rec) {
                        clearService();
                    }
                });
                $('#fecha_servicio').datebox({
                    editable: false,
                    required: true,
                    formatter: formatoSalud,
                    parser: parserSalud,
                    onSelect: function (rec) {
                        clearService();
                    }
                });
                $('#cmb_servicio').combobox();
                function clearService() {
                    $('#cmb_servicio').combobox('clear');
                    $('#div_cbm_service').hide();
                    $('#btnSelServDinn').show();
                    $('#btnIngresar').hide();
                }
                $('#btnSelServDinn').click(function () {
                    $('#cmb_servicio').combobox({
                        url: path + '?accion=getServiceDinner&dinner=' + $('#cmb_dinner').combo('getValue') + '&date=' + $('#fecha_servicio').combo('getValue'),
                        valueField: 'acron',
                        textField: 'description',
                        panelHeight: 200,
                        width: 250,
                        editable: false,
                        required: true,
                    });
                    $('#div_cbm_service').show();
                    $('#btnSelServDinn').hide();
                    $('#btnIngresar').show();
                });
                $('#cmb_empresa').combobox({
                    url: 'reporte.php?accion=catalogoEmpresa',
                    valueField: 'value',
                    textField: 'label',
                    panelHeight: 150,
                    width: 350,
                    editable: false,
                    required: true

                });
                var tablesToExcel = (function () {
                    var uri = 'data:application/vnd.ms-excel;base64,'
                            , tmplWorkbookXML = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">'
                            + '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>'
                            + '<Styles>'
                            + '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>'
                            + '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>'
                            + '</Styles>'
                            + '{worksheets}</Workbook>'
                            , tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>'
                            , tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>'
                            , base64 = function (s) {
                                return window.btoa(unescape(encodeURIComponent(s)))
                            }
                    , format = function (s, c) {
                        return s.replace(/{(\w+)}/g, function (m, p) {
                            return c[p];
                        })
                    }
                    return function (tables, wsnames, wbname, appname) {
                        var ctx = "";
                        var workbookXML = "";
                        var worksheetsXML = "";
                        var rowsXML = "";
                        for (var i = 0; i < tables.length; i++) {
                            if (!tables[i].nodeType)
                                tables[i] = document.getElementById(tables[i]);
                            for (var j = 0; j < tables[i].rows.length; j++) {
                                rowsXML += '<Row>'
                                for (var k = 0; k < tables[i].rows[j].cells.length; k++) {
                                    var dataType = tables[i].rows[j].cells[k].getAttribute("data-type");
                                    var dataStyle = tables[i].rows[j].cells[k].getAttribute("data-style");
                                    var dataValue = tables[i].rows[j].cells[k].getAttribute("data-value");
                                    dataValue = (dataValue) ? dataValue : tables[i].rows[j].cells[k].innerHTML;
                                    var dataFormula = tables[i].rows[j].cells[k].getAttribute("data-formula");
                                    dataFormula = (dataFormula) ? dataFormula : (appname == 'Calc' && dataType == 'DateTime') ? dataValue : null;
                                    ctx = {attributeStyleID: (dataStyle == 'Currency' || dataStyle == 'Date') ? ' ss:StyleID="' + dataStyle + '"' : ''
                                        , nameType: (dataType == 'Number' || dataType == 'DateTime' || dataType == 'Boolean' || dataType == 'Error') ? dataType : 'String'
                                        , data: (dataFormula) ? '' : dataValue
                                        , attributeFormula: (dataFormula) ? ' ss:Formula="' + dataFormula + '"' : ''
                                    };
                                    rowsXML += format(tmplCellXML, ctx);
                                }
                                rowsXML += '</Row>'
                            }
                            ctx = {rows: rowsXML, nameWS: wsnames[i] || 'Sheet' + i};
                            worksheetsXML += format(tmplWorksheetXML, ctx);
                            rowsXML = "";
                        }

                        ctx = {created: (new Date()).getTime(), worksheets: worksheetsXML};
                        workbookXML = format(tmplWorkbookXML, ctx);
                        console.log(workbookXML);
                        var link = document.createElement("A");
                        link.href = uri + base64(workbookXML);
                        link.download = wbname || 'Workbook.xls';
                        link.target = '_blank';
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    }
                })();
                $('#dtg_firma').edatagrid({
                    singleSelect: true,
                    rownumbers: true,
                    width: 770,
                    height: 500,
                    columns: [
                        [
                            {field: 'identification', align: 'center', resizable: true, width: 110, title: 'Identificación'},
                            {field: 'fullname', align: 'left', resizable: true, width: 190, title: 'Nombres y Apellidos'},
                            {field: 'company_id', align: 'left', resizable: true, width: 170, title: 'Empresa'},
                            {field: 'costcenter', align: 'left', resizable: true, width: 170, title: 'Centro de Costo'},
                            {field: 'estado', align: 'left', resizable: true, width: 70, title: 'Estado'},
                        ]],
                    toolbar: '#toolb'

                });

                $('#btnNuevoReg').click(function () {
                    var dg = $("#dtg_firma");
                    var opts = $("#dtg_firma").edatagrid('options')
                    dg.edatagrid('appendRow', {isNewRecord: true});
                    var rows = dg.edatagrid('getRows');
                    opts.editIndex = rows.length - 1;
                    dg.edatagrid('beginEdit', opts.editIndex);
                    dg.edatagrid('selectRow', opts.editIndex);
                    $('#btnNuevoReg').hide();
                });
                $('#btnIngresar').click(function () {
                    if ($("#frmDatos").form('validate')) {
                        $.ajax({
                            type: "POST",
                            url: path + "?accion=generaPadreCierre&dinner=" + $('#cmb_dinner').combo('getValue') + "&acron=" + $('#cmb_servicio').combo('getValue') + "&dateclose=" + $('#fecha_servicio').combo('getText'),
                            success: function (msg) {
                                try {
                                    var result = JSON.stringify(msg);
                                    result = JSON.parse(result);
                                    var str = "" + result;
                                    var res = str.replace("[", "");
                                    res = res.replace("]", "");
                                    result = JSON.parse(res);
                                    $('#idPadreClose_hi').val(result.id);
                                    $('#btnGenerar').hide();
                                    $('#btnNuevoReg').show();
                                    $('#btnDelReg').show();
                                    $('#btnProcesar').show();
                                    $('#lbl_param').show();
                                    $('#lbl_f_dinner').text($('#cmb_dinner').combo('getText'))
                                    $('#lbl_f_service').text($('#cmb_servicio').combo('getText'))
                                    $('#lbl_f_date').text($('#fecha_servicio').combo('getText'))
                                    $('#dd_InCieSer').window('close');

                                    if (result.ctstatus_id == "531") {
                                        $('#btnProcesar').hide();
                                        $('#btnNuevoReg').hide();
                                        $('#btnDelReg').hide();
                                        $('#btnSaveTmp').hide();
                                        $('#dtg_firma').edatagrid({
                                            url: path + '?accion=cargaFirmas&padre=' + $('#idPadreClose_hi').val(),
                                            saveUrl: '',
                                            updateUrl: '',
                                            destroyUrl: '',
                                            idField: "id",
                                            fitColumns: false,
                                            singleSelect: true,
                                            rownumbers: true,
                                            striped: true,
                                            width: 770,
                                            height: 500,
                                            columns: [
                                                [
                                                    {field: 'identification', align: 'center', resizable: true, width: 110, title: 'Identificación',
                                                    },
                                                    {field: 'fullname', align: 'left', resizable: true, width: 170, title: 'Nombres y Apellidos',
                                                    },
                                                    {field: 'company_id', align: 'left', resizable: true, width: 170, title: 'Empresa',
                                                        formatter: function (value) {
                                                            for (var i = 0; i < comedor.length; i++) {
                                                                if (comedor[i].id == value)
                                                                    return comedor[i].businessname;
                                                            }
                                                            return value;
                                                        }, },
                                                    {field: 'costcentername', align: 'left', resizable: true, width: 160, title: 'Centro de Costo',
                                                    },
                                                    {field: 'estado', align: 'left', resizable: true, width: 100, title: 'Estado',
                                                    }
                                                ]],
                                            onBeforeEdit: function (index, row) {
                                                return false;
                                            }});
                                    } else {
                                        $('#dtg_firma').edatagrid({
                                            url: path + '?accion=cargaFirmas&padre=' + $('#idPadreClose_hi').val(),
                                            saveUrl: path + '?accion=creaFirmasTMP&padre=' + $('#idPadreClose_hi').val(),
                                            updateUrl: path + '?accion=actualizaFirmasTMP&padre=' + $('#idPadreClose_hi').val(),
                                            destroyUrl: path + '?accion=eliminaFirmasTMP',
                                            idField: "id",
                                            fitColumns: false,
                                            singleSelect: true,
                                            rownumbers: true,
                                            striped: true,
                                            width: 770,
                                            height: 500,
                                            columns: [
                                                [
                                                    {field: 'identification', align: 'center', resizable: true, width: 110, title: 'Identificación',
                                                        editor: {
                                                            type: 'textbox',
                                                            options: {required: true
                                                            },
                                                        }},
                                                    {field: 'fullname', align: 'left', resizable: true, width: 170, title: 'Nombres y Apellidos',
                                                        editor: {
                                                            type: 'validatebox',
                                                            validType: 'fechaMenorActual',
                                                            parser: parserSalud,
                                                            options: {required: true}
                                                        }
                                                    },
                                                    {field: 'company_id', align: 'left', resizable: true, width: 170, title: 'Empresa',
                                                        formatter: function (value) {
                                                            for (var i = 0; i < comedor.length; i++) {
                                                                if (comedor[i].id == value)
                                                                    return comedor[i].businessname;
                                                            }
                                                            return value;
                                                        }, editor: {
                                                            type: 'combobox',
                                                            options: {
                                                                url: path + '?accion=getCompanyDinner&dinner=' + $('#cmb_dinner').combo('getValue'),
                                                                valueField: 'id',
                                                                textField: 'businessname', required: true, editable: false}
                                                        }},
                                                    {field: 'costcentername', align: 'left', resizable: true, width: 160, title: 'Centro de Costo',
                                                        editor: {
                                                            type: 'textbox'
                                                        }},
                                                    {field: 'estado', align: 'left', resizable: true, width: 100, title: 'Estado',
                                                    }
                                                ]],
                                            toolbar: '#toolb',
                                            onSuccess: function (index, row) {
                                                $('#dtg_firma').edatagrid('reload');
                                                $('#btnNuevoReg').show();
                                                $('#btnSaveTmp').hide();
                                            },
                                            onBeginEdit: function (index, row) {
                                                $('#btnSaveTmp').show();
                                            },
                                            destroyMsg: {
                                                norecord: {// when no record is selected
                                                    title: 'Advertencia',
                                                    msg: 'No se ha seleccionado registro para eliminar.'
                                                },
                                                confirm: {// when select a row
                                                    title: 'Por favor confirme',
                                                    msg: 'Esta seguro que desea eliminar el registro seleccionado?'
                                                }
                                            }
                                        });

                                    }


                                } catch (err)
                                {
                                    $.messager.show({
                                        title: 'Atención',
                                        msg: 'Error al acceder o crear registro de firmas, contáctese con el adminsitrador.'
                                    });
                                    console.log(err);
                                }
                            }
                        });
                    } else {
                        $.messager.show({
                            title: 'Atención',
                            msg: 'Por favor, seleccione los campos requeridos para continuar.'
                        });
                    }
                });
                $('#btnClose').click(function () {
                    $('#dd_InCieSer').window('close');
                });
                $('#btnDelReg').click(function () {
                    $('#dtg_firma').edatagrid('destroyRow');
                    $('#btnNuevoReg').show();
                });
                $('#btnSaveTmp').click(function () {
                    $('#dtg_firma').edatagrid('saveRow');
                });
                $('#btnProcesar').click(function () {

                    $.messager.confirm('Confirme por favor', 'Esta seguro de realizar el cierre del servicio?', function (r) {
                        if (r) {
                            $.messager.confirm('Confirme por favor', 'Esta opción no se puede revertir, está seguro de realizar el cierre del servicio?', function (r) {
                                if (r) {
                                    $.ajax({
                                        type: "POST",
                                        url: path + "?accion=procesaCierre&dinner=" + $('#cmb_dinner').combo('getValue') + "&acron=" + $('#cmb_servicio').combo('getValue') + "&dateclose=" + $('#fecha_servicio').combo('getText') + '&padre=' + $('#idPadreClose_hi').val(),
                                        success: function (msg) {
                                            var result = JSON.stringify(msg);
                                            result = JSON.parse(result);
                                            var str = "" + result;
                                            var res = str.replace("[", "");
                                            res = res.replace("]", "");
                                            result = JSON.parse(res);
                                            if (result.ctstatus_id == 531) {

                                                $('#btnProcesar').hide();
                                                $('#btnNuevoReg').hide();
                                                $('#btnDelReg').hide();
                                                $('#btnSaveTmp').hide();
                                                $('#dtg_firma').edatagrid({
                                                    saveUrl: '',
                                                    updateUrl: '',
                                                    destroyUrl: '',
                                                    onBeforeEdit: function (index, row) {
                                                        return false;
                                                    }});
                                            } else if (result.ctstatus_id == 530) {
                                                alert("Existió un inconveniente en el procesamiento de datos, por favor comunicar al administrador del sistema.")
                                            }

                                        }});



                                }
                            });
                        }
                    });
                });



                $('#btnGenerar').show();
                $('#btnNuevoReg').hide();
                $('#btnDelReg').hide();
                $('#btnSaveTmp').hide();
                $('#btnProcesar').hide();
                $('#tt').tabs('disableTab', 0);
                $('#tt').tabs('select', 1);
            });
            function accbtnGenerar() {
                $('#dd_InCieSer').window({
                    width: 320,
                    height: 280,
                    modal: true,
                    collapsible: false,
                    minimizable: false,
                    maximizable: false,
                    resizable: false,
                    title: 'Iniciar cierre de Servicio'
                });
                $('#dd_InCieSer').show();
            }
        </script>
    </head>
    <body>
        <div id="header">
            <div class="fila" >
                <div id="divNombreAplicacion" style="float:right;"> 
                    <img src="./etc/lib/img/goddard.gif" style="width: 110px; height: 50px"/>
                    &nbsp;
                    Cierre de Servicio
                </div>
            </div>
        </div>
        <div id="cuerpo" style="background-color: #FFFFFF; padding: 10px;width:800px;">
            <input type="hidden" id="idPadreClose_hi"/>
            <div id="tt" class="easyui-tabs" style="width:800px;height:550px;">  
                <div title="Pistoleo de Tickets" id="tt_ticket" style="overflow:auto;padding:20px;padding:20px;display:none;">  
                    <div style="width: 98%">
                        <fieldset style="width: 95%; margin:auto; padding: 10px;text-align: left;" class="clsFieldset"> 
                            <legend>Validación de Tickets</legend>
                            <div class="fila">
                                <div class="celda" style='width:100%; text-align: center;'>
                                    <div class="etiqueta">
                                        <label>Ticket</label><br/>
                                        <input name="dis_institucion_referencia" id="dis_institucion_referencia" /><br/>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        Icono OK o X<br/>
                        Nombre y Apellidos<br/>
                        Empresa<br/>
                        Fecha/Hora
                    </div>
                </div>
                <div title="Registro de Firmas Físicas" id="tt_firmas" style="overflow:auto;padding:0px;display:none;">  
                    <div style="width: 98%">  
                        <table id="dtg_firma" name="dtg_firma"></table> 
                    </div>
                </div>
            </div>
            <div id="toolb" style="text-align: left;height: 90px;"> 
                <div class="fila" id="lbl_param" style="display: none;">
                    <div class="celda" id="lbl_f_dinner" style="width: 33%;text-align: center;color:red;font-weight: bold;">
                        Comedor: "----------"  
                    </div>
                    <div class="celda" id="lbl_f_service" style="width: 33%;text-align: center;color:green;font-weight: bold;">
                        Servicio: "----------"  
                    </div>
                    <div class="celda" id="lbl_f_date" style="width: 33%;text-align: center;color:blue;font-weight: bold;">
                        Fecha: "--------------"
                    </div>
                </div>
                <div class="fila">
                    <div class="celda">
                        <br/>
                        <a id="btnGenerar" href="#" onclick="accbtnGenerar()" class="easyui-linkbutton" data-options="iconCls:'icon-edit', plain:true">Iniciar cierre de Servicio</a>  
                        <a id="btnNuevoReg" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add', plain:true">Nuevo Registro</a>  
                        <a id="btnSaveTmp" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save', plain:true">Guardar Registro</a> 
                        <a id="btnDelReg" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove', plain:true">Eliminar Registro</a>  
                        <a id="btnProcesar" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-lock', plain:true">Guardar y Realizar Cierre</a>  
                        <br/>
                    </div>
                </div>

            </div>

            <div id="dd_InCieSer" title="Iniciar cierre de Servicio" style="text-align: center; padding: 10px; display: none">
                <form id="frmDatos" method="post" >
                    <div class="fila">
                        <div class="celda">
                            <div class="etiqueta" >
                                <label> Comedor </label>
                            </div>
                            <div>
                                <input name="cmb_dinner" id="cmb_dinner" value=""/> 
                            </div>
                        </div>
                    </div>
                    <div class="fila" >
                        <div class="celda">
                            <div class="etiqueta">
                                <label>Fecha de Servicio</label><br/>
                                <input name="fecha_servicio" id="fecha_servicio" value="" /> 
                            </div>
                        </div>
                    </div>
                    <div class="fila">
                        <div class="celda">
                            <a id="btnSelServDinn" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Seleccionar Servicio</a>
                        </div>
                    </div>

                    <div class="fila" id="div_cbm_service" style="display: none;">
                        <div class="celda" id="div_ser">
                            <div class="etiqueta">
                                <label>Servicio</label><br/>
                                <input name="cmb_servicio" id="cmb_servicio" value="" /> 
                            </div>
                        </div>
                    </div>

                    <div class="fila" >
                        <div class="celda" style="text-align: center;padding-top: 10px;">
                            <a id="btnIngresar" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" style="display: none;">Iniciar Registro</a>  
                            <a id="btnClose" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-no'">Cancelar</a>  
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </body>
</html>
