<?php

if(!isset($_GET['id'])){
	echo "<h1>:(</h1> Ticket no definido!";
	die();
}
	
require_once('./varios/lib/tcpdf/tcpdf_barcodes_1d.php');
	$dinner ="";
	$company ="";
	$sigla ="";
	$fullname ="";
	$barcode ="";
	$fecha ="";


$dbconn = pg_connect("host=localhost dbname=hestia_pruebas user=postgres password=qws12345") or die('No se ha podido conectar: ' . pg_last_error());
$result = pg_query($dbconn, "select tran.id, dinn.name, comp.businessname, CASE WHEN tran.cttransaction_id=510 THEN serv.acronym
            WHEN tran.cttransaction_id=511 THEN 'R-'||serv.acronym
            ELSE '?' END as sigla, rost.fullname,tran.barcode,  tran.dateregistry ||' - '||tran.timeregistry as fecha
			from business.transaction tran, business.service serv, administration.companydinner comdin, administration.company comp, administration.dinner dinn, administration.roster rost
			where tran.service_id=serv.id and tran.companydinner_id=comdin.id
			and comdin.company_id=comp.id and comdin.dinner_id=dinn.id
			and tran.roster_id=rost.id and tran.id=".$_GET['id']);
if (!$result) { echo "<h1>:o</h1> Sql error!"; exit; }
while ($row = pg_fetch_row($result)) {
	$dinner =$row[1];
	$company =$row[2];
	$sigla =$row[3];
	$fullname =$row[4];
	$barcode =$row[5];
	$fecha =$row[6];
}
pg_free_result($result);
pg_close($dbconn);


$barcodeobj = new TCPDFBarcode($barcode, 'C39');

if($dinner==$company && $fecha==$fullname){
	echo "<h1>:/</h1> No existe ticket!";
	die();
}

?>


<table>
<tr>
	<td align="center"><font size=2><?php echo $dinner;?></font></td>
</tr>
<tr>
	<td align="center"><font size=1><?php echo $company;?></font></td>
</tr>
<tr>
	<td align="center"><font size=6><b><?php echo $sigla;?></b></font></td>
</tr>
<tr>
	<td align="center"><font size=1><?php echo $fullname;?></font></td>
</tr>
<tr>
	<td align="center"><?php echo $barcodeobj->getBarcodeHTML(2, 25, 'black');?></td>
</tr>
<tr>
	<td align="center"><font size=1><?php echo $fecha;?></font></td>
</tr>
</table>

<script type="text/javascript">

setTimeout(function(){ window.print(); window.close(); }, 100);

</script>
