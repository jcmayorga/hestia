<?php

require_once('./varios/lib/tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Juk');
$pdf->SetTitle('Códigos de Barra Generados');
$pdf->SetSubject('Hestia');


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(5, 5);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$style = array(
    'position' => '',
    'align' => 'L',
    'stretch' => false,
    'fitwidth' => true,
    'cellfitalign' => '',
    'border' => false,
    'hpadding' => 'auto',
    'vpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255),
    'text' => false,
    'font' => 'helvetica',
    'fontsize' => 8,
    'stretchtext' => 4
);
$pdf->SetFont('helvetica', '', 5);
$x=-1;
$y=7;
$w=41;
$h=12;
// ---------------------------------------------------------
$dbconn = pg_connect("host=localhost dbname=hestia user=postgres password=qws12345") or die('No se ha podido conectar: ' . pg_last_error());
$result = pg_query($dbconn, "SELECT b.businessname, a.unit, a.costcenter, a.namecenter, trim(substr(fullname, length(left(fullname,position(' ' in fullname))||left(substr(fullname, length(left(fullname,position(' ' in fullname)))+1, length(fullname)),position(' ' in substr(fullname, length(left(fullname,position(' ' in fullname)))+1, length(fullname))))))) as lastname, 
 trim(left(fullname,position(' ' in fullname))||left(substr(fullname, length(left(fullname,position(' ' in fullname)))+1, length(fullname)),position(' ' in substr(fullname, length(left(fullname,position(' ' in fullname)))+1, length(fullname))))) as firstname
,  a.barcode 
FROM administration.roster a , administration.company b
where a.company_id=b.id
and a.ctstatus_id in (505)
and b.id=".$_GET['id']." 
order by 1, 2, 3, 4,5,6
LIMIT 100 OFFSET ".$_GET['to']);
if (!$result) { echo "An error occurred.\n"; exit; }
$emp="";
$unidad="";
$costo="";
$ncosto="";
while ($row = pg_fetch_row($result)) {

if($costo!=$row[2]||$emp!=$row[0]||$unidad!=$row[1]){
	$emp=$row[0];
	$unidad=$row[1];
	$costo=$row[2];
	$ncosto=$row[3];
	
	$resolution= array(20, 40);
	$pdf->AddPage('L', $resolution);
	$pdf->MultiCell(35, 5, $emp, 0, 'C', false, 1, 3, 3, true, 0, false, true, 0, 'T', false);
	$pdf->MultiCell(35, 5, "******************************", 0, 'C', false, 1, 3, 5, true, 0, false, true, 0, 'T', false);
	$pdf->MultiCell(35, 5, $unidad, 0, 'C', false, 1, 3, 7, true, 0, false, true, 0, 'T', false);
	$pdf->MultiCell(35, 5, $costo, 0, 'C', false, 1, 3, 9, true, 0, false, true, 0, 'T', false);
	$pdf->MultiCell(35, 5, $ncosto, 0, 'C', false, 1, 3, 11, true, 0, false, true, 0, 'T', false);
	
}



	$resolution= array(20, 40);
	$pdf->AddPage('L', $resolution);
	
	$apellidos =$row[5];
	$nombres =$row[4];
	$barcode=$row[6];
	
	$pdf->MultiCell(35, 5, $apellidos, 0, 'C', false, 1, 3, 3, true, 0, false, true, 0, 'T', false);
	$pdf->MultiCell(35, 5, $nombres, 0, 'C', false, 1, 3, 5, true, 0, false, true, 0, 'T', false);
	$pdf->write1DBarcode($barcode, 'C39', $x, $y, $w, $h, 0.4, $style, 'N');
}
pg_free_result($result);
pg_close($dbconn);
$pdf->Output('codigos.pdf', 'I');


